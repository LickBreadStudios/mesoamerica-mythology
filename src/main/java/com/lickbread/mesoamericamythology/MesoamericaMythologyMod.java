package com.lickbread.mesoamericamythology;

import com.lickbread.mesoamericamythology.registry.*;
import com.lickbread.mesoamericamythology.registry.data.GodStatueHeadParser;
import com.lickbread.mesoamericamythology.registry.data.PerkTreeParser;
import com.lickbread.mesoamericamythology.registry.event.*;
import net.minecraftforge.eventbus.api.IEventBus;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.event.lifecycle.FMLCommonSetupEvent;
import net.minecraftforge.fml.javafmlmod.FMLJavaModLoadingContext;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

@Mod(MesoamericaMythologyMod.MOD_ID)
public class MesoamericaMythologyMod {

    public static final String MOD_ID = "mesoamericamythology";

    public static final Logger LOGGER = LogManager.getLogger(MOD_ID);

    public static IEventBus MOD_EVENT_BUS;

    public MesoamericaMythologyMod() {
        ModBlocks.BLOCKS.register(FMLJavaModLoadingContext.get().getModEventBus());
        ModBlocks.ITEM_BLOCKS.register(FMLJavaModLoadingContext.get().getModEventBus());
        ModBlockEntities.TILE_ENTITIES.register(FMLJavaModLoadingContext.get().getModEventBus());
        ModItems.ITEMS.register(FMLJavaModLoadingContext.get().getModEventBus());
        ModParticleTypes.PARTICLES.register(FMLJavaModLoadingContext.get().getModEventBus());
        ModEntityTypes.ENTITY_TYPES.register(FMLJavaModLoadingContext.get().getModEventBus());
        ModAdvancementTriggers.registerAdvancementTriggers();

        MOD_EVENT_BUS = FMLJavaModLoadingContext.get().getModEventBus();

        MOD_EVENT_BUS.register(ModBlockEntityRenderers.class);
        MOD_EVENT_BUS.register(ModCapabilities.class);
        MOD_EVENT_BUS.register(ModKeyBindings.class);
        MOD_EVENT_BUS.register(ModItemProperties.class);
        MOD_EVENT_BUS.register(ModCreativeModeTabs.class);
        MOD_EVENT_BUS.register(ModModelLayers.class);
        MOD_EVENT_BUS.register(ModEntityRenderers.class);
        MOD_EVENT_BUS.register(ModParticleFactories.class);
//        MOD_EVENT_BUS.register(ModFeatures.class);
        MOD_EVENT_BUS.register(ModEntityAttributes.class);

        MOD_EVENT_BUS.register(MesoamericaMythologyMod.class);
    }

    @SubscribeEvent
    public static void onCommonSetup(FMLCommonSetupEvent event) {
        GodStatueHeadParser.registerGodStatueHeads();
        PerkTreeParser.registerPerkTree();
//        ModStructures.registerStructures();
        ModRecipes.registerRecipes();
        ModDivineRelationshipTabs.registerTabs();
        ModPackets.registerPackets();
    }

}
