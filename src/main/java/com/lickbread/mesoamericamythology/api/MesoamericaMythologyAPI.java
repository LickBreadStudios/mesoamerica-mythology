package com.lickbread.mesoamericamythology.api;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.lickbread.mesoamericamythology.api.recipes.altar.*;
import com.lickbread.mesoamericamythology.api.recipes.outputs.IRecipeOutput;
import com.lickbread.mesoamericamythology.api.recipes.outputs.ItemStackRecipeOutput;
import com.lickbread.mesoamericamythology.api.recipes.outputs.PerkRecipeOutput;
import com.lickbread.mesoamericamythology.api.recipes.pedestal.*;
import com.lickbread.mesoamericamythology.client.screen.DivineRelationshipsTabGui;
import com.lickbread.mesoamericamythology.statueHead.God;
import com.lickbread.mesoamericamythology.statueHead.GodStatueHead;
import com.lickbread.mesoamericamythology.perks.Perk;
import com.lickbread.mesoamericamythology.perks.PerkTree;
import com.lickbread.mesoamericamythology.statueHead.quest.requirement.KillEntitiesRequirement;
import com.lickbread.mesoamericamythology.statueHead.quest.requirement.QuestRequirement;
import com.lickbread.mesoamericamythology.statueHead.quest.requirement.SacrificeEntitiesRequirement;
import com.lickbread.mesoamericamythology.statueHead.quest.reward.DivineFavourReward;
import com.lickbread.mesoamericamythology.statueHead.quest.reward.ExperienceReward;
import com.lickbread.mesoamericamythology.statueHead.quest.reward.ItemReward;
import com.lickbread.mesoamericamythology.statueHead.quest.reward.QuestReward;
import com.lickbread.mesoamericamythology.util.RuntimeTypeAdapterFactory;
import com.lickbread.mesoamericamythology.util.gson.EntityTypeSerializer;
import com.lickbread.mesoamericamythology.util.gson.ItemSerializer;
import com.lickbread.mesoamericamythology.util.gson.ItemStackSerializer;
import net.minecraft.world.entity.EntityType;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.item.BlockItem;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.lickbread.mesoamericamythology.constants.BloodLimits.BLOOD_PER_HEART_POINT;

public class MesoamericaMythologyAPI {

    public static GsonBuilder GSON_BUILDER;
    public static Gson GSON;

    static {
        RuntimeTypeAdapterFactory<QuestReward> questRewardsAdapterFactory = RuntimeTypeAdapterFactory.of(QuestReward.class)
                .registerSubtype(ItemReward.class)
                .registerSubtype(DivineFavourReward.class)
                .registerSubtype(ExperienceReward.class);
        RuntimeTypeAdapterFactory<QuestRequirement> questRequirementsAdapterFactory = RuntimeTypeAdapterFactory.of(QuestRequirement.class)
                .registerSubtype(KillEntitiesRequirement.class)
                .registerSubtype(SacrificeEntitiesRequirement.class);
        RuntimeTypeAdapterFactory<IAltarRecipeMatcher> altarRecipeMatcherAdapterFactory = RuntimeTypeAdapterFactory.of(IAltarRecipeMatcher.class)
                .registerSubtype(PotionAltarRecipeMatcher.class)
                .registerSubtype(StackAltarRecipeMatcher.class)
                .registerSubtype(BloodAltarRecipeMatcher.class);
        RuntimeTypeAdapterFactory<IInfusionPedestalRecipeMatcher> infusionPedestalRecipeMatcherAdapterFactory = RuntimeTypeAdapterFactory.of(IInfusionPedestalRecipeMatcher.class)
                .registerSubtype(PotionInfusionPedestalRecipeMatcher.class)
                .registerSubtype(PotionPowerPedestalRecipeMatcher.class)
                .registerSubtype(StackInfusionPedestalRecipeMatcher.class)
                .registerSubtype(BloodPedestalRecipeMatcher.class)
                .registerSubtype(StackPowerPedestalRecipeMatcher.class)
                .registerSubtype(SacrificialPedestalRecipeMatcher.class);
        RuntimeTypeAdapterFactory<IRecipeOutput> iRecipeOutputRuntimeTypeAdapterFactory = RuntimeTypeAdapterFactory.of(IRecipeOutput.class)
                .registerSubtype(ItemStackRecipeOutput.class)
                .registerSubtype(PerkRecipeOutput.class);

        GSON_BUILDER = new GsonBuilder()
                .setPrettyPrinting()
                .registerTypeAdapterFactory(questRewardsAdapterFactory)
                .registerTypeAdapterFactory(questRequirementsAdapterFactory)
                .registerTypeAdapterFactory(infusionPedestalRecipeMatcherAdapterFactory)
                .registerTypeAdapterFactory(altarRecipeMatcherAdapterFactory)
                .registerTypeAdapterFactory(iRecipeOutputRuntimeTypeAdapterFactory)
                .registerTypeAdapter(ItemStack.class, new ItemStackSerializer())
                .registerTypeAdapter(EntityType.class, new EntityTypeSerializer())
                .registerTypeAdapter(Item.class, new ItemSerializer())
                .registerTypeAdapter(BlockItem.class, new ItemSerializer());
        GSON = GSON_BUILDER.create();
    }

    private static final List<StoneAltarRecipe> STONE_ALTAR_RECIPES = new ArrayList<>();
    private static final List<InfusionPedestalRecipe> INFUSION_PEDESTAL_RECIPES = new ArrayList<>();
    private static final List<DivineRelationshipsTabGui> DIVINE_RELATIONSHIPS_TAB_GUIS = new ArrayList<>();
    private static final PerkTree PERK_TREE = new PerkTree();
    private static final Map<God, GodStatueHead> GOD_STATUE_HEADS = new HashMap<>();

    public static void addPerk(Perk perk) {
        PERK_TREE.addPerk(perk);
    }

    public static void addGodStatueHead(God god, GodStatueHead godStatueHead) {
        GOD_STATUE_HEADS.put(god, godStatueHead);
    }

    public static GodStatueHead getGodStatueHead(God god) {
        return GOD_STATUE_HEADS.get(god);
    }

    public static void addStoneAltarRecipe(List<IAltarRecipeMatcher> matchers, IRecipeOutput output, int bloodAmount) {
        STONE_ALTAR_RECIPES.add(new StoneAltarRecipe(matchers, output, bloodAmount));
    }

    public static void addInfusionPedestalRecipe(List<IInfusionPedestalRecipeMatcher> matchers, IRecipeOutput output, int bloodAmount) {
        INFUSION_PEDESTAL_RECIPES.add(new InfusionPedestalRecipe(matchers, output, bloodAmount));
    }

    public static void addDivineRelationshipsTabGui(DivineRelationshipsTabGui divineRelationshipsTabGui) {
        DIVINE_RELATIONSHIPS_TAB_GUIS.add(divineRelationshipsTabGui);
    }

    public static List<DivineRelationshipsTabGui> getDivineRelationshipsTabGuis() {
        return DIVINE_RELATIONSHIPS_TAB_GUIS;
    }

    public static List<StoneAltarRecipe> getStoneAltarRecipes() {
        return STONE_ALTAR_RECIPES;
    }

    public static List<InfusionPedestalRecipe> getInfusionPedestalRecipes() {
        return INFUSION_PEDESTAL_RECIPES;
    }

    public static PerkTree getPerkTree() {
        return PERK_TREE;
    }

    public static float getBloodForEntity(LivingEntity entity) {
        return entity.getMaxHealth() * BLOOD_PER_HEART_POINT;
    }
}
