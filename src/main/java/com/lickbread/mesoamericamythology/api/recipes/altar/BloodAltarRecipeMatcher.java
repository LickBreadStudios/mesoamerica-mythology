package com.lickbread.mesoamericamythology.api.recipes.altar;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.ToString;
import net.minecraft.world.item.ItemStack;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@ToString
public class BloodAltarRecipeMatcher implements IAltarRecipeMatcher {

    private float blood;

    @Override
    public boolean doesMatch(List<ItemStack> inputs, float bloodAmount) {
        return bloodAmount >= blood;
    }

    @Override
    public boolean stillMatches(List<ItemStack> inputs, float bloodAmount) {
        return this.doesMatch(inputs, bloodAmount);
    }
}
