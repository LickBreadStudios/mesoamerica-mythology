package com.lickbread.mesoamericamythology.api.recipes.altar;

import net.minecraft.world.item.ItemStack;

import java.util.List;

public interface IAltarRecipeMatcher {

    boolean doesMatch(List<ItemStack> inputs, float bloodAmount);

    boolean stillMatches(List<ItemStack> inputs, float bloodAmount);
}
