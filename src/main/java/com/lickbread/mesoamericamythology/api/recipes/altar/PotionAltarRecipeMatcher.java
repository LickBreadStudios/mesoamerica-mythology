package com.lickbread.mesoamericamythology.api.recipes.altar;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.ToString;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.alchemy.Potion;
import net.minecraft.world.item.alchemy.PotionUtils;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@ToString
public class PotionAltarRecipeMatcher implements IAltarRecipeMatcher {

    private Potion potion;
    private int count;

    @Override
    public boolean doesMatch(List<ItemStack> inputs, float bloodAmount) {
        int remainingCount = count;

        for (ItemStack input : inputs) {
            Potion inputPotion = PotionUtils.getPotion(input);
            if (inputPotion.equals(potion)) {
                remainingCount -= input.getCount();
            }
        }

        return remainingCount == 0;
    }

    @Override
    public boolean stillMatches(List<ItemStack> inputs, float bloodAmount) {
        return this.doesMatch(inputs, bloodAmount);
    }
}
