package com.lickbread.mesoamericamythology.api.recipes.altar;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.ToString;
import net.minecraft.world.item.ItemStack;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@ToString
public class StackAltarRecipeMatcher implements IAltarRecipeMatcher {

    private ItemStack itemStack;

    @Override
    public boolean doesMatch(List<ItemStack> inputs, float bloodAmount) {
        int remainingCount = itemStack.getCount();

        for (ItemStack input : inputs) {
            if (ItemStack.isSame(this.itemStack, input)) {
                remainingCount -= input.getCount();
            }
        }

        return remainingCount == 0;
    }

    @Override
    public boolean stillMatches(List<ItemStack> inputs, float bloodAmount) {
        return this.doesMatch(inputs, bloodAmount);
    }
}
