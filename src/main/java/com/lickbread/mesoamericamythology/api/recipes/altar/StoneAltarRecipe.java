package com.lickbread.mesoamericamythology.api.recipes.altar;

import com.google.common.collect.ImmutableList;
import com.lickbread.mesoamericamythology.MesoamericaMythologyMod;
import com.lickbread.mesoamericamythology.api.MesoamericaMythologyAPI;
import com.lickbread.mesoamericamythology.api.recipes.outputs.IRecipeOutput;
import com.lickbread.mesoamericamythology.blockEntities.StoneAltarBlockEntity;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;
import net.minecraft.world.item.ItemStack;
import net.minecraft.core.Vec3i;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Getter
@NoArgsConstructor
@ToString
public class StoneAltarRecipe {

    private static final int EXTRA_RANGE_XZ = 3;
    private static final int EXTRA_RANGE_Y = 1;
    private static final Vec3i EXTRA_RANGE_VEC = new Vec3i(EXTRA_RANGE_XZ, EXTRA_RANGE_Y, EXTRA_RANGE_XZ);

    private List<IAltarRecipeMatcher> matchers;
    private IRecipeOutput output;

    public StoneAltarRecipe(List<IAltarRecipeMatcher> matchers, IRecipeOutput output, float requiredBlood) {
        this.matchers = ImmutableList.<IAltarRecipeMatcher>builder().addAll(matchers).add(new BloodAltarRecipeMatcher(requiredBlood)).build();
        this.output = output;
    }

    private static Optional<StoneAltarRecipe> getMatchingRecipe(StoneAltarBlockEntity blockEntity, boolean shouldLog) {
        List<ItemStack> inputs = blockEntity.getInputs().stream().filter(itemStack -> !itemStack.isEmpty()).collect(Collectors.toList());
        float bloodAmount = blockEntity.getBloodPoolAmount();
        List<StoneAltarRecipe> recipes = MesoamericaMythologyAPI.getStoneAltarRecipes();

        for (StoneAltarRecipe recipe : recipes) {
            if (recipe.matches(inputs, bloodAmount)) {
                if (shouldLog) MesoamericaMythologyMod.LOGGER.debug("Found recipe " + recipe.toString());
                return Optional.of(recipe);
            }
        }

        if (shouldLog) MesoamericaMythologyMod.LOGGER.debug("No altar recipe found for inputs " + Arrays.toString(inputs.toArray()));
        return Optional.empty();
    }

    public boolean stillMatches(StoneAltarBlockEntity blockEntity) {
        List<ItemStack> inputs = blockEntity.getInputs().stream().filter(itemStack -> !itemStack.isEmpty()).collect(Collectors.toList());
        return this.getMatchers().stream().allMatch(iRecipeMatcher -> iRecipeMatcher.stillMatches(inputs, blockEntity.getBloodPoolAmount()));
    }

    private boolean matches(List<ItemStack> inputs, float bloodPoolAmount) {
        return this.getMatchers().stream().allMatch(iRecipeMatcher -> iRecipeMatcher.doesMatch(inputs, bloodPoolAmount));
    }

    public static Optional<StoneAltarRecipe> getRecipe(StoneAltarBlockEntity blockEntity, boolean shouldLog) {
        return getMatchingRecipe(blockEntity, shouldLog);
    }
}
