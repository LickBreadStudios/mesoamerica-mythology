package com.lickbread.mesoamericamythology.api.recipes.outputs;

import net.minecraft.world.entity.player.Player;
import net.minecraft.world.level.block.entity.BlockEntity;

public interface IRecipeOutput {

    void createOutput(BlockEntity blockEntity, Player playerEntity);

}
