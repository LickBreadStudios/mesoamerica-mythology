package com.lickbread.mesoamericamythology.api.recipes.outputs;

import com.lickbread.mesoamericamythology.util.BlockPosUtil;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;
import net.minecraft.world.entity.item.ItemEntity;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.level.ItemLike;
import net.minecraft.world.phys.Vec3;

@AllArgsConstructor(access = AccessLevel.PRIVATE)
@Getter
@ToString
public class ItemStackRecipeOutput implements IRecipeOutput {
    private ItemStack output;

    @Override
    public void createOutput(BlockEntity blockEntity, Player playerEntity) {
        Vec3 itemEntityLocation = BlockPosUtil.getPosTopCentre(blockEntity.getBlockPos());
        ItemEntity entity = new ItemEntity(blockEntity.getLevel(), itemEntityLocation.x, itemEntityLocation.y, itemEntityLocation.z, this.output);
        entity.setDeltaMovement(0.0D, 0.0D, 0.0D);
        blockEntity.getLevel().addFreshEntity(entity);
    }

    public static ItemStackRecipeOutput of(ItemLike item, int count) {
        return of(new ItemStack(item, count));
    }

    public static ItemStackRecipeOutput of(ItemLike item) {
        return of(new ItemStack(item));
    }

    public static ItemStackRecipeOutput of(ItemStack itemstack) {
        return new ItemStackRecipeOutput(itemstack);
    }
}
