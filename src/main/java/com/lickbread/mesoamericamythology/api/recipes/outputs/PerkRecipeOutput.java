package com.lickbread.mesoamericamythology.api.recipes.outputs;

import com.lickbread.mesoamericamythology.api.MesoamericaMythologyAPI;
import com.lickbread.mesoamericamythology.perks.Perk;
import com.lickbread.mesoamericamythology.statueHead.relationship.IDivineFavourCapability;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;
import net.minecraft.client.resources.language.I18n;
import net.minecraft.network.chat.Component;
import net.minecraft.server.level.ServerPlayer;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.level.block.entity.BlockEntity;

import static com.lickbread.mesoamericamythology.capabilities.DivineFavourCapabilityProvider.DIVINE_FAVOUR_CAPABILITY;

@AllArgsConstructor(access = AccessLevel.PRIVATE)
@Getter
@ToString
public class PerkRecipeOutput implements IRecipeOutput {
    private String perkId;

    @Override
    public void createOutput(BlockEntity blockEntity, Player playerEntity) {
        IDivineFavourCapability capability = playerEntity.getCapability(DIVINE_FAVOUR_CAPABILITY).orElse(null);
        Perk perk = MesoamericaMythologyAPI.getPerkTree().getPerk(this.perkId);
        capability.addPerk(perk, (ServerPlayer) playerEntity);
        playerEntity.sendSystemMessage(Component.translatable(I18n.get("mesoamericamythology.recipe.obtained_perk", I18n.get(perk.getTitleStringId()))));
    }

    public static PerkRecipeOutput of(String perkId) {
        return new PerkRecipeOutput(perkId);
    }
}