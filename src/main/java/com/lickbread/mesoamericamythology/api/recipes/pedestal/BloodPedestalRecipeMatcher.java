package com.lickbread.mesoamericamythology.api.recipes.pedestal;

import com.google.common.collect.ImmutableList;
import com.lickbread.mesoamericamythology.blockEntities.AbstractBloodBasinBlockEntity;
import com.lickbread.mesoamericamythology.util.BlockPosUtil;
import com.lickbread.mesoamericamythology.util.MinMaxBounds;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;
import net.minecraft.core.BlockPos;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.entity.BlockEntity;

import java.util.List;
import java.util.stream.Collectors;

@NoArgsConstructor
@ToString
public class BloodPedestalRecipeMatcher implements IInfusionPedestalRecipeMatcher {

    private float requiredBlood;

    @Getter
    private List<BlockPos> matchingBloodBasins;

    public BloodPedestalRecipeMatcher(float requiredBlood) {
        this.requiredBlood = requiredBlood;
    }

    @Override
    public boolean doesMatch(Level level, BlockPos pos, ItemStack stack) {
        List<AbstractBloodBasinBlockEntity> nearbyBloodBasins = BlockPosUtil.getAllBlockEntitiesWithinBounds(level, new MinMaxBounds(pos.subtract(InfusionPedestalRecipe.EXTRA_RANGE_VEC), pos.offset(InfusionPedestalRecipe.EXTRA_RANGE_VEC)), AbstractBloodBasinBlockEntity.class);

        float[] remainingBlood = { this.requiredBlood };
        matchingBloodBasins = nearbyBloodBasins.stream()
                .filter(blockEntity -> {
                    if (this.doesMatchInitially(blockEntity)) {
                        boolean canFit = remainingBlood[0] > 0;
                        remainingBlood[0] -= blockEntity.getBloodPoolAmount();
                        return canFit;
                    }
                    return false;
                })
                .map(BlockEntity::getBlockPos)
                .collect(Collectors.toList());

        return !matchingBloodBasins.isEmpty();
    }

    private boolean doesMatchInitially(AbstractBloodBasinBlockEntity blockEntity) {
        return this.doesMatch(blockEntity) && blockEntity.canAttach();
    }

    private boolean doesMatchDuring(AbstractBloodBasinBlockEntity blockEntity) {
        return this.doesMatch(blockEntity);
    }

    private boolean doesMatch(AbstractBloodBasinBlockEntity blockEntity) {
        return blockEntity.getBloodPoolAmount() > 0F;
    }

    @Override
    public boolean stillMatches(Level level, BlockPos pos, ItemStack stack) {
        float[] remainingBlood = { this.requiredBlood };
        BlockPosUtil.getAllBlockEntities(level, this.matchingBloodBasins, AbstractBloodBasinBlockEntity.class)
                .forEach(blockEntity -> {
                    if (this.doesMatchDuring(blockEntity)) {
                        remainingBlood[0] -= blockEntity.getBloodPoolAmount();
                    }
                });

        return remainingBlood[0] <= 0F;
    }

    @Override
    public List<BlockPos> getBloodPools() {
        return this.matchingBloodBasins;
    }

    @Override
    public List<BlockPos> getCuauhxicalli() {
        return ImmutableList.of();
    }

    @Override
    public List<BlockPos> getSacrificePedestals() {
        return ImmutableList.of();
    }

    @Override
    public List<BlockPos> getPowerPedestals() {
        return ImmutableList.of();
    }
}
