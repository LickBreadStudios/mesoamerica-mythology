package com.lickbread.mesoamericamythology.api.recipes.pedestal;

import com.google.common.collect.ImmutableList;
import com.lickbread.mesoamericamythology.blocks.altar.CuauhxicalliBlock;
import com.lickbread.mesoamericamythology.blocks.state.CuauhxicalliState;
import com.lickbread.mesoamericamythology.items.LivingHeartItem;
import com.lickbread.mesoamericamythology.blockEntities.CuauhxicalliBlockEntity;
import com.lickbread.mesoamericamythology.util.BlockPosUtil;
import com.lickbread.mesoamericamythology.util.MinMaxBounds;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;
import net.minecraft.world.entity.EntityType;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.core.BlockPos;
import net.minecraft.world.level.Level;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@NoArgsConstructor
@ToString
public class CuauhxicalliHeartPedestalRecipeMatcher<I extends LivingHeartItem, T extends EntityType<? extends LivingEntity>> implements IInfusionPedestalRecipeMatcher {

    private I itemType;
    private Optional<T> entityType;
    private int count;

    @Getter
    private List<BlockPos> matchingCuauhxicalli;

    // Works for specific entity
    public CuauhxicalliHeartPedestalRecipeMatcher(I itemType, T entityType, int count) {
        this.itemType = itemType;
        this.entityType = Optional.of(entityType);
        this.count = count;
    }

    // Works for any entity
    public CuauhxicalliHeartPedestalRecipeMatcher(I itemType, int count) {
        this.itemType = itemType;
        this.entityType = Optional.empty();
        this.count = count;
    }

    @Override
    public boolean doesMatch(Level level, BlockPos pos, ItemStack stack) {
        List<CuauhxicalliBlockEntity> nearbyCuauhxicalli = BlockPosUtil.getAllBlockEntitiesWithinBounds(level, new MinMaxBounds(pos.subtract(InfusionPedestalRecipe.EXTRA_RANGE_VEC), pos.offset(InfusionPedestalRecipe.EXTRA_RANGE_VEC)), CuauhxicalliBlockEntity.class);

        int[] remainingCount = { count };
        matchingCuauhxicalli = nearbyCuauhxicalli.stream()
                .filter(blockEntity -> {
                    if (this.doesMatchInitially(blockEntity)) {
                        boolean canFit = remainingCount[0] > 0;
                        remainingCount[0]--;
                        return canFit;
                    }
                    return false;
                })
                .map(BlockEntity::getBlockPos)
                .collect(Collectors.toList());

        return !matchingCuauhxicalli.isEmpty();
    }

    private boolean doesMatchInitially(CuauhxicalliBlockEntity blockEntity) {
        return blockEntity.getBlockState().getValue(CuauhxicalliBlock.CUAUHXICALLI_STATE) == CuauhxicalliState.HAS_HEART && this.doesMatch(blockEntity);
    }

    private boolean doesMatchDuring(CuauhxicalliBlockEntity blockEntity) {
        return this.doesMatch(blockEntity);
    }

    private boolean doesMatch(CuauhxicalliBlockEntity blockEntity) {
        if (blockEntity.getHeldItem().isEmpty()) {
            return false;
        }

        if (!blockEntity.getHeldItem().getItem().getClass().isAssignableFrom(itemType.getClass())) {
            return false;
        }

        if (!this.entityType.isPresent()) {
            return true;
        }

        Optional<EntityType<? extends LivingEntity>> entityType = LivingHeartItem.getEntityType(blockEntity.getHeldItem());
        return entityType.isPresent() && entityType.get() == this.entityType.get();
    }

    @Override
    public boolean stillMatches(Level level, BlockPos pos, ItemStack stack) {
        int[] remainingCount = { count };
        BlockPosUtil.getAllBlockEntities(level, this.matchingCuauhxicalli, CuauhxicalliBlockEntity.class)
                .forEach(blockEntity -> {
                    if (this.doesMatchDuring(blockEntity)) {
                        remainingCount[0]--;
                    }
                });

        return remainingCount[0] == 0;
    }

    @Override
    public List<BlockPos> getBloodPools() {
        return ImmutableList.of();
    }

    @Override
    public List<BlockPos> getCuauhxicalli() {
        return matchingCuauhxicalli;
    }

    @Override
    public List<BlockPos> getSacrificePedestals() {
        return ImmutableList.of();
    }

    @Override
    public List<BlockPos> getPowerPedestals() {
        return ImmutableList.of();
    }
}
