package com.lickbread.mesoamericamythology.api.recipes.pedestal;

import net.minecraft.world.item.ItemStack;
import net.minecraft.core.BlockPos;
import net.minecraft.world.level.Level;

import java.util.List;

public interface IInfusionPedestalRecipeMatcher {

    boolean doesMatch(Level level, BlockPos pos, ItemStack stack);

    boolean stillMatches(Level level, BlockPos pos, ItemStack stack);

    List<BlockPos> getBloodPools();

    List<BlockPos> getCuauhxicalli();

    List<BlockPos> getSacrificePedestals();

    List<BlockPos> getPowerPedestals();

}
