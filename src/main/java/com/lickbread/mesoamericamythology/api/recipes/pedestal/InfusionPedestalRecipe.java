package com.lickbread.mesoamericamythology.api.recipes.pedestal;

import com.google.common.collect.ImmutableList;
import com.lickbread.mesoamericamythology.MesoamericaMythologyMod;
import com.lickbread.mesoamericamythology.api.MesoamericaMythologyAPI;
import com.lickbread.mesoamericamythology.api.recipes.outputs.IRecipeOutput;
import com.lickbread.mesoamericamythology.blockEntities.InfusionPedestalBlockEntity;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;
import net.minecraft.core.BlockPos;
import net.minecraft.core.Vec3i;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.Level;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Getter
@NoArgsConstructor
@ToString
public class InfusionPedestalRecipe {

    public static final int EXTRA_RANGE_XZ = 4;
    public static final int EXTRA_RANGE_Y = 2;
    public static final Vec3i EXTRA_RANGE_VEC = new Vec3i(EXTRA_RANGE_XZ, EXTRA_RANGE_Y, EXTRA_RANGE_XZ);

    private List<IInfusionPedestalRecipeMatcher> matchers;
    private IRecipeOutput output;

    public InfusionPedestalRecipe(List<IInfusionPedestalRecipeMatcher> matchers, IRecipeOutput output, float requiredBlood) {
        this.matchers = ImmutableList.<IInfusionPedestalRecipeMatcher>builder().add(new BloodPedestalRecipeMatcher(requiredBlood)).addAll(matchers).build();;
        this.output = output;
    }

    private InfusionPedestalRecipe(List<IInfusionPedestalRecipeMatcher> matchers, IRecipeOutput output) {
        this.matchers = matchers;
        this.output = output;
    }

    private static Optional<InfusionPedestalRecipeWithAttachedBlockEntities> getMatchingRecipe(InfusionPedestalBlockEntity blockEntity) {
        ItemStack heldItem = blockEntity.getHeldItem();
        if (heldItem.isEmpty()) {
            return Optional.empty();
        }
        Level level = blockEntity.getLevel();
        BlockPos pos = blockEntity.getBlockPos();

        List<InfusionPedestalRecipe> recipes = MesoamericaMythologyAPI.getInfusionPedestalRecipes();

        for (InfusionPedestalRecipe recipe : recipes) {
            if (recipe.matches(level, pos, heldItem)) {
                MesoamericaMythologyMod.LOGGER.debug("Found recipe " + recipe.toString());

                List<BlockPos> usablePowerPedestals = new ArrayList<>();
                List<BlockPos> usableSacrificePedestals = new ArrayList<>();
                List<BlockPos> usableBloodPools = new ArrayList<>();
                List<BlockPos> usableCuauhxicalli = new ArrayList<>();

                recipe.matchers.forEach(matcher -> {
                    usablePowerPedestals.addAll(matcher.getPowerPedestals());
                    usableSacrificePedestals.addAll(matcher.getSacrificePedestals());
                    usableBloodPools.addAll(matcher.getBloodPools());
                    usableCuauhxicalli.addAll(matcher.getCuauhxicalli());
                });

                return Optional.of(new InfusionPedestalRecipeWithAttachedBlockEntities(recipe.matchers, recipe.output,
                        usablePowerPedestals, usableSacrificePedestals, usableBloodPools, usableCuauhxicalli));
            }
        }

        MesoamericaMythologyMod.LOGGER.debug("No pedestal recipe found for input " + heldItem);
        return Optional.empty();
    }

    public boolean stillMatches(InfusionPedestalBlockEntity blockEntity) {
        ItemStack heldItem = blockEntity.getHeldItem();
        if (heldItem.isEmpty()) {
            return false;
        }
        Level level = blockEntity.getLevel();
        BlockPos pos = blockEntity.getBlockPos();
        return this.getMatchers().stream().allMatch(iRecipeMatcher -> iRecipeMatcher.stillMatches(level, pos, heldItem));
    }

    private boolean matches(Level level, BlockPos pos, ItemStack heldItem) {
        return this.getMatchers().stream().allMatch(iRecipeMatcher -> iRecipeMatcher.doesMatch(level, pos, heldItem));
    }

    public static Optional<InfusionPedestalRecipeWithAttachedBlockEntities> getRecipe(InfusionPedestalBlockEntity blockEntity) {
        return getMatchingRecipe(blockEntity);
    }

    @Getter
    @NoArgsConstructor
    public static class InfusionPedestalRecipeWithAttachedBlockEntities extends InfusionPedestalRecipe {
        private List<BlockPos> usablePowerPedestals;
        private List<BlockPos> usableSacrificePedestals;
        private List<BlockPos> usableBloodPools;
        private List<BlockPos> usableCuauhxicalli;

        public InfusionPedestalRecipeWithAttachedBlockEntities(List<IInfusionPedestalRecipeMatcher> matchers, IRecipeOutput output, List<BlockPos> usablePowerPedestals, List<BlockPos> usableSacrificePedestals, List<BlockPos> usableBloodPools, List<BlockPos> usableCuauhxicalli) {
            super(matchers, output);
            this.usablePowerPedestals = usablePowerPedestals;
            this.usableSacrificePedestals = usableSacrificePedestals;
            this.usableBloodPools = usableBloodPools;
            this.usableCuauhxicalli = usableCuauhxicalli;
        }
    }
}
