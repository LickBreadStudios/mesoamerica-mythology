package com.lickbread.mesoamericamythology.api.recipes.pedestal;

import com.google.common.collect.ImmutableList;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.ToString;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.alchemy.Potion;
import net.minecraft.world.item.alchemy.PotionUtils;
import net.minecraft.core.BlockPos;
import net.minecraft.world.level.Level;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@ToString
public class PotionInfusionPedestalRecipeMatcher implements IInfusionPedestalRecipeMatcher {

    private Potion potion;

    @Override
    public boolean doesMatch(Level level, BlockPos pos, ItemStack stack) {
        Potion inputPotion = PotionUtils.getPotion(stack);
        return inputPotion.equals(potion);
    }

    @Override
    public boolean stillMatches(Level level, BlockPos pos, ItemStack stack) {
        return this.doesMatch(level, pos, stack);
    }

    @Override
    public List<BlockPos> getBloodPools() {
        return ImmutableList.of();
    }

    @Override
    public List<BlockPos> getCuauhxicalli() {
        return ImmutableList.of();
    }

    @Override
    public List<BlockPos> getSacrificePedestals() {
        return ImmutableList.of();
    }

    @Override
    public List<BlockPos> getPowerPedestals() {
        return ImmutableList.of();
    }
}
