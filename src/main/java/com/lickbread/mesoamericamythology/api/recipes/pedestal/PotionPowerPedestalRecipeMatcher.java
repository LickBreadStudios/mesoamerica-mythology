package com.lickbread.mesoamericamythology.api.recipes.pedestal;

import com.google.common.collect.ImmutableList;
import com.lickbread.mesoamericamythology.blockEntities.PowerPedestalBlockEntity;
import com.lickbread.mesoamericamythology.blocks.altar.PedestalBlock;
import com.lickbread.mesoamericamythology.blocks.state.PedestalBlockState;
import com.lickbread.mesoamericamythology.util.BlockPosUtil;
import com.lickbread.mesoamericamythology.util.MinMaxBounds;
import lombok.NoArgsConstructor;
import lombok.ToString;
import net.minecraft.core.BlockPos;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.alchemy.Potion;
import net.minecraft.world.item.alchemy.PotionUtils;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.entity.BlockEntity;

import java.util.List;
import java.util.stream.Collectors;

@NoArgsConstructor
@ToString
public class PotionPowerPedestalRecipeMatcher implements IInfusionPedestalRecipeMatcher {

    private Potion potion;
    private int count;

    public PotionPowerPedestalRecipeMatcher(Potion potion, int count) {
        this.potion = potion;
        this.count = count;
    }

    private List<BlockPos> matchingPowerPedestals;

    @Override
    public boolean doesMatch(Level level, BlockPos pos, ItemStack stack) {
        List<PowerPedestalBlockEntity> nearbyPedestals = BlockPosUtil.getAllBlockEntitiesWithinBounds(level, new MinMaxBounds(pos.subtract(InfusionPedestalRecipe.EXTRA_RANGE_VEC), pos.offset(InfusionPedestalRecipe.EXTRA_RANGE_VEC)), PowerPedestalBlockEntity.class);

        int[] remainingCount = { count };
        matchingPowerPedestals = nearbyPedestals.stream()
                .filter(powerPedestalBlockEntity -> {
                    if (this.doesMatchInitially(powerPedestalBlockEntity)) {
                        boolean canFit = remainingCount[0] > 0;
                        remainingCount[0]--;
                        return canFit;
                    }
                    return false;
                })
                .map(BlockEntity::getBlockPos)
                .collect(Collectors.toList());

        return remainingCount[0] == 0;
    }

    private boolean doesMatchInitially(PowerPedestalBlockEntity blockEntity) {
        return blockEntity.getBlockState().getValue(PedestalBlock.PEDESTAL_STATE) == PedestalBlockState.INACTIVE && this.doesMatch(blockEntity);
    }

    private boolean doesMatch(PowerPedestalBlockEntity blockEntity) {
        if (!blockEntity.getHeldItem().isEmpty()) {
            Potion inputPotion = PotionUtils.getPotion(blockEntity.getHeldItem());
            return inputPotion.equals(potion);
        }
        return false;
    }

    private boolean doesMatchDuring(PowerPedestalBlockEntity blockEntity) {
        return this.doesMatch(blockEntity);
    }

    @Override
    public boolean stillMatches(Level level, BlockPos pos, ItemStack stack) {
        int[] remainingCount = { count };
        BlockPosUtil.getAllBlockEntities(level, this.matchingPowerPedestals, PowerPedestalBlockEntity.class)
                .forEach(powerPedestalBlockEntity -> {
                    if (this.doesMatchDuring(powerPedestalBlockEntity)) {
                        remainingCount[0]--;
                    }
                });

        return remainingCount[0] == 0;
    }

    @Override
    public List<BlockPos> getBloodPools() {
        return ImmutableList.of();
    }

    @Override
    public List<BlockPos> getCuauhxicalli() {
        return ImmutableList.of();
    }

    @Override
    public List<BlockPos> getSacrificePedestals() {
        return ImmutableList.of();
    }

    @Override
    public List<BlockPos> getPowerPedestals() {
        return matchingPowerPedestals;
    }
}
