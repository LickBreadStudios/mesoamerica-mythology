package com.lickbread.mesoamericamythology.api.recipes.pedestal;

import com.google.common.collect.ImmutableList;
import com.lickbread.mesoamericamythology.blocks.altar.PedestalBlock;
import com.lickbread.mesoamericamythology.blocks.state.PedestalBlockState;
import com.lickbread.mesoamericamythology.blockEntities.SacrificePedestalBlockEntity;
import com.lickbread.mesoamericamythology.util.BlockPosUtil;
import com.lickbread.mesoamericamythology.util.MinMaxBounds;
import lombok.NoArgsConstructor;
import lombok.ToString;
import net.minecraft.world.entity.EntityType;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.core.BlockPos;
import net.minecraft.world.level.Level;

import java.util.List;
import java.util.stream.Collectors;

@NoArgsConstructor
@ToString
public class SacrificialPedestalRecipeMatcher implements IInfusionPedestalRecipeMatcher {

    private EntityType<?> entityType;
    private int count;

    public SacrificialPedestalRecipeMatcher(EntityType<?> entityType, int count) {
        this.entityType = entityType;
        this.count = count;
    }

    private List<BlockPos> matchingSacrificePedestals;

    @Override
    public boolean doesMatch(Level level, BlockPos pos, ItemStack stack) {
        List<SacrificePedestalBlockEntity> nearbyPedestals = BlockPosUtil.getAllBlockEntitiesWithinBounds(level, new MinMaxBounds(pos.subtract(InfusionPedestalRecipe.EXTRA_RANGE_VEC), pos.offset(InfusionPedestalRecipe.EXTRA_RANGE_VEC)), SacrificePedestalBlockEntity.class);

        int[] remainingCount = { count };
        matchingSacrificePedestals = nearbyPedestals.stream()
                .filter(sacrificePedestalBlockEntity -> {
                    if (this.doesMatchInitially(sacrificePedestalBlockEntity)) {
                        boolean canFit = remainingCount[0] > 0;
                        remainingCount[0]--;
                        return canFit;
                    }
                    return false;
                })
                .map(BlockEntity::getBlockPos)
                .collect(Collectors.toList());

        return remainingCount[0] == 0;
    }

    private boolean doesMatchInitially(SacrificePedestalBlockEntity blockEntity) {
        return blockEntity.getBlockState().getValue(PedestalBlock.PEDESTAL_STATE) == PedestalBlockState.INACTIVE && this.doesMatch(blockEntity);
    }

    private boolean doesMatchDuring(SacrificePedestalBlockEntity blockEntity) {
        return this.doesMatch(blockEntity);
    }

    private boolean doesMatch(SacrificePedestalBlockEntity blockEntity) {
        return blockEntity.getEntity().isPresent() && blockEntity.getEntity().get().getType() == this.entityType;
    }

    @Override
    public boolean stillMatches(Level level, BlockPos pos, ItemStack stack) {
        int[] remainingCount = { count };
        BlockPosUtil.getAllBlockEntities(level, this.matchingSacrificePedestals, SacrificePedestalBlockEntity.class)
                .forEach(sacrificePedestalBlockEntity -> {
                    if (this.doesMatchDuring(sacrificePedestalBlockEntity)) {
                        remainingCount[0]--;
                    }
                });

        return remainingCount[0] == 0;
    }

    @Override
    public List<BlockPos> getBloodPools() {
        return ImmutableList.of();
    }

    @Override
    public List<BlockPos> getCuauhxicalli() {
        return ImmutableList.of();
    }

    @Override
    public List<BlockPos> getSacrificePedestals() {
        return this.matchingSacrificePedestals;
    }

    @Override
    public List<BlockPos> getPowerPedestals() {
        return ImmutableList.of();
    }
}
