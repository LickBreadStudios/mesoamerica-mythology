package com.lickbread.mesoamericamythology.blockEntities;

import com.lickbread.mesoamericamythology.util.SerializerUtil;
import lombok.Getter;
import net.minecraft.core.BlockPos;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.nbt.ListTag;
import net.minecraft.network.protocol.game.ClientboundBlockEntityDataPacket;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.level.block.entity.BlockEntityType;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.phys.Vec3;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

public abstract class AbstractBloodBasinBlockEntity extends BlockEntity {

    private static final String BUFFER_POS_KEY = "BufferPos";
    private static final String BUFFER_AMOUNT_KEY = "BufferAmount";
    private static final String BLOOD_BUFFER_KEY = "BloodBuffer";
    private static final String BLOOD_AMOUNT_KEY = "BloodAmount";
    private static final String ACTIVE_INFUSION_PEDESTAL = "ActiveInfusionPedestal";
    private static final String ATTACHED_GOD_STATUE_HEAD = "AttachedGodStatueHead";

    public AbstractBloodBasinBlockEntity(BlockEntityType<?> type, BlockPos pos, BlockState state) {
        super(type, pos, state);
        this.bloodPoolBuffer = new HashMap<>();
        this.bloodPoolAmount = 0;
        this.activeInfusionPedestal = Optional.empty();
        this.attachedGodStatueHead = Optional.empty();
    }

    @Getter
    protected float bloodPoolAmount; // The amount of blood in the well
    protected Map<Vec3, Float> bloodPoolBuffer; // The amount of blood received recently that hasn't yet transferred into the well

    @Getter
    protected Optional<BlockPos> activeInfusionPedestal;

    @Getter
    protected Optional<BlockPos> attachedGodStatueHead;

    public abstract float getMaxBlood();

    public abstract float getBloodTransferRate();

    public abstract Vec3 getRandomBloodParticlePos();

    public boolean canAttach() {
        return !this.attachedGodStatueHead.isPresent() && !this.activeInfusionPedestal.isPresent();
    }

    public static void clientTick(Level level, BlockPos pos, BlockState state, AbstractBloodBasinBlockEntity blockEntity) {
        blockEntity.transferBlood();
    }

    public static void serverTick(Level level, BlockPos pos, BlockState state, AbstractBloodBasinBlockEntity blockEntity) {
        blockEntity.transferBlood();
        blockEntity.sendUpdates();
    }

    public void setAttachedGodStatueHead(BlockPos attachedGodStatueHead) {
        if (this.canAttach()) {
            this.attachedGodStatueHead = Optional.of(attachedGodStatueHead);
        }
    }

    public void removeAttachedGodStatueHead() {
        this.attachedGodStatueHead = Optional.empty();
    }

    public void setActiveInfusionPedestal(BlockPos activeInfusionPedestal) {
        if (this.canAttach()) {
            this.activeInfusionPedestal = Optional.of(activeInfusionPedestal);
        }
    }

    public void reset() {
        this.bloodPoolAmount = 0;
        this.activeInfusionPedestal = Optional.empty();
        this.attachedGodStatueHead = Optional.empty();
    }

    /**
     * Transfers blood from the blood buffer into the main blood pool
     */
    private void transferBlood() {
        if (this.bloodPoolBuffer.size() > 0) {
            Map<Vec3, Float> remainingBloodBuffer = new HashMap<>();
            this.bloodPoolBuffer.entrySet().forEach(entry -> this.transferBloodBufferAmount(entry, remainingBloodBuffer));
            this.bloodPoolAmount = Math.min(this.bloodPoolAmount, this.getMaxBlood());
            this.bloodPoolBuffer = remainingBloodBuffer;
        }
    }

    private void transferBloodBufferAmount(Map.Entry<Vec3, Float> entry, Map<Vec3, Float> remainingBloodBuffer) {
        if (entry.getValue() < this.getBloodTransferRate()) {
            this.bloodPoolAmount += entry.getValue();
        } else {
            entry.setValue(entry.getValue() - this.getBloodTransferRate());
            this.bloodPoolAmount += this.getBloodTransferRate();
            remainingBloodBuffer.put(entry.getKey(), entry.getValue());
        }
    }

    public float getTotalBloodAmount() {
        return this.bloodPoolAmount + this.getCurrentBloodBufferAmount();
    }

    public float getCurrentBloodBufferAmount() {
        return (float) this.bloodPoolBuffer.values().stream().mapToDouble(Float::doubleValue).sum();
    }

    public Set<Vec3> getCurrentBloodBufferPositions() {
        return this.bloodPoolBuffer.keySet();
    }

    public void addBlood(float bloodAmount, Vec3 entityPosition) {
        this.bloodPoolBuffer.put(entityPosition, bloodAmount);
        this.sendUpdates();
    }

    protected void sendUpdates() {
        this.setChanged();
        BlockState state = this.getLevel().getBlockState(this.getBlockPos());
        this.getLevel().sendBlockUpdated(this.getBlockPos(), state, state, Block.UPDATE_ALL_IMMEDIATE);
    }

    @Override
    public void load(CompoundTag compound) {
        super.load(compound);

        ListTag bloodBufferNBT = (ListTag) compound.get(BLOOD_BUFFER_KEY);
        this.bloodPoolBuffer.clear();
        for (int i = 0; i < bloodBufferNBT.size(); i++) {
            CompoundTag tag = bloodBufferNBT.getCompound(i);
            float bufferAmount = tag.getFloat(BUFFER_AMOUNT_KEY);
            this.bloodPoolBuffer.put(SerializerUtil.deserializeVec3(tag.getCompound(BUFFER_POS_KEY)), bufferAmount);
        }

        this.bloodPoolAmount = compound.getFloat(BLOOD_AMOUNT_KEY);

        if (compound.contains(ACTIVE_INFUSION_PEDESTAL)) {
            this.activeInfusionPedestal = Optional.of(SerializerUtil.deserializeBlockPos(compound.getCompound(ACTIVE_INFUSION_PEDESTAL)));
        }

        if (compound.contains(ATTACHED_GOD_STATUE_HEAD)) {
            this.attachedGodStatueHead = Optional.of(SerializerUtil.deserializeBlockPos(compound.getCompound(ATTACHED_GOD_STATUE_HEAD)));
        }
    }

    @Override
    public void saveAdditional(CompoundTag compound) {
        super.saveAdditional(compound);

        ListTag bloodBufferList = new ListTag();
        for (Map.Entry<Vec3, Float> entry : this.bloodPoolBuffer.entrySet()) {
            CompoundTag stackNBT = new CompoundTag();
            stackNBT.putFloat(BUFFER_AMOUNT_KEY, entry.getValue());
            stackNBT.put(BUFFER_POS_KEY, SerializerUtil.serialize(entry.getKey()));
            bloodBufferList.add(stackNBT);
        }

        compound.put(BLOOD_BUFFER_KEY, bloodBufferList);
        compound.putFloat(BLOOD_AMOUNT_KEY, this.bloodPoolAmount);
        this.activeInfusionPedestal.ifPresent(blockPos -> compound.put(ACTIVE_INFUSION_PEDESTAL, SerializerUtil.serialize(blockPos)));
        this.attachedGodStatueHead.ifPresent(blockPos -> compound.put(ATTACHED_GOD_STATUE_HEAD, SerializerUtil.serialize(blockPos)));
    }

    // Prepare a packet for syncing our TE to the client. Since we only have to sync the stack
    // and that's all we have we just write our entire NBT here. If you have a complex
    // tile entity that doesn't need to have all information on the client you can write
    // a more optimal NBT here.
    @Override
    public ClientboundBlockEntityDataPacket getUpdatePacket() {
        return ClientboundBlockEntityDataPacket.create(this);
    }

    // getUpdateTag() is called whenever the chunkdata is sent to the
    // client. In contrast getUpdatePacket() is called when the tile entity
    // itself wants to sync to the client. In many cases you want to send
    // over the same information in getUpdateTag() as in getUpdatePacket().
    @Override
    public CompoundTag getUpdateTag() {
        CompoundTag tag = super.getUpdateTag();
        this.saveAdditional(tag);
        return tag;
    }
}
