package com.lickbread.mesoamericamythology.blockEntities;

import com.lickbread.mesoamericamythology.constants.BloodLimits;
import com.lickbread.mesoamericamythology.init.ModBlockEntityTypeObjects;
import com.lickbread.mesoamericamythology.util.BlockPosUtil;
import com.lickbread.mesoamericamythology.util.MathUtil;
import net.minecraft.core.BlockPos;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.phys.Vec3;

import java.util.Random;

public class BloodBasinBlockEntity extends AbstractBloodBasinBlockEntity {

    private static final double MAX_PARTICLE_RANGE = 0.5D;

    public BloodBasinBlockEntity(BlockPos pos, BlockState state) {
        super(ModBlockEntityTypeObjects.BLOOD_BASIN, pos, state);
    }

    @Override
    public float getMaxBlood() {
        return BloodLimits.MAX_BLOOD_BLOOD_BASIN;
    }

    @Override
    public float getBloodTransferRate() {
        return BloodLimits.BLOOD_TRANSFER_RATE_BLOOD_BASIN;
    }

    @Override
    public Vec3 getRandomBloodParticlePos() {
        Random random = new Random();
        return BlockPosUtil.getPosTopCentre(this.getBlockPos()).add(MathUtil.getInRandom(random, MAX_PARTICLE_RANGE), 0D, MathUtil.getInRandom(random, MAX_PARTICLE_RANGE));
    }

}
