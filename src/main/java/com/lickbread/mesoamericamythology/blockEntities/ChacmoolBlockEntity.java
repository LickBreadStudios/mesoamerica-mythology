package com.lickbread.mesoamericamythology.blockEntities;

import com.lickbread.mesoamericamythology.init.ModBlockEntityTypeObjects;
import net.minecraft.core.BlockPos;
import net.minecraft.core.NonNullList;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.network.protocol.game.ClientboundBlockEntityDataPacket;
import net.minecraft.world.entity.item.ItemEntity;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.level.block.state.BlockState;

public class ChacmoolBlockEntity extends BlockEntity {

    private static final String ITEM_KEY = "Item";

    public ChacmoolBlockEntity(BlockPos pos, BlockState state) {
        super(ModBlockEntityTypeObjects.CHACMOOL, pos, state);
    }

    private ItemStack heldItem = ItemStack.EMPTY;

    public boolean addItem(ItemStack item) {
        if (!this.getLevel().isClientSide && !item.isEmpty() && this.heldItem.isEmpty()) {
            this.heldItem = item;
            this.sendUpdates();
            return true;
        }

        return false;
    }

    public void removeLastItem() {
        if (!this.heldItem.isEmpty()) {
            ItemEntity entity = new ItemEntity(this.getLevel(), this.getBlockPos().getX() + 0.5D, (double) this.getBlockPos().getY() + 1.1D, (double) this.getBlockPos().getZ() + 0.5D, this.heldItem);
            this.getLevel().addFreshEntity(entity);
            this.heldItem = ItemStack.EMPTY;
            this.sendUpdates();
        }
    }

    public NonNullList<ItemStack> getInputs() {
        return NonNullList.of(this.heldItem);
    }

    private void sendUpdates() {
        super.setChanged();
        BlockState state = this.getLevel().getBlockState(this.getBlockPos());
        this.getLevel().sendBlockUpdated(this.getBlockPos(), state, state, Block.UPDATE_ALL_IMMEDIATE);
    }

    @Override
    public void load(CompoundTag compound) {
        super.load(compound);

        this.heldItem = ItemStack.of(compound.getCompound(ITEM_KEY));
    }

    @Override
    public void saveAdditional(CompoundTag compound) {
        super.saveAdditional(compound);

        compound.put(ITEM_KEY, this.heldItem.serializeNBT());
    }

    // Prepare a packet for syncing our TE to the client. Since we only have to sync the stack
    // and that's all we have we just write our entire NBT here. If you have a complex
    // tile entity that doesn't need to have all information on the client you can write
    // a more optimal NBT here.
    @Override
    public ClientboundBlockEntityDataPacket getUpdatePacket() {
        return ClientboundBlockEntityDataPacket.create(this);
    }

    // getUpdateTag() is called whenever the chunkdata is sent to the
    // client. In contrast getUpdatePacket() is called when the tile entity
    // itself wants to sync to the client. In many cases you want to send
    // over the same information in getUpdateTag() as in getUpdatePacket().
    @Override
    public CompoundTag getUpdateTag() {
        CompoundTag tag = super.getUpdateTag();
        this.saveAdditional(tag);
        return tag;
    }

}
