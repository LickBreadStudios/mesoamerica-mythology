package com.lickbread.mesoamericamythology.blockEntities;

import com.lickbread.mesoamericamythology.blocks.traps.FireTrapBlock;
import com.lickbread.mesoamericamythology.init.ModBlockEntityTypeObjects;
import com.lickbread.mesoamericamythology.registry.ModDamageSources;
import com.lickbread.mesoamericamythology.util.MathUtil;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.core.particles.ParticleTypes;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.core.Direction;
import net.minecraft.sounds.SoundSource;
import net.minecraft.sounds.SoundEvents;
import net.minecraft.world.phys.AABB;
import net.minecraft.core.BlockPos;
import net.minecraft.world.level.Level;
import net.minecraft.server.level.ServerLevel;

import java.util.List;
import java.util.Random;

import static com.lickbread.mesoamericamythology.blocks.traps.FireTrapBlock.FACING;

public class FireTrapBlockEntity extends BlockEntity {

    private static final int TICKS_TO_DAMAGE = 5;

    private static final double FIRE_LENGTH = 5.0D;
    private static final double FIRE_WIDTH = 0.25D;

    private static final double PARTICLE_PERPENDICULAR_OFFSET = -0.05D;
    private static final double PARTICLE_PARALLEL_OFFSET = 0.5D;
    public static final int MIN_PARTICLES = 5;
    public static final int MAX_PARTICLES = 10;
    public static final double PARTICLE_PERPENDICULAR_SPEED_VARIANCE = 0.015D;
    public static final double PARTICLE_PARALLEL_SPEED_MIN = 0.2D;
    public static final double PARTICLE_PARALLEL_SPEED_VARIANCE = 0.05D;
    public static final double FLAME_CHANCE = 0.85D;

    private int currentTick;
    private Random rand;

    public FireTrapBlockEntity(BlockPos pos, BlockState state) {
        super(ModBlockEntityTypeObjects.FIRE_TRAP, pos, state);
        this.currentTick = 0;
        this.rand = new Random();
    }

    public static void clientTick(Level level, BlockPos pos, BlockState state, FireTrapBlockEntity blockEntity) {
        if (state.getValue(FireTrapBlock.ACTIVATED)) {
            blockEntity.flameTick(state, level, pos);
        }
    }

    public static void serverTick(Level level, BlockPos pos, BlockState state, FireTrapBlockEntity blockEntity) {
        if (state.getValue(FireTrapBlock.ACTIVATED)) {
            blockEntity.currentTick++;
            if (blockEntity.currentTick > TICKS_TO_DAMAGE) {
                blockEntity.damageTick(state, (ServerLevel) level, pos);
                blockEntity.currentTick = 0;
            }
        }
    }

    public void damageTick(BlockState state, ServerLevel level, BlockPos pos) {
        Direction dir = state.getValue(FACING);
        AABB damageBounds = MathUtil.getDamageBox(pos, dir, FIRE_WIDTH, FIRE_LENGTH);
        List<LivingEntity> entities = level.getEntitiesOfClass(LivingEntity.class, damageBounds);

        for (LivingEntity entity : entities) {
            if (!entity.fireImmune()) {
                entity.hurt(ModDamageSources.source(level, ModDamageSources.FIRE_TRAP), 3.0F);
                entity.setSecondsOnFire(5);
            }
        }

        level.playLocalSound(pos.getX(), pos.getY(), pos.getZ(), SoundEvents.FIRE_EXTINGUISH, SoundSource.BLOCKS, 0.1F, 1.0F, false);
    }

    public void flameTick(BlockState state, Level level, BlockPos pos) {
        Direction dir = state.getValue(FACING);

        double x = (double)pos.getX() + 0.5D;
        double y = (double)pos.getY() + 0.5D;
        double z = (double)pos.getZ() + 0.5D;

        for (int i = 0; i < rand.nextInt(MAX_PARTICLES) + MIN_PARTICLES; i++) {
            double xPos = 0.0D, yPos = 0.0D, zPos = 0.0D;
            double xVec = 0.0D, yVec = 0.0D, zVec = 0.0D;
            switch (dir) {
                case WEST:
                    xPos = x - MathUtil.getInRandom(rand, PARTICLE_PARALLEL_OFFSET);
                    yPos = y + MathUtil.getInRandom(rand, PARTICLE_PERPENDICULAR_OFFSET);
                    zPos = z + MathUtil.getInRandom(rand, PARTICLE_PERPENDICULAR_OFFSET);
                    xVec = -(PARTICLE_PARALLEL_SPEED_MIN + MathUtil.getInRandom(rand, PARTICLE_PARALLEL_SPEED_VARIANCE));
                    yVec = MathUtil.getInRandom(rand, PARTICLE_PERPENDICULAR_SPEED_VARIANCE);
                    zVec = MathUtil.getInRandom(rand, PARTICLE_PERPENDICULAR_SPEED_VARIANCE);
                    break;
                case EAST:
                    xPos = x + MathUtil.getInRandom(rand, PARTICLE_PARALLEL_OFFSET);
                    yPos = y + MathUtil.getInRandom(rand, PARTICLE_PERPENDICULAR_OFFSET);
                    zPos = z + MathUtil.getInRandom(rand, PARTICLE_PERPENDICULAR_OFFSET);
                    xVec = PARTICLE_PARALLEL_SPEED_MIN + MathUtil.getInRandom(rand, PARTICLE_PARALLEL_SPEED_VARIANCE);
                    yVec = MathUtil.getInRandom(rand, PARTICLE_PERPENDICULAR_SPEED_VARIANCE);
                    zVec = MathUtil.getInRandom(rand, PARTICLE_PERPENDICULAR_SPEED_VARIANCE);
                    break;
                case NORTH:
                    xPos = x + MathUtil.getInRandom(rand, PARTICLE_PERPENDICULAR_OFFSET);
                    yPos = y + MathUtil.getInRandom(rand, PARTICLE_PERPENDICULAR_OFFSET);
                    zPos = z - MathUtil.getInRandom(rand, PARTICLE_PARALLEL_OFFSET);
                    xVec = MathUtil.getInRandom(rand, PARTICLE_PERPENDICULAR_SPEED_VARIANCE);
                    yVec = MathUtil.getInRandom(rand, PARTICLE_PERPENDICULAR_SPEED_VARIANCE);
                    zVec = -(PARTICLE_PARALLEL_SPEED_MIN + MathUtil.getInRandom(rand, PARTICLE_PARALLEL_SPEED_VARIANCE));
                    break;
                case SOUTH:
                    xPos = x + MathUtil.getInRandom(rand, PARTICLE_PERPENDICULAR_OFFSET);
                    yPos = y + MathUtil.getInRandom(rand, PARTICLE_PERPENDICULAR_OFFSET);
                    zPos = z + MathUtil.getInRandom(rand, PARTICLE_PARALLEL_OFFSET);
                    xVec = MathUtil.getInRandom(rand, PARTICLE_PERPENDICULAR_SPEED_VARIANCE);
                    yVec = MathUtil.getInRandom(rand, PARTICLE_PERPENDICULAR_SPEED_VARIANCE);
                    zVec = PARTICLE_PARALLEL_SPEED_MIN + MathUtil.getInRandom(rand, PARTICLE_PARALLEL_SPEED_VARIANCE);
                    break;
                case DOWN:
                    xPos = x + MathUtil.getInRandom(rand, PARTICLE_PERPENDICULAR_OFFSET);
                    yPos = y - MathUtil.getInRandom(rand, PARTICLE_PARALLEL_OFFSET);
                    zPos = z + MathUtil.getInRandom(rand, PARTICLE_PERPENDICULAR_OFFSET);
                    xVec = MathUtil.getInRandom(rand, PARTICLE_PERPENDICULAR_SPEED_VARIANCE);
                    yVec = -(PARTICLE_PARALLEL_SPEED_MIN + MathUtil.getInRandom(rand, PARTICLE_PARALLEL_SPEED_VARIANCE));
                    zVec = MathUtil.getInRandom(rand, PARTICLE_PERPENDICULAR_SPEED_VARIANCE);
                    break;
                case UP:
                    xPos = x + MathUtil.getInRandom(rand, PARTICLE_PERPENDICULAR_OFFSET);
                    yPos = y + MathUtil.getInRandom(rand, PARTICLE_PARALLEL_OFFSET);
                    zPos = z + MathUtil.getInRandom(rand, PARTICLE_PERPENDICULAR_OFFSET);
                    xVec = MathUtil.getInRandom(rand, PARTICLE_PERPENDICULAR_SPEED_VARIANCE);
                    yVec = PARTICLE_PARALLEL_SPEED_MIN + MathUtil.getInRandom(rand, PARTICLE_PARALLEL_SPEED_VARIANCE);
                    zVec = MathUtil.getInRandom(rand, PARTICLE_PERPENDICULAR_SPEED_VARIANCE);
                    break;
            }
            if (rand.nextDouble() < FLAME_CHANCE) {
                level.addParticle(ParticleTypes.FLAME, xPos, yPos, zPos, xVec, yVec, zVec);
            } else {
                level.addParticle(ParticleTypes.SMOKE, xPos, yPos, zPos, xVec, yVec, zVec);
            }
        }
    }
}
