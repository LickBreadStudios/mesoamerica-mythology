package com.lickbread.mesoamericamythology.blockEntities;

import com.lickbread.mesoamericamythology.MesoamericaMythologyMod;
import com.lickbread.mesoamericamythology.blocks.CarvedStoneBlock;
import com.lickbread.mesoamericamythology.blocks.GodStatueHeadBlock;
import com.lickbread.mesoamericamythology.blocks.state.GodStatueHeadState;
import com.lickbread.mesoamericamythology.init.ModBlockEntityTypeObjects;
import com.lickbread.mesoamericamythology.registry.ModItems;
import com.lickbread.mesoamericamythology.statueHead.God;
import com.lickbread.mesoamericamythology.statueHead.interaction.GiveOfferingInteraction;
import com.lickbread.mesoamericamythology.statueHead.interaction.GodStatueHeadInteraction;
import com.lickbread.mesoamericamythology.statueHead.interaction.TalkInteraction;
import com.lickbread.mesoamericamythology.util.MinMaxBounds;
import com.lickbread.mesoamericamythology.util.SerializerUtil;
import lombok.Getter;
import lombok.Setter;
import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.core.Vec3i;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.nbt.ListTag;
import net.minecraft.network.protocol.game.ClientboundBlockEntityDataPacket;
import net.minecraft.server.level.ServerPlayer;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.Items;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.phys.AABB;

import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import static com.lickbread.mesoamericamythology.util.SerializerUtil.deserializeBlockPos;

public class GodStatueHeadBlockEntity extends BlockEntity {

    private static final String MULTI_BLOCK_POSITIONS = "SlavePositions";
    private static final String BLOOD_BASIN_POSITIONS = "BloodBasinPositions";
    private static final String GOD_STATUE_HEAD_TYPE = "GodStatueHeadType";
    private static final String IS_ACTIVATED = "IsActivated";

    private static final int PERPENDICULAR_OFFSET = 1;
    private static final int PARALLEL_OFFSET = 1;
    private static final int WIDTH = PERPENDICULAR_OFFSET * 2 + 1;
    private static final int HEIGHT = PERPENDICULAR_OFFSET * 2 + 1;
    private static final int DEPTH = PARALLEL_OFFSET + 1;
    public static final int TOTAL_BLOCKS = WIDTH * HEIGHT * DEPTH;

    private static final int TIME_BETWEEN_INTERACTIONS = 100; // Millis

    @Getter
    @Setter
    private God god;
    private final List<BlockPos> multiBlockPositions;
    private final List<BlockPos> attachedBloodBasins;
    private ZonedDateTime lastInteractTime;
    private GodStatueHeadInteraction currentInteraction; // Represents the current interactions being had.

    @Getter
    private boolean isActivated = false;

    public GodStatueHeadBlockEntity(BlockPos pos, BlockState state) {
        super(ModBlockEntityTypeObjects.GOD_STATUE, pos, state);
        this.multiBlockPositions = new ArrayList<>();
        this.attachedBloodBasins = new ArrayList<>();
        this.lastInteractTime = ZonedDateTime.now();
    }

    public void setGodStatueHeadState(GodStatueHeadState state) {
        this.getLevel().setBlockAndUpdate(this.getBlockPos(), this.getBlockState().setValue(GodStatueHeadBlock.HEAD_STATE, state));
    }

    public boolean tryActivate() {
        if (this.isActivated) {
            return true;
        }

        this.multiBlockPositions.clear();
        List<BlockPos> surroundingBlocks = this.getBlocksForDirection(this.getBlockState().getValue(GodStatueHeadBlock.FACING));

        if (surroundingBlocks.size() != TOTAL_BLOCKS - 1) {
            return false;
        }
        surroundingBlocks.forEach(blockPos -> CarvedStoneBlock.setMaster(this.getLevel(), this.getBlockPos(), blockPos));
        this.multiBlockPositions.addAll(surroundingBlocks);
        this.tryAttachNearbyBloodBasins();

        this.getLevel().setBlockAndUpdate(this.getBlockPos(), this.getBlockState().setValue(GodStatueHeadBlock.HEAD_STATE, GodStatueHeadState.PASSIVE));
        this.isActivated = true;
        MesoamericaMythologyMod.LOGGER.debug("Activated GodStatueHeadBlockEntity with type " + this.god);
        return true;
    }

    public boolean interact(ServerPlayer player, InteractionHand hand) {
        if (player.isCrouching()) {
            return false;
        }

        if (this.isInteracting()) {
            return true;
        }

        if (ZonedDateTime.now().toInstant().toEpochMilli() - this.lastInteractTime.toInstant().toEpochMilli() < TIME_BETWEEN_INTERACTIONS) {
            return false;
        }
        this.lastInteractTime = ZonedDateTime.now();

        ItemStack heldItem = player.getItemInHand(hand);
        if (!this.isActivated()) {
            if (heldItem.getItem() == ModItems.BLOOD_BOWL.get() && this.tryActivate()) {
                heldItem.setCount(heldItem.getCount() - 1);
                player.getInventory().add(new ItemStack(Items.BOWL, 1));
                this.talk(player, true);
            }
        } else if (!heldItem.isEmpty()) {
            this.giveOffering(heldItem.copy(), player);
            player.getInventory().removeItem(heldItem);
        } else {
            this.talk(player, false);
        }
        return true;
    }

    private boolean isInteracting() {
        return this.currentInteraction != null;
    }

    public void finishInteraction() {
        this.currentInteraction = null;
        this.setGodStatueHeadState(GodStatueHeadState.PASSIVE);
    }

    private void giveOffering(ItemStack itemStack, ServerPlayer player) {
        this.currentInteraction = new GiveOfferingInteraction(this, this.god, player, itemStack);
        this.currentInteraction.interact();
    }

    private void talk(ServerPlayer player, boolean newStatueHead) {
        this.currentInteraction = new TalkInteraction(this, this.god, player, newStatueHead);
        this.currentInteraction.interact();
    }

    private void tryAttachNearbyBloodBasins() {
        MinMaxBounds minMaxBounds = getMinMaxBounds(this.getBlockPos(), this.getBlockState().getValue(GodStatueHeadBlock.FACING));
        Vec3i one = new Vec3i(1, 1,1);
        // Expand minMaxBounds by one in each direction
        Stream<BlockPos> blocks = BlockPos.betweenClosedStream(minMaxBounds.min.subtract(one), minMaxBounds.max.offset(one).offset(one));
        blocks.map(BlockPos::new) // Copy as otherwise we iterate over the same value since it is mutable by default
                .map(blockPos -> this.getLevel().getBlockEntity(blockPos))
                .filter(blockEntity -> blockEntity instanceof AbstractBloodBasinBlockEntity)
                .map(blockEntity -> (AbstractBloodBasinBlockEntity) blockEntity)
                .filter(AbstractBloodBasinBlockEntity::canAttach)
                .forEach(bloodBasinBlockEntity -> {
                    bloodBasinBlockEntity.setAttachedGodStatueHead(this.getBlockPos());
                    this.attachedBloodBasins.add(bloodBasinBlockEntity.getBlockPos());
                    MesoamericaMythologyMod.LOGGER.debug("Attached blood basin at [{}] to statue head of {}", bloodBasinBlockEntity.getBlockPos().toShortString(), this.god.toString());
                });
    }

    private List<BlockPos> getBlocksForDirection(Direction direction) {
        MinMaxBounds minMaxBounds = getMinMaxBounds(this.getBlockPos(), direction);
        Stream<BlockPos> blocks = BlockPos.betweenClosedStream(minMaxBounds.min, minMaxBounds.max);
        return blocks
                .map(BlockPos::new) // Copy as otherwise we iterate over the same value since it is mutable by default
                .filter(blockPos -> this.getLevel().getBlockState(blockPos).getBlock() instanceof CarvedStoneBlock)
                .collect(Collectors.toList());
    }

    public static MinMaxBounds getMinMaxBounds(BlockPos pos, Direction direction) {
        BlockPos minPos;
        BlockPos maxPos;

        switch (direction) {
            case NORTH:
                minPos = pos.offset(-PERPENDICULAR_OFFSET , -PERPENDICULAR_OFFSET, 0);
                maxPos = pos.offset(PERPENDICULAR_OFFSET, PERPENDICULAR_OFFSET, PARALLEL_OFFSET);
                break;
            case WEST:
                minPos = pos.offset(0, -PERPENDICULAR_OFFSET, -PERPENDICULAR_OFFSET);
                maxPos = pos.offset(PARALLEL_OFFSET, PERPENDICULAR_OFFSET, PERPENDICULAR_OFFSET);
                break;
            case SOUTH:
                minPos = pos.offset(-PERPENDICULAR_OFFSET , -PERPENDICULAR_OFFSET, -PARALLEL_OFFSET);
                maxPos = pos.offset(PERPENDICULAR_OFFSET, PERPENDICULAR_OFFSET, 0);
                break;
            case EAST:
                minPos = pos.offset(-PARALLEL_OFFSET, -PERPENDICULAR_OFFSET, -PERPENDICULAR_OFFSET);
                maxPos = pos.offset(0, PERPENDICULAR_OFFSET, PERPENDICULAR_OFFSET);
                break;
            default:
                return MinMaxBounds.ZERO;
        }

        return new MinMaxBounds(minPos, maxPos);
    }

    @Override
    public void setRemoved() {
        // TODO this causes issues on world unload
        this.multiBlockPositions.forEach(blockPos -> this.getLevel().destroyBlock(blockPos, true));
        this.attachedBloodBasins.forEach(blockPos -> ((AbstractBloodBasinBlockEntity) this.getLevel().getBlockEntity(blockPos)).removeAttachedGodStatueHead());
        super.setRemoved();
    }

    @Override
    public void load(CompoundTag compound) {
        super.load(compound);

        ListTag multiBlockPositions = (ListTag) compound.get(MULTI_BLOCK_POSITIONS);
        this.multiBlockPositions.clear();
        IntStream.range(0, multiBlockPositions.size()).forEach(i -> this.multiBlockPositions.add(deserializeBlockPos(multiBlockPositions.getCompound(i))));

        ListTag attachedBloodBasins = (ListTag) compound.get(BLOOD_BASIN_POSITIONS);
        this.attachedBloodBasins.clear();
        IntStream.range(0, attachedBloodBasins.size()).forEach(i -> this.attachedBloodBasins.add(deserializeBlockPos(attachedBloodBasins.getCompound(i))));

        this.isActivated = compound.getBoolean(IS_ACTIVATED);

        this.god = God.getEnum(compound.getString(GOD_STATUE_HEAD_TYPE));
    }

    @Override
    public void saveAdditional(CompoundTag compound) {
        super.saveAdditional(compound);

        ListTag multiBlockPositions = this.multiBlockPositions.stream()
                .map(SerializerUtil::serialize)
                .collect(Collectors.toCollection(ListTag::new));
        compound.put(MULTI_BLOCK_POSITIONS, multiBlockPositions);

        ListTag bloodBasinPositions = this.attachedBloodBasins.stream()
                .map(SerializerUtil::serialize)
                .collect(Collectors.toCollection(ListTag::new));
        compound.put(BLOOD_BASIN_POSITIONS, bloodBasinPositions);

        compound.putBoolean(IS_ACTIVATED, this.isActivated);
        compound.putString(GOD_STATUE_HEAD_TYPE, this.god.toString());
    }

    // Prepare a packet for syncing our TE to the client. Since we only have to sync the stack
    // and that's all we have we just write our entire Tag here. If you have a complex
    // tile entity that doesn't need to have all information on the client you can write
    // a more optimal Tag here.
    @Override
    public ClientboundBlockEntityDataPacket getUpdatePacket() {
        return ClientboundBlockEntityDataPacket.create(this);
    }

    // getUpdateTag() is called whenever the chunkdata is sent to the
    // client. In contrast getUpdatePacket() is called when the tile entity
    // itself wants to sync to the client. In many cases you want to send
    // over the same information in getUpdateTag() as in getUpdatePacket().
    @Override
    public CompoundTag getUpdateTag() {
        CompoundTag tag = super.getUpdateTag();
        this.saveAdditional(tag);
        return tag;
    }

    @Override
    public AABB getRenderBoundingBox() {
        return getMinMaxBounds(this.getBlockPos(), this.getBlockState().getValue(GodStatueHeadBlock.FACING)).convertToAxisAlignedBB();
    }
}
