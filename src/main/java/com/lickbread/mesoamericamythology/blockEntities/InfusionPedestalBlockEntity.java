package com.lickbread.mesoamericamythology.blockEntities;

import com.lickbread.mesoamericamythology.MesoamericaMythologyMod;
import com.lickbread.mesoamericamythology.api.recipes.pedestal.InfusionPedestalRecipe;
import com.lickbread.mesoamericamythology.blocks.altar.AbstractBloodBasinBlock;
import com.lickbread.mesoamericamythology.blocks.altar.CuauhxicalliBlock;
import com.lickbread.mesoamericamythology.blocks.altar.InfusionPedestalBlock;
import com.lickbread.mesoamericamythology.blocks.altar.PedestalBlock;
import com.lickbread.mesoamericamythology.blocks.state.CuauhxicalliState;
import com.lickbread.mesoamericamythology.blocks.state.PedestalBlockState;
import com.lickbread.mesoamericamythology.init.ModBlockEntityTypeObjects;
import com.lickbread.mesoamericamythology.registry.ModAdvancementTriggers;
import com.lickbread.mesoamericamythology.util.MathUtil;
import lombok.AllArgsConstructor;
import lombok.Getter;
import net.minecraft.core.BlockPos;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.server.level.ServerPlayer;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.level.block.state.BlockState;

import java.time.ZonedDateTime;
import java.util.Arrays;
import java.util.Optional;
import java.util.Random;

import static com.lickbread.mesoamericamythology.api.MesoamericaMythologyAPI.GSON;

public class InfusionPedestalBlockEntity extends PedestalBlockEntity {

    /**
     * Enum representing the progress through crafting
     *
     * 1200 ticks = 60 seconds to craft
     */
    @Getter
    @AllArgsConstructor
    public enum CraftingStage {
        INACTIVE(0, 0),
        PREPARE(100, 0),
        CRAFT(1000, InfusionPedestalBlockEntity.CraftingStage.PREPARE.getStageTicks()),
        FINISH(100, InfusionPedestalBlockEntity.CraftingStage.PREPARE.getStageTicks() + InfusionPedestalBlockEntity.CraftingStage.CRAFT.getStageTicks());

        private final int stageTicks;
        private final int totalStageTicks;

        public static int totalTicks() {
            return Arrays.stream(InfusionPedestalBlockEntity.CraftingStage.values()).mapToInt(InfusionPedestalBlockEntity.CraftingStage::getStageTicks).sum();
        }
    }

    private static final int MAX_INSTABILITY_LEVEL = 100; // 10 seconds
    private static final int INSTABILITY_LEVEL_RATE = 1;

    private static final int MIN_ACTIVATE_PARTICLE_TICKS = CraftingStage.PREPARE.stageTicks / 4;
    private static final int MAX_ACTIVATE_PARTICLE_TICKS = CraftingStage.PREPARE.stageTicks;

    private static final String CRAFTING_TIME_KEY = "CraftingTime";
    private static final String CURRENT_INSTABILITY_KEY = "CurrentInstability";
    private static final String CRAFTING_FAILED_KEY = "CraftingFailed";
    private static final String CRAFTING_STAGE_KEY = "CraftingStage";
    private static final String RECIPE_KEY = "Recipe";

    public InfusionPedestalBlockEntity(BlockPos pos, BlockState state) {
        super(ModBlockEntityTypeObjects.INFUSION_PEDESTAL, pos, state);
        this.instabilityLevel = 0;
        this.hasCraftingFailed = false;
        this.craftingStageTicks = 0;
        this.currentCraftingStage = InfusionPedestalBlockEntity.CraftingStage.INACTIVE;
        this.lastPlayer = Optional.empty();
        this.recipe = Optional.empty();
    }

    @Getter
    private InfusionPedestalBlockEntity.CraftingStage currentCraftingStage;
    @Getter
    private int craftingStageTicks; // Time spent crafting for the current stage. Starts at CRAFTING_TICKS and goes to 0
    @Getter
    private int instabilityLevel;
    @Getter
    private boolean hasCraftingFailed;
    @Getter
    private Optional<InfusionPedestalRecipe.InfusionPedestalRecipeWithAttachedBlockEntities> recipe;

    public Optional<ServerPlayer> lastPlayer; // The last player to interact with this altar

    public static void clientTick(Level level, BlockPos pos, BlockState state, InfusionPedestalBlockEntity blockEntity) {
        blockEntity.updateCrafting();
    }

    public static void serverTick(Level level, BlockPos pos, BlockState state, InfusionPedestalBlockEntity blockEntity) {
        blockEntity.updateCrafting();
    }

    /**
     * Main crafting update function
     */
    private void updateCrafting() {
        if (this.isCrafting()) {
            this.increaseCraftingTick();

            if (!this.getLevel().isClientSide) {
                this.increaseInstability();
                this.sendUpdates();
            }
        } else {
            this.decreaseInstability();
        }
    }

    private void increaseInstability() {
        if (!recipe.isPresent() || !this.recipe.get().stillMatches(this)) {
            this.instabilityLevel += INSTABILITY_LEVEL_RATE;
        }
        if (this.instabilityLevel > MAX_INSTABILITY_LEVEL) {
            this.hasCraftingFailed = true;
        }
    }

    private void increaseCraftingTick() {
        this.craftingStageTicks += 1;

        if (this.getLevel().isClientSide) {
            return;
        }

        if (this.craftingStageTicks >= this.currentCraftingStage.stageTicks) {
            this.craftingStageTicks = 0;

            switch (this.currentCraftingStage) {
                case PREPARE:
                    this.currentCraftingStage = InfusionPedestalBlockEntity.CraftingStage.CRAFT;
                    this.setBlockAndUpdate(PedestalBlockState.CRAFTING);
                    break;
                case CRAFT:
                    this.currentCraftingStage = InfusionPedestalBlockEntity.CraftingStage.FINISH;
                    this.setBlockAndUpdate(PedestalBlockState.END);
                    break;
                case FINISH:
                    this.finishCraft();
                    break;
            }
        }
    }

    private void setBlockAndUpdate(PedestalBlockState state) {
        this.getLevel().setBlockAndUpdate(this.getBlockPos(), this.getLevel().getBlockState(this.getBlockPos()).setValue(InfusionPedestalBlock.PEDESTAL_STATE, state));
        this.recipe.ifPresent(recipe -> {
            recipe.getUsableSacrificePedestals().stream()
                    .map(pos -> this.getLevel().getBlockEntity(pos))
                    .filter(blockEntity -> blockEntity instanceof SacrificePedestalBlockEntity)
                    .forEach(blockEntity -> this.getLevel().setBlockAndUpdate(blockEntity.getBlockPos(), blockEntity.getBlockState().setValue(PedestalBlock.PEDESTAL_STATE, state)));
            recipe.getUsablePowerPedestals().stream()
                    .map(pos -> this.getLevel().getBlockEntity(pos))
                    .filter(blockEntity -> blockEntity instanceof PowerPedestalBlockEntity)
                    .forEach(blockEntity -> this.getLevel().setBlockAndUpdate(blockEntity.getBlockPos(), blockEntity.getBlockState().setValue(PedestalBlock.PEDESTAL_STATE, state)));
            recipe.getUsableBloodPools().stream()
                    .map(pos -> this.getLevel().getBlockEntity(pos))
                    .filter(blockEntity -> blockEntity instanceof AbstractBloodBasinBlockEntity)
                    .forEach(blockEntity -> this.getLevel().setBlockAndUpdate(blockEntity.getBlockPos(), blockEntity.getBlockState().setValue(AbstractBloodBasinBlock.BLOOD_BASIN_STATE, state.toBloodBasinState())));
            recipe.getUsableCuauhxicalli().stream()
                    .map(pos -> this.getLevel().getBlockEntity(pos))
                    .filter(blockEntity -> blockEntity instanceof CuauhxicalliBlockEntity)
                    .forEach(blockEntity -> this.getLevel().setBlockAndUpdate(blockEntity.getBlockPos(), blockEntity.getBlockState().setValue(CuauhxicalliBlock.CUAUHXICALLI_STATE, CuauhxicalliState.ACTIVE)));
        });
    }

    private void finishCraft() {
        if (!this.getLevel().isClientSide) {
            if (recipe.isPresent()) {
                this.createOutput();
                this.consumeInputs();
            } else {
                MesoamericaMythologyMod.LOGGER.debug("Finished pedestal crafting. No recipe to complete");
            }

            this.hasCraftingFailed = false;
            this.craftingStageTicks = 0;
            this.heldItem = ItemStack.EMPTY;
            this.lastPlayer = Optional.empty();
            this.currentCraftingStage = InfusionPedestalBlockEntity.CraftingStage.INACTIVE;
            this.setBlockAndUpdate(PedestalBlockState.INACTIVE);

            this.sendUpdates();
        }
    }

    private void consumeInputs() {
        InfusionPedestalRecipe.InfusionPedestalRecipeWithAttachedBlockEntities recipe1 = this.recipe.get();
        recipe1.getUsableSacrificePedestals().stream()
                .map(pos -> this.getLevel().getBlockEntity(pos))
                .filter(blockEntity -> blockEntity instanceof SacrificePedestalBlockEntity)
                .forEach(blockEntity -> ((SacrificePedestalBlockEntity) blockEntity).reset());
        recipe1.getUsablePowerPedestals().stream()
                .map(pos -> this.getLevel().getBlockEntity(pos))
                .filter(blockEntity -> blockEntity instanceof PowerPedestalBlockEntity)
                .forEach(blockEntity -> ((PowerPedestalBlockEntity) blockEntity).reset());
        recipe1.getUsableBloodPools().stream()
                .map(pos -> this.getLevel().getBlockEntity(pos))
                .filter(blockEntity -> blockEntity instanceof AbstractBloodBasinBlockEntity)
                .forEach(blockEntity -> ((AbstractBloodBasinBlockEntity) blockEntity).reset());
        recipe1.getUsableCuauhxicalli().stream()
                .map(pos -> this.getLevel().getBlockEntity(pos))
                .filter(blockEntity -> blockEntity instanceof CuauhxicalliBlockEntity)
                .forEach(blockEntity -> ((CuauhxicalliBlockEntity) blockEntity).reset());
    }

    private void createOutput() {
        if (recipe.get().stillMatches(this) && !this.hasCraftingFailed) {
            MesoamericaMythologyMod.LOGGER.debug("Finished pedestal crafting. Creating output of " + recipe.get().getOutput().toString());
            this.recipe.get().getOutput().createOutput(this, this.lastPlayer.get());
            ModAdvancementTriggers.ANY_PEDESTAL_RECIPE_COMPLETED_TRIGGER.trigger(this.lastPlayer.get());
        } else {
            MesoamericaMythologyMod.LOGGER.debug("Finished pedestal crafting. No match = no item");
        }
    }


    public void cancelCrafting() {
        if (!this.getLevel().isClientSide && this.isCrafting() && this.canInteract()) {
            this.lastInteractionTime = ZonedDateTime.now();

            this.craftingStageTicks = 0;
            this.currentCraftingStage= CraftingStage.INACTIVE;
            this.setBlockAndUpdate(PedestalBlockState.INACTIVE);
        }
    }

    private void decreaseInstability() {
        this.instabilityLevel = Math.max(0, this.instabilityLevel - 1);
    }

    /**
     * Attempts to start crafting. Only works on Infusion Pedestals
     */
    public void craft(ServerPlayer playerEntity) {
        if (!this.getLevel().isClientSide && this.canCraft() && this.canInteract()) {
            this.lastInteractionTime = ZonedDateTime.now();

            MesoamericaMythologyMod.LOGGER.debug("Starting pedestal craft.");
            this.craftingStageTicks = 0;
            this.currentCraftingStage = InfusionPedestalBlockEntity.CraftingStage.PREPARE;
            this.hasCraftingFailed = false;
            this.lastPlayer = Optional.of(playerEntity);
            this.recipe = InfusionPedestalRecipe.getRecipe(this);
            this.setBlockAndUpdate(PedestalBlockState.PREPARING);
            this.setupRecipeBlocks();
            this.sendUpdates();
        }
    }

    private void setupRecipeBlocks() {
        this.recipe.ifPresent(recipe -> {
            long gameTicks = this.getLevel().getGameTime();
            Random random = new Random();
            recipe.getUsableSacrificePedestals().forEach(pos -> {
                BlockEntity blockEntity = this.getLevel().getBlockEntity(pos);
                if (blockEntity instanceof SacrificePedestalBlockEntity) {
                    ((SacrificePedestalBlockEntity) blockEntity).setActiveInfusionPedestal(MathUtil.getInRandom(random, MIN_ACTIVATE_PARTICLE_TICKS, MAX_ACTIVATE_PARTICLE_TICKS), gameTicks, this.getBlockPos());
                }
            });
            recipe.getUsablePowerPedestals().forEach(pos -> {
                BlockEntity blockEntity = this.getLevel().getBlockEntity(pos);
                if (blockEntity instanceof PowerPedestalBlockEntity) {
                    ((PowerPedestalBlockEntity) blockEntity).setActiveInfusionPedestal(MathUtil.getInRandom(random, MIN_ACTIVATE_PARTICLE_TICKS, MAX_ACTIVATE_PARTICLE_TICKS), gameTicks, this.getBlockPos());
                }
            });
            recipe.getUsableBloodPools().forEach(pos -> {
                BlockEntity blockEntity = this.getLevel().getBlockEntity(pos);
                if (blockEntity instanceof AbstractBloodBasinBlockEntity) {
                    ((AbstractBloodBasinBlockEntity) blockEntity).setActiveInfusionPedestal(this.getBlockPos());
                }
            });
            recipe.getUsableCuauhxicalli().forEach(pos -> {
                BlockEntity blockEntity = this.getLevel().getBlockEntity(pos);
                if (blockEntity instanceof CuauhxicalliBlockEntity) {
                    ((CuauhxicalliBlockEntity) blockEntity).setActiveInfusionPedestal(this.getBlockPos());
                }
            });
        });
    }

    private boolean canCraft() {
        return !this.isCrafting() && !this.heldItem.isEmpty();
    }

    public boolean isCrafting() {
        return this.currentCraftingStage != InfusionPedestalBlockEntity.CraftingStage.INACTIVE;
    }

    public int getCraftingTick() {
        return this.currentCraftingStage.totalStageTicks + this.craftingStageTicks;
    }

    @Override
    public void load(CompoundTag compound) {
        super.load(compound);

        this.craftingStageTicks = compound.getInt(CRAFTING_TIME_KEY);
        this.currentCraftingStage = InfusionPedestalBlockEntity.CraftingStage.valueOf(compound.getString(CRAFTING_STAGE_KEY));
        this.instabilityLevel = compound.getInt(CURRENT_INSTABILITY_KEY);
        this.hasCraftingFailed = compound.getBoolean(CRAFTING_FAILED_KEY);
        if (compound.contains(RECIPE_KEY)) {
            this.recipe = Optional.of(GSON.fromJson(compound.getString(RECIPE_KEY), InfusionPedestalRecipe.InfusionPedestalRecipeWithAttachedBlockEntities.class));
        } else {
            this.recipe = Optional.empty();
        }
    }

    @Override
    public void saveAdditional(CompoundTag compound) {
        super.saveAdditional(compound);

        compound.putInt(CRAFTING_TIME_KEY, this.craftingStageTicks);
        compound.putString(CRAFTING_STAGE_KEY, this.currentCraftingStage.name());
        compound.putInt(CURRENT_INSTABILITY_KEY, this.instabilityLevel);
        compound.putBoolean(CRAFTING_FAILED_KEY, this.hasCraftingFailed);
        this.recipe.ifPresent(recipe -> compound.putString(RECIPE_KEY, GSON.toJson(recipe)));
    }

}
