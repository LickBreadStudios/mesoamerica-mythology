package com.lickbread.mesoamericamythology.blockEntities;

import com.lickbread.mesoamericamythology.init.ModBlockEntityTypeObjects;
import com.lickbread.mesoamericamythology.util.SerializerUtil;
import lombok.Getter;
import net.minecraft.core.BlockPos;
import net.minecraft.core.NonNullList;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.network.protocol.game.ClientboundBlockEntityDataPacket;
import net.minecraft.world.entity.item.ItemEntity;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.level.block.entity.BlockEntityType;
import net.minecraft.world.level.block.state.BlockState;

import java.time.ZonedDateTime;
import java.util.Optional;

public class PedestalBlockEntity extends BlockEntity {

    protected static final String ITEM_KEY = "Item";
    protected static final String ACTIVATE_PARTICLE_TICKS = "ActivateParticleTicks";
    protected static final String LAST_STATE_CHANGE_TICKS = "LastStateChangeTicks";
    protected static final String ACTIVE_INFUSION_PEDESTAL = "ActiveInfusionPedestal";

    protected static final int MIN_TIME_BETWEEN_INTERACTIONS = 100; // Millis

    @Getter
    protected int activateParticleTicks;
    @Getter
    protected long lastStateChangeTicks;
    @Getter
    protected Optional<BlockPos> activeInfusionPedestal;

    public PedestalBlockEntity(BlockEntityType<?> type, BlockPos pos, BlockState state) {
        super(type, pos, state);
        this.lastInteractionTime = ZonedDateTime.now();
        this.heldItem = ItemStack.EMPTY;
        this.activateParticleTicks = 0;
        this.lastStateChangeTicks = 0;
        this.activeInfusionPedestal = Optional.empty();
    }

    public PedestalBlockEntity(BlockPos pos, BlockState state) {
        super(ModBlockEntityTypeObjects.PEDESTAL, pos, state);
        this.lastInteractionTime = ZonedDateTime.now();
        this.heldItem = ItemStack.EMPTY;
        this.activateParticleTicks = 0;
        this.lastStateChangeTicks = 0;
        this.activeInfusionPedestal = Optional.empty();
    }

    protected ZonedDateTime lastInteractionTime;

    @Getter
    protected ItemStack heldItem;

    protected void reset() {
        this.heldItem = ItemStack.EMPTY;
        this.activeInfusionPedestal = Optional.empty();
        this.activateParticleTicks = 0;
        this.lastStateChangeTicks = 0;
        this.setChanged();
    }

    protected void setActiveInfusionPedestal(int activateParticleTicks, long lastStateChangeTicks, BlockPos activeInfusionPedestal) {
        this.activateParticleTicks = activateParticleTicks;
        this.lastStateChangeTicks = lastStateChangeTicks;
        this.activeInfusionPedestal = Optional.ofNullable(activeInfusionPedestal);
        this.setChanged();
    }

    /**
     * Adds an item to the pedestal
     */
    public boolean addItem(ItemStack item) {
        if (!this.getLevel().isClientSide && !item.isEmpty() && this.heldItem.isEmpty() && this.canInteract()) {
            this.lastInteractionTime = ZonedDateTime.now();

            this.heldItem = item;
            this.sendUpdates();
            return true;
        }

        return false;
    }

    public void removeLastItem() {
        if (!this.heldItem.isEmpty() && this.canInteract()) {
            this.lastInteractionTime = ZonedDateTime.now();

            ItemEntity entity = new ItemEntity(this.getLevel(), this.getBlockPos().getX() + 0.5D, (double) this.getBlockPos().getY() + 1.1D, (double) this.getBlockPos().getZ() + 0.5D, this.heldItem);
            this.getLevel().addFreshEntity(entity);
            this.heldItem = ItemStack.EMPTY;
            this.sendUpdates();
        }
    }

    protected boolean canInteract() {
        return ZonedDateTime.now().toInstant().toEpochMilli() - this.lastInteractionTime.toInstant().toEpochMilli() >= MIN_TIME_BETWEEN_INTERACTIONS;
    }

    public NonNullList<ItemStack> getInputs() {
        return NonNullList.of(this.heldItem);
    }

    protected void sendUpdates() {
        this.setChanged();
        BlockState state = this.getLevel().getBlockState(this.getBlockPos());
        this.getLevel().sendBlockUpdated(this.getBlockPos(), state, state, Block.UPDATE_ALL_IMMEDIATE);
    }

    @Override
    public void load(CompoundTag compound) {
        super.load(compound);

        this.heldItem = ItemStack.of(compound.getCompound(ITEM_KEY));
        this.lastStateChangeTicks = compound.getLong(LAST_STATE_CHANGE_TICKS);
        this.activateParticleTicks = compound.getInt(ACTIVATE_PARTICLE_TICKS);
        if (compound.contains(ACTIVE_INFUSION_PEDESTAL)) {
            this.activeInfusionPedestal = Optional.of(SerializerUtil.deserializeBlockPos(compound.getCompound(ACTIVE_INFUSION_PEDESTAL)));
        }
    }

    @Override
    public void saveAdditional(CompoundTag compound) {
        super.saveAdditional(compound);

        compound.put(ITEM_KEY, this.heldItem.serializeNBT());
        compound.putInt(ACTIVATE_PARTICLE_TICKS, this.activateParticleTicks);
        compound.putLong(LAST_STATE_CHANGE_TICKS, this.lastStateChangeTicks);
        this.activeInfusionPedestal.ifPresent(blockPos -> compound.put(ACTIVE_INFUSION_PEDESTAL, SerializerUtil.serialize(blockPos)));
    }

    // Prepare a packet for syncing our TE to the client. Since we only have to sync the stack
    // and that's all we have we just write our entire NBT here. If you have a complex
    // tile entity that doesn't need to have all information on the client you can write
    // a more optimal NBT here.
    @Override
    public ClientboundBlockEntityDataPacket getUpdatePacket() {
        return ClientboundBlockEntityDataPacket.create(this);
    }

    // getUpdateTag() is called whenever the chunkdata is sent to the
    // client. In contrast getUpdatePacket() is called when the tile entity
    // itself wants to sync to the client. In many cases you want to send
    // over the same information in getUpdateTag() as in getUpdatePacket().
    @Override
    public CompoundTag getUpdateTag() {
        CompoundTag tag = super.getUpdateTag();
        this.saveAdditional(tag);
        return tag;
    }
}
