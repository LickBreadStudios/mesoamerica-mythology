package com.lickbread.mesoamericamythology.blockEntities;

import com.lickbread.mesoamericamythology.init.ModBlockEntityTypeObjects;
import net.minecraft.core.BlockPos;
import net.minecraft.world.level.block.state.BlockState;

public class PowerPedestalBlockEntity extends PedestalBlockEntity {

    public PowerPedestalBlockEntity(BlockPos pos, BlockState state) {
        super(ModBlockEntityTypeObjects.POWER_PEDESTAL, pos, state);
    }

}
