package com.lickbread.mesoamericamythology.blockEntities;

import com.lickbread.mesoamericamythology.init.ModBlockEntityTypeObjects;
import net.minecraft.core.BlockPos;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.world.entity.EntityType;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.level.block.state.BlockState;

import java.time.ZonedDateTime;
import java.util.Optional;

public class SacrificePedestalBlockEntity extends PedestalBlockEntity {

    protected static final String ENTITY_KEY = "Entity";

    public SacrificePedestalBlockEntity(BlockPos pos, BlockState state) {
        super(ModBlockEntityTypeObjects.SACRIFICE_PEDESTAL, pos, state);
        this.heldEntityNbt = Optional.empty();
    }

    private Optional<CompoundTag> heldEntityNbt;

    public boolean addEntity(Optional<LivingEntity> heldEntity) {
        if (!this.getLevel().isClientSide && !this.heldEntityNbt.isPresent() && heldEntity.isPresent() && this.canInteract()) {
            this.lastInteractionTime = ZonedDateTime.now();

            CompoundTag entityNbt = new CompoundTag();
            heldEntity.get().save(entityNbt);
            this.heldEntityNbt = Optional.of(entityNbt);
            this.sendUpdates();
            return true;
        }

        return false;
    }

    public Optional<LivingEntity> removeEntity() {
        if (this.heldEntityNbt.isPresent() && this.canInteract()) {
            this.lastInteractionTime = ZonedDateTime.now();

            Optional<LivingEntity> toRemove = this.getEntity();
            this.heldEntityNbt = Optional.empty();
            this.sendUpdates();
            return toRemove;
        }

        return Optional.empty();
    }

    public boolean hasEntity() {
        return heldEntityNbt.isPresent();
    }

    public Optional<LivingEntity> getEntity() {
        return heldEntityNbt.flatMap(nbt -> Optional.ofNullable((LivingEntity) EntityType.create(nbt, this.getLevel()).orElse(null)));
    }

    @Override
    protected void reset() {
        this.heldEntityNbt = Optional.empty();
        super.reset();
    }

    @Override
    public void load(CompoundTag compound) {
        super.load(compound);

        this.heldEntityNbt = compound.contains(ENTITY_KEY) ? Optional.of(compound.getCompound(ENTITY_KEY)) : Optional.empty();
    }

    @Override
    public void saveAdditional(CompoundTag compound) {
        super.saveAdditional(compound);

        this.heldEntityNbt.ifPresent(entityNbt -> compound.put(ENTITY_KEY, entityNbt));
    }
}
