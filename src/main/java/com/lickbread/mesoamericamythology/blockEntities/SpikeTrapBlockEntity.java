package com.lickbread.mesoamericamythology.blockEntities;

import com.lickbread.mesoamericamythology.blocks.traps.SpikeTrapBlock;
import com.lickbread.mesoamericamythology.init.ModBlockEntityTypeObjects;
import com.lickbread.mesoamericamythology.registry.ModDamageSources;
import com.lickbread.mesoamericamythology.util.MathUtil;
import lombok.Getter;
import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.phys.AABB;

import java.time.ZonedDateTime;
import java.util.List;

import static com.lickbread.mesoamericamythology.blocks.traps.FireTrapBlock.FACING;

public class SpikeTrapBlockEntity extends BlockEntity {

    private static final int TICKS_TO_DAMAGE = 5;
    private static final double SPIKE_LENGTH = 1.0D;
    private static final double SPIKE_WIDTH = 0.9D;

    private int currentTick;
    @Getter
    private ZonedDateTime lastActivatedTime;
    @Getter
    private ZonedDateTime lastInactivatedTime;
    @Getter
    private boolean setLastActivatedTime;
    @Getter
    private boolean setLastInactivatedTime;

    public SpikeTrapBlockEntity(BlockPos pos, BlockState state) {
        super(ModBlockEntityTypeObjects.SPIKE_TRAP, pos, state);
        this.currentTick = 0;
        this.lastActivatedTime = ZonedDateTime.now().minusSeconds(10);
        this.lastInactivatedTime = ZonedDateTime.now().minusSeconds(10);
        this.setLastActivatedTime = false;
        this.setLastInactivatedTime = false;
    }


    public static void clientTick(Level level, BlockPos pos, BlockState state, SpikeTrapBlockEntity blockEntity) {
        if (state.getValue(SpikeTrapBlock.ACTIVATED)) {
            if (!blockEntity.setLastActivatedTime) {
                blockEntity.lastActivatedTime = ZonedDateTime.now();
                blockEntity.setLastActivatedTime = true;
                blockEntity.setLastInactivatedTime = false;
            }
        } else if (blockEntity.setLastActivatedTime) {
            blockEntity.setLastActivatedTime = false;
            blockEntity.setLastInactivatedTime = true;
            blockEntity.lastInactivatedTime = ZonedDateTime.now();
        }
    }

    public static void serverTick(Level level, BlockPos pos, BlockState state, SpikeTrapBlockEntity blockEntity) {
        if (state.getValue(SpikeTrapBlock.ACTIVATED)) {
            if (!blockEntity.setLastActivatedTime) {
                blockEntity.lastActivatedTime = ZonedDateTime.now();
                blockEntity.setLastActivatedTime = true;
                blockEntity.setLastInactivatedTime = false;
            }

            blockEntity.currentTick++;
            if (blockEntity.currentTick > TICKS_TO_DAMAGE) {
                blockEntity.damageTick(state, (ServerLevel) level, pos);
                blockEntity.currentTick = 0;
            }
        } else if (blockEntity.setLastActivatedTime) {
            blockEntity.setLastActivatedTime = false;
            blockEntity.setLastInactivatedTime = true;
            blockEntity.lastInactivatedTime = ZonedDateTime.now();
        }
    }

    public void damageTick(BlockState state, ServerLevel worldIn, BlockPos pos) {
        Direction dir = state.getValue(FACING);
        AABB damageBounds = MathUtil.getDamageBox(pos, dir, SPIKE_WIDTH, SPIKE_LENGTH);
        List<LivingEntity> entities = worldIn.getEntitiesOfClass(LivingEntity.class, damageBounds);

        for (LivingEntity entity : entities) {
            entity.hurt(ModDamageSources.source(worldIn, ModDamageSources.SPIKE_TRAP), 6.0F);
        }
    }

    @Override
    public AABB getRenderBoundingBox() {
        Direction dir = this.getBlockState().getValue(SpikeTrapBlock.FACING);
        if (dir.getAxisDirection() == Direction.AxisDirection.POSITIVE) {
            return new AABB(this.getBlockPos().getX(), this.getBlockPos().getY(), this.getBlockPos().getZ(),
                    this.getBlockPos().getX() + 1.0D + dir.step().x() * SPIKE_LENGTH, this.getBlockPos().getY() + 1.0D + dir.step().y() * SPIKE_LENGTH, this.getBlockPos().getZ() + 1.0D + dir.step().z() * SPIKE_LENGTH);
        } else {
            return new AABB(this.getBlockPos().getX() + 1.0D, this.getBlockPos().getY() + 1.0D, this.getBlockPos().getZ() + 1.0D,
                    this.getBlockPos().getX() + dir.step().x() * SPIKE_LENGTH, this.getBlockPos().getY() + dir.step().y() * SPIKE_LENGTH, this.getBlockPos().getZ() + dir.step().z() * SPIKE_LENGTH);
        }
    }
}
