package com.lickbread.mesoamericamythology.blockEntities;

import com.lickbread.mesoamericamythology.MesoamericaMythologyMod;
import com.lickbread.mesoamericamythology.api.recipes.altar.StoneAltarRecipe;
import com.lickbread.mesoamericamythology.blocks.altar.StoneAltarBlock;
import com.lickbread.mesoamericamythology.blocks.state.AltarBlockState;
import com.lickbread.mesoamericamythology.constants.BloodLimits;
import com.lickbread.mesoamericamythology.init.ModBlockEntityTypeObjects;
import com.lickbread.mesoamericamythology.registry.ModAdvancementTriggers;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import net.minecraft.core.BlockPos;
import net.minecraft.core.NonNullList;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.nbt.ListTag;
import net.minecraft.server.level.ServerPlayer;
import net.minecraft.world.entity.EntityType;
import net.minecraft.world.entity.LightningBolt;
import net.minecraft.world.entity.item.ItemEntity;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.phys.AABB;
import net.minecraft.world.phys.Vec3;

import java.time.ZonedDateTime;
import java.util.Arrays;
import java.util.Optional;
import java.util.Random;

import static com.lickbread.mesoamericamythology.api.MesoamericaMythologyAPI.GSON;

public class StoneAltarBlockEntity extends AbstractBloodBasinBlockEntity {

    /**
     * Enum representing the progress through crafting
     *
     * 275 ticks = 13.75 seconds to craft
     */
    @Getter
    @AllArgsConstructor
    public enum CraftingStage {
        INACTIVE(0, 0),
        PREPARE(75, 0),
        CRAFT(175, CraftingStage.PREPARE.getStageTicks()),
        FINISH(25, CraftingStage.PREPARE.getStageTicks() + CraftingStage.CRAFT.getStageTicks());

        private final int stageTicks;
        private final int totalStageTicks;

        public static int totalTicks() {
            return Arrays.stream(CraftingStage.values()).mapToInt(CraftingStage::getStageTicks).sum();
        }
    }

    private static final int MIN_MILLIS_BETWEEN_INTERACTIONS = 100; // Millis

    public static final int MAX_INVENTORY_SIZE = 8;


    public static final double BLOOD_SPHERE_HEIGHT = 0.75D;

    private static final int MAX_INSTABILITY_LEVEL = 60; // 3 seconds
    private static final int INSTABILITY_LEVEL_RATE = 1;

    private static final double LIGHTNING_CHANCE_ON_SUCCESS = 0.3D;
    private static final double LIGHTNING_CHANCE_ON_FAILURE = 0.5D;
    private static final double MAX_LIGHTNING_RANGE = 8D;

    private static final String INPUTS_KEY = "Inputs";
    private static final String ITEM_KEY = "Item";
    private static final String CRAFTING_TIME_KEY = "CraftingTime";
    private static final String CURRENT_INPUT_INDEX_KEY = "CurrentInputIndex";
    private static final String CURRENT_INSTABILITY_KEY = "CurrentInstability";
    private static final String CRAFTING_FAILED_KEY = "CraftingFailed";
    private static final String CRAFTING_STAGE_KEY = "CraftingStage";
    private static final String RECIPE_KEY = "Recipe";

    public StoneAltarBlockEntity(BlockPos pos, BlockState state) {
        super(ModBlockEntityTypeObjects.STONE_ALTAR, pos, state);
        this.lastInteractionTime = ZonedDateTime.now();
        this.inputs = NonNullList.withSize(MAX_INVENTORY_SIZE, ItemStack.EMPTY);
        this.instabilityLevel = 0;
        this.hasCraftingFailed = false;
        this.craftingStageTicks = 0;
        this.inputIndex = 0;
        this.currentCraftingStage = CraftingStage.INACTIVE;
        this.currentBloodPoolRotation = Vec3.ZERO;
        this.lastPlayer = Optional.empty();
        this.recipe = Optional.empty();
    }

    @Getter
    private final NonNullList<ItemStack> inputs;
    @Getter
    private int inputIndex; // The current inventory slot that can be added to

    private ZonedDateTime lastInteractionTime;

    @Getter
    private CraftingStage currentCraftingStage;
    private int craftingStageTicks; // Time spent crafting for the current stage. Starts at CRAFTING_TICKS and goes to 0
    @Getter
    private int instabilityLevel;
    @Getter
    private boolean hasCraftingFailed;
    private Optional<StoneAltarRecipe> recipe;

    @Getter
    @Setter
    public Vec3 currentBloodPoolRotation; // Current angle of blood pool

    public Optional<ServerPlayer> lastPlayer; // The last player to interact with this altar

    @Override
    public boolean canAttach() {
        return false;
    }

    @Override
    public float getMaxBlood() {
        return BloodLimits.MAX_BLOOD_STONE_ALTAR;
    }

    @Override
    public float getBloodTransferRate() {
        return BloodLimits.BLOOD_TRANSFER_RATE_STONE_ALTAR;
    }

    @Override
    public Vec3 getRandomBloodParticlePos() {
        throw new UnsupportedOperationException("You shouldn't be making blood particles on altars");
    }


    public static void clientTick(Level level, BlockPos pos, BlockState state, StoneAltarBlockEntity blockEntity) {
        AbstractBloodBasinBlockEntity.clientTick(level, pos, state, blockEntity);
        if (blockEntity.isCrafting()) {
            blockEntity.increaseCraftingTick();
        }
    }

    public static void serverTick(Level level, BlockPos pos, BlockState state, StoneAltarBlockEntity blockEntity) {
        AbstractBloodBasinBlockEntity.serverTick(level, pos, state, blockEntity);
        if (blockEntity.isCrafting()) {
            blockEntity.increaseCraftingTick();
            blockEntity.increaseInstability();
//            this.consumeBlood();
            blockEntity.summonLightning();
            blockEntity.sendUpdates();
        } else {
            blockEntity.decreaseInstability();
        }
    }

    private void consumeBlood() {
        if (this.currentCraftingStage != CraftingStage.FINISH) {
            return;
        }

        float craftingProgress = (float) Math.pow(this.craftingStageTicks / (double) CraftingStage.FINISH.getStageTicks(), 2D);
        this.bloodPoolAmount = this.bloodPoolAmount * craftingProgress;
    }

    private void summonLightning() {
        if (this.currentCraftingStage != CraftingStage.FINISH) {
            return;
        }

        Random random = new Random();
        Vec3 lightningPos = new Vec3(this.getBlockPos().getX(), this.getBlockPos().getY(), this.getBlockPos().getZ());
        boolean effectOnly = true;
        double lightningSummonChance = LIGHTNING_CHANCE_ON_SUCCESS;

        if (this.hasCraftingFailed) {
            float craftingProgress = (float) Math.pow(this.craftingStageTicks / (double) CraftingStage.FINISH.getStageTicks(), 2D);
            double theta = random.nextDouble() * 360.0f;
            double radius = random.nextDouble() * (1D - craftingProgress) * MAX_LIGHTNING_RANGE;
            lightningPos = lightningPos.add(Math.cos(theta) * radius, 0D,Math.sin(theta) * radius);
            effectOnly = false;
            lightningSummonChance = LIGHTNING_CHANCE_ON_FAILURE;
        }

        if (random.nextDouble() < lightningSummonChance) {
            LightningBolt lightningBoltEntity = EntityType.LIGHTNING_BOLT.create(this.getLevel());
            lightningBoltEntity.moveTo(lightningPos);
            lightningBoltEntity.setVisualOnly(effectOnly);
            this.getLevel().addFreshEntity(lightningBoltEntity);
        }
    }

    private void increaseInstability() {
        if (!recipe.isPresent() || !this.recipe.get().stillMatches(this)) {
            this.instabilityLevel += INSTABILITY_LEVEL_RATE;
        }
        if (this.instabilityLevel > MAX_INSTABILITY_LEVEL) {
            this.hasCraftingFailed = true;
        }
    }

    private void increaseCraftingTick() {
        this.craftingStageTicks += 1;

        if (this.getLevel().isClientSide) {
            return;
        }

        if (this.craftingStageTicks >= this.currentCraftingStage.stageTicks) {
            this.craftingStageTicks = 0;

            switch (this.currentCraftingStage) {
                case PREPARE:
                    this.currentCraftingStage = CraftingStage.CRAFT;
                    this.getLevel().setBlockAndUpdate(this.getBlockPos(), this.getLevel().getBlockState(this.getBlockPos()).setValue(StoneAltarBlock.ALTAR_STATE, AltarBlockState.CRAFTING));
                    break;
                case CRAFT:
                    this.currentCraftingStage = CraftingStage.FINISH;
                    this.getLevel().setBlockAndUpdate(this.getBlockPos(), this.getLevel().getBlockState(this.getBlockPos()).setValue(StoneAltarBlock.ALTAR_STATE, AltarBlockState.END));
                    break;
                case FINISH:
                    this.finishCraft();
                    break;
            }
        }
    }

    private void decreaseInstability() {
        this.instabilityLevel = Math.max(0, this.instabilityLevel - 1);
    }

    public void craft(ServerPlayer playerEntity) {
        if (!this.getLevel().isClientSide && this.canCraft()) {
            if (ZonedDateTime.now().toInstant().toEpochMilli() - this.lastInteractionTime.toInstant().toEpochMilli() < MIN_MILLIS_BETWEEN_INTERACTIONS) {
                return;
            }
            this.lastInteractionTime = ZonedDateTime.now();

            MesoamericaMythologyMod.LOGGER.debug("Starting altar craft. Current blood is " + this.bloodPoolAmount);
            this.craftingStageTicks = 0;
            this.currentCraftingStage = CraftingStage.PREPARE;
            this.hasCraftingFailed = false;
            this.lastPlayer = Optional.of(playerEntity);
            this.getLevel().setBlockAndUpdate(this.getBlockPos(), this.getLevel().getBlockState(this.getBlockPos()).setValue(StoneAltarBlock.ALTAR_STATE, AltarBlockState.BLOOD));
            this.recipe = StoneAltarRecipe.getRecipe(this, true);
            this.sendUpdates();
        }
    }

    private boolean canCraft() {
        return !this.isCrafting() && this.bloodPoolAmount > 0.0F && this.inputIndex > 0;
    }

    private void finishCraft() {
        if (!this.getLevel().isClientSide) {
            if (recipe.isPresent() && recipe.get().stillMatches(this) && !this.hasCraftingFailed) {
                MesoamericaMythologyMod.LOGGER.debug("Finished altar crafting. Creating output of " + recipe.get().getOutput().toString());
                recipe.get().getOutput().createOutput(this, this.lastPlayer.get());
                ModAdvancementTriggers.ANY_ALTAR_RECIPE_COMPLETED_TRIGGER.trigger(this.lastPlayer.get());
            } else {
                MesoamericaMythologyMod.LOGGER.debug("Finished altar crafting. No recipe to complete");
            }

            this.hasCraftingFailed = false;
            this.craftingStageTicks = 0;
            this.inputIndex = 0;
            this.bloodPoolAmount = 0;
            this.inputs.clear();
            this.bloodPoolBuffer.clear();
            this.lastPlayer = Optional.empty();
            this.currentCraftingStage = CraftingStage.INACTIVE;
            this.getLevel().setBlockAndUpdate(this.getBlockPos(), this.getLevel().getBlockState(this.getBlockPos()).setValue(StoneAltarBlock.ALTAR_STATE, AltarBlockState.INACTIVE));

            this.sendUpdates();
        }
    }

    public boolean addItem(ItemStack item) {
        if (!this.getLevel().isClientSide && !item.isEmpty() && !this.isCrafting()) {
            if (this.inputIndex < MAX_INVENTORY_SIZE) {
                if (ZonedDateTime.now().toInstant().toEpochMilli() - this.lastInteractionTime.toInstant().toEpochMilli() < MIN_MILLIS_BETWEEN_INTERACTIONS) {
                    return false;
                }
                this.lastInteractionTime = ZonedDateTime.now();

                this.inputs.set(this.inputIndex, item);
                this.inputIndex++;

                this.sendUpdates();
                return true;
            }
        }

        return false;
    }

    public void removeItems() {
        if (!this.getLevel().isClientSide && !this.isCrafting()) {
            if (ZonedDateTime.now().toInstant().toEpochMilli() - this.lastInteractionTime.toInstant().toEpochMilli() < MIN_MILLIS_BETWEEN_INTERACTIONS) {
                return;
            }
            this.lastInteractionTime = ZonedDateTime.now();


            for (int i = this.inputIndex; i > 0; i--) {
                this.removeItem();
            }
            this.sendUpdates();
        }
    }

    public void cancelCrafting() {
        if (!this.getLevel().isClientSide && this.isCrafting()) {
            if (ZonedDateTime.now().toInstant().toEpochMilli() - this.lastInteractionTime.toInstant().toEpochMilli() < MIN_MILLIS_BETWEEN_INTERACTIONS) {
                return;
            }
            this.lastInteractionTime = ZonedDateTime.now();

            this.craftingStageTicks = 0;
            this.getLevel().setBlockAndUpdate(this.getBlockPos(), this.getLevel().getBlockState(this.getBlockPos()).setValue(StoneAltarBlock.ALTAR_STATE, AltarBlockState.INACTIVE));
            this.removeItems();
        }
    }

    // Private method to get around timing
    private void removeItem() {
        this.inputIndex--;
        ItemStack item = this.inputs.get(this.inputIndex);
        ItemEntity entity = new ItemEntity(this.getLevel(), this.getBlockPos().getX() + 0.5D, (double) this.getBlockPos().getY() + 1.1D, (double) this.getBlockPos().getZ() + 0.5D, item);
        this.getLevel().addFreshEntity(entity);
        this.inputs.set(this.inputIndex, ItemStack.EMPTY);
        this.sendUpdates();
    }

    // Public method when called by block
    public void removeLastItem() {
        if (this.inputIndex != 0) {
            if (ZonedDateTime.now().toInstant().toEpochMilli() - this.lastInteractionTime.toInstant().toEpochMilli() < MIN_MILLIS_BETWEEN_INTERACTIONS) {
                return;
            }
            this.lastInteractionTime = ZonedDateTime.now();

            this.removeItem();
        }
    }

    public boolean isCrafting() {
        return this.currentCraftingStage != CraftingStage.INACTIVE;
    }

    public int getCraftingTick() {
        return this.currentCraftingStage.totalStageTicks + this.craftingStageTicks;
    }

    @Override
    public void load(CompoundTag compound) {
        super.load(compound);

        ListTag inputTag = (ListTag) compound.get(INPUTS_KEY);
        this.inputs.clear();
        for (int i = 0; i < inputTag.size(); i++) {
            CompoundTag tag = inputTag.getCompound(i);
            this.inputs.set(i, ItemStack.of(tag.getCompound(ITEM_KEY)));
        }

        this.craftingStageTicks = compound.getInt(CRAFTING_TIME_KEY);
        this.currentCraftingStage = CraftingStage.valueOf(compound.getString(CRAFTING_STAGE_KEY));
        this.inputIndex = compound.getInt(CURRENT_INPUT_INDEX_KEY);
        this.instabilityLevel = compound.getInt(CURRENT_INSTABILITY_KEY);
        this.hasCraftingFailed = compound.getBoolean(CRAFTING_FAILED_KEY);
        if (compound.contains(RECIPE_KEY)) {
            this.recipe = Optional.of(GSON.fromJson(compound.getString(RECIPE_KEY), StoneAltarRecipe.class));
        } else {
            this.recipe = Optional.empty();
        }
    }

    @Override
    public void saveAdditional(CompoundTag compound) {
        super.saveAdditional(compound);

        ListTag inputsList = new ListTag();
        for (int i = 0; i < inputs.size(); i++) {
            CompoundTag stackTag = new CompoundTag();
            stackTag.put(ITEM_KEY, this.inputs.get(i).serializeNBT());
            inputsList.add(stackTag);
        }

        compound.put(INPUTS_KEY, inputsList);
        compound.putInt(CRAFTING_TIME_KEY, this.craftingStageTicks);
        compound.putString(CRAFTING_STAGE_KEY, this.currentCraftingStage.name());
        compound.putInt(CURRENT_INPUT_INDEX_KEY, this.inputIndex);
        compound.putInt(CURRENT_INSTABILITY_KEY, this.instabilityLevel);
        compound.putBoolean(CRAFTING_FAILED_KEY, this.hasCraftingFailed);
        this.recipe.ifPresent(stoneAltarRecipe -> compound.putString(RECIPE_KEY, GSON.toJson(stoneAltarRecipe)));
    }

    @Override
    public AABB getRenderBoundingBox() {
        return new AABB(this.getBlockPos().getX(), this.getBlockPos().getY(), this.getBlockPos().getZ(), this.getBlockPos().getX() + 1D, this.getBlockPos().getY() + 2D, this.getBlockPos().getZ() + 1D);
    }
}
