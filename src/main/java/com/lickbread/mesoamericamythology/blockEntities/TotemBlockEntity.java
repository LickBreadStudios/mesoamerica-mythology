package com.lickbread.mesoamericamythology.blockEntities;

import com.lickbread.mesoamericamythology.blocks.TotemType;
import com.lickbread.mesoamericamythology.init.ModBlockEntityTypeObjects;
import lombok.Getter;
import lombok.Setter;
import net.minecraft.core.BlockPos;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.network.protocol.game.ClientboundBlockEntityDataPacket;
import net.minecraft.world.effect.MobEffect;
import net.minecraft.world.effect.MobEffectInstance;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.phys.AABB;
import net.minecraft.world.phys.Vec3;

import java.util.List;

public class TotemBlockEntity extends BlockEntity {

    private static final String TOTEM_TYPE = "TotemType";

    private static final float TICK_INCREMENT = 1.0F;
    public static final float MAX_TICKS = 40.0F;
    public static final int MAX_RANGE = 7;
    public static final int DURATION = 60; // ticks

    @Getter
    private float currentTick = 0F;
    @Getter
    @Setter
    private TotemType totemType;

    public TotemBlockEntity(BlockPos pos, BlockState state) {
        super(ModBlockEntityTypeObjects.TOTEM, pos, state);
        this.totemType = TotemType.SPEED;
    }


    public static void serverTick(Level level, BlockPos pos, BlockState state, TotemBlockEntity blockEntity) {
        blockEntity.currentTick += TICK_INCREMENT;
        if (blockEntity.currentTick > MAX_TICKS) {
            blockEntity.currentTick = 0.0F;
            blockEntity.applyEffects();
        }
    }

    public void applyEffects() {
        MobEffect effect = this.totemType.getEffect();

        List<Player> players = this.getLevel().getEntitiesOfClass(Player.class, new AABB(this.getBlockPos().getX() - MAX_RANGE, this.getBlockPos().getY() - MAX_RANGE, this.getBlockPos().getZ() - MAX_RANGE, this.getBlockPos().getX() + MAX_RANGE, this.getBlockPos().getY() + MAX_RANGE, this.getBlockPos().getZ() + MAX_RANGE));

        players.stream().filter(player -> Math.sqrt(player.position().distanceTo(new Vec3(this.getBlockPos().getX(), this.getBlockPos().getY(), this.getBlockPos().getZ()))) <= MAX_RANGE)
                .forEach(player -> player.addEffect(new MobEffectInstance(effect, DURATION, 0)));
    }

    @Override
    public AABB getRenderBoundingBox() {
        return new AABB(this.getBlockPos().getX() - MAX_RANGE, this.getBlockPos().getY() - MAX_RANGE, this.getBlockPos().getZ() - MAX_RANGE, this.getBlockPos().getX() + MAX_RANGE, this.getBlockPos().getY() + MAX_RANGE, this.getBlockPos().getZ() + MAX_RANGE);
    }

    @Override
    public void load(CompoundTag compound) {
        super.load(compound);
        this.totemType = TotemType.getEnum(compound.getString(TOTEM_TYPE));
    }

    @Override
    public void saveAdditional(CompoundTag compound) {
        super.saveAdditional(compound);
        compound.putString(TOTEM_TYPE, this.totemType.toString());
    }

    // Prepare a packet for syncing our TE to the client. Since we only have to sync the stack
    // and that's all we have we just write our entire Tag here. If you have a complex
    // tile entity that doesn't need to have all information on the client you can write
    // a more optimal Tag here.
    @Override
    public ClientboundBlockEntityDataPacket getUpdatePacket() {
        return ClientboundBlockEntityDataPacket.create(this);
    }

    // getUpdateTag() is called whenever the chunkdata is sent to the
    // client. In contrast getUpdatePacket() is called when the tile entity
    // itself wants to sync to the client. In many cases you want to send
    // over the same information in getUpdateTag() as in getUpdatePacket().
    @Override
    public CompoundTag getUpdateTag() {
        CompoundTag tag = super.getUpdateTag();
        this.saveAdditional(tag);
        return tag;
    }
}
