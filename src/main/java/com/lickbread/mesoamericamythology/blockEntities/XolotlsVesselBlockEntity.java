package com.lickbread.mesoamericamythology.blockEntities;

import com.lickbread.mesoamericamythology.init.ModBlockEntityTypeObjects;
import net.minecraft.core.BlockPos;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.level.block.state.BlockState;

public class XolotlsVesselBlockEntity extends BlockEntity {

    public XolotlsVesselBlockEntity(BlockPos pos, BlockState state) {
        super(ModBlockEntityTypeObjects.XOLOTLS_VESSEL, pos, state);
    }
}
