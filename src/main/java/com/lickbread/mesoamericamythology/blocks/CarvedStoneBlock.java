package com.lickbread.mesoamericamythology.blocks;

import com.lickbread.mesoamericamythology.blocks.state.ActivatedEnumState;
import com.lickbread.mesoamericamythology.blockEntities.GodStatueHeadBlockEntity;
import net.minecraft.server.level.ServerPlayer;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.RenderShape;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.material.Material;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.context.BlockPlaceContext;
import net.minecraft.world.level.block.state.properties.DirectionProperty;
import net.minecraft.world.level.block.state.properties.EnumProperty;
import net.minecraft.world.level.block.state.StateDefinition;
import net.minecraft.world.level.block.state.properties.BlockStateProperties;
import net.minecraft.world.InteractionResult;
import net.minecraft.core.Direction;
import net.minecraft.world.InteractionHand;
import net.minecraft.core.BlockPos;
import net.minecraft.world.phys.BlockHitResult;
import net.minecraft.world.phys.shapes.CollisionContext;
import net.minecraft.world.phys.shapes.VoxelShape;
import net.minecraft.world.phys.shapes.Shapes;
import net.minecraft.world.level.BlockGetter;
import net.minecraft.world.level.Level;

import java.util.Optional;

public class CarvedStoneBlock extends Block {

    public static final EnumProperty<ActivatedEnumState> ACTIVATED = EnumProperty.create("activated", ActivatedEnumState.class);
    public static final DirectionProperty FACING = BlockStateProperties.FACING;

    public CarvedStoneBlock() {
        super(Block.Properties.of(Material.STONE).strength(2.0F, 8.0F).dynamicShape());
        this.registerDefaultState(this.getStateDefinition().any().setValue(ACTIVATED, ActivatedEnumState.INACTIVE).setValue(FACING, Direction.NORTH));
    }

    @Override
    public InteractionResult use(BlockState blockState, Level level, BlockPos blockPos, Player player, InteractionHand hand, BlockHitResult blockRayTraceResult) {
        if (level.isClientSide()) {
            return InteractionResult.PASS;
        }

        if (blockState.getValue(ACTIVATED) != ActivatedEnumState.ACTIVE) {
            return InteractionResult.PASS;
        }

        Optional<GodStatueHeadBlockEntity> master = this.getMaster(level, blockPos, blockState.getValue(FACING));
        if (master.isPresent()) {
            if (master.get().interact((ServerPlayer) player, hand)) {
                return InteractionResult.SUCCESS;
            }
        }
        return InteractionResult.PASS;
    }

    @Override
    public void onRemove(BlockState state, Level level, BlockPos blockPos, BlockState newState, boolean isMoving) {
        // TODO this causes issues on world unload
        if (state.getBlock() != newState.getBlock()) {
            Optional<GodStatueHeadBlockEntity> master = this.getMaster(level, blockPos, state.getValue(FACING));
            if (master.isPresent()) {
                level.destroyBlock(master.get().getBlockPos(), true);
            }
            super.onRemove(state, level, blockPos, newState, isMoving);
        }
    }

    @Override
    public RenderShape getRenderShape(BlockState state) {
        return state.getValue(ACTIVATED) == ActivatedEnumState.ACTIVE ? RenderShape.INVISIBLE : RenderShape.MODEL;
    }

    @Override
    public VoxelShape getShape(BlockState state, BlockGetter worldIn, BlockPos pos, CollisionContext context) {
        return Shapes.block(); //state.getValue(ACTIVATED) == ActivatedEnumState.ACTIVE ? VoxelShapes.empty() : VoxelShapes.block();
    }

    public BlockState getStateForPlacement(BlockPlaceContext context) {
        return this.defaultBlockState().setValue(ACTIVATED, ActivatedEnumState.INACTIVE).setValue(FACING, Direction.NORTH);
    }

    protected void createBlockStateDefinition(StateDefinition.Builder<Block, BlockState> builder) {
        builder.add(ACTIVATED, FACING);
    }

    public static void setMaster(Level level, BlockPos statueHeadPos, BlockPos carvedStonePos) {
        BlockState blockState = level.getBlockState(carvedStonePos);
        blockState = blockState.setValue(ACTIVATED, ActivatedEnumState.ACTIVE);

        if (statueHeadPos.getY() > carvedStonePos.getY()) {
            blockState = blockState.setValue(FACING, Direction.UP);
        } else if (statueHeadPos.getY() < carvedStonePos.getY()) {
            blockState = blockState.setValue(FACING, Direction.DOWN);
        } else if (statueHeadPos.getX() > carvedStonePos.getX()) {
            blockState = blockState.setValue(FACING, Direction.EAST);
        } else if (statueHeadPos.getX() < carvedStonePos.getX()) {
            blockState = blockState.setValue(FACING, Direction.WEST);
        } else if (statueHeadPos.getZ() > carvedStonePos.getZ()) {
            blockState = blockState.setValue(FACING, Direction.SOUTH);
        } else if (statueHeadPos.getZ() < carvedStonePos.getZ()) {
            blockState = blockState.setValue(FACING, Direction.NORTH);
        }

        level.setBlockAndUpdate(carvedStonePos, blockState);
    }

    private Optional<GodStatueHeadBlockEntity> getMaster(Level level, BlockPos blockPos, Direction direction) {
        Direction currentDirection = direction;
        BlockPos currentBlockPos = blockPos;
        int attempts = 0;
        while (attempts < GodStatueHeadBlockEntity.TOTAL_BLOCKS) {
            currentBlockPos = currentBlockPos.offset(currentDirection.getNormal());
            BlockState blockState = level.getBlockState(currentBlockPos);
            if (blockState.getBlock() instanceof CarvedStoneBlock && blockState.getValue(ACTIVATED) == ActivatedEnumState.ACTIVE) {
                currentDirection = blockState.getValue(FACING);
            } else if (blockState.getBlock() instanceof GodStatueHeadBlock) {
                return Optional.of((GodStatueHeadBlockEntity) level.getBlockEntity(currentBlockPos));
            }

            attempts++;
        }

        return Optional.empty();
    }
}
