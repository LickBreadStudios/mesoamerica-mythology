package com.lickbread.mesoamericamythology.blocks;

import com.lickbread.mesoamericamythology.blockEntities.GodStatueHeadBlockEntity;
import com.lickbread.mesoamericamythology.blocks.state.GodStatueHeadState;
import com.lickbread.mesoamericamythology.init.ModBlockEntityTypeObjects;
import com.lickbread.mesoamericamythology.statueHead.God;
import com.lickbread.mesoamericamythology.util.MinMaxBounds;
import com.lickbread.mesoamericamythology.util.PropertiesUtil;
import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.server.level.ServerPlayer;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.InteractionResult;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.context.BlockPlaceContext;
import net.minecraft.world.level.BlockGetter;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.*;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.block.state.StateDefinition;
import net.minecraft.world.level.block.state.properties.DirectionProperty;
import net.minecraft.world.level.block.state.properties.EnumProperty;
import net.minecraft.world.level.material.Material;
import net.minecraft.world.level.pathfinder.PathComputationType;
import net.minecraft.world.phys.BlockHitResult;
import net.minecraft.world.phys.shapes.CollisionContext;
import net.minecraft.world.phys.shapes.Shapes;
import net.minecraft.world.phys.shapes.VoxelShape;

import net.minecraft.world.level.block.state.BlockBehaviour.Properties;

public class GodStatueHeadBlock extends BaseEntityBlock {

    public static final DirectionProperty FACING = DirectionProperty.create("facing", Direction.NORTH, Direction.EAST, Direction.SOUTH, Direction.WEST);
    public static final EnumProperty<GodStatueHeadState> HEAD_STATE = EnumProperty.create("head_state", GodStatueHeadState.class);

    private final God god;

    public GodStatueHeadBlock(God god) {
        super(Properties
                .of(Material.STONE)
                .strength(2.0F, 8.0F)
                .dynamicShape()
                .isValidSpawn(PropertiesUtil::neverAllowSpawn));
        this.registerDefaultState(this.getStateDefinition().any().setValue(FACING, Direction.NORTH).setValue(HEAD_STATE, GodStatueHeadState.PASSIVE));
        this.god = god;
    }

    @Override
    public InteractionResult use(BlockState blockState, Level level, BlockPos blockPos, Player player, InteractionHand hand, BlockHitResult blockRayTraceResult) {
        if (level.isClientSide()) {
            return InteractionResult.PASS;
        }

        BlockEntity tile = level.getBlockEntity(blockPos);
        if (tile instanceof GodStatueHeadBlockEntity && !level.isClientSide()) {
            GodStatueHeadBlockEntity statueBlockEntity = (GodStatueHeadBlockEntity) tile;

            if (statueBlockEntity.interact((ServerPlayer) player, hand)) {
                return InteractionResult.SUCCESS;
            }
        }
        return InteractionResult.PASS;
    }

    @Override
    public BlockEntity newBlockEntity(BlockPos pos, BlockState state) {
        GodStatueHeadBlockEntity godStatueHeadBlockEntity = ModBlockEntityTypeObjects.GOD_STATUE.create(pos, state);
        godStatueHeadBlockEntity.setGod(this.god);
        return godStatueHeadBlockEntity;
    }

    @Override
    public boolean isPathfindable(BlockState state, BlockGetter worldIn, BlockPos pos, PathComputationType type) {
        return false;
    }

    @Override
    public RenderShape getRenderShape(BlockState state) {
        return RenderShape.MODEL; //state.getValue(MULTI_BLOCK) == MultiBlockState.TRUE ? BlockRenderType.ENTITYBLOCK_ANIMATED : BlockRenderType.MODEL;
    }

    @Override
    public VoxelShape getShape(BlockState state, BlockGetter worldIn, BlockPos pos, CollisionContext context) {
        return /*state.getValue(MULTI_BLOCK) == MultiBlockState.TRUE ? getVoxelShapeForDirection(state) :*/ Shapes.block();
    }

    private VoxelShape getVoxelShapeForDirection(BlockState state) {
        MinMaxBounds minMaxBounds = GodStatueHeadBlockEntity.getMinMaxBounds(BlockPos.ZERO, state.getValue(FACING));
        return Shapes.create(minMaxBounds.convertToAxisAlignedBB());
    }

    @Override
    public BlockState rotate(BlockState state, Rotation rot) {
        return state.setValue(FACING, rot.rotate(state.getValue(FACING)));
    }

    @Override
    public BlockState mirror(BlockState state, Mirror mirrorIn) {
        return state.rotate(mirrorIn.getRotation(state.getValue(FACING)));
    }

    @Override
    protected void createBlockStateDefinition(StateDefinition.Builder<Block, BlockState> builder) {
        builder.add(FACING, HEAD_STATE);
    }

    @Override
    public BlockState getStateForPlacement(BlockPlaceContext context) {
        return this.defaultBlockState().setValue(FACING, context.getHorizontalDirection().getOpposite()).setValue(HEAD_STATE, GodStatueHeadState.INACTIVE);
    }

}
