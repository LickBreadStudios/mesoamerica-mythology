package com.lickbread.mesoamericamythology.blocks;

import com.lickbread.mesoamericamythology.init.ModBlockEntityTypeObjects;
import com.lickbread.mesoamericamythology.blockEntities.OfferingBowlBlockEntity;
import com.lickbread.mesoamericamythology.util.PropertiesUtil;
import net.minecraft.world.level.block.BaseEntityBlock;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.RenderShape;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.material.Material;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.Containers;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.pathfinder.PathComputationType;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.InteractionResult;
import net.minecraft.world.InteractionHand;
import net.minecraft.core.BlockPos;
import net.minecraft.world.phys.BlockHitResult;
import net.minecraft.world.phys.shapes.CollisionContext;
import net.minecraft.world.phys.shapes.VoxelShape;
import net.minecraft.world.level.BlockGetter;
import net.minecraft.world.level.Level;

public class OfferingBowlBlock extends BaseEntityBlock {

    private static final VoxelShape VOXEL_SHAPE = Block.box(1.0D, 0.0D, 1.0D, 15.0D, 2.0D, 15.0D);

    public OfferingBowlBlock() {
        super(Block.Properties.of(Material.STONE).strength(4.0F, 8.0F).dynamicShape().isValidSpawn(PropertiesUtil::neverAllowSpawn));
    }

    /**
     * use - right click
     */
    @Override
    public InteractionResult use(BlockState blockState, Level level, BlockPos blockPos, Player playerEntity, InteractionHand hand, BlockHitResult blockRayTraceResult) {
        BlockEntity tile = level.getBlockEntity(blockPos);
        if (tile instanceof OfferingBowlBlockEntity) {
            OfferingBowlBlockEntity offeringBowl = (OfferingBowlBlockEntity) tile;

            if (!level.isClientSide()) {
                if (!playerEntity.isCrouching()) {
                    ItemStack heldItem = playerEntity.getItemInHand(hand);
                    if (heldItem.isEmpty()) {
                        offeringBowl.removeItem();
                    } else {
                        ItemStack stack = heldItem.copy();
                        stack.setCount(1);
                        if (offeringBowl.addItem(stack)) {
                            heldItem.shrink(1);
                        } else {
                            offeringBowl.removeItem();
                        }
                    }
                }
            }
            return InteractionResult.CONSUME;
        }
        return InteractionResult.PASS;
    }

    @Override
    public BlockEntity newBlockEntity(BlockPos pos, BlockState state) {
        return ModBlockEntityTypeObjects.OFFERING_BOWL.create(pos, state);
    }

    @Override
    public boolean isPathfindable(BlockState state, BlockGetter worldIn, BlockPos pos, PathComputationType type) {
        return false;
    }

    @Override
    public void onRemove(BlockState state, Level worldIn, BlockPos pos, BlockState newState, boolean isMoving) {
        if (state.getBlock() != newState.getBlock()) {
            BlockEntity blockEntity = worldIn.getBlockEntity(pos);
            if (blockEntity instanceof OfferingBowlBlockEntity) {
                Containers.dropContents(worldIn, pos, ((OfferingBowlBlockEntity) blockEntity).getInputs());
            }
            super.onRemove(state, worldIn, pos, newState, isMoving);
        }
    }

    @Override
    public RenderShape getRenderShape(BlockState state) {
        return RenderShape.ENTITYBLOCK_ANIMATED;
    }

    @Override
    public VoxelShape getShape(BlockState state, BlockGetter worldIn, BlockPos pos, CollisionContext context) {
        return VOXEL_SHAPE;
    }
}
