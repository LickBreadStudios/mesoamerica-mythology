package com.lickbread.mesoamericamythology.blocks;

import lombok.AllArgsConstructor;
import lombok.Getter;
import net.minecraft.world.effect.MobEffect;
import net.minecraft.world.effect.MobEffects;
import net.minecraft.util.StringRepresentable;

@Getter
@AllArgsConstructor
public enum TotemType implements StringRepresentable {
    SPEED("speed", MobEffects.MOVEMENT_SPEED),
    REGEN("regen", MobEffects.REGENERATION),
    HASTE("haste", MobEffects.DIG_SPEED),
    FIRE_RESISTANCE("fire_resistance", MobEffects.FIRE_RESISTANCE),
    STRENGTH("strength", MobEffects.DAMAGE_BOOST),
    JUMP_BOOST("jump_boost", MobEffects.JUMP),
    RESISTANCE("resistance", MobEffects.DAMAGE_RESISTANCE),
    INVISIBILITY("invisibility", MobEffects.INVISIBILITY),
    NIGHT_VISION("night_vision", MobEffects.NIGHT_VISION),
    LEVITATION("levitation", MobEffects.LEVITATION),
    SATURATION("saturation", MobEffects.SATURATION),
    POISON("poison", MobEffects.POISON),
    SLOWNESS("slowness", MobEffects.MOVEMENT_SLOWDOWN),
    WEAKNESS("weakness", MobEffects.WEAKNESS);

    private final String name;
    private final MobEffect effect;

    @Override
    public String toString() {
        return this.name;
    }

    public static TotemType getEnum(String string) {
        for (TotemType totemType : TotemType.values()) {
            if (totemType.toString().equals(string)) {
                return totemType;
            }
        }
        throw new IllegalArgumentException("Enum value does not exist: " + string);
    }

    @Override
    public String getSerializedName() {
        return this.name;
    }
}