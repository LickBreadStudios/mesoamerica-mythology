package com.lickbread.mesoamericamythology.blocks;

import com.lickbread.mesoamericamythology.init.ModBlockEntityTypeObjects;
import net.minecraft.world.level.block.BaseEntityBlock;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.RenderShape;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.material.Material;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.phys.AABB;
import net.minecraft.core.BlockPos;
import net.minecraft.world.phys.shapes.CollisionContext;
import net.minecraft.world.phys.shapes.VoxelShape;
import net.minecraft.world.phys.shapes.Shapes;
import net.minecraft.world.level.BlockGetter;

public class XolotlsVesselBlock extends BaseEntityBlock {

    public static final AABB AABB = new AABB(0.28125D, 0.0D, 0.28125D, 0.71875D, 1.0D, 0.71875D);

    public XolotlsVesselBlock() {
        super(Block.Properties.of(Material.GLASS).strength(2.0F, 8.0F).dynamicShape());
    }

    @Override
    public RenderShape getRenderShape(BlockState state) {
        return RenderShape.ENTITYBLOCK_ANIMATED;
    }

    @Override
    public BlockEntity newBlockEntity(BlockPos pos, BlockState state) {
        return ModBlockEntityTypeObjects.XOLOTLS_VESSEL.create(pos, state);
    }

    @Override
    public VoxelShape getShape(BlockState state, BlockGetter worldIn, BlockPos pos, CollisionContext context) {
        return Shapes.create(AABB);
    }
}