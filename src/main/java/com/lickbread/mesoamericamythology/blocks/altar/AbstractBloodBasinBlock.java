package com.lickbread.mesoamericamythology.blocks.altar;

import com.lickbread.mesoamericamythology.blockEntities.AbstractBloodBasinBlockEntity;
import com.lickbread.mesoamericamythology.blocks.state.BloodBasinState;
import com.lickbread.mesoamericamythology.init.ModBlockEntityTypeObjects;
import com.lickbread.mesoamericamythology.util.PropertiesUtil;
import net.minecraft.core.BlockPos;
import net.minecraft.world.item.context.BlockPlaceContext;
import net.minecraft.world.level.BlockGetter;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.BaseEntityBlock;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.RenderShape;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.level.block.entity.BlockEntityTicker;
import net.minecraft.world.level.block.entity.BlockEntityType;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.block.state.StateDefinition;
import net.minecraft.world.level.block.state.properties.EnumProperty;
import net.minecraft.world.phys.shapes.CollisionContext;
import net.minecraft.world.phys.shapes.VoxelShape;

import javax.annotation.Nullable;

import net.minecraft.world.level.block.state.BlockBehaviour.Properties;

public abstract class AbstractBloodBasinBlock extends BaseEntityBlock {
    public static final EnumProperty<BloodBasinState> BLOOD_BASIN_STATE = EnumProperty.create("blood_basin_state", BloodBasinState.class);

    public AbstractBloodBasinBlock(Properties properties) {
        super(properties
                .lightLevel((state) -> state.getValue(BLOOD_BASIN_STATE) != BloodBasinState.INACTIVE ? 15 : 0)
                .dynamicShape()
                .noOcclusion()
                .isValidSpawn(PropertiesUtil::neverAllowSpawn)
        );
        this.registerDefaultState(this.stateDefinition.any().setValue(BLOOD_BASIN_STATE, BloodBasinState.INACTIVE));
    }

    @Override
    public BlockEntity newBlockEntity(BlockPos pos, BlockState state) {
        return ModBlockEntityTypeObjects.BLOOD_BASIN.create(pos, state);
    }

    @Override
    @Nullable
    public <T extends BlockEntity> BlockEntityTicker<T> getTicker(Level level, BlockState state, BlockEntityType<T> type) {
        return createTickerHelper(type, ModBlockEntityTypeObjects.BLOOD_BASIN, level.isClientSide() ? AbstractBloodBasinBlockEntity::clientTick : AbstractBloodBasinBlockEntity::serverTick);
    }

    @Override
    public RenderShape getRenderShape(BlockState state) {
        return RenderShape.ENTITYBLOCK_ANIMATED;
    }

    public BlockState getStateForPlacement(BlockPlaceContext context) {
        return this.defaultBlockState().setValue(BLOOD_BASIN_STATE, BloodBasinState.INACTIVE);
    }

    protected void createBlockStateDefinition(StateDefinition.Builder<Block, BlockState> builder) {
        builder.add(BLOOD_BASIN_STATE);
    }

    @Override
    public abstract VoxelShape getShape(BlockState state, BlockGetter worldIn, BlockPos pos, CollisionContext context);
}
