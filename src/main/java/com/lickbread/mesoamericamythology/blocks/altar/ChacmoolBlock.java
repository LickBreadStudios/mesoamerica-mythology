package com.lickbread.mesoamericamythology.blocks.altar;

import com.lickbread.mesoamericamythology.init.ModBlockEntityTypeObjects;
import com.lickbread.mesoamericamythology.registry.ModItems;
import com.lickbread.mesoamericamythology.blockEntities.ChacmoolBlockEntity;
import com.lickbread.mesoamericamythology.util.PropertiesUtil;
import net.minecraft.world.level.block.BaseEntityBlock;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.RenderShape;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.material.Material;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.Containers;
import net.minecraft.world.item.context.BlockPlaceContext;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.pathfinder.PathComputationType;
import net.minecraft.world.level.block.state.properties.DirectionProperty;
import net.minecraft.world.level.block.state.StateDefinition;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.InteractionResult;
import net.minecraft.core.Direction;
import net.minecraft.world.InteractionHand;
import net.minecraft.core.BlockPos;
import net.minecraft.world.phys.BlockHitResult;
import net.minecraft.world.level.BlockGetter;
import net.minecraft.world.level.Level;

import net.minecraft.world.level.block.state.BlockBehaviour.Properties;

/**
 * Item holding bowl - looks like warrior reclining
 */
public class ChacmoolBlock extends BaseEntityBlock {

    public static final DirectionProperty FACING = DirectionProperty.create("facing", Direction.NORTH, Direction.EAST, Direction.SOUTH, Direction.WEST);

    public ChacmoolBlock() {
        super(Properties
                .of(Material.STONE)
                .strength(1.5F, 6.0F)
                .dynamicShape()
                .isValidSpawn(PropertiesUtil::neverAllowSpawn)
        );
        this.registerDefaultState(this.getStateDefinition().any().setValue(FACING, Direction.NORTH));
    }

    /**
     * use - right click
     */
    @Override
    public InteractionResult use(BlockState blockState, Level level, BlockPos blockPos, Player playerEntity, InteractionHand hand, BlockHitResult blockRayTraceResult) {
        BlockEntity tile = level.getBlockEntity(blockPos);
        if (tile instanceof ChacmoolBlockEntity && !level.isClientSide()) {
            ChacmoolBlockEntity chacmool = (ChacmoolBlockEntity) tile;

            if (!playerEntity.isCrouching()) {
                ItemStack heldItem = playerEntity.getItemInHand(hand);
                if (heldItem.getItem() == ModItems.MONSTER_HEART.get()) {
                    ItemStack stack = heldItem.copy();
                    stack.setCount(1);
                    if (chacmool.addItem(stack)) {
                        heldItem.shrink(1);
                    }
                } else if (heldItem.isEmpty()) {
                    chacmool.removeLastItem();
                }
                return InteractionResult.CONSUME;
            }
        }
        return InteractionResult.PASS;
    }

    @Override
    public BlockEntity newBlockEntity(BlockPos pos, BlockState state) {
        return ModBlockEntityTypeObjects.CHACMOOL.create(pos, state);
    }

    @Override
    public boolean isPathfindable(BlockState state, BlockGetter worldIn, BlockPos pos, PathComputationType type) {
        return false;
    }

    @Override
    public void onRemove(BlockState state, Level worldIn, BlockPos pos, BlockState newState, boolean isMoving) {
        if (state.getBlock() != newState.getBlock()) {
            BlockEntity blockEntity = worldIn.getBlockEntity(pos);
            if (blockEntity instanceof ChacmoolBlockEntity) {
                Containers.dropContents(worldIn, pos, ((ChacmoolBlockEntity) blockEntity).getInputs());
            }
            super.onRemove(state, worldIn, pos, newState, isMoving);
        }
    }

    @Override
    public RenderShape getRenderShape(BlockState state) {
        return RenderShape.ENTITYBLOCK_ANIMATED;
    }

    public BlockState getStateForPlacement(BlockPlaceContext context) {
        return this.defaultBlockState().setValue(FACING, context.getHorizontalDirection().getOpposite());
    }

    protected void createBlockStateDefinition(StateDefinition.Builder<Block, BlockState> builder) {
        builder.add(FACING);
    }
}
