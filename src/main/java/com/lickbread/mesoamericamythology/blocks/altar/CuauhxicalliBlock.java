package com.lickbread.mesoamericamythology.blocks.altar;

import com.lickbread.mesoamericamythology.blockEntities.CuauhxicalliBlockEntity;
import com.lickbread.mesoamericamythology.blocks.state.CuauhxicalliState;
import com.lickbread.mesoamericamythology.init.ModBlockEntityTypeObjects;
import com.lickbread.mesoamericamythology.util.PropertiesUtil;
import net.minecraft.core.BlockPos;
import net.minecraft.world.Containers;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.InteractionResult;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.context.BlockPlaceContext;
import net.minecraft.world.level.BlockGetter;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.BaseEntityBlock;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.RenderShape;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.block.state.StateDefinition;
import net.minecraft.world.level.block.state.properties.EnumProperty;
import net.minecraft.world.level.material.Material;
import net.minecraft.world.level.pathfinder.PathComputationType;
import net.minecraft.world.phys.BlockHitResult;
import net.minecraft.world.phys.shapes.BooleanOp;
import net.minecraft.world.phys.shapes.CollisionContext;
import net.minecraft.world.phys.shapes.Shapes;
import net.minecraft.world.phys.shapes.VoxelShape;

import net.minecraft.world.level.block.state.BlockBehaviour.Properties;

/**
 * Heart holding bowl - looks mostly like animals
 */
public class CuauhxicalliBlock extends BaseEntityBlock {

    private static final VoxelShape VOXEL_SHAPE = Shapes.join(Block.box(0.0D, 0.0D, 0.0D, 16.0D, 11.0D, 16.0D),
            Block.box(3.0D, 11.0D, 3.0D, 13.0D, 15.0D, 13.0D), BooleanOp.OR);

    public static final EnumProperty<CuauhxicalliState> CUAUHXICALLI_STATE = EnumProperty.create("state", CuauhxicalliState.class);

    public CuauhxicalliBlock() {
        super(Properties
                .of(Material.STONE)
                .strength(1.5F, 6.0F)
                .isValidSpawn(PropertiesUtil::neverAllowSpawn)
                .dynamicShape());
        this.registerDefaultState(this.getStateDefinition().any().setValue(CUAUHXICALLI_STATE, CuauhxicalliState.INACTIVE));
    }

    /**
     * use - right click
     */
    @Override
    public InteractionResult use(BlockState blockState, Level level, BlockPos blockPos, Player playerEntity, InteractionHand hand, BlockHitResult blockRayTraceResult) {
        BlockEntity tile = level.getBlockEntity(blockPos);
        if (tile instanceof CuauhxicalliBlockEntity && !level.isClientSide()) {
            CuauhxicalliBlockEntity cuauhxicalli = (CuauhxicalliBlockEntity) tile;

            if (!playerEntity.isCrouching()) {
                ItemStack heldItem = playerEntity.getItemInHand(hand);
                if (!heldItem.isEmpty()) {
                    ItemStack stack = heldItem.copy();
                    stack.setCount(1);
                    if (cuauhxicalli.addItem(stack)) {
                        heldItem.setCount(heldItem.getCount() - 1);
                        if (heldItem.getCount() <= 0) {
                            playerEntity.getInventory().removeItem(heldItem);
                        } else {
                            playerEntity.getInventory().setItem(playerEntity.getInventory().selected, heldItem);
                        }
                    }
                } else {
                    cuauhxicalli.removeLastItem();
                }
                return InteractionResult.CONSUME;
            }
        }
        return InteractionResult.PASS;
    }

    @Override
    public BlockEntity newBlockEntity(BlockPos pos, BlockState state) {
        return ModBlockEntityTypeObjects.CUAUHXICALLI.create(pos, state);
    }

    @Override
    public boolean isPathfindable(BlockState state, BlockGetter worldIn, BlockPos pos, PathComputationType type) {
        return false;
    }

    @Override
    public VoxelShape getShape(BlockState state, BlockGetter worldIn, BlockPos pos, CollisionContext context) {
        return VOXEL_SHAPE;
    }

    @Override
    public void onRemove(BlockState state, Level worldIn, BlockPos pos, BlockState newState, boolean isMoving) {
        if (state.getBlock() != newState.getBlock()) {
            BlockEntity blockEntity = worldIn.getBlockEntity(pos);
            if (blockEntity instanceof CuauhxicalliBlockEntity) {
                Containers.dropContents(worldIn, pos, ((CuauhxicalliBlockEntity) blockEntity).getInputs());
            }
            super.onRemove(state, worldIn, pos, newState, isMoving);
        }
    }

    @Override
    public RenderShape getRenderShape(BlockState state) {
        return RenderShape.MODEL;
    }

    public BlockState getStateForPlacement(BlockPlaceContext context) {
        return this.defaultBlockState().setValue(CUAUHXICALLI_STATE, CuauhxicalliState.INACTIVE);
    }

    protected void createBlockStateDefinition(StateDefinition.Builder<Block, BlockState> builder) {
        builder.add(CUAUHXICALLI_STATE);
    }
}
