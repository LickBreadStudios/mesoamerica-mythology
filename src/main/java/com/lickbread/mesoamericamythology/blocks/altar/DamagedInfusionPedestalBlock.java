package com.lickbread.mesoamericamythology.blocks.altar;

import com.lickbread.mesoamericamythology.util.PropertiesUtil;
import net.minecraft.core.BlockPos;
import net.minecraft.world.level.BlockGetter;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.material.Material;
import net.minecraft.world.phys.shapes.CollisionContext;
import net.minecraft.world.phys.shapes.VoxelShape;

import net.minecraft.world.level.block.state.BlockBehaviour.Properties;

public class DamagedInfusionPedestalBlock extends Block {

    private static final VoxelShape VOXEL_SHAPE = Block.box(2.0D, 0.0D, 2.0D, 14.0D, 15.0D, 14.0D);

    public DamagedInfusionPedestalBlock() {
        super(Properties
                .of(Material.STONE)
                .strength(1.5F, 6.0F)
                .dynamicShape()
                .isValidSpawn(PropertiesUtil::neverAllowSpawn));
    }

    @Override
    public VoxelShape getShape(BlockState state, BlockGetter worldIn, BlockPos pos, CollisionContext context) {
        return VOXEL_SHAPE;
    }

}
