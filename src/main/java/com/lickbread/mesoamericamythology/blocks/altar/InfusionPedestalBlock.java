package com.lickbread.mesoamericamythology.blocks.altar;

import com.lickbread.mesoamericamythology.blockEntities.InfusionPedestalBlockEntity;
import com.lickbread.mesoamericamythology.init.ModBlockEntityTypeObjects;
import net.minecraft.core.BlockPos;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.level.block.entity.BlockEntityTicker;
import net.minecraft.world.level.block.entity.BlockEntityType;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.material.Material;

import javax.annotation.Nullable;

import net.minecraft.world.level.block.state.BlockBehaviour.Properties;

public class InfusionPedestalBlock extends PedestalBlock {

    public InfusionPedestalBlock() {
        super(Properties.of(Material.STONE).strength(1.5F, 6.0F).dynamicShape());
    }

    @Override
    public BlockEntity newBlockEntity(BlockPos pos, BlockState state) {
        return ModBlockEntityTypeObjects.INFUSION_PEDESTAL.create(pos, state);
    }

    @Override
    @Nullable
    public <T extends BlockEntity> BlockEntityTicker<T> getTicker(Level level, BlockState state, BlockEntityType<T> type) {
        return createTickerHelper(type, ModBlockEntityTypeObjects.INFUSION_PEDESTAL, level.isClientSide() ? InfusionPedestalBlockEntity::clientTick : InfusionPedestalBlockEntity::serverTick);
    }
}
