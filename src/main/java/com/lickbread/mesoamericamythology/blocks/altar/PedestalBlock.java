package com.lickbread.mesoamericamythology.blocks.altar;

import com.lickbread.mesoamericamythology.blockEntities.InfusionPedestalBlockEntity;
import com.lickbread.mesoamericamythology.blockEntities.PedestalBlockEntity;
import com.lickbread.mesoamericamythology.blockEntities.SacrificePedestalBlockEntity;
import com.lickbread.mesoamericamythology.blocks.state.PedestalBlockState;
import com.lickbread.mesoamericamythology.items.VoidPocketItem;
import com.lickbread.mesoamericamythology.registry.ModDamageSources;
import com.lickbread.mesoamericamythology.registry.ModItems;
import com.lickbread.mesoamericamythology.util.PropertiesUtil;
import net.minecraft.core.BlockPos;
import net.minecraft.server.level.ServerPlayer;
import net.minecraft.world.Containers;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.InteractionResult;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.context.BlockPlaceContext;
import net.minecraft.world.level.BlockGetter;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.BaseEntityBlock;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.RenderShape;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.block.state.StateDefinition;
import net.minecraft.world.level.block.state.properties.EnumProperty;
import net.minecraft.world.level.pathfinder.PathComputationType;
import net.minecraft.world.phys.BlockHitResult;
import net.minecraft.world.phys.shapes.CollisionContext;
import net.minecraft.world.phys.shapes.VoxelShape;

public abstract class PedestalBlock extends BaseEntityBlock {

    public static final EnumProperty<PedestalBlockState> PEDESTAL_STATE = EnumProperty.create("pedestal_state", PedestalBlockState.class);

    private static final VoxelShape VOXEL_SHAPE = Block.box(2.0D, 0.0D, 2.0D, 14.0D, 15.0D, 14.0D);

    public PedestalBlock(Properties properties) {
        super(properties
                .lightLevel((state) -> state.getValue(PEDESTAL_STATE) != PedestalBlockState.INACTIVE ? 15 : 0)
                .isValidSpawn(PropertiesUtil::neverAllowSpawn));
        this.registerDefaultState(this.getStateDefinition().any().setValue(PEDESTAL_STATE, PedestalBlockState.INACTIVE));
    }

    /**
     * use - right click
     */
    @Override
    public InteractionResult use(BlockState blockState, Level level, BlockPos blockPos, Player playerEntity, InteractionHand hand, BlockHitResult blockRayTraceResult) {
        BlockEntity tile = level.getBlockEntity(blockPos);
        if (tile instanceof PedestalBlockEntity) {
            PedestalBlockEntity pedestal = (PedestalBlockEntity) tile;

            if (!level.isClientSide()) {
                if (!playerEntity.isCrouching()) {
                    ItemStack heldItem = playerEntity.getItemInHand(hand);
                    if (heldItem.isEmpty()) {
                        if (tile instanceof InfusionPedestalBlockEntity && ((InfusionPedestalBlockEntity) tile).isCrafting()) {
                            ((InfusionPedestalBlockEntity) tile).cancelCrafting();
                        } else {
                            pedestal.removeLastItem();
                        }
                    } else if (heldItem.getItem() == ModItems.OBSIDIAN_KNIFE.get()) {
                        ModDamageSources.attackPlayerWithObsidianKnife(level, playerEntity);
                        if (tile instanceof InfusionPedestalBlockEntity) {
                            ((InfusionPedestalBlockEntity)tile).craft((ServerPlayer) playerEntity);
                        }
                    } else if (heldItem.getItem() == ModItems.VOID_POCKET.get() && tile instanceof SacrificePedestalBlockEntity) {
                        if (VoidPocketItem.hasEntity(heldItem) && !((SacrificePedestalBlockEntity) tile).getEntity().isPresent()) {
                            ((SacrificePedestalBlockEntity) tile).addEntity(VoidPocketItem.getEntity(heldItem, level));
                        } else if (!VoidPocketItem.hasEntity(heldItem) && ((SacrificePedestalBlockEntity) tile).getEntity().isPresent()) {
                            VoidPocketItem.setEntity(heldItem, ((SacrificePedestalBlockEntity) tile).removeEntity().get());
                        }
                    } else if (!(tile instanceof SacrificePedestalBlockEntity)) {
                        ItemStack stack = heldItem.copy();
                        stack.setCount(1);
                        if (pedestal.addItem(stack)) {
                            heldItem.shrink(1);
                        } else {
                            pedestal.removeLastItem();
                        }
                    }
                }
            }
            return InteractionResult.CONSUME;
        }
        return InteractionResult.PASS;
    }

    @Override
    public boolean isPathfindable(BlockState state, BlockGetter worldIn, BlockPos pos, PathComputationType type) {
        return false;
    }

    @Override
    public void onRemove(BlockState state, Level worldIn, BlockPos pos, BlockState newState, boolean isMoving) {
        if (state.getBlock() != newState.getBlock()) {
            BlockEntity blockEntity = worldIn.getBlockEntity(pos);
            if (blockEntity instanceof PedestalBlockEntity && !(blockEntity instanceof SacrificePedestalBlockEntity)) {
                Containers.dropContents(worldIn, pos, ((PedestalBlockEntity) blockEntity).getInputs());
            } else if (blockEntity instanceof SacrificePedestalBlockEntity) {
                ((SacrificePedestalBlockEntity) blockEntity).removeEntity().ifPresent(entity -> {
                    entity.setPos(pos.getX(), pos.getY(), pos.getZ());
                    worldIn.addFreshEntity(entity);
                });
            }
            super.onRemove(state, worldIn, pos, newState, isMoving);
        }
    }

    @Override
    public RenderShape getRenderShape(BlockState state) {
        return RenderShape.ENTITYBLOCK_ANIMATED;
    }

    @Override
    public VoxelShape getShape(BlockState state, BlockGetter worldIn, BlockPos pos, CollisionContext context) {
        return VOXEL_SHAPE;
    }

    public BlockState getStateForPlacement(BlockPlaceContext context) {
        return this.defaultBlockState().setValue(PEDESTAL_STATE, PedestalBlockState.INACTIVE);
    }

    protected void createBlockStateDefinition(StateDefinition.Builder<Block, BlockState> builder) {
        builder.add(PEDESTAL_STATE);
    }
}
