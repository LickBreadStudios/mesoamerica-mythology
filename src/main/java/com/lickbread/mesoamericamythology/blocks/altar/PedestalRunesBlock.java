package com.lickbread.mesoamericamythology.blocks.altar;

import com.lickbread.mesoamericamythology.blocks.state.PedestalRunesState;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.material.Material;
import net.minecraft.world.item.context.BlockPlaceContext;
import net.minecraft.world.level.block.state.properties.EnumProperty;
import net.minecraft.world.level.block.state.StateDefinition;

public class PedestalRunesBlock extends Block {

    public static final EnumProperty<PedestalRunesState> PEDESTAL_STATE = EnumProperty.create("pedestal_runes_state", PedestalRunesState.class);

    public PedestalRunesBlock() {
        super(Block.Properties.of(Material.WOOL).strength(100.0F, 100.0F));
        this.registerDefaultState(this.getStateDefinition().any().setValue(PEDESTAL_STATE, PedestalRunesState.PREPARING));
    }

    public BlockState getStateForPlacement(BlockPlaceContext context) {
        return this.defaultBlockState().setValue(PEDESTAL_STATE, PedestalRunesState.PREPARING);
    }

    protected void createBlockStateDefinition(StateDefinition.Builder<Block, BlockState> builder) {
        builder.add(PEDESTAL_STATE);
    }
}
