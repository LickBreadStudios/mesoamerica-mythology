package com.lickbread.mesoamericamythology.blocks.altar;

import com.lickbread.mesoamericamythology.init.ModBlockEntityTypeObjects;
import net.minecraft.core.BlockPos;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.material.Material;

public class PowerPedestalBlock extends PedestalBlock {

    public PowerPedestalBlock() {
        super(Block.Properties.of(Material.STONE).strength(1.5F, 6.0F).dynamicShape());
    }

    @Override
    public BlockEntity newBlockEntity(BlockPos pos, BlockState state) {
        return ModBlockEntityTypeObjects.POWER_PEDESTAL.create(pos, state);
    }

}
