package com.lickbread.mesoamericamythology.blocks.altar;

import com.lickbread.mesoamericamythology.init.ModBlockEntityTypeObjects;
import net.minecraft.core.BlockPos;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.material.Material;

import net.minecraft.world.level.block.state.BlockBehaviour.Properties;

public class SacrificePedestalBlock extends PedestalBlock {

    public SacrificePedestalBlock() {
        super(Properties.of(Material.STONE).strength(1.5F, 6.0F).dynamicShape());
    }

    @Override
    public BlockEntity newBlockEntity(BlockPos pos, BlockState state) {
        return ModBlockEntityTypeObjects.SACRIFICE_PEDESTAL.create(pos, state);
    }

}
