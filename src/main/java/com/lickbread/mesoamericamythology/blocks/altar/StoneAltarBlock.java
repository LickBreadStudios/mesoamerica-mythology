package com.lickbread.mesoamericamythology.blocks.altar;

import com.lickbread.mesoamericamythology.blockEntities.StoneAltarBlockEntity;
import com.lickbread.mesoamericamythology.blocks.state.AltarBlockState;
import com.lickbread.mesoamericamythology.init.ModBlockEntityTypeObjects;
import com.lickbread.mesoamericamythology.registry.ModDamageSources;
import com.lickbread.mesoamericamythology.registry.ModItems;
import com.lickbread.mesoamericamythology.util.PropertiesUtil;
import net.minecraft.core.BlockPos;
import net.minecraft.server.level.ServerPlayer;
import net.minecraft.world.Containers;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.InteractionResult;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.context.BlockPlaceContext;
import net.minecraft.world.level.BlockGetter;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.BaseEntityBlock;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.RenderShape;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.level.block.entity.BlockEntityTicker;
import net.minecraft.world.level.block.entity.BlockEntityType;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.block.state.StateDefinition;
import net.minecraft.world.level.block.state.properties.EnumProperty;
import net.minecraft.world.level.material.Material;
import net.minecraft.world.level.pathfinder.PathComputationType;
import net.minecraft.world.phys.BlockHitResult;
import net.minecraft.world.phys.shapes.CollisionContext;
import net.minecraft.world.phys.shapes.VoxelShape;

import javax.annotation.Nullable;

public class StoneAltarBlock extends BaseEntityBlock {
    public static final EnumProperty<AltarBlockState> ALTAR_STATE = EnumProperty.create("altar_state", AltarBlockState.class);

    private static final VoxelShape VOXEL_SHAPE = Block.box(1.0D, 0.0D, 1.0D, 15.0D, 16.0D, 15.0D);

    public StoneAltarBlock() {
        super(Properties.of(Material.STONE)
                .strength(1.5F, 6.0F)
                .dynamicShape()
                .isValidSpawn(PropertiesUtil::neverAllowSpawn));
        this.registerDefaultState(this.getStateDefinition().any().setValue(ALTAR_STATE, AltarBlockState.INACTIVE));
    }

    /**
     * use - right click
     */
    @Override
    public InteractionResult use(BlockState blockState, Level level, BlockPos blockPos, Player playerEntity, InteractionHand hand, BlockHitResult blockRayTraceResult) {
        BlockEntity tile = level.getBlockEntity(blockPos);
        if (tile instanceof StoneAltarBlockEntity) {
            StoneAltarBlockEntity altar = (StoneAltarBlockEntity) tile;

            if (!level.isClientSide()) {
                if (!playerEntity.isCrouching()) {
                    ItemStack heldItem = playerEntity.getItemInHand(hand);
                    if (heldItem.getItem() == ModItems.OBSIDIAN_KNIFE.get()) {
                        ModDamageSources.attackPlayerWithObsidianKnife(level, playerEntity);
                        altar.craft((ServerPlayer) playerEntity);
                    } else if (heldItem.isEmpty()) {
                        altar.removeLastItem();
                    } else {
                        ItemStack stack = heldItem.copy();
                        stack.setCount(1);
                        if (altar.addItem(stack)) {
                            heldItem.shrink(1);
                        }
                    }
                } else {
                    if (!altar.isCrafting()) {
                        altar.removeItems();
                    } else {
                        altar.cancelCrafting();
                    }
                }
            }
            return InteractionResult.CONSUME;
        }
        return InteractionResult.PASS;
    }

    @Override
    public BlockEntity newBlockEntity(BlockPos pos, BlockState state) {
        return ModBlockEntityTypeObjects.STONE_ALTAR.create(pos, state);
    }

    @Override
    @Nullable
    public <T extends BlockEntity> BlockEntityTicker<T> getTicker(Level level, BlockState state, BlockEntityType<T> type) {
        return createTickerHelper(type, ModBlockEntityTypeObjects.STONE_ALTAR, level.isClientSide() ? StoneAltarBlockEntity::clientTick : StoneAltarBlockEntity::serverTick);
    }

    @Override
    public boolean isPathfindable(BlockState state, BlockGetter worldIn, BlockPos pos, PathComputationType type) {
        return false;
    }
    
    @Override
    public void onRemove(BlockState state, Level worldIn, BlockPos pos, BlockState newState, boolean isMoving) {
        if (state.getBlock() != newState.getBlock()) {
            BlockEntity blockEntity = worldIn.getBlockEntity(pos);
            if (blockEntity instanceof StoneAltarBlockEntity) {
                Containers.dropContents(worldIn, pos, ((StoneAltarBlockEntity) blockEntity).getInputs());
            }
            super.onRemove(state, worldIn, pos, newState, isMoving);
        }
    }

    @Override
    public RenderShape getRenderShape(BlockState state) {
        return RenderShape.ENTITYBLOCK_ANIMATED;
    }

    @Override
    public VoxelShape getShape(BlockState state, BlockGetter blockGetter, BlockPos pos, CollisionContext context) {
        return VOXEL_SHAPE;
    }

    public BlockState getStateForPlacement(BlockPlaceContext context) {
        return this.defaultBlockState().setValue(ALTAR_STATE, AltarBlockState.INACTIVE);
    }

    protected void createBlockStateDefinition(StateDefinition.Builder<Block, BlockState> builder) {
        builder.add(ALTAR_STATE);
    }
}
