package com.lickbread.mesoamericamythology.blocks.state;

import lombok.AllArgsConstructor;
import net.minecraft.util.StringRepresentable;

@AllArgsConstructor
public enum ActivatedEnumState implements StringRepresentable {
    INACTIVE("inactive"),
    ACTIVE("active");

    private final String name;

    @Override
    public String toString() {
        return name;
    }

    @Override
    public String getSerializedName() {
        return name;
    }
}
