package com.lickbread.mesoamericamythology.blocks.state;

import lombok.AllArgsConstructor;
import net.minecraft.util.StringRepresentable;

@AllArgsConstructor
public enum AltarBlockState implements StringRepresentable {
    INACTIVE("inactive"),
    BLOOD("blood"),
    CRAFTING("crafting"),
    END("end");

    private final String name;

    @Override
    public String toString() {
        return name;
    }

    @Override
    public String getSerializedName() {
        return name;
    }
}
