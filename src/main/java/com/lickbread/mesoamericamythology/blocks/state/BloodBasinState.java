package com.lickbread.mesoamericamythology.blocks.state;

import lombok.AllArgsConstructor;
import net.minecraft.util.StringRepresentable;

@AllArgsConstructor
public enum BloodBasinState implements StringRepresentable {
    INACTIVE("inactive"),
    PREPARING("preparing"),
    CRAFTING("crafting"),
    END("end");

    private final String name;

    @Override
    public String toString() {
        return name;
    }

    @Override
    public String getSerializedName() {
        return name;
    }
}
