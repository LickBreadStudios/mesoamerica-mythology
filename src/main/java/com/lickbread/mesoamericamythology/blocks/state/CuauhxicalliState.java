package com.lickbread.mesoamericamythology.blocks.state;

import lombok.AllArgsConstructor;
import net.minecraft.util.StringRepresentable;

@AllArgsConstructor
public enum CuauhxicalliState implements StringRepresentable {
    INACTIVE("inactive"),
    HAS_HEART("has_heart"),
    ACTIVE("active");

    private final String name;

    @Override
    public String toString() {
        return name;
    }

    @Override
    public String getSerializedName() {
        return name;
    }
}