package com.lickbread.mesoamericamythology.blocks.state;

import lombok.AllArgsConstructor;
import net.minecraft.util.StringRepresentable;

@AllArgsConstructor
public enum GodStatueHeadState implements StringRepresentable {
    INACTIVE("inactive"),
    PASSIVE("passive"),
    ANGRY("angry"),
    PLEASED("pleased"),
    DISPLEASED("displeased"),
    SPITTING("spitting"),
    TALKING_0("talking_0"),
    TALKING_1("talking_1"),
    TALKING_2("talking_2");

    private final String name;

    @Override
    public String toString() {
        return name;
    }

    @Override
    public String getSerializedName() {
        return name;
    }
}
