package com.lickbread.mesoamericamythology.blocks.state;

import lombok.AllArgsConstructor;
import net.minecraft.util.StringRepresentable;

@AllArgsConstructor
public enum PedestalBlockState implements StringRepresentable {
    INACTIVE("inactive", BloodBasinState.INACTIVE),
    PREPARING("preparing", BloodBasinState.PREPARING),
    CRAFTING("crafting", BloodBasinState.CRAFTING),
    END("end", BloodBasinState.END);

    private final String name;
    private final BloodBasinState bloodBasinState;

    public BloodBasinState toBloodBasinState() {
        return this.bloodBasinState;
    }

    @Override
    public String toString() {
        return name;
    }

    @Override
    public String getSerializedName() {
        return name;
    }
}
