package com.lickbread.mesoamericamythology.blocks.state;

import lombok.AllArgsConstructor;
import lombok.Getter;
import net.minecraft.util.StringRepresentable;

@AllArgsConstructor
public enum PedestalRunesState implements StringRepresentable {
    PREPARING("preparing", 154F / 255F, 255F / 255F, 105F / 255F),
    CRAFTING("crafting", 255F / 255F, 239F / 255F, 105F / 255F),
    END("end", 207F / 255F, 0F / 255F, 0F / 255F);

    private final String name;
    @Getter
    private final float red;
    @Getter
    private final float green;
    @Getter
    private final float blue;

    @Override
    public String toString() {
        return name;
    }

    @Override
    public String getSerializedName() {
        return name;
    }
}
