package com.lickbread.mesoamericamythology.blocks.traps;

import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.world.entity.item.FallingBlockEntity;
import net.minecraft.world.level.BlockGetter;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.LevelAccessor;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.FallingBlock;
import net.minecraft.world.level.block.SoundType;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.material.Material;
import net.minecraft.world.level.material.MaterialColor;
import net.minecraft.world.phys.shapes.CollisionContext;
import net.minecraft.world.phys.shapes.VoxelShape;

public class FallingTrapBlock extends FallingBlock {

    protected static final VoxelShape SHAPE = Block.box(0.0D, 8.0D, 0.0D, 16.0D, 16.0D, 16.0D);

    public FallingTrapBlock() {
        super(Block.Properties.of(Material.STONE, MaterialColor.STONE).strength(5.0F, 6.0F).sound(SoundType.STONE));
    }

    // Override default behaviour
    @Override
    public void onPlace(BlockState state, Level worldIn, BlockPos pos, BlockState oldState, boolean isMoving) {
    }

    @Override
    public BlockState updateShape(BlockState stateIn, Direction facing, BlockState facingState, LevelAccessor worldIn, BlockPos currentPos, BlockPos facingPos) {
        if (!worldIn.isClientSide()) {
            this.checkActivated((Level) worldIn, currentPos, stateIn);
        }

        return stateIn;
    }

    @Override
    public void neighborChanged(BlockState state, Level worldIn, BlockPos pos, Block blockIn, BlockPos fromPos, boolean isMoving) {
        if (!worldIn.isClientSide()) {
            this.checkActivated(worldIn, pos, state);
        }
    }

    @Override
    public void onLand(Level worldIn, BlockPos pos, BlockState fallingState, BlockState hitState, FallingBlockEntity fallingBlock) {
        worldIn.destroyBlock(pos, true);
    }

    private void checkActivated(Level worldIn, BlockPos pos, BlockState state) {
        boolean isPowered = worldIn.hasNeighborSignal(pos);
        if (isPowered) {
            worldIn.scheduleTick(pos, this, this.getDelayAfterPlace());
        }
    }

    public VoxelShape getShape(BlockState state, BlockGetter worldIn, BlockPos pos, CollisionContext context) {
        return SHAPE;
    }
}
