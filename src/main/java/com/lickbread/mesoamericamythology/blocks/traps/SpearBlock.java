package com.lickbread.mesoamericamythology.blocks.traps;

import com.lickbread.mesoamericamythology.registry.ModDamageSources;
import com.lickbread.mesoamericamythology.util.PropertiesUtil;
import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.item.context.BlockPlaceContext;
import net.minecraft.world.level.BlockGetter;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.Mirror;
import net.minecraft.world.level.block.Rotation;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.block.state.StateDefinition;
import net.minecraft.world.level.block.state.properties.BlockStateProperties;
import net.minecraft.world.level.block.state.properties.DirectionProperty;
import net.minecraft.world.level.material.Material;
import net.minecraft.world.level.pathfinder.PathComputationType;
import net.minecraft.world.phys.shapes.CollisionContext;
import net.minecraft.world.phys.shapes.VoxelShape;

public class SpearBlock extends Block {

    public static final float SPEAR_DAMAGE = 6F;

    public static final DirectionProperty FACING = BlockStateProperties.FACING;

    protected static final VoxelShape SPEAR_VERTICAL_AABB = Block.box(6.0D, 0.0D, 6.0D, 10.0D, 48.0D, 10.0D);
    protected static final VoxelShape SPEAR_NS_AABB = Block.box(6.0D, 6.0D, 0.0D, 10.0D, 10.0D, 48.0D);
    protected static final VoxelShape SPEAR_EW_AABB = Block.box(0.0D, 6.0D, 6.0D, 48.0D, 10.0D, 10.0D);

    public SpearBlock() {
        super(Properties
                .of(Material.WOOD)
                .strength(2.0F, 2.0F)
                .noCollission()
                .isValidSpawn(PropertiesUtil::neverAllowSpawn));
        this.registerDefaultState(this.defaultBlockState().setValue(FACING, Direction.NORTH));
    }

    @Override
    public boolean isPathfindable(BlockState state, BlockGetter worldIn, BlockPos pos, PathComputationType type) {
        return false;
    }

    @Override
    public VoxelShape getShape(BlockState state, BlockGetter worldIn, BlockPos pos, CollisionContext context) {
        switch(state.getValue(FACING).getAxis()) {
            case X:
            default:
                return SPEAR_EW_AABB;
            case Z:
                return SPEAR_NS_AABB;
            case Y:
                return SPEAR_VERTICAL_AABB;
        }
    }

    @Override
    public void entityInside(BlockState state, Level level, BlockPos pos, Entity entityIn) {
        entityIn.hurt(ModDamageSources.source(level, ModDamageSources.SPEAR_TRAP), SPEAR_DAMAGE);
    }

    @Override
    public BlockState rotate(BlockState state, Rotation rot) {
        return state.setValue(FACING, rot.rotate(state.getValue(FACING)));
    }

    @Override
    public BlockState mirror(BlockState state, Mirror mirrorIn) {
        return state.rotate(mirrorIn.getRotation(state.getValue(FACING)));
    }

    @Override
    protected void createBlockStateDefinition(StateDefinition.Builder<Block, BlockState> builder) {
        builder.add(FACING);
    }

    @Override
    public BlockState getStateForPlacement(BlockPlaceContext context) {
        return this.defaultBlockState().setValue(FACING, context.getClickedFace());
    }
}
