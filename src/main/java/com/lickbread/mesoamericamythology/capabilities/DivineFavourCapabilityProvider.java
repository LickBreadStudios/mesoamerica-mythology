package com.lickbread.mesoamericamythology.capabilities;

import com.lickbread.mesoamericamythology.MesoamericaMythologyMod;
import com.lickbread.mesoamericamythology.statueHead.relationship.DivineFavourCapability;
import com.lickbread.mesoamericamythology.statueHead.relationship.IDivineFavourCapability;
import net.minecraft.core.Direction;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.nbt.Tag;
import net.minecraft.resources.ResourceLocation;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.capabilities.CapabilityManager;
import net.minecraftforge.common.capabilities.CapabilityToken;
import net.minecraftforge.common.capabilities.ICapabilitySerializable;
import net.minecraftforge.common.util.LazyOptional;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import static com.lickbread.mesoamericamythology.api.MesoamericaMythologyAPI.GSON;

public class DivineFavourCapabilityProvider implements ICapabilitySerializable<Tag> {

    private static final String CAPABILITY_KEY = "Capability";

    public static ResourceLocation DIVINE_FAVOUR_CAPABILITY_KEY = new ResourceLocation(MesoamericaMythologyMod.MOD_ID, "divine_favour");

    public static Capability<IDivineFavourCapability> DIVINE_FAVOUR_CAPABILITY = CapabilityManager.get(new CapabilityToken<>(){});

    private DivineFavourCapability divineFavourCapability = new DivineFavourCapability();

    @Nonnull
    @Override
    public <T> LazyOptional<T> getCapability(@Nonnull Capability<T> cap, @Nullable Direction side) {
        return (LazyOptional<T>)LazyOptional.of(() -> divineFavourCapability);
    }

    @Override
    public Tag serializeNBT() {
        CompoundTag nbt = new CompoundTag();
        String capabilityJson = GSON.toJson(this.divineFavourCapability);
        nbt.putString(CAPABILITY_KEY, capabilityJson);
        return nbt;
    }

    @Override
    public void deserializeNBT(Tag nbt) {
        DivineFavourCapability parsedCapability = GSON.fromJson(((CompoundTag) nbt).getString(CAPABILITY_KEY), DivineFavourCapability.class);
        this.divineFavourCapability.copyFrom(parsedCapability);
    }
}
