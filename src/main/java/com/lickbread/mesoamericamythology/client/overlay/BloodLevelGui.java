package com.lickbread.mesoamericamythology.client.overlay;

import com.lickbread.mesoamericamythology.MesoamericaMythologyMod;
import com.lickbread.mesoamericamythology.blockEntities.AbstractBloodBasinBlockEntity;
import com.mojang.blaze3d.systems.RenderSystem;
import com.mojang.blaze3d.vertex.PoseStack;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiComponent;
import net.minecraft.core.BlockPos;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.phys.BlockHitResult;
import net.minecraft.world.phys.HitResult;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;

@OnlyIn(Dist.CLIENT)
public class BloodLevelGui extends GuiComponent {

    private static final ResourceLocation BLOOD_TEXTURE = new ResourceLocation(MesoamericaMythologyMod.MOD_ID, "textures/gui/blood_level.png");

    public void render() {
        Minecraft client = Minecraft.getInstance();
        HitResult lookingAt = client.hitResult;

        if (lookingAt == null || lookingAt.getType() != HitResult.Type.BLOCK || client.level == null || client.player == null || !(lookingAt instanceof BlockHitResult)) {
            return;
        }

        BlockPos pos = ((BlockHitResult) lookingAt).getBlockPos();
        BlockEntity blockEntity = client.level.getBlockEntity(pos);

        if (blockEntity instanceof AbstractBloodBasinBlockEntity) {
            AbstractBloodBasinBlockEntity bloodBasinBlockEntity = (AbstractBloodBasinBlockEntity) blockEntity;
            float currentBlood = bloodBasinBlockEntity.getBloodPoolAmount();
            float maxBlood = bloodBasinBlockEntity.getMaxBlood();
            String bloodString = (int) currentBlood + " / " + (int) maxBlood;

            PoseStack poseStack = new PoseStack();
            int width = client.getWindow().getGuiScaledWidth();
            int height = client.getWindow().getGuiScaledHeight();
            int length = client.font.width(bloodString);

            drawCenteredString(poseStack, client.font, bloodString, width / 2 + length / 2 + 12, (height / 2) - 4, Integer.parseInt("FFAA00", 16));
            RenderSystem.setShaderTexture(0, BLOOD_TEXTURE);
            // x in screen, y in screen, x texture offset, y texture offset, x texture length, y texture length
            this.blit(poseStack, 20, 20, 0, 0, 16, 16);
        }
    }
}
