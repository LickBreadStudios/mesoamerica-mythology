package com.lickbread.mesoamericamythology.client.renderer.blockEntity;

import com.lickbread.mesoamericamythology.blockEntities.AbstractBloodBasinBlockEntity;
import com.lickbread.mesoamericamythology.blocks.altar.AbstractBloodBasinBlock;
import com.lickbread.mesoamericamythology.particle.BloodDripParticleData;
import com.lickbread.mesoamericamythology.particle.DeathBloodParticleData;
import com.lickbread.mesoamericamythology.util.BlockPosUtil;
import com.lickbread.mesoamericamythology.util.MathUtil;
import com.mojang.blaze3d.vertex.PoseStack;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.MultiBufferSource;
import net.minecraft.client.renderer.blockentity.BlockEntityRenderer;
import net.minecraft.client.renderer.blockentity.BlockEntityRendererProvider;
import net.minecraft.util.profiling.ProfilerFiller;
import net.minecraft.world.phys.Vec3;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;

import java.util.Random;
import java.util.Set;

@OnlyIn(Dist.CLIENT)
public abstract class AbstractBloodBasinRenderer<T extends AbstractBloodBasinBlockEntity> implements BlockEntityRenderer<T> {

    private static final double BLOOD_BUFFER_PARTICLE_START_VARIANCE = 0.5D;
    private static final double BLOOD_BUFFER_PARTICLE_END_VARIANCE = 0.1D;
    private static final int MAX_BLOOD_BUFFER_PARTICLES = 25;
    private static final int MIN_BLOOD_BUFFER_PARTICLES = 10;
    private static final double BLOOD_BUFFER_PARTICLE_CHANCE = 0.5D;

    private static final double PREPARING_PARTICLE_CHANCE = 0.1D;

    public AbstractBloodBasinRenderer(BlockEntityRendererProvider.Context context) {
    }

    @Override
    public void render(T blockEntity, float partialTicks, PoseStack poseStack, MultiBufferSource buffer, int combinedLightIn, int combinedOverlayIn) {
        poseStack.pushPose();
        if (blockEntity.getCurrentBloodBufferAmount() > 0) {
            this.renderBloodBufferParticles(blockEntity);
        }
        if (blockEntity.getBlockState().hasProperty(AbstractBloodBasinBlock.BLOOD_BASIN_STATE)) {
            switch (blockEntity.getBlockState().getValue(AbstractBloodBasinBlock.BLOOD_BASIN_STATE)) {
                case INACTIVE:
                    break;
                case PREPARING:
                    this.renderPreparingBasin(blockEntity);
                    break;
                case CRAFTING:
                    this.renderCraftingBasin(blockEntity);
                    break;
                case END:
                    this.renderEndBasin(blockEntity);
                    break;
            }
        }

        poseStack.popPose();
    }

    protected void renderPreparingBasin(T blockEntity) {
        this.renderFloatingBloodDrips(blockEntity);
    }

    protected void renderCraftingBasin(T blockEntity) {
        if (!blockEntity.getActiveInfusionPedestal().isPresent()) {
            return;
        }

        Random rand = new Random();
        Vec3 end0 = BlockPosUtil.getPosTopCentre(blockEntity.getActiveInfusionPedestal().get()).add(0D, 0.25D, 0D)
                .add(MathUtil.getInRandom(rand, BLOOD_BUFFER_PARTICLE_END_VARIANCE), MathUtil.getInRandom(rand, BLOOD_BUFFER_PARTICLE_END_VARIANCE), MathUtil.getInRandom(rand, BLOOD_BUFFER_PARTICLE_END_VARIANCE));
        Vec3 end1 = end0.add(0D, 1D, 0D);

        Vec3 start0 = blockEntity.getRandomBloodParticlePos();
        Vec3 start1 = start0.add(0D, Math.max(start0.y + 1.0d, end1.y) - start0.y, 0D);

        for (int i = 0; i < rand.nextInt(MAX_BLOOD_BUFFER_PARTICLES) + MIN_BLOOD_BUFFER_PARTICLES; i++) {
            if (rand.nextDouble() < BLOOD_BUFFER_PARTICLE_CHANCE) {
                blockEntity.getLevel().addParticle(new DeathBloodParticleData(start0, start1, end1, end0), start0.x, start0.y, start0.z, 0D, 0D, 0D);
            }
        }
    }

    protected void renderEndBasin(T blockEntity) {
    }

    protected void renderFloatingBloodDrips(T blockEntity) {
        Random random = new Random();
        if (random.nextDouble() >= PREPARING_PARTICLE_CHANCE) {
            return;
        }

        Vec3 pos = blockEntity.getRandomBloodParticlePos();
        blockEntity.getLevel().addParticle(new BloodDripParticleData(-0.2D), pos.x, pos.y, pos.z, 0.0D, 0.0D, 0.0D);

    }

    /**
     * Distance from the top of the BlockPos to the basin top in the model for rendering start/end of blood particles
     */
    protected abstract float getBasinTopY();

    protected void renderBloodBufferParticles(T blockEntity) {
        ProfilerFiller profiler = Minecraft.getInstance().getProfiler();
        profiler.push("renderBloodBufferParticles");
        Set<Vec3> blockPositions = blockEntity.getCurrentBloodBufferPositions();

        Random rand = new Random();
        for (Vec3 origin : blockPositions) {
            Vec3 randOffsetEnd = MathUtil.randomOffset(BLOOD_BUFFER_PARTICLE_END_VARIANCE, 0F, BLOOD_BUFFER_PARTICLE_END_VARIANCE);
            Vec3 end0 = BlockPosUtil.getPosTopCentre(blockEntity.getBlockPos()).add(0D, this.getBasinTopY(), 0D).add(randOffsetEnd);
            Vec3 end1 = end0.add(0D, 1D, 0D);

            Vec3 randOffsetStart = MathUtil.randomOffset(BLOOD_BUFFER_PARTICLE_START_VARIANCE, BLOOD_BUFFER_PARTICLE_START_VARIANCE, BLOOD_BUFFER_PARTICLE_START_VARIANCE);
            Vec3 start0 = origin.add(randOffsetStart);
            Vec3 start1 = start0.add(0D, Math.max(start0.y + 1.0d, end1.y) - start0.y, 0D);

            for (int i = 0; i < rand.nextInt(MAX_BLOOD_BUFFER_PARTICLES) + MIN_BLOOD_BUFFER_PARTICLES; i++) {
                if (rand.nextDouble() < BLOOD_BUFFER_PARTICLE_CHANCE) {
                    blockEntity.getLevel().addParticle(new DeathBloodParticleData(start0, start1, end1, end0), start0.x, start0.y, start0.z, 0D, 0D, 0D);
                }
            }
        }
        profiler.pop();
    }
}
