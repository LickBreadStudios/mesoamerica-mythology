package com.lickbread.mesoamericamythology.client.renderer.blockEntity;

import com.lickbread.mesoamericamythology.blockEntities.PedestalBlockEntity;
import com.lickbread.mesoamericamythology.blocks.altar.PedestalBlock;
import com.lickbread.mesoamericamythology.blocks.altar.PedestalRunesBlock;
import com.lickbread.mesoamericamythology.blocks.state.PedestalBlockState;
import com.lickbread.mesoamericamythology.blocks.state.PedestalRunesState;
import com.lickbread.mesoamericamythology.particle.InfusionPedestalCraftParticleData;
import com.lickbread.mesoamericamythology.particle.InfusionPedestalRunesParticleData;
import com.lickbread.mesoamericamythology.registry.ModBlocks;
import com.lickbread.mesoamericamythology.registry.ModParticleTypes;
import com.lickbread.mesoamericamythology.util.BlockPosUtil;
import com.lickbread.mesoamericamythology.util.MathUtil;
import com.lickbread.mesoamericamythology.util.RendererUtil;
import com.mojang.blaze3d.vertex.PoseStack;
import com.mojang.math.Axis;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.MultiBufferSource;
import net.minecraft.client.renderer.Sheets;
import net.minecraft.client.renderer.blockentity.BlockEntityRenderer;
import net.minecraft.client.renderer.blockentity.BlockEntityRendererProvider;
import net.minecraft.core.BlockPos;
import net.minecraft.world.item.BlockItem;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemDisplayContext;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.phys.Vec3;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;

import java.util.Random;

@OnlyIn(Dist.CLIENT)
public abstract class AbstractPedestalRenderer<T extends PedestalBlockEntity> implements BlockEntityRenderer<T> {

    protected static final float TIME_SCALE = 1F;

    protected static final double ITEM_HEIGHT = 0.125D;
    protected static final double BLOCK_HEIGHT = 0.1D;

    protected static final float ITEM_RENDER_SCALE = 0.50F;
    protected static final float BLOCK_RENDER_SCALE = 0.85F;

    protected static final float ITEM_FLOAT_HEIGHT = 1.125F;
    protected static final float ITEM_FLOAT_AMPLITUDE = 0.05F;

    protected static final float RUNES_TIME_SCALE = 0.5F;
    protected static final float RUNES_SIZE_SCALE = 1.75F;
    protected static final double RUNES_PARTICLE_CHANCE = 0.05D;
    protected static final double MIN_RUNES_PARTICLE_DIST = 0.45D * RUNES_SIZE_SCALE;
    protected static final double MAX_RUNES_PARTICLE_DIST = 0.55D * RUNES_SIZE_SCALE;

    protected static final double CRAFTING_PARTICLE_CHANCE = 0.3D;
    protected static final float CRAFTING_PARTICLE_VARIANCE = 0.25F;

    public AbstractPedestalRenderer(BlockEntityRendererProvider.Context context) {
    }

    /**
     * Main rendering function
     */
    @Override
    public void render(T pedestalBlockEntity, float partialTicks, PoseStack poseStack, MultiBufferSource buffer, int combinedLightIn, int combinedOverlayIn) {
        PedestalBlockState state = pedestalBlockEntity.getBlockState().getValue(PedestalBlock.PEDESTAL_STATE);
        switch (state) {
            case INACTIVE:
                this.renderInactivePedestal(pedestalBlockEntity, partialTicks, poseStack, buffer, combinedLightIn, combinedOverlayIn);
                break;
            case PREPARING:
                this.renderPreparingPedestal(pedestalBlockEntity, partialTicks, poseStack, buffer, combinedLightIn, combinedOverlayIn);
                break;
            case CRAFTING:
                this.renderCraftingPedestal(pedestalBlockEntity, partialTicks, poseStack, buffer, combinedLightIn, combinedOverlayIn);
                break;
            case END:
                this.renderEndPedestal(pedestalBlockEntity, partialTicks, poseStack, buffer, combinedLightIn, combinedOverlayIn);
                break;
        }
    }

    protected void renderEndPedestal(T pedestalBlockEntity, float partialTicks, PoseStack poseStack, MultiBufferSource buffer, int combinedLightIn, int combinedOverlayIn) {
        poseStack.pushPose();

        RendererUtil.renderSolidModel(pedestalBlockEntity, partialTicks, poseStack, buffer, combinedLightIn, combinedOverlayIn);
        this.renderRunes(pedestalBlockEntity, PedestalRunesState.END, partialTicks, poseStack, buffer, combinedLightIn, combinedOverlayIn);

        poseStack.popPose();
    }

    protected void renderCraftingPedestal(T pedestalBlockEntity, float partialTicks, PoseStack poseStack, MultiBufferSource buffer, int combinedLightIn, int combinedOverlayIn) {
        poseStack.pushPose();

        RendererUtil.renderSolidModel(pedestalBlockEntity, partialTicks, poseStack, buffer, combinedLightIn, combinedOverlayIn);
        this.renderRunes(pedestalBlockEntity, PedestalRunesState.CRAFTING, partialTicks, poseStack, buffer, combinedLightIn, combinedOverlayIn);
        this.renderCraftingParticles(pedestalBlockEntity);
        this.renderItem(pedestalBlockEntity, partialTicks, poseStack, buffer, combinedLightIn, combinedOverlayIn);

        poseStack.popPose();
    }

    protected void renderPreparingPedestal(T pedestalBlockEntity, float partialTicks, PoseStack poseStack, MultiBufferSource buffer, int combinedLightIn, int combinedOverlayIn) {
        poseStack.pushPose();

        RendererUtil.renderSolidModel(pedestalBlockEntity, partialTicks, poseStack, buffer, combinedLightIn, combinedOverlayIn);
        if (pedestalBlockEntity.getLevel().getGameTime() >= pedestalBlockEntity.getLastStateChangeTicks() + pedestalBlockEntity.getActivateParticleTicks()) {
            this.renderRunes(pedestalBlockEntity, PedestalRunesState.PREPARING, partialTicks, poseStack, buffer, combinedLightIn, combinedOverlayIn);
        }
        this.renderItem(pedestalBlockEntity, partialTicks, poseStack, buffer, combinedLightIn, combinedOverlayIn);

        poseStack.popPose();
    }

    protected void renderInactivePedestal(T pedestalBlockEntity, float partialTicks, PoseStack poseStack, MultiBufferSource buffer, int combinedLightIn, int combinedOverlayIn) {
        poseStack.pushPose();

        RendererUtil.renderSolidModel(pedestalBlockEntity, partialTicks, poseStack, buffer, combinedLightIn, combinedOverlayIn);
        this.renderItem(pedestalBlockEntity, partialTicks, poseStack, buffer, combinedLightIn, combinedOverlayIn);

        poseStack.popPose();
    }

    protected void renderCraftingParticles(T pedestalBlockEntity) {
        if (!pedestalBlockEntity.getActiveInfusionPedestal().isPresent()) {
            return;
        }

        Random random = new Random();
        if (random.nextDouble() >= CRAFTING_PARTICLE_CHANCE) {
            return;
        }
        Level level = pedestalBlockEntity.getLevel();

        Vec3 particleStartCentre = BlockPosUtil.getPosBottomCentre(pedestalBlockEntity.getBlockPos()).add(0D, ITEM_FLOAT_HEIGHT + BLOCK_HEIGHT * BLOCK_RENDER_SCALE, 0D);
        Vec3 particleEndCentre = BlockPosUtil.getPosBottomCentre(pedestalBlockEntity.getActiveInfusionPedestal().get()).add(0D, ITEM_FLOAT_HEIGHT + BLOCK_HEIGHT * BLOCK_RENDER_SCALE, 0D);

        Vec3 start1 = particleStartCentre.add(particleEndCentre.subtract(particleStartCentre).scale(0.25F));
        Vec3 end1 = particleEndCentre.add(particleStartCentre.subtract(particleEndCentre).scale(0.25F));

        Vec3 start0 = particleStartCentre.add(new Vec3(MathUtil.getInRandom(random, CRAFTING_PARTICLE_VARIANCE), MathUtil.getInRandom(random, CRAFTING_PARTICLE_VARIANCE), MathUtil.getInRandom(random, CRAFTING_PARTICLE_VARIANCE)));
        Vec3 end0 = particleEndCentre.add(new Vec3(MathUtil.getInRandom(random, CRAFTING_PARTICLE_VARIANCE), MathUtil.getInRandom(random, CRAFTING_PARTICLE_VARIANCE), MathUtil.getInRandom(random, CRAFTING_PARTICLE_VARIANCE)));
        level.addParticle(new InfusionPedestalCraftParticleData(ModParticleTypes.INFUSION_PEDESTAL_CRAFT_PARTICLE.get(), pedestalBlockEntity.getHeldItem(), start0, start1, end1, end0), start0.x, start0.y, start0.z, 0D, 0D, 0D);
    }

    protected void renderRunes(T pedestalBlockEntity, PedestalRunesState pedestalRunesState, float partialTicks, PoseStack poseStack, MultiBufferSource buffer, int combinedLightIn, int combinedOverlayIn) {
        BlockPos pos = pedestalBlockEntity.getBlockPos();
        Level level = pedestalBlockEntity.getLevel();

        Vec3 position = new Vec3(0.5D, 0.01D, 0.5D);
        float tick = (level.getGameTime() + partialTicks) / RUNES_TIME_SCALE;

        poseStack.pushPose();
        poseStack.translate(position.x, position.y, position.z);
        poseStack.mulPose(Axis.YP.rotationDegrees(tick));
        poseStack.scale(RUNES_SIZE_SCALE, RUNES_SIZE_SCALE, RUNES_SIZE_SCALE);
        poseStack.translate(-position.x, 0.0D, -position.z);

        BlockState state = ModBlocks.PEDESTAL_RUNES.get().defaultBlockState().setValue(PedestalRunesBlock.PEDESTAL_STATE, pedestalRunesState);

        RendererUtil.renderModel(level, pos, state, Sheets.cutoutBlockSheet(), partialTicks, poseStack, buffer, combinedLightIn, combinedOverlayIn);

        poseStack.popPose();

        Random random = new Random();
        if (random.nextDouble() >= RUNES_PARTICLE_CHANCE) {
            return;
        }

        double theta = Math.toRadians(random.nextDouble() * 360D);
        double r = MathUtil.getInRandom(random, MIN_RUNES_PARTICLE_DIST, MAX_RUNES_PARTICLE_DIST);
        Vec3 particlePosition = new Vec3(pos.getX(), pos.getY(), pos.getZ()).add(position).add(Math.cos(theta) * r, 0.0D, Math.sin(theta) * r);
        level.addParticle(new InfusionPedestalRunesParticleData(pedestalRunesState.getRed(), pedestalRunesState.getGreen(), pedestalRunesState.getBlue()), particlePosition.x, particlePosition.y, particlePosition.z, 0D, 0.025D, 0D);
    }

    protected void renderItem(T pedestalBlockEntity, float partialTicks, PoseStack poseStack, MultiBufferSource buffer, int combinedLightIn, int combinedOverlayIn) {
        ItemStack item = pedestalBlockEntity.getHeldItem();
        Level level = pedestalBlockEntity.getLevel();

        if (item.isEmpty()) {
            return;
        }


        poseStack.pushPose();

        float tick = (level.getGameTime() + partialTicks) / TIME_SCALE;

        Vec3 initialPosition = this.getItemPosition(tick, item.getItem());
        float scale = this.getItemScale(item.getItem());

        poseStack.translate(initialPosition.x, initialPosition.y, initialPosition.z);
        poseStack.mulPose(Axis.YP.rotationDegrees(tick));
        poseStack.scale(scale, scale, scale);

        Minecraft.getInstance().getItemRenderer().renderStatic(item, ItemDisplayContext.FIXED, combinedLightIn, combinedOverlayIn, poseStack, buffer, level, /* ??? */ 0);

        poseStack.popPose();

    }

    protected Vec3 getItemPosition(float tick, Item item) {
        double x = 0.5D;
        double y = ITEM_FLOAT_HEIGHT + (item instanceof BlockItem ? BLOCK_HEIGHT : ITEM_HEIGHT) + Math.sin(Math.toRadians(tick)) * ITEM_FLOAT_AMPLITUDE;
        double z = 0.5D;
        return new Vec3(x, y, z);
    }

    protected float getItemScale(Item item) {
        return item instanceof BlockItem ? BLOCK_RENDER_SCALE : ITEM_RENDER_SCALE;
    }
}
