package com.lickbread.mesoamericamythology.client.renderer.blockEntity;

import com.lickbread.mesoamericamythology.blockEntities.BloodBasinBlockEntity;
import com.lickbread.mesoamericamythology.util.RendererUtil;
import com.mojang.blaze3d.vertex.PoseStack;
import net.minecraft.client.renderer.MultiBufferSource;
import net.minecraft.client.renderer.blockentity.BlockEntityRendererProvider;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;

@OnlyIn(Dist.CLIENT)
public class BloodBasinRenderer extends AbstractBloodBasinRenderer<BloodBasinBlockEntity> {

    public BloodBasinRenderer(BlockEntityRendererProvider.Context context) {
        super(context);
    }

    @Override
    public void render(BloodBasinBlockEntity blockEntity, float partialTicks, PoseStack poseStack, MultiBufferSource buffer, int combinedLightIn, int combinedOverlayIn) {
        super.render(blockEntity, partialTicks, poseStack, buffer, combinedLightIn, combinedOverlayIn);
        poseStack.pushPose();

        RendererUtil.renderSolidModel(blockEntity, partialTicks, poseStack, buffer, combinedLightIn, combinedOverlayIn);

        poseStack.popPose();
    }

    @Override
    protected float getBasinTopY() {
        return -0.5F;
    }
}
