package com.lickbread.mesoamericamythology.client.renderer.blockEntity;

import com.lickbread.mesoamericamythology.blockEntities.GodStatueHeadBlockEntity;
import com.lickbread.mesoamericamythology.util.RendererUtil;
import com.mojang.blaze3d.vertex.PoseStack;
import net.minecraft.client.renderer.MultiBufferSource;
import net.minecraft.client.renderer.blockentity.BlockEntityRenderer;
import net.minecraft.client.renderer.blockentity.BlockEntityRendererProvider;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;

@OnlyIn(Dist.CLIENT)
public class GodStatueHeadRenderer implements BlockEntityRenderer<GodStatueHeadBlockEntity> {

    public GodStatueHeadRenderer(BlockEntityRendererProvider.Context context) {
    }

    /**
     * Main rendering function
     */
    @Override
    public void render(GodStatueHeadBlockEntity godStatueHeadBlockEntity, float partialTicks, PoseStack poseStack, MultiBufferSource buffer, int combinedLightIn, int combinedOverlayIn) {
        RendererUtil.renderSolidModel(godStatueHeadBlockEntity, partialTicks, poseStack, buffer, combinedLightIn, combinedOverlayIn);
    }

}


