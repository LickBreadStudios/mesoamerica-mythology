package com.lickbread.mesoamericamythology.client.renderer.blockEntity;

import com.lickbread.mesoamericamythology.api.recipes.outputs.IRecipeOutput;
import com.lickbread.mesoamericamythology.api.recipes.outputs.ItemStackRecipeOutput;
import com.lickbread.mesoamericamythology.api.recipes.outputs.PerkRecipeOutput;
import com.lickbread.mesoamericamythology.blockEntities.InfusionPedestalBlockEntity;
import com.lickbread.mesoamericamythology.blockEntities.PowerPedestalBlockEntity;
import com.lickbread.mesoamericamythology.blockEntities.SacrificePedestalBlockEntity;
import com.lickbread.mesoamericamythology.blocks.altar.PedestalBlock;
import com.lickbread.mesoamericamythology.blocks.state.PedestalBlockState;
import com.lickbread.mesoamericamythology.blocks.state.PedestalRunesState;
import com.lickbread.mesoamericamythology.particle.AltarBreakItemParticleData;
import com.lickbread.mesoamericamythology.particle.InfusionPedestalActivateParticleData;
import com.lickbread.mesoamericamythology.registry.ModParticleTypes;
import com.lickbread.mesoamericamythology.util.BlockPosUtil;
import com.lickbread.mesoamericamythology.util.MathUtil;
import com.lickbread.mesoamericamythology.util.RendererUtil;
import com.mojang.blaze3d.vertex.PoseStack;
import net.minecraft.client.renderer.MultiBufferSource;
import net.minecraft.client.renderer.blockentity.BlockEntityRendererProvider;
import net.minecraft.core.BlockPos;
import net.minecraft.world.item.BlockItem;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.phys.Vec3;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;

import java.util.Random;

import static com.lickbread.mesoamericamythology.blockEntities.InfusionPedestalBlockEntity.CraftingStage.FINISH;

@OnlyIn(Dist.CLIENT)
public class InfusionPedestalRenderer extends AbstractPedestalRenderer<InfusionPedestalBlockEntity> {

    private static final double PARTICLE_ARC_HEIGHT = 2.0D;
    private static final int MIN_ACTIVATE_PARTICLES = 3;
    private static final int MAX_ACTIVATE_PARTICLES = 10;
    private static final int PARTICLE_AGE_VARIANCE = 3;
    private static final int MIN_COMPLETE_PARTICLES = 15;
    private static final int MAX_COMPLETE_PARTICLES = 25;
    private static final double MIN_COMPLETE_PARTICLE_DISTANCE = 1.5D;
    private static final double MAX_COMPLETE_PARTICLE_DISTANCE = 3.0D;

    public InfusionPedestalRenderer(BlockEntityRendererProvider.Context context) {
        super(context);
    }

    @Override
    public void render(InfusionPedestalBlockEntity pedestalBlockEntity, float partialTicks, PoseStack poseStack, MultiBufferSource buffer, int combinedLightIn, int combinedOverlayIn) {
        PedestalBlockState state = pedestalBlockEntity.getBlockState().getValue(PedestalBlock.PEDESTAL_STATE);
        switch (state) {
            case PREPARING:
                if (pedestalBlockEntity.getCraftingStageTicks() == 2) {
                    this.spawnActivateParticles(pedestalBlockEntity);
                }
                break;
            case END:
                if (pedestalBlockEntity.getCraftingStageTicks() == 2) {
                    this.spawnEndParticles(pedestalBlockEntity);
                }
                break;
        }
        super.render(pedestalBlockEntity, partialTicks, poseStack, buffer, combinedLightIn, combinedOverlayIn);
    }

    private void spawnEndParticles(InfusionPedestalBlockEntity pedestalBlockEntity) {
        Random random = new Random();
        Level level = pedestalBlockEntity.getLevel();

        Vec3 heldItemBlockCentre = BlockPosUtil.getPosTopCentre(pedestalBlockEntity.getBlockPos()).add(0.0D, ITEM_FLOAT_HEIGHT + (pedestalBlockEntity.getHeldItem().getItem() instanceof BlockItem ? BLOCK_HEIGHT : ITEM_HEIGHT), 0.0D);
        int heldItemParticles = MathUtil.getInRandom(random, MIN_COMPLETE_PARTICLES, MAX_COMPLETE_PARTICLES);
        for (int i = 0; i < heldItemParticles; i++) {
            Vec3 end = heldItemBlockCentre.add(MathUtil.getRandomPointInSphere().scale(MathUtil.getInRandom(random, MIN_COMPLETE_PARTICLE_DISTANCE, MAX_COMPLETE_PARTICLE_DISTANCE)));
            Vec3 velocity = end.subtract(heldItemBlockCentre).scale(1D/FINISH.getStageTicks());
            level.addParticle(new AltarBreakItemParticleData(ModParticleTypes.ALTAR_BREAK_ITEM_PARTICLE.get(), end, pedestalBlockEntity.getHeldItem()), heldItemBlockCentre.x, heldItemBlockCentre.y, heldItemBlockCentre.z, velocity.x, velocity.y, velocity.z);
        }

        IRecipeOutput output = pedestalBlockEntity.getRecipe().get().getOutput();
        if (output instanceof ItemStackRecipeOutput) {
            this.renderItemStackRecipeOutputParticles(pedestalBlockEntity, ((ItemStackRecipeOutput) output).getOutput());
        } else if (output instanceof PerkRecipeOutput) {
            this.renderPerkRecipeOutputParticles(pedestalBlockEntity);
        }
    }
    private void renderPerkRecipeOutputParticles(InfusionPedestalBlockEntity pedestalBlockEntity) {
        // TODO
    }

    private void renderItemStackRecipeOutputParticles(InfusionPedestalBlockEntity pedestalBlockEntity, ItemStack itemStack) {
        Random random = new Random();
        Level level = pedestalBlockEntity.getLevel();

        Vec3 outputItemBlockCentre = BlockPosUtil.getPosTopCentre(pedestalBlockEntity.getBlockPos()).add(0.0D, ITEM_FLOAT_HEIGHT + (itemStack.getItem() instanceof BlockItem ? BLOCK_HEIGHT : ITEM_HEIGHT), 0.0D);
        int createItemParticles = MathUtil.getInRandom(random, MIN_COMPLETE_PARTICLES, MAX_COMPLETE_PARTICLES);
        for (int i = 0; i < createItemParticles; i++) {
            Vec3 start = outputItemBlockCentre.add(MathUtil.getRandomPointInSphere().scale(random.nextBoolean() ? 1 : -1 * MathUtil.getInRandom(random, MIN_COMPLETE_PARTICLE_DISTANCE, MAX_COMPLETE_PARTICLE_DISTANCE)));
            Vec3 velocity = outputItemBlockCentre.subtract(start).scale(1D / FINISH.getStageTicks());
            level.addParticle(new AltarBreakItemParticleData(ModParticleTypes.ALTAR_BREAK_ITEM_PARTICLE.get(), outputItemBlockCentre, itemStack), start.x, start.y, start.z, velocity.x, velocity.y, velocity.z);
        }
    }

    private void spawnActivateParticles(InfusionPedestalBlockEntity pedestalBlockEntity) {
        Level level = pedestalBlockEntity.getLevel();
        Vec3 start0 = BlockPosUtil.getPosTopCentre(pedestalBlockEntity.getBlockPos());
        Vec3 start1 = start0.add(0D, PARTICLE_ARC_HEIGHT, 0D);

        pedestalBlockEntity.getRecipe().ifPresent(recipe -> {
            recipe.getUsableSacrificePedestals().forEach(pos -> {
                BlockEntity blockEntity = level.getBlockEntity(pos);
                if (blockEntity instanceof SacrificePedestalBlockEntity) {
                    int lifetime = ((SacrificePedestalBlockEntity) blockEntity).getActivateParticleTicks();
                    this.spawnActivateParticles(level, start0, start1, blockEntity.getBlockPos(), lifetime);
                }
            });
            recipe.getUsablePowerPedestals().forEach(pos -> {
                BlockEntity blockEntity = level.getBlockEntity(pos);
                if (blockEntity instanceof PowerPedestalBlockEntity) {
                    int lifetime = ((PowerPedestalBlockEntity) blockEntity).getActivateParticleTicks();
                    this.spawnActivateParticles(level, start0, start1, blockEntity.getBlockPos(), lifetime);
                }
            });
        });
    }

    private void spawnActivateParticles(Level level, Vec3 start0, Vec3 start1, BlockPos pos, int lifetime) {
        Random random = new Random();
        Vec3 centerTop = BlockPosUtil.getPosTopCentre(pos);
        int particles = MathUtil.getInRandom(random, MIN_ACTIVATE_PARTICLES, MAX_ACTIVATE_PARTICLES);
        for (int i = 0; i < particles; i++) {
            int age = lifetime + MathUtil.getInRandom(random, PARTICLE_AGE_VARIANCE);
            Vec3 end0 = centerTop.add(MathUtil.getInRandom(random, 0.5D), 0D, MathUtil.getInRandom(random, 0.5D));
            Vec3 end1 = end0.add(0D, PARTICLE_ARC_HEIGHT, 0D);
            level.addParticle(new InfusionPedestalActivateParticleData(start0, start1, end1, end0, age), start0.x, start0.y, start0.z, 0D, 0D, 0D);
        }
    }

    @Override
    protected void renderEndPedestal(InfusionPedestalBlockEntity pedestalBlockEntity, float partialTicks, PoseStack poseStack, MultiBufferSource buffer, int combinedLightIn, int combinedOverlayIn) {
        poseStack.pushPose();

        RendererUtil.renderSolidModel(pedestalBlockEntity, partialTicks, poseStack, buffer, combinedLightIn, combinedOverlayIn);
        this.renderRunes(pedestalBlockEntity, PedestalRunesState.END, partialTicks, poseStack, buffer, combinedLightIn, combinedOverlayIn);
        this.renderItem(pedestalBlockEntity, partialTicks, poseStack, buffer, combinedLightIn, combinedOverlayIn); // Make sure to render item

        poseStack.popPose();
    }

    @Override
    protected void renderCraftingParticles(InfusionPedestalBlockEntity pedestalBlockEntity) {
        // Intentionally do nothing
    }
}
