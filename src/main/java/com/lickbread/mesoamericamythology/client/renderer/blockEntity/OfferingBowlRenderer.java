package com.lickbread.mesoamericamythology.client.renderer.blockEntity;

import com.lickbread.mesoamericamythology.blockEntities.OfferingBowlBlockEntity;
import com.lickbread.mesoamericamythology.util.RendererUtil;
import com.mojang.blaze3d.vertex.PoseStack;
import com.mojang.math.Axis;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.MultiBufferSource;
import net.minecraft.client.renderer.blockentity.BlockEntityRenderer;
import net.minecraft.client.renderer.blockentity.BlockEntityRendererProvider;
import net.minecraft.world.item.BlockItem;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemDisplayContext;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.Level;
import net.minecraft.world.phys.Vec3;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;

@OnlyIn(Dist.CLIENT)
public class OfferingBowlRenderer implements BlockEntityRenderer<OfferingBowlBlockEntity> {

    protected static final float TIME_SCALE = 1F;

    protected static final double ITEM_HEIGHT = 0.125D;
    protected static final double BLOCK_HEIGHT = 0.1D;

    protected static final float ITEM_RENDER_SCALE = 0.50F;
    protected static final float BLOCK_RENDER_SCALE = 0.85F;

    protected static final float ITEM_FLOAT_HEIGHT = 0.20F;
    protected static final float ITEM_FLOAT_AMPLITUDE = 0.05F;

    public OfferingBowlRenderer(BlockEntityRendererProvider.Context context) {
    }

    /**
     * Main rendering function
     */
    @Override
    public void render(OfferingBowlBlockEntity offeringBowlBlockEntity, float partialTicks, PoseStack poseStack, MultiBufferSource buffer, int combinedLightIn, int combinedOverlayIn) {
        poseStack.pushPose();

        RendererUtil.renderSolidModel(offeringBowlBlockEntity, partialTicks, poseStack, buffer, combinedLightIn, combinedOverlayIn);
        this.renderItem(offeringBowlBlockEntity, partialTicks, poseStack, buffer, combinedLightIn, combinedOverlayIn);

        poseStack.popPose();
    }

    protected void renderItem(OfferingBowlBlockEntity offeringBowlBlockEntity, float partialTicks, PoseStack poseStack, MultiBufferSource buffer, int combinedLightIn, int combinedOverlayIn) {
        ItemStack item = offeringBowlBlockEntity.getHeldItem();
        Level level = offeringBowlBlockEntity.getLevel();

        if (item.isEmpty()) {
            return;
        }

        poseStack.pushPose();

        float tick = (level.getGameTime() + partialTicks) / TIME_SCALE;

        Vec3 initialPosition = this.getItemPosition(tick, item.getItem());
        float scale = this.getItemScale(item.getItem());

        poseStack.translate(initialPosition.x, initialPosition.y, initialPosition.z);
        poseStack.mulPose(Axis.YP.rotationDegrees(tick));
        poseStack.scale(scale, scale, scale);

        Minecraft.getInstance().getItemRenderer().renderStatic(item, ItemDisplayContext.FIXED, combinedLightIn, combinedOverlayIn, poseStack, buffer, level, /* ??? */ 0);

        poseStack.popPose();
    }

    protected Vec3 getItemPosition(float tick, Item item) {
        double x = 0.5D;
        double y = ITEM_FLOAT_HEIGHT + (item instanceof BlockItem ? BLOCK_HEIGHT : ITEM_HEIGHT) + Math.sin(Math.toRadians(tick)) * ITEM_FLOAT_AMPLITUDE;
        double z = 0.5D;
        return new Vec3(x, y, z);
    }

    protected float getItemScale(Item item) {
        return item instanceof BlockItem ? BLOCK_RENDER_SCALE : ITEM_RENDER_SCALE;
    }
}

