package com.lickbread.mesoamericamythology.client.renderer.blockEntity;

import com.lickbread.mesoamericamythology.blockEntities.PedestalBlockEntity;
import net.minecraft.client.renderer.blockentity.BlockEntityRendererProvider;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;

@OnlyIn(Dist.CLIENT)
public class PedestalRenderer extends AbstractPedestalRenderer<PedestalBlockEntity> {

    public PedestalRenderer(BlockEntityRendererProvider.Context context) {
        super(context);
    }

}
