package com.lickbread.mesoamericamythology.client.renderer.blockEntity;

import com.lickbread.mesoamericamythology.blockEntities.PowerPedestalBlockEntity;
import net.minecraft.client.renderer.blockentity.BlockEntityRendererProvider;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;

@OnlyIn(Dist.CLIENT)
public class PowerPedestalRenderer extends AbstractPedestalRenderer<PowerPedestalBlockEntity> {

    public PowerPedestalRenderer(BlockEntityRendererProvider.Context context) {
        super(context);
    }

}
