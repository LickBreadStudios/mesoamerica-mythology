package com.lickbread.mesoamericamythology.client.renderer.blockEntity;

import com.lickbread.mesoamericamythology.blockEntities.SacrificePedestalBlockEntity;
import com.mojang.blaze3d.vertex.PoseStack;
import com.mojang.math.Axis;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.MultiBufferSource;
import net.minecraft.client.renderer.blockentity.BlockEntityRendererProvider;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.level.Level;
import net.minecraft.world.phys.Vec3;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;

import java.util.Optional;

@OnlyIn(Dist.CLIENT)
public class SacrificePedestalRenderer extends AbstractPedestalRenderer<SacrificePedestalBlockEntity> {

    private static final float ENTITY_SCALE = 0.5F;
    private static final float ENTITY_HEIGHT = -0.1F;

    public SacrificePedestalRenderer(BlockEntityRendererProvider.Context context) {
        super(context);
    }

    @Override
    public void render(SacrificePedestalBlockEntity pedestalBlockEntity, float partialTicks, PoseStack poseStack, MultiBufferSource buffer, int combinedLightIn, int combinedOverlayIn) {
        this.renderEntity(pedestalBlockEntity, partialTicks, poseStack, buffer, combinedLightIn, combinedOverlayIn);
        super.render(pedestalBlockEntity, partialTicks, poseStack, buffer, combinedLightIn, combinedOverlayIn);
    }

    /**
     * Actually renders the entity
     */
    protected void renderEntity(SacrificePedestalBlockEntity pedestalBlockEntity, float partialTicks, PoseStack poseStack, MultiBufferSource buffer, int combinedLightIn, int combinedOverlayIn) {
        if (!pedestalBlockEntity.hasEntity() || !pedestalBlockEntity.getEntity().isPresent()) {
            return;
        }

        Optional<LivingEntity> heldEntity = pedestalBlockEntity.getEntity();
        Level level = pedestalBlockEntity.getLevel();

        poseStack.pushPose();

        float tick = (level.getGameTime() + partialTicks) / TIME_SCALE;

        Vec3 initialPosition = this.getEntityPosition(tick);
        float scale = ENTITY_SCALE;

        poseStack.translate(initialPosition.x, initialPosition.y, initialPosition.z);
        poseStack.mulPose(Axis.YP.rotationDegrees(tick));
        poseStack.scale(scale, scale, scale);

        Minecraft.getInstance().getEntityRenderDispatcher().render(heldEntity.get(), 0D, 0D, 0D, 0, 0, poseStack, buffer, combinedLightIn);

        poseStack.popPose();

    }

    protected Vec3 getEntityPosition(float tick) {
        double x = 0.5D;
        double y = ITEM_FLOAT_HEIGHT + Math.sin(Math.toRadians(tick)) * ITEM_FLOAT_AMPLITUDE + ENTITY_HEIGHT;
        double z = 0.5D;
        return new Vec3(x, y, z);
    }

}
