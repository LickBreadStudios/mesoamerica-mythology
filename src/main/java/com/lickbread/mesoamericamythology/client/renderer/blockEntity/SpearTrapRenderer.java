package com.lickbread.mesoamericamythology.client.renderer.blockEntity;

import com.lickbread.mesoamericamythology.blockEntities.SpearTrapBlockEntity;
import com.lickbread.mesoamericamythology.blocks.traps.SpearTrapBlock;
import com.lickbread.mesoamericamythology.registry.ModBlocks;
import com.lickbread.mesoamericamythology.util.RendererUtil;
import com.mojang.blaze3d.vertex.PoseStack;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.MultiBufferSource;
import net.minecraft.client.renderer.Sheets;
import net.minecraft.client.renderer.block.BlockRenderDispatcher;
import net.minecraft.client.renderer.blockentity.BlockEntityRenderer;
import net.minecraft.client.renderer.blockentity.BlockEntityRendererProvider;
import net.minecraft.core.BlockPos;
import net.minecraft.util.RandomSource;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.phys.Vec3;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;

import java.time.ZonedDateTime;

import static com.lickbread.mesoamericamythology.util.MathUtil.convertVector3i;

@OnlyIn(Dist.CLIENT)
public class SpearTrapRenderer implements BlockEntityRenderer<SpearTrapBlockEntity> {

    private static final double SPEAR_DISTANCE = 3D;
    private static final double ANIMATION_MILLIS = 100D;

    public SpearTrapRenderer(BlockEntityRendererProvider.Context context) {
    }

    @Override
    public void render(SpearTrapBlockEntity trapBlockEntity, float partialTicks, PoseStack poseStack, MultiBufferSource buffer, int combinedLightIn, int combinedOverlayIn) {
        RendererUtil.renderSolidModel(trapBlockEntity, partialTicks, poseStack, buffer, combinedLightIn, combinedOverlayIn);
        this.renderSpikes(trapBlockEntity, partialTicks, poseStack, buffer, combinedLightIn, combinedOverlayIn);
    }

    private void renderSpikes(SpearTrapBlockEntity trapBlockEntity, float partialTicks, PoseStack poseStack, MultiBufferSource buffer, int combinedLightIn, int combinedOverlayIn) {
        double scale;
        if (trapBlockEntity.isSetLastActivatedTime() && !trapBlockEntity.isSetLastInactivatedTime()) {
            scale = Math.min(ZonedDateTime.now().toInstant().toEpochMilli() - trapBlockEntity.getLastActivatedTime().toInstant().toEpochMilli(), ANIMATION_MILLIS) / ANIMATION_MILLIS;
        } else if (!trapBlockEntity.isSetLastActivatedTime() && trapBlockEntity.isSetLastInactivatedTime()) {
            if (ZonedDateTime.now().toInstant().toEpochMilli() - trapBlockEntity.getLastInactivatedTime().toInstant().toEpochMilli() > ANIMATION_MILLIS) {
                scale = 0D;
            } else {
                scale = 1D - Math.min(ZonedDateTime.now().toInstant().toEpochMilli() - trapBlockEntity.getLastInactivatedTime().toInstant().toEpochMilli(), ANIMATION_MILLIS) / ANIMATION_MILLIS;
            }
        } else { // If either are true, or both aren't scale 0;
            scale = 0D;
        }

        if (scale == 0D) {
            return;
        }

        BlockState state = trapBlockEntity.getBlockState();
        Level level = trapBlockEntity.getLevel();
        BlockPos pos = trapBlockEntity.getBlockPos().offset(state.getValue(SpearTrapBlock.FACING).getNormal());

        Vec3 Vec3 = convertVector3i(state.getValue(SpearTrapBlock.FACING).getNormal())
                .scale(scale)
                .scale(SPEAR_DISTANCE)
                .subtract(convertVector3i(state.getValue(SpearTrapBlock.FACING).getNormal()));

        poseStack.pushPose();
        poseStack.translate(Vec3.x, Vec3.y, Vec3.z);

        BlockRenderDispatcher blockRenderer = Minecraft.getInstance().getBlockRenderer();
        BlockState spearState = ModBlocks.SPEAR.get().defaultBlockState().setValue(SpearTrapBlock.FACING, state.getValue(SpearTrapBlock.FACING));
        blockRenderer.getModelRenderer().tesselateBlock(level, blockRenderer.getBlockModel(spearState), state, pos, poseStack, buffer.getBuffer(Sheets.cutoutBlockSheet()), false, RandomSource.create(), combinedLightIn, combinedOverlayIn);

        poseStack.popPose();

    }
}
