package com.lickbread.mesoamericamythology.client.renderer.blockEntity;

import com.lickbread.mesoamericamythology.api.recipes.altar.StoneAltarRecipe;
import com.lickbread.mesoamericamythology.blockEntities.StoneAltarBlockEntity;
import com.lickbread.mesoamericamythology.blocks.altar.StoneAltarBlock;
import com.lickbread.mesoamericamythology.blocks.state.AltarBlockState;
import com.lickbread.mesoamericamythology.constants.BloodLimits;
import com.lickbread.mesoamericamythology.particle.AltarBreakItemParticleData;
import com.lickbread.mesoamericamythology.particle.DeathBloodParticleData;
import com.lickbread.mesoamericamythology.registry.ModBlocks;
import com.lickbread.mesoamericamythology.registry.ModParticleTypes;
import com.lickbread.mesoamericamythology.util.MathUtil;
import com.lickbread.mesoamericamythology.util.RendererUtil;
import com.mojang.blaze3d.platform.Lighting;
import com.mojang.blaze3d.vertex.PoseStack;
import com.mojang.math.Axis;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.MultiBufferSource;
import net.minecraft.client.renderer.blockentity.BlockEntityRendererProvider;
import net.minecraft.world.item.BlockItem;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemDisplayContext;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.phys.Vec3;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;

import java.util.List;
import java.util.Optional;
import java.util.Random;

import static com.lickbread.mesoamericamythology.util.BlockPosUtil.toVec3;

@OnlyIn(Dist.CLIENT)
public class StoneAltarRenderer extends AbstractBloodBasinRenderer<StoneAltarBlockEntity> {

    private static final float MAX_BLOOD_SPHERE_SCALE = 0.9F;
    private static final float MIN_BLOOD_SPHERE_SCALE = 0.3F;
    private static final float MAX_BLOOD_SPHERE_ROTATION_AMOUNT = 0.75F;
    private static final float MIN_BLOOD_SPHERE_ROTATION_AMOUNT = 0.25F;

    // Crafting animation
    private static final float MAX_DEGREES_ROTATION_AROUND_CENTER_PER_TICK = 5F;
    private static final float MAX_DEGREES_ROTATION_PER_TICK = -MAX_DEGREES_ROTATION_AROUND_CENTER_PER_TICK;
    private static final float PERCENTAGE_CRAFTING_TO_REACH_MIDDLE = 0.5F;
    private static final float TICKS_CRAFTING_TO_REACH_MIDDLE = PERCENTAGE_CRAFTING_TO_REACH_MIDDLE * StoneAltarBlockEntity.CraftingStage.totalTicks();
    private static final double CRAFTING_PARTICLE_CHANCE = 0.25D;

    private static final int MAX_BLOOD_TRANSFER_PARTICLES = 10;
    private static final int MIN_BLOOD_TRANSFER_PARTICLES = 5;
    private static final double BLOOD_TRANSFER_PARTICLE_CHANCE = 0.1D;

    private static final double BLOOD_DRIP_PARTICLE_CHANCE = 0.01D;

    private static final double ITEM_HEIGHT = 0.01D;
    private static final double BLOCK_HEIGHT = 0.072D;

    private static final double ITEM_RENDER_DISTANCE = 0.35D;
    private static final double EXTRA_ITEM_RENDER_DISTANCE = 1.0D;

    private static final float ITEM_RENDER_SCALE = 0.20F;
    private static final float BLOCK_RENDER_SCALE = 0.30F;
    public static final double CRAFTING_PARTICLE_SPEED_SCALE = 0.15D;

    public StoneAltarRenderer(BlockEntityRendererProvider.Context context) {
        super(context);
    }

    /**
     * Main rendering function
     */
    @Override
    public void render(StoneAltarBlockEntity stoneAltarBlockEntity, float partialTicks, PoseStack poseStack, MultiBufferSource buffer, int combinedLightIn, int combinedOverlayIn) {
        super.render(stoneAltarBlockEntity, partialTicks, poseStack, buffer, combinedLightIn, combinedOverlayIn);
        poseStack.pushPose();

        RendererUtil.renderSolidModel(stoneAltarBlockEntity, partialTicks, poseStack, buffer, combinedLightIn, combinedOverlayIn);
        if (stoneAltarBlockEntity.getBloodPoolAmount() > 0) {
            // TODO should I render blood particles?
        }
        this.renderItems(stoneAltarBlockEntity, partialTicks, poseStack, buffer, combinedLightIn, combinedOverlayIn);
        if (stoneAltarBlockEntity.isCrafting()) {
            Optional<StoneAltarRecipe> recipe = StoneAltarRecipe.getRecipe(stoneAltarBlockEntity, false);
            this.renderInstability(stoneAltarBlockEntity, recipe, poseStack, buffer, combinedLightIn, combinedOverlayIn);
            this.renderBloodSphere(stoneAltarBlockEntity, partialTicks, poseStack, buffer, combinedLightIn, combinedOverlayIn);
        }

        poseStack.popPose();
    }

    @Override
    protected float getBasinTopY() {
        return 0F;
    }

    private void renderInstability(StoneAltarBlockEntity stoneAltarBlockEntity, Optional<StoneAltarRecipe> recipe, PoseStack poseStack, MultiBufferSource buffer, int combinedLightIn, int combinedOverlayIn) {

    }

    private void renderBloodSphere(StoneAltarBlockEntity stoneAltarBlockEntity, float partialTicks, PoseStack poseStack, MultiBufferSource buffer, int combinedLightIn, int combinedOverlayIn) {
        poseStack.pushPose();

        Level level = stoneAltarBlockEntity.getLevel();
        Random rand = new Random();

        float currentTick = stoneAltarBlockEntity.getCraftingTick() + partialTicks;
        float tickPercentage = Math.min(currentTick / (float) StoneAltarBlockEntity.CraftingStage.CRAFT.getTotalStageTicks(), 1.0F);
        float bloodPoolFillPercentage = tickPercentage * stoneAltarBlockEntity.getBloodPoolAmount() / BloodLimits.MAX_BLOOD_STONE_ALTAR;
        float scale = tickPercentage * MIN_BLOOD_SPHERE_SCALE + (MAX_BLOOD_SPHERE_SCALE - MIN_BLOOD_SPHERE_SCALE) * bloodPoolFillPercentage;

//        // Spawn drip particles
        double yValue = StoneAltarBlockEntity.BLOOD_SPHERE_HEIGHT + 1.0D;
        // TODO
//        Vec3 pointInSphere = MathUtil.getRandomPointInSphere().scale(scale * BLOOD_SPHERE_PARTICLE_SCALE);
//        double particleOffset = (scale * BLOOD_SPHERE_PARTICLE_SCALE) / 2.0D;
//
//        if (world.isClientSide() && rand.nextDouble() < BLOOD_DRIP_PARTICLE_CHANCE) {
//            world.addParticle(new BloodDripParticleData(),
//                    stoneAltarBlockEntity.getBlockPos().getX() + pointInSphere.x + 0.5D - particleOffset,
//                    stoneAltarBlockEntity.getBlockPos().getY() + pointInSphere.y + yValue - particleOffset,
//                    stoneAltarBlockEntity.getBlockPos().getZ() + pointInSphere.z + 0.5D - particleOffset,
//                    0.0D, 0.0D, 0.0D);
//        }

        if (stoneAltarBlockEntity.getCurrentCraftingStage() == StoneAltarBlockEntity.CraftingStage.PREPARE) {
            Vec3 center = new Vec3(stoneAltarBlockEntity.getBlockPos().getX() + 0.5D,
                    stoneAltarBlockEntity.getBlockPos().getY() + 1.0D,
                    stoneAltarBlockEntity.getBlockPos().getZ() + 0.5D);

            Vec3 end0 = center.add(0D, StoneAltarBlockEntity.BLOOD_SPHERE_HEIGHT, 0D);
            Vec3 end1 = end0.add(0D, -StoneAltarBlockEntity.BLOOD_SPHERE_HEIGHT * 0.1F, 0D);
            // Spawn transfer particles
            for (int i = 0; i < rand.nextInt(MAX_BLOOD_TRANSFER_PARTICLES) + MIN_BLOOD_TRANSFER_PARTICLES; i++) {
                if (rand.nextDouble() < BLOOD_TRANSFER_PARTICLE_CHANCE) {
                    double x = rand.nextDouble() * 1.0D;
                    double z = rand.nextDouble() * 1.0D;

                    Vec3 start0 = new Vec3(stoneAltarBlockEntity.getBlockPos().getX() + x,
                            stoneAltarBlockEntity.getBlockPos().getY() + 1.0D,
                            stoneAltarBlockEntity.getBlockPos().getZ() + z);

                    level.addParticle(new DeathBloodParticleData(start0, center, end1, end0), start0.x, start0.y, start0.z, 0D, 0D, 0D);
                }
            }
        }

        float rotAmount = rand.nextFloat() * (MAX_BLOOD_SPHERE_ROTATION_AMOUNT - MIN_BLOOD_SPHERE_ROTATION_AMOUNT) + MIN_BLOOD_SPHERE_ROTATION_AMOUNT;
        Vec3 rotation = stoneAltarBlockEntity.getCurrentBloodPoolRotation().add(new Vec3(rotAmount, rotAmount, rotAmount));
        stoneAltarBlockEntity.setCurrentBloodPoolRotation(rotation);

        poseStack.translate(0.5D, yValue, 0.5D);
        poseStack.mulPose(Axis.XP.rotationDegrees((float) rotation.x));
        poseStack.mulPose(Axis.YP.rotationDegrees((float) rotation.y));
        poseStack.mulPose(Axis.ZP.rotationDegrees((float) rotation.z));
        poseStack.scale(scale, scale, scale);

        Minecraft.getInstance().getItemRenderer().renderStatic(new ItemStack(ModBlocks.BLOOD_SPHERE.get()), ItemDisplayContext.FIXED, combinedLightIn, combinedOverlayIn, poseStack, buffer, level, /* ??? */ 0);

        poseStack.popPose();

    }

    private void renderItems(StoneAltarBlockEntity stoneAltarBlockEntity, float partialTicks, PoseStack poseStack, MultiBufferSource buffer, int combinedLightIn, int combinedOverlayIn) {
        List<ItemStack> stack = stoneAltarBlockEntity.getInputs();
        BlockState state = stoneAltarBlockEntity.getLevel().getBlockState(stoneAltarBlockEntity.getBlockPos());
        Vec3 blockPos = toVec3(stoneAltarBlockEntity.getBlockPos());
        Level level = stoneAltarBlockEntity.getLevel();
        Random rand = new Random();

        Lighting.setupFor3DItems();
        for (int i = 0; i < stoneAltarBlockEntity.getInputIndex(); i++) {
            if (state.getValue(StoneAltarBlock.ALTAR_STATE) == AltarBlockState.INACTIVE) {
                this.renderInactiveItem(i, stoneAltarBlockEntity.getInputIndex(), stack.get(i), poseStack, buffer, combinedLightIn, combinedOverlayIn, level);
            } else if (state.getValue(StoneAltarBlock.ALTAR_STATE) != AltarBlockState.END) {
                this.renderCraftingItem(i, stoneAltarBlockEntity.getInputIndex(), stack.get(i), blockPos, level, rand, stoneAltarBlockEntity, partialTicks, poseStack, buffer, combinedLightIn, combinedOverlayIn);
            }
        }
    }

    private void renderInactiveItem(int currentItemIndex, int maxItemIndex, ItemStack item, PoseStack poseStack, MultiBufferSource buffer, int combinedLightIn, int combinedOverlayIn, Level level) {
        if (item.isEmpty()) {
            return;
        }


        poseStack.pushPose();

        Vec3 initialPosition = this.getItemInitialPosition(currentItemIndex, maxItemIndex, item.getItem());
        Vec3 initialRotation = this.getItemInitialRotation(currentItemIndex, maxItemIndex, item.getItem());
        float scale = this.getItemScale(item.getItem());

        poseStack.translate(initialPosition.x, initialPosition.y, initialPosition.z);
        poseStack.mulPose(Axis.YP.rotationDegrees((float) initialRotation.y()));
        poseStack.mulPose(Axis.XP.rotationDegrees((float) initialRotation.x()));
        poseStack.scale(scale, scale, scale);

        Minecraft.getInstance().getItemRenderer().renderStatic(item, ItemDisplayContext.FIXED, combinedLightIn, combinedOverlayIn, poseStack, buffer, level, /* ??? */ 0);

        poseStack.popPose();

    }

    private void renderCraftingItem(int currentItemIndex, int maxItemIndex, ItemStack item, Vec3 blockPos, Level level, Random rand, StoneAltarBlockEntity stoneAltarBlockEntity, float partialTicks, PoseStack poseStack, MultiBufferSource buffer, int combinedLightIn, int combinedOverlayIn) {
        if (item.isEmpty()) {
            return;
        }


        poseStack.pushPose();

        float craftingTick = stoneAltarBlockEntity.getCraftingTick() + partialTicks;

        Vec3 currentPosition = this.getItemCurrentPosition(craftingTick, currentItemIndex, maxItemIndex, item.getItem());
        Vec3 currentRotation = this.getItemCurrentRotation(craftingTick, currentItemIndex, maxItemIndex, item.getItem());
        float scale = this.getItemScale(item.getItem());

        poseStack.translate(currentPosition.x, currentPosition.y, currentPosition.z);
        poseStack.mulPose(Axis.YP.rotationDegrees((float) currentRotation.y()));
        poseStack.mulPose(Axis.XP.rotationDegrees((float) currentRotation.x()));
        poseStack.scale(scale, scale, scale);

        Minecraft.getInstance().getItemRenderer().renderStatic(item, ItemDisplayContext.FIXED, combinedLightIn, combinedOverlayIn, poseStack, buffer, level, /* ??? */ 0);

        // Create Particles
        if (this.getLerpTime(craftingTick) >= 1.0D && rand.nextDouble() < CRAFTING_PARTICLE_CHANCE) {
            Vec3 trueDestination = new Vec3(blockPos.x + 0.5D, blockPos.y + StoneAltarBlockEntity.BLOOD_SPHERE_HEIGHT + 1.0D, blockPos.z + 0.5D);
            Vec3 truePosition = new Vec3(blockPos.x + currentPosition.x, blockPos.y + currentPosition.y, blockPos.z + currentPosition.z);
            Vec3 velocity = trueDestination.subtract(truePosition).scale(CRAFTING_PARTICLE_SPEED_SCALE);
            level.addParticle(new AltarBreakItemParticleData(ModParticleTypes.ALTAR_BREAK_ITEM_PARTICLE.get(), trueDestination, item), truePosition.x, truePosition.y, truePosition.z, velocity.x, velocity.y, velocity.z);
        }

        poseStack.popPose();
    }

    private Vec3 getItemInitialPosition(int itemIndex, int maxItemIndex, Item item) {
        double degrees = ((double) itemIndex / (double) maxItemIndex) * 360D;
        double radians = Math.toRadians(degrees);
        double x = 0.5D + Math.cos(radians) * ITEM_RENDER_DISTANCE;
        double y = 1.0D + (item instanceof BlockItem ? BLOCK_HEIGHT : ITEM_HEIGHT);
        double z = 0.5D + Math.sin(radians) * ITEM_RENDER_DISTANCE;
        return new Vec3(x, y, z);
    }

    private Vec3 getItemCurrentPosition(float tick, int itemIndex, int maxItemIndex, Item item) {
        double degrees = ((double) itemIndex / (double) maxItemIndex) * 360D + tick * (tick / (double) StoneAltarBlockEntity.CraftingStage.totalTicks() * MAX_DEGREES_ROTATION_AROUND_CENTER_PER_TICK);
        double radians = Math.toRadians(degrees);
        double x = 0.5D + Math.cos(radians) * MathUtil.lerp(this.getLerpTime(tick), ITEM_RENDER_DISTANCE, EXTRA_ITEM_RENDER_DISTANCE);
        double y = 1.0D + (item instanceof BlockItem ? BLOCK_HEIGHT : ITEM_HEIGHT) + MathUtil.lerp(this.getLerpTime(tick), 0.0D, StoneAltarBlockEntity.BLOOD_SPHERE_HEIGHT);
        double z = 0.5D + Math.sin(radians) * MathUtil.lerp(this.getLerpTime(tick), ITEM_RENDER_DISTANCE, EXTRA_ITEM_RENDER_DISTANCE);
        return new Vec3(x, y, z);
    }

    private Vec3 getItemInitialRotation(int currentItemIndex, int maxItemIndex, Item item) {
        float x = item instanceof BlockItem ? 0F : 90F;
        float y = 90F - currentItemIndex * 360F / maxItemIndex;
        float z = 0F;
        return new Vec3(x, y, z);
    }

    private Vec3 getItemCurrentRotation(float tick, int currentItemIndex, int maxItemIndex, Item item) {
        float x = item instanceof BlockItem ? 0F : 90F - MathUtil.lerp(this.getLerpTime(tick), 0.0F, 90.0F);
        float y = 90F - currentItemIndex * 360F / maxItemIndex + tick * (tick / (float) StoneAltarBlockEntity.CraftingStage.totalTicks() * MAX_DEGREES_ROTATION_PER_TICK);
        float z = 0F;
        return new Vec3(x, y, z);
    }

    private float getItemScale(Item item) {
        return item instanceof BlockItem ? BLOCK_RENDER_SCALE : ITEM_RENDER_SCALE;
    }

    private float getLerpTime(float tick) {
        return Math.min(TICKS_CRAFTING_TO_REACH_MIDDLE, tick) / TICKS_CRAFTING_TO_REACH_MIDDLE;
    }
}
