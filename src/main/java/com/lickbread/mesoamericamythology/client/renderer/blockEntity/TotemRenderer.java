package com.lickbread.mesoamericamythology.client.renderer.blockEntity;

import com.lickbread.mesoamericamythology.blockEntities.TotemBlockEntity;
import com.lickbread.mesoamericamythology.particle.TotemEffectParticleData;
import com.lickbread.mesoamericamythology.util.BlockPosUtil;
import com.lickbread.mesoamericamythology.util.RendererUtil;
import com.mojang.blaze3d.vertex.PoseStack;
import net.minecraft.client.renderer.MultiBufferSource;
import net.minecraft.client.renderer.blockentity.BlockEntityRenderer;
import net.minecraft.client.renderer.blockentity.BlockEntityRendererProvider;
import net.minecraft.core.BlockPos;
import net.minecraft.world.effect.MobEffect;
import net.minecraft.world.level.Level;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;

import java.util.Random;

@OnlyIn(Dist.CLIENT)
public class TotemRenderer implements BlockEntityRenderer<TotemBlockEntity> {

    private static final int MAX_PARTICLES = 10;
    private static final double RANGE_VARIANCE = 0.1D;
    private static final float TICK_VARIANCE = 1F;

    public TotemRenderer(BlockEntityRendererProvider.Context context) {
    }

    @Override
    public void render(TotemBlockEntity totemBlockEntity, float partialTicks, PoseStack poseStack, MultiBufferSource buffer, int combinedLightIn, int combinedOverlayIn) {
        RendererUtil.renderSolidModel(totemBlockEntity, partialTicks, poseStack, buffer, combinedLightIn, combinedOverlayIn);
        this.renderParticles(totemBlockEntity);
    }

    private void renderParticles(TotemBlockEntity totemBlockEntity) {
        Level level = totemBlockEntity.getLevel();
        BlockPos blockPos = totemBlockEntity.getBlockPos();

        float currentTick = totemBlockEntity.getCurrentTick();

        Random rand = new Random();
        MobEffect potion = totemBlockEntity.getTotemType().getEffect();
        int liquidColor = potion.getColor();

        for (int i = 0; i < MAX_PARTICLES; i++) {
            double randX = rand.nextDouble() * 2.0D * RANGE_VARIANCE;
            double randZ = rand.nextDouble() * 2.0D *  RANGE_VARIANCE;

            float randTick = rand.nextFloat() * 2.0F * TICK_VARIANCE;

            double x = blockPos.getX() + Math.cos(currentTick - TICK_VARIANCE + randTick + i) * (TotemBlockEntity.MAX_RANGE - RANGE_VARIANCE + randX) + 0.5D;
            double z = blockPos.getZ() + Math.sin(currentTick - TICK_VARIANCE + randTick + i) * (TotemBlockEntity.MAX_RANGE - RANGE_VARIANCE + randZ) + 0.5D;

            double y = BlockPosUtil.getNearestAirYWithRange(level, new BlockPos((int) x, totemBlockEntity.getBlockPos().getY(), (int) z), TotemBlockEntity.MAX_RANGE, true) + 0.05D;

            double velX = (double) (liquidColor >> 16 & 255) / 255.0D;
            double velY = (double) (liquidColor >> 8 & 255) / 255.0D;
            double velZ = (double) (liquidColor & 255) / 255.0D;

            level.addParticle(new TotemEffectParticleData((float) velX, (float) velY, (float) velZ), x, y, z, velX, velY, velZ);
        }
    }
}
