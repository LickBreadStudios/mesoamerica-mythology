//package com.lickbread.mesoamericamythology.client.renderer.entity;
//
//import com.lickbread.mesoamericamythology.MesoamericaMythologyMod;
//import com.lickbread.mesoamericamythology.entity.SpanishConquistadorGeneralEntity;
//import net.minecraft.client.renderer.entity.EntityRenderDispatcher;
//import net.minecraft.client.renderer.entity.EntityRendererProvider;
//import net.minecraft.client.renderer.entity.MobRenderer;
//import net.minecraft.client.model.HumanoidModel;
//import net.minecraft.resources.ResourceLocation;
//
//public class SpanishConquistadorGeneralRenderer extends MobRenderer<SpanishConquistadorGeneralEntity, HumanoidModel<SpanishConquistadorGeneralEntity>> {
//
//    private static final ResourceLocation SPANISH_CONQUISTADOR_GENERAL_TEXTURE = new ResourceLocation(MesoamericaMythologyMod.MOD_ID, "textures/entity/spanish_conquistador/spanish_conquistador_general.png");
//
//    public SpanishConquistadorGeneralRenderer(EntityRendererProvider.Context context) {
//        super(context, new HumanoidModel<SpanishConquistadorGeneralEntity>(context.bakeLayer()), 0.7F);
//    }
//
//    @Override
//    public ResourceLocation getTextureLocation(SpanishConquistadorGeneralEntity entity) {
//        return SPANISH_CONQUISTADOR_GENERAL_TEXTURE;
//    }
//}
