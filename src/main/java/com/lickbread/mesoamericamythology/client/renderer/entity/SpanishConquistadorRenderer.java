//package com.lickbread.mesoamericamythology.client.renderer.entity;
//
//import com.lickbread.mesoamericamythology.MesoamericaMythologyMod;
//import com.lickbread.mesoamericamythology.entity.SpanishConquistadorEntity;
//import net.minecraft.client.renderer.entity.EntityRendererProvider;
//import net.minecraft.client.renderer.entity.MobRenderer;
//import net.minecraft.client.model.HumanoidModel;
//import net.minecraft.resources.ResourceLocation;
//
//public class SpanishConquistadorRenderer extends MobRenderer<SpanishConquistadorEntity, HumanoidModel<SpanishConquistadorEntity>> {
//
//    private static final ResourceLocation SPANISH_CONQUISTADOR_TEXTURE = new ResourceLocation(MesoamericaMythologyMod.MOD_ID, "textures/entity/spanish_conquistador/spanish_conquistador.png");
//
//    public SpanishConquistadorRenderer(EntityRendererProvider.Context context) {
//        super(context, new HumanoidModel<SpanishConquistadorEntity>(context.bakeLayer()), 0.7F);
//    }
//    @Override
//    public ResourceLocation getTextureLocation(SpanishConquistadorEntity entity) {
//        return SPANISH_CONQUISTADOR_TEXTURE;
//    }
//}
