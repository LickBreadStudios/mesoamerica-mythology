package com.lickbread.mesoamericamythology.client.renderer.item;

import com.lickbread.mesoamericamythology.MesoamericaMythologyMod;
import com.lickbread.mesoamericamythology.client.renderer.model.ChimalliModel;
import com.lickbread.mesoamericamythology.client.renderer.model.FeatherShieldModel;
import com.lickbread.mesoamericamythology.client.renderer.model.JavelinModel;
import com.lickbread.mesoamericamythology.items.ChimalliShield;
import com.lickbread.mesoamericamythology.registry.ModItems;
import com.lickbread.mesoamericamythology.registry.ModModelLayerLocations;
import com.mojang.blaze3d.vertex.PoseStack;
import com.mojang.blaze3d.vertex.VertexConsumer;
import com.mojang.datafixers.util.Pair;
import net.minecraft.client.model.geom.EntityModelSet;
import net.minecraft.client.renderer.BlockEntityWithoutLevelRenderer;
import net.minecraft.client.renderer.MultiBufferSource;
import net.minecraft.client.renderer.Sheets;
import net.minecraft.client.renderer.blockentity.BannerRenderer;
import net.minecraft.client.renderer.blockentity.BlockEntityRenderDispatcher;
import net.minecraft.client.renderer.entity.ItemRenderer;
import net.minecraft.client.resources.model.Material;
import net.minecraft.core.Holder;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.item.DyeColor;
import net.minecraft.world.item.ItemDisplayContext;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.ShieldItem;
import net.minecraft.world.level.block.entity.BannerBlockEntity;
import net.minecraft.world.level.block.entity.BannerPattern;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;

import java.util.List;

@OnlyIn(Dist.CLIENT)
public class CustomBlockEntityWithoutLevelRenderer extends BlockEntityWithoutLevelRenderer {

    public CustomBlockEntityWithoutLevelRenderer(BlockEntityRenderDispatcher renderDispatcher, EntityModelSet modelSet) {
        super(renderDispatcher, modelSet);

        chimalliModel = new ChimalliModel(modelSet.bakeLayer(ModModelLayerLocations.CHIMALLI_LAYER));
        featherShieldModel = new FeatherShieldModel(modelSet.bakeLayer(ModModelLayerLocations.FEATHER_SHIELD_LAYER));
        javelinModel = new JavelinModel(modelSet.bakeLayer(ModModelLayerLocations.THROWN_JAVELIN_LAYER));
    }

    @Override
    public void renderByItem(ItemStack itemStackIn, ItemDisplayContext transformTypeIn, PoseStack poseStack, MultiBufferSource buffer, int combinedLightIn, int combinedOverlayIn) {
        if (itemStackIn.is(ModItems.CHIMALLI.get())) {
            this.renderChimalli(itemStackIn, transformTypeIn, poseStack, buffer, combinedLightIn, combinedOverlayIn);
        } else if (itemStackIn.is(ModItems.FEATHER_SHIELD.get())) {
            this.renderFeatherShield(itemStackIn, transformTypeIn, poseStack, buffer, combinedLightIn, combinedOverlayIn);
        } else if (itemStackIn.is(ModItems.JAVELIN.get())) {
            this.renderJavelin(itemStackIn, transformTypeIn, poseStack, buffer, combinedLightIn, combinedOverlayIn);
        }
    }

    /**
     * Chimalli
     */
    public static final ResourceLocation CHIMALLI_LOCATION_SHIELD_BASE = new ResourceLocation(MesoamericaMythologyMod.MOD_ID, "entity/chimalli_base");
    public static final ResourceLocation CHIMALLI_LOCATION_SHIELD_BASE_NO_PATTERN = new ResourceLocation(MesoamericaMythologyMod.MOD_ID, "entity/chimalli_base_nopattern");

    public static final Material CHIMALLI_MATERIAL_SHIELD_BASE = new Material(Sheets.SHIELD_SHEET, CHIMALLI_LOCATION_SHIELD_BASE);
    public static final Material CHIMALLI_MATERIAL_SHIELD_NO_PATTERN = new Material(Sheets.SHIELD_SHEET, CHIMALLI_LOCATION_SHIELD_BASE_NO_PATTERN);

    private final ChimalliModel chimalliModel;

    private void renderChimalli(ItemStack itemStack, ItemDisplayContext transformType, PoseStack poseStack, MultiBufferSource buffer, int combinedLightIn, int combinedOverlayIn) {
        boolean hasBannerPattern = itemStack.getTagElement("BlockEntityTag") != null;
        poseStack.pushPose();
        poseStack.scale(1.0F, -1.0F, -1.0F);
        Material material = hasBannerPattern ? CHIMALLI_MATERIAL_SHIELD_BASE : CHIMALLI_MATERIAL_SHIELD_NO_PATTERN;
        // TODO: Not sure if getFoilBuffer is correct
        VertexConsumer ivertexbuilder = material.sprite().wrap(ItemRenderer.getFoilBuffer(buffer, this.chimalliModel.renderType(material.atlasLocation()), false, itemStack.hasFoil()));
        this.chimalliModel.getHandle().render(poseStack, ivertexbuilder, combinedLightIn, combinedOverlayIn, 1.0F, 1.0F, 1.0F, 1.0F);
        this.chimalliModel.getFeathers().render(poseStack, ivertexbuilder, combinedLightIn, combinedOverlayIn, 1.0F, 1.0F, 1.0F, 1.0F);
        if (hasBannerPattern) {
            List<Pair<Holder<BannerPattern>, DyeColor>> list = BannerBlockEntity.createPatterns(ShieldItem.getColor(itemStack), BannerBlockEntity.getItemPatterns(itemStack));
            BannerRenderer.renderPatterns(poseStack, buffer, combinedLightIn, combinedOverlayIn, this.chimalliModel.getPlate(), material, false, list);
        } else {
            this.chimalliModel.getPlate().render(poseStack, ivertexbuilder, combinedLightIn, combinedOverlayIn, 1.0F, 1.0F, 1.0F, 1.0F);
        }

        boolean isBlocking = itemStack.getOrCreateTagElement(ChimalliShield.CHIMALLI_SHIELD).getBoolean(ChimalliShield.IS_BLOCKING_KEY);
        if (isBlocking) {
            this.chimalliModel.getOutline().render(poseStack, ivertexbuilder, combinedLightIn, combinedOverlayIn, 1.0F, 1.0F, 1.0F, 1.0F);
        }

        poseStack.popPose();
    }

    /**
     * FeatherShield
     */
    public static final ResourceLocation FEATHER_SHIELD_LOCATION_SHIELD_BASE = new ResourceLocation(MesoamericaMythologyMod.MOD_ID, "entity/feather_shield_base");
    public static final ResourceLocation FEATHER_SHIELD_LOCATION_SHIELD_BASE_NO_PATTERN = new ResourceLocation(MesoamericaMythologyMod.MOD_ID, "entity/feather_shield_base_nopattern");

    public static final Material FEATHER_SHIELD_MATERIAL_SHIELD_BASE = new Material(Sheets.SHIELD_SHEET, FEATHER_SHIELD_LOCATION_SHIELD_BASE);
    public static final Material FEATHER_SHIELD_MATERIAL_SHIELD_NO_PATTERN = new Material(Sheets.SHIELD_SHEET, FEATHER_SHIELD_LOCATION_SHIELD_BASE_NO_PATTERN);

    private final FeatherShieldModel featherShieldModel;

    public void renderFeatherShield(ItemStack itemStackIn, ItemDisplayContext transformTypeIn, PoseStack poseStack, MultiBufferSource buffer, int combinedLightIn, int combinedOverlayIn) {
        boolean flag = itemStackIn.getTagElement("BlockEntityTag") != null;
        poseStack.pushPose();
        poseStack.scale(1.0F, -1.0F, -1.0F);
        Material material = flag ? FEATHER_SHIELD_MATERIAL_SHIELD_BASE : FEATHER_SHIELD_MATERIAL_SHIELD_NO_PATTERN;
        VertexConsumer ivertexbuilder = material.sprite().wrap(ItemRenderer.getFoilBuffer(buffer, this.featherShieldModel.renderType(material.atlasLocation()), false, itemStackIn.hasFoil()));
        this.featherShieldModel.getHandle().render(poseStack, ivertexbuilder, combinedLightIn, combinedOverlayIn, 1.0F, 1.0F, 1.0F, 1.0F);
        this.featherShieldModel.getFeathers().render(poseStack, ivertexbuilder, combinedLightIn, combinedOverlayIn, 1.0F, 1.0F, 1.0F, 1.0F);
        if (flag) {
            List<Pair<Holder<BannerPattern>, DyeColor>> list = BannerBlockEntity.createPatterns(ShieldItem.getColor(itemStackIn), BannerBlockEntity.getItemPatterns(itemStackIn));
            BannerRenderer.renderPatterns(poseStack, buffer, combinedLightIn, combinedOverlayIn, this.featherShieldModel.getPlate(), material, false, list);
        } else {
            this.featherShieldModel.getPlate().render(poseStack, ivertexbuilder, combinedLightIn, combinedOverlayIn, 1.0F, 1.0F, 1.0F, 1.0F);
        }

        poseStack.popPose();
    }

    /**
     * Javelin
     */

    private final JavelinModel javelinModel;

    public void renderJavelin(ItemStack itemStackIn, ItemDisplayContext transformTypeIn, PoseStack poseStack, MultiBufferSource buffer, int combinedLightIn, int combinedOverlayIn) {
        poseStack.pushPose();
        poseStack.scale(1.0F, -1.0F, -1.0F);
        VertexConsumer vertexconsumer1 = ItemRenderer.getFoilBufferDirect(buffer, this.javelinModel.renderType(JavelinModel.TEXTURE), false, false);
        this.javelinModel.renderToBuffer(poseStack, vertexconsumer1, combinedLightIn, combinedOverlayIn, 1.0F, 1.0F, 1.0F, 1.0F);
        poseStack.popPose();
    }
}
