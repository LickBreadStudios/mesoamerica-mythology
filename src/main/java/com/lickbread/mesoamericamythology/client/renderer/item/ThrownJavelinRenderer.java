package com.lickbread.mesoamericamythology.client.renderer.item;

import com.lickbread.mesoamericamythology.MesoamericaMythologyMod;
import com.lickbread.mesoamericamythology.client.renderer.model.JavelinModel;
import com.lickbread.mesoamericamythology.entity.ThrownJavelin;
import com.lickbread.mesoamericamythology.registry.ModModelLayerLocations;
import com.mojang.blaze3d.vertex.PoseStack;
import com.mojang.blaze3d.vertex.VertexConsumer;
import com.mojang.math.Axis;
import net.minecraft.client.renderer.MultiBufferSource;
import net.minecraft.client.renderer.entity.EntityRenderer;
import net.minecraft.client.renderer.entity.EntityRendererProvider;
import net.minecraft.client.renderer.entity.ItemRenderer;
import net.minecraft.client.renderer.texture.OverlayTexture;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.util.Mth;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;

@OnlyIn(Dist.CLIENT)
public class ThrownJavelinRenderer extends EntityRenderer<ThrownJavelin> {
    public static final ResourceLocation JAVELIN_LOCATION = new ResourceLocation(MesoamericaMythologyMod.MOD_ID, "textures/entity/javelin.jpg");
    private final JavelinModel model;

    public ThrownJavelinRenderer(EntityRendererProvider.Context context) {
        super(context);
        this.model = new JavelinModel(context.bakeLayer(ModModelLayerLocations.THROWN_JAVELIN_LAYER));
    }

    public void render(ThrownJavelin javelin, float p_116112_, float p_116113_, PoseStack poseStack, MultiBufferSource bufferSource, int p_116116_) {
        poseStack.pushPose();
        poseStack.mulPose(Axis.YP.rotationDegrees(Mth.lerp(p_116113_, javelin.yRotO, javelin.getYRot()) - 90.0F));
        poseStack.mulPose(Axis.ZP.rotationDegrees(Mth.lerp(p_116113_, javelin.xRotO, javelin.getXRot()) + 90.0F));
        VertexConsumer vertexconsumer = ItemRenderer.getFoilBufferDirect(bufferSource, this.model.renderType(this.getTextureLocation(javelin)), false, false);
        this.model.renderToBuffer(poseStack, vertexconsumer, p_116116_, OverlayTexture.NO_OVERLAY, 1.0F, 1.0F, 1.0F, 1.0F);
        poseStack.popPose();
        super.render(javelin, p_116112_, p_116113_, poseStack, bufferSource, p_116116_);
    }

    public ResourceLocation getTextureLocation(ThrownJavelin javelin) {
        return JAVELIN_LOCATION;
    }
}