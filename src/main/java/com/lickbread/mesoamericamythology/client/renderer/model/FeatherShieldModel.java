package com.lickbread.mesoamericamythology.client.renderer.model;

import com.mojang.blaze3d.vertex.PoseStack;
import com.mojang.blaze3d.vertex.VertexConsumer;
import lombok.Getter;
import net.minecraft.client.model.geom.PartPose;
import net.minecraft.client.model.geom.builders.CubeListBuilder;
import net.minecraft.client.model.geom.builders.LayerDefinition;
import net.minecraft.client.model.geom.builders.MeshDefinition;
import net.minecraft.client.model.geom.builders.PartDefinition;
import net.minecraft.client.renderer.RenderType;
import net.minecraft.client.model.Model;
import net.minecraft.client.model.geom.ModelPart;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;

@OnlyIn(Dist.CLIENT)
@Getter
public class FeatherShieldModel extends Model {

    private static final String PLATE = "plate";
    private static final String HANDLE = "handle";
    private static final String FEATHERS = "feathers";

    private final ModelPart root;
    private final ModelPart plate;
    private final ModelPart handle;
    private final ModelPart feathers;

    public FeatherShieldModel(ModelPart root) {
        super(RenderType::entityCutout);
        this.root = root;
        this.plate = root.getChild(PLATE);
        this.handle = root.getChild(HANDLE);
        this.feathers = root.getChild(FEATHERS);
    }

    public void renderToBuffer(PoseStack poseStack, VertexConsumer buffer, int packedLightIn, int packedOverlayIn, float red, float green, float blue, float alpha) {
        this.plate.render(poseStack, buffer, packedLightIn, packedOverlayIn, red, green, blue, alpha);
        this.handle.render(poseStack, buffer, packedLightIn, packedOverlayIn, red, green, blue, alpha);
        this.feathers.render(poseStack, buffer, packedLightIn, packedOverlayIn, red, green, blue, alpha);
    }

    public static LayerDefinition createLayerDefinition() {
        MeshDefinition meshdefinition = new MeshDefinition();
        PartDefinition partdefinition = meshdefinition.getRoot();
        partdefinition.addOrReplaceChild(PLATE, CubeListBuilder.create()
                .texOffs(0, 0)
                .addBox(-6.0F, -6.0F, -2.0F, 12.0F, 12.0F, 1.0F), PartPose.ZERO);
        partdefinition.addOrReplaceChild(HANDLE, CubeListBuilder.create()
                .texOffs(26, 0)
                .addBox(-1.0F, -3.0F, -1.0F, 2.0F, 6.0F, 6.0F), PartPose.ZERO);
        partdefinition.addOrReplaceChild(FEATHERS, CubeListBuilder.create()
                .texOffs(0, 13)
                .addBox(-9.0F, -9.0F, -1.99F, 18.0F, 18.0F, .98F), PartPose.ZERO);
        return LayerDefinition.create(meshdefinition, 64, 64);
    }
}
