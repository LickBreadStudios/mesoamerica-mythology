package com.lickbread.mesoamericamythology.client.screen;

import com.google.common.collect.Lists;
import com.lickbread.mesoamericamythology.MesoamericaMythologyMod;
import com.lickbread.mesoamericamythology.api.MesoamericaMythologyAPI;
import com.lickbread.mesoamericamythology.statueHead.God;
import com.lickbread.mesoamericamythology.statueHead.relationship.IDivineFavourCapability;
import com.mojang.blaze3d.systems.RenderSystem;
import com.mojang.blaze3d.vertex.PoseStack;
import net.minecraft.client.GameNarrator;
import net.minecraft.client.gui.components.Button;
import net.minecraft.client.gui.screens.Screen;
import net.minecraft.network.chat.Component;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.entity.player.Player;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;

import java.util.List;

import static com.lickbread.mesoamericamythology.capabilities.DivineFavourCapabilityProvider.DIVINE_FAVOUR_CAPABILITY;
import static com.lickbread.mesoamericamythology.registry.event.ModKeyBindings.OPEN_DIVINE_FAVOUR_RELATIONSHIP;

@OnlyIn(Dist.CLIENT)
public class DivineRelationshipsScreen extends Screen {

    private static final int GUI_WIDTH = 720;
    private static final int GUI_HEIGHT = 720;

    private static final int MAX_TABS = DivineRelationshipsTabType.MAX_TABS;

    private static final ResourceLocation TABS = new ResourceLocation(MesoamericaMythologyMod.MOD_ID, "textures/gui/divinerelationships/tabs.png");

    private static final Component SAD_LABEL = Component.translatable("screen.mesoamericamythology.divine_favour.sad_label");
    private static final Component EMPTY = Component.translatable("screen.mesoamericamythology.divine_favour.empty");
    private static final Component GUI_LABEL = Component.translatable("screen.mesoamericamythology.divine_favour.title");

    private final List<DivineRelationshipsTabGui> tabs = Lists.newLinkedList();
    private DivineRelationshipsTabGui selectedTab;
    private God selectedGod;
    private int tabPage, maxPages;

    private final Player player;
    private final IDivineFavourCapability capability;

    public DivineRelationshipsScreen(Player player) {
        super(GameNarrator.NO_TITLE);
        this.player = player;
        this.capability = this.player.getCapability(DIVINE_FAVOUR_CAPABILITY).orElse(null);
        this.selectedGod = God.HUITZILOPOCHTLI;
    }

    IDivineFavourCapability getCapability() {
        return this.capability;
    }

    /**
     * Called immediately after being opened
     */
    @Override
    protected void init() {
        this.setupTabs();
    }

    /**
     * Add each tab to the list
     */
    private void setupTabs() {
        this.tabs.clear();

        this.tabs.addAll(MesoamericaMythologyAPI.getDivineRelationshipsTabGuis());
        this.tabs.forEach(tab -> tab.setScreen(this));
        int index = 0;
        int page = 0;
        for (DivineRelationshipsTabGui tabGui : this.tabs) {
            tabGui.setTabLocation(index, page);
            index++;
            if (index > MAX_TABS) {
                index = 0;
                page++;
            }
        }

        if (page > 0) {
            int guiLeft = (this.width - GUI_WIDTH) / 2;
            int guiTop = (this.height - GUI_HEIGHT) / 2;
            this.addWidget(
                    Button.builder(Component.literal("<"), (onPress) -> tabPage = Math.max(tabPage - 1, 0))
                            .bounds(guiLeft, guiTop - 50, 20, 20)
                            .build()
            );
            this.addWidget(
                    Button.builder(Component.literal(">"), (onPress) -> tabPage = Math.min(tabPage + 1, maxPages))
                            .bounds(guiLeft + GUI_WIDTH - 20, guiTop - 50, 20, 20)
                            .build()
            );
            maxPages = page;
        }

        this.selectedTab = this.tabs.get(0);
    }

    @Override
    public boolean mouseClicked(double mouseX, double mouseY, int button) {
        if (button == 0) {
            int guiLeft = DivineRelationshipsTabType.MAX_WIDTH / 4;
            int guiTop = (this.height - DivineRelationshipsTabType.MAX_HEIGHT) / 2;
            for (DivineRelationshipsTabGui tab : this.tabs) {
                if (tab.getPage() == tabPage && tab.isInsideTabSelector(guiLeft, guiTop, mouseX, mouseY)) {
                    this.selectedTab = tab;
                    break;
                }
            }
        }

        return super.mouseClicked(mouseX, mouseY, button);
    }

    /**
     * Seems to be about closing the UI if the advancements key bind is pressed
     */
    @Override
    public boolean keyPressed(int keyCode, int scanCode, int modifiers) {
        if (OPEN_DIVINE_FAVOUR_RELATIONSHIP.matches(keyCode, scanCode)) {
            this.minecraft.setScreen((Screen)null);
            this.minecraft.mouseHandler.grabMouse();
            return true;
        } else {
            return super.keyPressed(keyCode, scanCode, modifiers);
        }
    }

    /**
     * Called to render the UI
     */
    @Override
    public void render(PoseStack poseStack, int mouseX, int mouseY, float partialTicks) {
        int guiCenterX = this.width / 2;
        int guiCenterY = this.height / 2;
        this.renderBackground(poseStack);
        // TODO HANDLE PAGES!
        if (maxPages != 0) {
            Component page = Component.literal(String.format("%d / %d", tabPage + 1, maxPages + 1));
            int width = this.font.width(page);
//            RenderSystem.disableLighting();
//            this.font.draw(PoseStack, page.getString(), guiLeft + (GUI_WIDTH / 2) - (width / 2), guiTop - 44, -1);
        }
        this.drawWindowBackground(poseStack, guiCenterX, guiCenterY);
        this.drawWindowForeground(poseStack, guiCenterX, guiCenterY);
        this.drawTabText(poseStack, guiCenterX, guiCenterY);
        this.drawWindowTooltips(poseStack, mouseX, mouseY, guiCenterX, guiCenterY);
    }

    private void drawTabText(PoseStack poseStack, int guiCenterX, int guiCenterY) {
        RenderSystem.setShaderColor(1.0F, 1.0F, 1.0F, 1.0F);
        if (this.selectedTab != null) {
            poseStack.pushPose();
//            int i = (this.width - GUI_WIDTH) / 2;
//            int j = (this.height - GUI_HEIGHT) / 2;
            poseStack.translate(/*(float)i, (float)j*/0, 0, 200.0F);
            this.selectedTab.renderText(poseStack, guiCenterX, guiCenterY);
            poseStack.popPose();
        }
    }

    private void drawWindowBackground(PoseStack poseStack, int guiCenterX, int guiCenterY) {
        if (this.selectedTab == null) {
            int color = -16777216;
            fill(poseStack, guiCenterX - GUI_WIDTH / 2, guiCenterY - GUI_HEIGHT / 2, guiCenterX + GUI_WIDTH / 2, guiCenterY + GUI_HEIGHT / 2, color);
            drawCenteredString(poseStack, this.font, EMPTY, guiCenterX,  guiCenterY + 10, -1);
            drawCenteredString(poseStack, this.font, SAD_LABEL, guiCenterX, guiCenterY - 10, -1);
        } else {
            poseStack.pushPose();
            int i = (this.width - GUI_WIDTH) / 2;
            int j = (this.height - GUI_HEIGHT) / 2;
            poseStack.translate((float)i, (float)j, 0.0F);
            this.selectedTab.drawTabBackground(poseStack);
            poseStack.popPose();
        }
    }

    public void drawWindowForeground(PoseStack poseStack, int guiCenterX, int guiCenterY) {
        int guiLeft = DivineRelationshipsTabType.MAX_WIDTH / 4;
        int guiTop = (this.height - DivineRelationshipsTabType.MAX_HEIGHT) / 2;

        RenderSystem.setShaderColor(1.0F, 1.0F, 1.0F, 1.0F);
        RenderSystem.enableBlend();
        RenderSystem.setShaderTexture(0, TABS);
        for (DivineRelationshipsTabGui tab : this.tabs) {
            if (tab.getPage() == tabPage) {
                tab.renderTabSelectorBackground(poseStack, guiLeft, guiTop, tab == this.selectedTab);
            }
        }

//        RenderSystem.enableRescaleNormal();
        RenderSystem.defaultBlendFunc();

        for (DivineRelationshipsTabGui tab : this.tabs) {
            if (tab.getPage() == tabPage) {
                tab.renderTabSelectorForeground(poseStack, guiLeft, guiTop, this.itemRenderer);
            }
        }

        RenderSystem.disableBlend();
    }

    private void drawWindowTooltips(PoseStack poseStack, int mouseX, int mouseY, int guiCentreX, int guiCentreY) {
        RenderSystem.setShaderColor(1.0F, 1.0F, 1.0F, 1.0F);
        if (this.selectedTab != null) {
            poseStack.pushPose();
            RenderSystem.enableDepthTest();
            int i = (this.width - GUI_WIDTH) / 2;
            int j = (this.height - GUI_HEIGHT) / 2;
            poseStack.translate((float)i, (float)j, 000.0F);
            this.selectedTab.drawTabTooltips(poseStack, mouseX - i, mouseY - j);
            RenderSystem.disableDepthTest();
            poseStack.popPose();
        }

        int guiLeft = DivineRelationshipsTabType.MAX_WIDTH / 4;
        int guiTop = (this.height - DivineRelationshipsTabType.MAX_HEIGHT) / 2;
        for (DivineRelationshipsTabGui tab : this.tabs) {
            if (tab.getPage() == tabPage && tab.isInsideTabSelector(guiLeft, guiTop, mouseX, mouseY)) {
                this.renderTooltip(poseStack, tab.getTitle(), mouseX, mouseY);
            }
        }
    }
}
