package com.lickbread.mesoamericamythology.client.screen;

import com.mojang.blaze3d.systems.RenderSystem;
import com.mojang.blaze3d.vertex.PoseStack;
import lombok.Getter;
import lombok.Setter;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiComponent;
import net.minecraft.client.renderer.entity.ItemRenderer;
import net.minecraft.network.chat.Component;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.util.Mth;
import net.minecraft.world.item.ItemStack;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;

@OnlyIn(Dist.CLIENT)
public abstract class DivineRelationshipsTabGui extends GuiComponent {
    
    protected static final int MAX_WIDTH = 720;
    protected static final int MAX_HEIGHT = 720;

    protected final Minecraft minecraft;
    @Getter
    @Setter
    protected DivineRelationshipsScreen screen;
    protected final DivineRelationshipsTabType type;
    protected int index;
    @Getter
    protected int page;
    protected final ItemStack icon;
    @Getter
    protected final Component title;
    protected final ResourceLocation background;

    protected float fade;

    public DivineRelationshipsTabGui(DivineRelationshipsTabType type, ItemStack icon, Component title, ResourceLocation background) {
        this.type = type;
        this.minecraft = Minecraft.getInstance();
        this.index = 0;
        this.icon = icon;
        this.title = title;
        this.background = background;
        this.page = 0;
    }

    public void setTabLocation(int index, int page) {
        this.index = index;
        this.page = page;
    }

    public void renderTabSelectorBackground(PoseStack poseStack, int guiLeft, int guiTop, boolean isSelected) {
        this.type.renderTabSelectorBackground(poseStack, this, guiLeft, guiTop, isSelected, this.index);
    }

    public void renderTabSelectorForeground(PoseStack poseStack, int guiLeft, int guiTop, ItemRenderer renderer) {
        this.type.renderIcon(poseStack, guiLeft, guiTop, this.index, renderer, this.icon);
    }

    public void drawTabBackground(PoseStack poseStack) {
        // Not sure what this does
        poseStack.pushPose();
        RenderSystem.enableDepthTest();
        poseStack.translate(0.0F, 0.0F, 950.0F);
        RenderSystem.colorMask(false, false, false, false);
        fill(poseStack, 4680, 2260, -4680, -2260, -16777216);
        RenderSystem.colorMask(true, true, true, true);
        poseStack.translate(0.0F, 0.0F, -950.0F);
        RenderSystem.depthFunc(518);
        fill(poseStack, MAX_WIDTH, MAX_HEIGHT, 0, 0, -16777216);
        RenderSystem.depthFunc(515);

        // Render background
        RenderSystem.setShaderTexture(0, this.background);
        // 0,0 is relative from the screen itself
        blit(poseStack, 0, 0, 0, 0, MAX_WIDTH, MAX_HEIGHT, MAX_WIDTH, MAX_HEIGHT);
        // Render main info
        int i = MAX_WIDTH / 2;
        int j = MAX_HEIGHT / 2;
        this.renderWindow(poseStack, i, j);

        // Not sure what this does
        RenderSystem.depthFunc(518);
        poseStack.translate(0.0F, 0.0F, -950.0F);
        RenderSystem.colorMask(false, false, false, false);
        fill(poseStack, 4680, 2260, -4680, -2260, -16777216);
        RenderSystem.colorMask(true, true, true, true);
        poseStack.translate(0.0F, 0.0F, 950.0F);
        RenderSystem.depthFunc(515);
        poseStack.popPose();
    }

    /**
     * Main render function for gui
     */
    protected abstract void renderWindow(PoseStack poseStack, int guiCentreX, int guiCentreY);

    public abstract void renderText(PoseStack poseStack, int guiCentreX, int guiCentreY);

    public void drawTabTooltips(PoseStack poseStack, int mouseX, int mouseY) {
        poseStack.pushPose();
        poseStack.translate(0.0F, 0.0F, 200.0F);
        fill(poseStack, 0, 0, MAX_WIDTH, MAX_HEIGHT, Mth.floor(this.fade * 255.0F) << 24);
        boolean flag = false;
        int i = MAX_WIDTH / 2;
        int j = MAX_HEIGHT / 2;
        if (mouseX > 0 && mouseX < MAX_WIDTH && mouseY > 0 && mouseY < MAX_HEIGHT) {
            flag = this.renderTooltips(poseStack, mouseX, mouseY, i, j);
        }

        poseStack.popPose();
        if (flag) {
            this.fade = Mth.clamp(this.fade + 0.02F, 0.0F, 0.3F);
        } else {
            this.fade = Mth.clamp(this.fade - 0.04F, 0.0F, 1.0F);
        }
    }

    protected abstract boolean renderTooltips(PoseStack poseStack, int mouseX, int mouseY, int guiCentreX, int guiCentreY);

    public boolean isInsideTabSelector(int guiLeft, int guiTop, double mouseX, double mouseY) {
        return this.type.inInsideTabSelector(guiLeft, guiTop, this.index, mouseX, mouseY);
    }
}