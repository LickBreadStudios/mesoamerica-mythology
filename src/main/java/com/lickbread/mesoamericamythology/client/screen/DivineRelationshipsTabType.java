package com.lickbread.mesoamericamythology.client.screen;

import com.mojang.blaze3d.vertex.PoseStack;
import net.minecraft.client.gui.GuiComponent;
import net.minecraft.client.renderer.entity.ItemRenderer;
import net.minecraft.util.Mth;
import net.minecraft.world.item.ItemStack;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;

@OnlyIn(Dist.CLIENT)
public enum DivineRelationshipsTabType {
    PERKS(0, 0, 32, 32, 1),
    RECIPES(32, 0, 32, 32, 1),
    GOD(64, 0, 24, 24, 8);

    private static final int X_DISTANCE = 6;
    private static final int Y_DISTANCE = 6;
    private static final int SECTION_DISTANCE = Mth.floor(Y_DISTANCE * 1.25F);

    public static final int MAX_TABS = PERKS.max + RECIPES.max + GOD.max;
    public static final int MAX_WIDTH = PERKS.width + RECIPES.width + X_DISTANCE;
    public static final int MAX_HEIGHT = PERKS.height + SECTION_DISTANCE + GOD.max / 2 * (Y_DISTANCE + GOD.height);

    private static final int GOD_WIDTH = (MAX_WIDTH - (GOD.width * 2 + X_DISTANCE)) / 2;
    private static final int GOD_HEIGHT = PERKS.height + SECTION_DISTANCE;

    private final int textureX;
    private final int textureY;
    private final int width;
    private final int height;
    private final int max;

    DivineRelationshipsTabType(int textureX, int textureY, int width, int height, int max) {
        this.textureX = textureX;
        this.textureY = textureY;
        this.width = width;
        this.height = height;
        this.max = max;
    }

    public void renderTabSelectorBackground(PoseStack poseStack, GuiComponent abstractGui, int guiLeft, int guiTop, boolean isSelected, int index) {
        int y = isSelected ? this.textureY : this.textureY + this.height;
        abstractGui.blit(poseStack, guiLeft + this.getX(index), guiTop + this.getY(index), this.textureX, y, this.width, this.height);
    }

    public void renderIcon(PoseStack poseStack, int guiLeft, int guiTop, int index, ItemRenderer renderItemIn, ItemStack stack) {
        int i = guiLeft + this.getX(index) + (this == GOD ? 3 : 8);
        int j = guiTop + this.getY(index) + (this == GOD ? 3 : 8);
        renderItemIn.renderGuiItem(poseStack, stack, i, j);
    }

    private int getX(int index) {
        switch(this) {
            case PERKS:
                return 0;
            case RECIPES:
                return PERKS.width + X_DISTANCE;
            case GOD:
                return GOD_WIDTH + ((index - 2) % 2 == 0 ? 0 : this.width + X_DISTANCE);
            default:
                throw new UnsupportedOperationException("Don't know what this tab type is! " + this);
        }
    }

    private int getY(int index) {
        switch(this) {
            case PERKS:
            case RECIPES:
                return 0;
            case GOD:
                return GOD_HEIGHT + Mth.floor((index - 2) / 2F) * Y_DISTANCE;
            default:
                throw new UnsupportedOperationException("Don't know what this tab type is! " + this);
        }
    }

    public boolean inInsideTabSelector(int guiLeft, int guiTop, int index, double mouseX, double mouseY) {
        int i = guiLeft + this.getX(index);
        int j = guiTop + this.getY(index);
        return mouseX > (double)i && mouseX < (double)(i + this.width) && mouseY > (double)j && mouseY < (double)(j + this.height);
    }
}
