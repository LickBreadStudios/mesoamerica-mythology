package com.lickbread.mesoamericamythology.client.screen;

import com.google.gson.annotations.SerializedName;
import lombok.Getter;
import net.minecraft.ChatFormatting;
import net.minecraft.network.chat.Component;

@Getter
public enum FrameType {
   @SerializedName("task")
   TASK("task", 0, ChatFormatting.GREEN),
   @SerializedName("challenge")
   CHALLENGE("challenge", 26, ChatFormatting.DARK_PURPLE),
   @SerializedName("goal")
   GOAL("goal", 52, ChatFormatting.GREEN);

   private final String name;
   private final int textureOffset;
   private final Component displayName;
   private final ChatFormatting chatColor;

   FrameType(String name, int textureOffset, ChatFormatting chatColor) {
      this.name = name;
      this.textureOffset = textureOffset;
      this.displayName = Component.translatable("mesoamericamythology.frame." + name);
      this.chatColor = chatColor;
   }
}