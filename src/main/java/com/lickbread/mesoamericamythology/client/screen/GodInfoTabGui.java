package com.lickbread.mesoamericamythology.client.screen;

import com.lickbread.mesoamericamythology.MesoamericaMythologyMod;
import com.lickbread.mesoamericamythology.statueHead.God;
import com.lickbread.mesoamericamythology.statueHead.relationship.GodStatueHeadQuest;
import com.lickbread.mesoamericamythology.util.RendererUtil;
import com.mojang.blaze3d.vertex.PoseStack;
import net.minecraft.client.resources.language.I18n;
import net.minecraft.locale.Language;
import net.minecraft.network.chat.*;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.util.FormattedCharSequence;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.Items;

import java.util.List;
import java.util.Optional;

public class GodInfoTabGui extends DivineRelationshipsTabGui {

    private static final String LABEL_ID = "screen.mesoamericamythology.divine_relationships.god.label.%s";
    private static final String NO_QUEST_ID = "screen.mesoamericamythology.divine_relationships.god.no_quest";

    private static final ResourceLocation BACKGROUND = new ResourceLocation(MesoamericaMythologyMod.MOD_ID, "textures/gui/divinerelationships/divine_relationships_god_background.png");

    private final God god;
    private final int titleWidth;

    public GodInfoTabGui(God god) {
        super(DivineRelationshipsTabType.GOD, new ItemStack(Items.STICK), Component.translatable(String.format(LABEL_ID, god.name().toLowerCase())), BACKGROUND);
        this.god = god;
        this.titleWidth = this.minecraft.font.width(this.title.getString());
    }

    @Override
    protected void renderWindow(PoseStack poseStack, int guiCentreX, int guiCentreY) {
    }

    @Override
    public void renderText(PoseStack poseStack, int guiCentreX, int guiCentreY) {
        this.renderName(poseStack);
        this.renderInfo(poseStack);
        this.renderImage(poseStack);
        this.renderQuest(poseStack);
    }

    private void renderName(PoseStack poseStack) {
        this.minecraft.font.drawShadow(poseStack, this.title, (this.screen.width / 2) - (this.titleWidth / 2), 8, -1);
    }

    private void renderInfo(PoseStack poseStack) {

    }

    private void renderImage(PoseStack poseStack) {

    }

    private void renderQuest(PoseStack poseStack) {
        Optional<GodStatueHeadQuest> quest = this.screen.getCapability().getOrCreateRelationship(this.god).getCurrentQuest();
        MutableComponent string = Component.translatable(quest.isPresent() ? quest.toString() : I18n.get(NO_QUEST_ID));
        List<FormattedCharSequence> strings = Language.getInstance().getVisualOrder(RendererUtil.findOptimalLines(this.minecraft.font, ComponentUtils.mergeStyles(string, Style.EMPTY), this.screen.width / 2 - 16));

        for(int i = 0; i < strings.size(); i++) {
            this.minecraft.font.draw(poseStack, strings.get(i), this.screen.width / 2 + 8, 16 + i * 9, -1);
        }

    }

    @Override
    protected boolean renderTooltips(PoseStack poseStack, int mouseX, int mouseY, int guiCentreX, int guiCentreY) {
        return false;
    }
}
