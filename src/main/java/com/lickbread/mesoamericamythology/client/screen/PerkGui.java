package com.lickbread.mesoamericamythology.client.screen;

import com.lickbread.mesoamericamythology.MesoamericaMythologyMod;
import com.lickbread.mesoamericamythology.api.MesoamericaMythologyAPI;
import com.lickbread.mesoamericamythology.perks.Perk;
import com.lickbread.mesoamericamythology.util.RendererUtil;
import com.mojang.blaze3d.systems.RenderSystem;
import com.mojang.blaze3d.vertex.PoseStack;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiComponent;
import net.minecraft.client.resources.language.I18n;
import net.minecraft.locale.Language;
import net.minecraft.network.chat.Component;
import net.minecraft.network.chat.ComponentUtils;
import net.minecraft.network.chat.Style;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.util.FormattedCharSequence;
import net.minecraft.util.Mth;
import net.minecraft.world.item.ItemStack;

import java.util.List;

/**
 * A lot of this is copied from {@link net.minecraft.client.gui.screens.advancements.AdvancementsScreen}
 */
public class PerkGui extends GuiComponent {

    private static final ResourceLocation WIDGETS_LOCATION = new ResourceLocation(MesoamericaMythologyMod.MOD_ID, "textures/gui/divinerelationships/widgets.png");
    private static final int[] TEST_SPLIT_OFFSETS = new int[]{0, 10, -10, 25, -25};

    private Minecraft minecraft;
    private Perk perk;
    private int x;
    private int y;
    private PerksTreeTabGui tab;
    private FormattedCharSequence title;
    private List<FormattedCharSequence> description;
    private int tooltipWidth;

    public PerkGui(Perk perk, PerksTreeTabGui tab) {
        this.perk = perk;
        this.minecraft = Minecraft.getInstance();
        this.tab = tab;
        this.calculatePosition();
        Component titleText = Component.translatable(I18n.get(this.perk.getTitleStringId()));
        Component descriptionText = Component.translatable(I18n.get(this.perk.getDescriptionStringId()));
        this.title = Language.getInstance().getVisualOrder(this.minecraft.font.substrByWidth(titleText, 163));
        int l = 29 + this.minecraft.font.width(this.title);
        this.description = Language.getInstance().getVisualOrder(RendererUtil.findOptimalLines(this.minecraft.font, ComponentUtils.mergeStyles(descriptionText.copy(), Style.EMPTY.withColor(this.perk.getDisplayInfo().getType().getChatColor())), l));
        for(FormattedCharSequence ireorderingprocessor : this.description) {
            l = Math.max(l, this.minecraft.font.width(ireorderingprocessor));
        }
        this.tooltipWidth = l + 3 + 5;
    }

    public void draw(PoseStack poseStack, int guiCentreX, int guiCentreY) {
        if (this.isHidden()) {
            return;
        }

        int state = this.getPerkState();
        int x = guiCentreX + this.x;
        int y = guiCentreY + this.y;

        RenderSystem.setShaderTexture(0, WIDGETS_LOCATION);
        // Subtract 13 to become centred
        this.blit(poseStack, x - 13, y - 13, this.perk.getDisplayInfo().getType().getTextureOffset(), 128 + state * 26, 26, 26);
        this.minecraft.getItemRenderer().renderAndDecorateFakeItem(poseStack, new ItemStack(this.perk.getDisplayInfo().getIcon()), 0, 0);
    }

    private boolean isHidden() {
        return false;
    }

    private int getPerkState() {
        // 1 = not obtained
        // 0 = obtained
        return this.tab.screen.getCapability().hasPerk(this.perk.getId()) ? 0 : 1;
    }

    public void drawTooltip(PoseStack poseStack, int guiCentreX, int guiCentreY) {
        int x = guiCentreX + this.x - 13 - 3;
        int y = guiCentreY + this.y - 13;

        boolean flag = x + this.tooltipWidth + 26 >= this.tab.getScreen().width;
        boolean flag1 = this.tab.getScreen().height - y - 26 <= 6 + this.description.size() * 9;
        int state = this.getPerkState();

        RenderSystem.setShaderTexture(0, WIDGETS_LOCATION);
        RenderSystem.setShaderColor(1.0F, 1.0F, 1.0F, 1.0F);
        RenderSystem.enableBlend();
        int i1;
        if (flag) {
            i1 = x - this.tooltipWidth + 26 + 6;
        } else {
            i1 = x;
        }

        int j1 = 32 + this.description.size() * 9;
        if (!this.description.isEmpty()) {
            if (flag1) {
                RendererUtil.render9Sprite(this, poseStack, i1, y + 26 - j1, this.tooltipWidth, j1, 10, 200, 26, 0, 52);
            } else {
                RendererUtil.render9Sprite(this, poseStack, i1, y, this.tooltipWidth, j1, 10, 200, 26, 0, 52);
            }
        }

        // x in screen, y in screen, x texture offset, y texture offset, x texture length, y texture length
        this.blit(poseStack, i1, y, 0, state * 26, 3, 26);
        this.blit(poseStack, i1 + 3, y, 200 - this.tooltipWidth + 3, state * 26, this.tooltipWidth - 3, 26);
        this.blit(poseStack, x + 3, y, this.perk.getDisplayInfo().getType().getTextureOffset(), 128 + state * 26, 26, 26);
        if (flag) {
            this.minecraft.font.drawShadow(poseStack, this.title, (float)(i1 + 5), (float)(y + 9), -1);
        } else {
            this.minecraft.font.drawShadow(poseStack, this.title, (float)(x + 32), (float)(y + 9), -1);
        }

        if (flag1) {
            for(int k1 = 0; k1 < this.description.size(); ++k1) {
                this.minecraft.font.draw(poseStack, this.description.get(k1), (float)(i1 + 5), (float)(y + 26 - j1 + 7 + k1 * 9), -5592406);
            }
        } else {
            for(int l1 = 0; l1 < this.description.size(); ++l1) {
                this.minecraft.font.draw(poseStack, this.description.get(l1), (float)(i1 + 5), (float)(y + 9 + 17 + l1 * 9), -5592406);
            }
        }

        this.minecraft.getItemRenderer().renderAndDecorateFakeItem(poseStack, new ItemStack(this.perk.getDisplayInfo().getIcon()), x + 8, y + 5);
    }

    public boolean isMouseOver(int mouseX, int mouseY, int guiCentreX, int guiCentreY) {
        if (this.isHidden()) {
            return false;
        }

        int i = this.x + guiCentreX - 13;
        int j = i + 26;
        int k = this.y + guiCentreY - 13;
        int l = k + 26;
        return mouseX >= i && mouseX <= j && mouseY >= k && mouseY <= l;
    }

    private void calculatePosition() {
        List<String> perksInSection = MesoamericaMythologyAPI.getPerkTree().getPerksInTierAndSection(this.perk.getTier(), this.perk.getSection());
        int index = perksInSection.indexOf(this.perk.getId());
        int r = this.perk.getTier().getR();
        // Get the angle of display. This is done by rotating around the circle based upon the section,
        // adding a small buffer using the tier, and then dividing that by the number of perks to display
        int anglePerPerk = (60 - (this.perk.getTier().getTheta() * 2)) / (perksInSection.size() + 1);
        int theta = this.perk.getSection().getTheta() + this.perk.getTier().getTheta() + anglePerPerk * (index + 1);

        this.x = Mth.floor(r * Mth.cos((float) Math.toRadians(theta)));
        this.y = Mth.floor(r * Mth.sin((float) Math.toRadians(theta)));
    }
}
