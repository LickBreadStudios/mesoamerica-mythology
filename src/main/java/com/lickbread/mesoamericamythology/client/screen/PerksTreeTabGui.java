package com.lickbread.mesoamericamythology.client.screen;

import com.lickbread.mesoamericamythology.MesoamericaMythologyMod;
import com.lickbread.mesoamericamythology.api.MesoamericaMythologyAPI;
import com.lickbread.mesoamericamythology.perks.PerkTree;
import com.lickbread.mesoamericamythology.registry.ModItems;
import com.mojang.blaze3d.vertex.PoseStack;
import net.minecraft.network.chat.Component;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.item.ItemStack;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Displays current status about divine favour and relationship
 */
public class PerksTreeTabGui extends DivineRelationshipsTabGui {

    private static final ItemStack ICON = new ItemStack(ModItems.JADE.get(), 1);
    private static final Component LABEL = Component.translatable("screen.mesoamericamythology.divine_relationships.perkstree.label");
    private static final ResourceLocation BACKGROUND = new ResourceLocation(MesoamericaMythologyMod.MOD_ID, "textures/gui/divinerelationships/divine_relationships_perk_background.png");

    private List<PerkGui> displayedPerks = new ArrayList<>();

    public PerksTreeTabGui() {
        super(DivineRelationshipsTabType.PERKS, ICON, LABEL, BACKGROUND);
    }

    @Override
    public void setScreen(DivineRelationshipsScreen screen) {
        super.setScreen(screen);
        PerkTree perkTree = MesoamericaMythologyAPI.getPerkTree();
        this.displayedPerks = perkTree.getPerks().values().stream().map(perk -> new PerkGui(perk, this)).collect(Collectors.toList());
    }

    @Override
    protected void renderWindow(PoseStack poseStack, int guiCentreX, int guiCentreY) {
        displayedPerks.forEach(perkGui -> {
            perkGui.draw(poseStack, guiCentreX, guiCentreY);
        });
    }

    @Override
    public void renderText(PoseStack poseStack, int guiCentreX, int guiCentreY) {
    }

    @Override
    protected boolean renderTooltips(PoseStack poseStack, int mouseX, int mouseY, int guiCentreX, int guiCentreY) {
        for (PerkGui perkGui : displayedPerks) {
            if (perkGui.isMouseOver(mouseX, mouseY, guiCentreX, guiCentreY)) {
                perkGui.drawTooltip(poseStack, guiCentreX, guiCentreY);
                return true;
            }
        }

        return false;
    }

}
