package com.lickbread.mesoamericamythology.client.screen;

import com.lickbread.mesoamericamythology.MesoamericaMythologyMod;
import com.mojang.blaze3d.vertex.PoseStack;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.Items;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.network.chat.Component;

public class RecipeTabGui extends DivineRelationshipsTabGui {

    private static final Component LABEL = Component.translatable("screen.mesoamericamythology.divine_relationships.recipebook.label");

    private static final ResourceLocation BACKGROUND = new ResourceLocation(MesoamericaMythologyMod.MOD_ID, "textures/gui/divinerelationships/divine_relationships_god_background.png");

    public RecipeTabGui() {
        super(DivineRelationshipsTabType.RECIPES, new ItemStack(Items.BOOK), LABEL, BACKGROUND);
    }

    @Override
    protected void renderWindow(PoseStack poseStack, int guiCentreX, int guiCentreY) {

    }

    @Override
    public void renderText(PoseStack poseStack, int guiCentreX, int guiCentreY) {
    }

    @Override
    protected boolean renderTooltips(PoseStack poseStack, int mouseX, int mouseY, int guiCentreX, int guiCentreY) {
        return false;
    }
}
