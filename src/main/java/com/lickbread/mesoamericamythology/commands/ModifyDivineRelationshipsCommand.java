package com.lickbread.mesoamericamythology.commands;

import com.lickbread.mesoamericamythology.MesoamericaMythologyMod;
import com.lickbread.mesoamericamythology.api.MesoamericaMythologyAPI;
import com.lickbread.mesoamericamythology.statueHead.God;
import com.lickbread.mesoamericamythology.statueHead.relationship.IDivineFavourCapability;
import com.mojang.brigadier.CommandDispatcher;
import com.mojang.brigadier.builder.LiteralArgumentBuilder;
import com.mojang.brigadier.context.CommandContext;
import net.minecraft.client.resources.language.I18n;
import net.minecraft.commands.CommandSourceStack;
import net.minecraft.commands.Commands;
import net.minecraft.commands.arguments.MessageArgument;
import net.minecraft.network.chat.Component;
import net.minecraft.server.level.ServerPlayer;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.player.Player;
import net.minecraftforge.server.command.EnumArgument;

import static com.lickbread.mesoamericamythology.api.MesoamericaMythologyAPI.GSON;
import static com.lickbread.mesoamericamythology.capabilities.DivineFavourCapabilityProvider.DIVINE_FAVOUR_CAPABILITY;

public class ModifyDivineRelationshipsCommand {

    private static final LiteralArgumentBuilder<CommandSourceStack> CLEAR_QUEST_COMMAND = Commands.literal("clearquest")
            .then(Commands.argument("god", EnumArgument.enumArgument(God.class))
                    .executes(ModifyDivineRelationshipsCommand::clearQuest)
            );

    private static final LiteralArgumentBuilder<CommandSourceStack> DUMP_COMMAND = Commands.literal("dump")
            .executes(ModifyDivineRelationshipsCommand::dumpCapability);

    private static final LiteralArgumentBuilder<CommandSourceStack> PERK_COMMAND = Commands.literal("perk")
            .then(Commands.literal("add")
                    .then(Commands.argument("perkId", MessageArgument.message())
                            .executes(commandContext -> addPerk(commandContext, MessageArgument.getMessage(commandContext, "perkId").getString()))
                    )
            )
            .then(Commands.literal("remove")
                    .then(Commands.argument("perkId", MessageArgument.message())
                            .executes(commandContext -> removePerk(commandContext, MessageArgument.getMessage(commandContext, "perkId").getString()))
                    )
            )
            .then(Commands.literal("clear")
                    .executes(commandContext -> clearPerks(commandContext))
            );

    public static void register(CommandDispatcher<CommandSourceStack> dispatcher) {
        LiteralArgumentBuilder<CommandSourceStack> modifyDivineRelationshipsCommand = Commands.literal("divinerelationships")
                .requires((commandSource) -> commandSource.hasPermission(2))
                .then(CLEAR_QUEST_COMMAND)
                .then(DUMP_COMMAND)
                .then(PERK_COMMAND);

        dispatcher.register(modifyDivineRelationshipsCommand);
    }

    private static int clearQuest(CommandContext<CommandSourceStack> commandContext) {
        God god = commandContext.getArgument("god", God.class);
        Entity entity = commandContext.getSource().getEntity();
        if (entity instanceof Player) {
            IDivineFavourCapability divineFavourCapability = entity.getCapability(DIVINE_FAVOUR_CAPABILITY).orElseGet(null);
            divineFavourCapability.clearCurrentQuest(god, (ServerPlayer) entity);
            entity.sendSystemMessage(Component.translatable(I18n.get("mesoamericamythology.command.cleared_quest", god.name())));
        }
        return 1;
    }

    private static int dumpCapability(CommandContext<CommandSourceStack> commandContext) {
        Entity entity = commandContext.getSource().getEntity();
        if (entity instanceof Player) {
            IDivineFavourCapability divineFavourCapability = entity.getCapability(DIVINE_FAVOUR_CAPABILITY).orElseGet(null);
            MesoamericaMythologyMod.LOGGER.info("Dumped Capability:\n" + GSON.toJson(divineFavourCapability));
            entity.sendSystemMessage(Component.translatable(I18n.get("mesoamericamythology.command.dumped_capability")));
        }
        return 1;
    }

    private static int addPerk(CommandContext<CommandSourceStack> commandContext, String perkId) {
        Entity entity = commandContext.getSource().getEntity();
        if (entity instanceof Player) {
            IDivineFavourCapability divineFavourCapability = entity.getCapability(DIVINE_FAVOUR_CAPABILITY).orElseGet(null);
            divineFavourCapability.addPerk(MesoamericaMythologyAPI.getPerkTree().getPerk(perkId), (ServerPlayer) entity);
            entity.sendSystemMessage(Component.translatable((I18n.get("mesoamericamythology.command.added_perk", perkId)), entity.getUUID()));
        }
        return 1;
    }

    private static int removePerk(CommandContext<CommandSourceStack> commandContext, String perkId) {
        Entity entity = commandContext.getSource().getEntity();
        if (entity instanceof Player) {
            IDivineFavourCapability divineFavourCapability = entity.getCapability(DIVINE_FAVOUR_CAPABILITY).orElseGet(null);
            divineFavourCapability.removePerk(MesoamericaMythologyAPI.getPerkTree().getPerk(perkId), (ServerPlayer) entity);
            entity.sendSystemMessage(Component.translatable((I18n.get("mesoamericamythology.command.removed_perk", perkId))));
        }
        return 1;
    }

    private static int clearPerks(CommandContext<CommandSourceStack> commandContext) {
        Entity entity = commandContext.getSource().getEntity();
        if (entity instanceof Player) {
            IDivineFavourCapability divineFavourCapability = entity.getCapability(DIVINE_FAVOUR_CAPABILITY).orElseGet(null);
            divineFavourCapability.clearPerks((ServerPlayer) entity);
            entity.sendSystemMessage(Component.translatable((I18n.get("mesoamericamythology.command.cleared_perk"))));
        }
        return 1;
    }
}
