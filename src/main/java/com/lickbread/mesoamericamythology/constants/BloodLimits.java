package com.lickbread.mesoamericamythology.constants;

public class BloodLimits {

    public static final float BLOOD_PER_HEART_POINT = 50.0F;

    public static final float MAX_BLOOD_STONE_ALTAR = 10000.0F;
    public static final float MAX_BLOOD_BLOOD_BASIN = 25000.0F;

    public static final float BLOOD_TRANSFER_RATE_STONE_ALTAR = 25.0F;
    public static final float BLOOD_TRANSFER_RATE_BLOOD_BASIN = 25.0F;
}
