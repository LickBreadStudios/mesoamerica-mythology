package com.lickbread.mesoamericamythology.controls;

import com.lickbread.mesoamericamythology.client.screen.DivineRelationshipsScreen;
import com.lickbread.mesoamericamythology.registry.event.ModKeyBindings;
import net.minecraft.client.Minecraft;
import net.minecraftforge.event.TickEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;

@Mod.EventBusSubscriber
public class ModKeyInputs {

    @SubscribeEvent
    public static void onKeyInput(TickEvent.ClientTickEvent event) {
        if (event.phase == TickEvent.Phase.END) {
            while(ModKeyBindings.OPEN_DIVINE_FAVOUR_RELATIONSHIP.consumeClick()) {
                Minecraft.getInstance().setScreen(new DivineRelationshipsScreen(Minecraft.getInstance().player));
            }
        }
    }
}
