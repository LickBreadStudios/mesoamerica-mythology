package com.lickbread.mesoamericamythology.criterionTrigger;

import com.google.gson.JsonObject;
import com.lickbread.mesoamericamythology.MesoamericaMythologyMod;
import net.minecraft.advancements.critereon.SimpleCriterionTrigger;
import net.minecraft.advancements.critereon.AbstractCriterionTriggerInstance;
import net.minecraft.advancements.critereon.EntityPredicate;
import net.minecraft.server.level.ServerPlayer;
import net.minecraft.advancements.critereon.DeserializationContext;
import net.minecraft.resources.ResourceLocation;

public class AnyAltarRecipeCompletedTrigger extends SimpleCriterionTrigger<AnyAltarRecipeCompletedTrigger.Instance> {

    private static final ResourceLocation resourceId = new ResourceLocation(MesoamericaMythologyMod.MOD_ID, "any_altar_recipe_complete");

    public AnyAltarRecipeCompletedTrigger() {
    }

    @Override
    protected AnyAltarRecipeCompletedTrigger.Instance createInstance(JsonObject json, EntityPredicate.Composite entityPredicate, DeserializationContext conditionsParser) {
        return new AnyAltarRecipeCompletedTrigger.Instance(entityPredicate);
    }

    public void trigger(ServerPlayer serverPlayerEntity) {
        this.trigger(serverPlayerEntity, (instance) -> true);
    }

    public ResourceLocation getId() {
        return resourceId;
    }

    public static class Instance extends AbstractCriterionTriggerInstance {

        public Instance(EntityPredicate.Composite entityPredicate) {
            super(AnyAltarRecipeCompletedTrigger.resourceId, entityPredicate);
        }
    }
}
