package com.lickbread.mesoamericamythology.entity;

import com.lickbread.mesoamericamythology.registry.ModDamageSources;
import com.lickbread.mesoamericamythology.registry.ModEntityTypes;
import com.lickbread.mesoamericamythology.registry.ModItems;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.sounds.SoundEvent;
import net.minecraft.sounds.SoundEvents;
import net.minecraft.world.damagesource.DamageSource;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.EntityType;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.entity.projectile.AbstractArrow;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.enchantment.EnchantmentHelper;
import net.minecraft.world.level.Level;
import net.minecraft.world.phys.EntityHitResult;
import net.minecraft.world.phys.Vec3;

import javax.annotation.Nullable;

public class ThrownJavelin extends AbstractArrow {

   public static final String THROWN_JAVELIN_NAME = "thrown_javelin";

   private ItemStack javelinItem = new ItemStack(ModItems.JAVELIN.get());
   private boolean dealtDamage;

   public ThrownJavelin(EntityType<? extends ThrownJavelin> entityType, Level level) {
      super(entityType, level);
   }

   public ThrownJavelin(Level level, LivingEntity entity, ItemStack itemStack) {
      super(ModEntityTypes.THROWN_JAVELIN.get(), entity, level);
      this.javelinItem = itemStack.copy();
   }

   protected ItemStack getPickupItem() {
      return this.javelinItem.copy();
   }

   @Nullable
   protected EntityHitResult findHitEntity(Vec3 p_37575_, Vec3 p_37576_) {
      return this.dealtDamage ? null : super.findHitEntity(p_37575_, p_37576_);
   }

   protected void onHitEntity(EntityHitResult p_37573_) {
      Entity entity = p_37573_.getEntity();
      float f = 8.0F;
      if (entity instanceof LivingEntity) {
         LivingEntity livingentity = (LivingEntity)entity;
         f += EnchantmentHelper.getDamageBonus(this.javelinItem, livingentity.getMobType());
      }

      Entity entity1 = this.getOwner();
      DamageSource damagesource = ModDamageSources.source(level, ModDamageSources.THROWN_JAVELIN, this, entity1);
      this.dealtDamage = true;
      if (entity.hurt(damagesource, f)) {
         if (entity.getType() == EntityType.ENDERMAN) {
            return;
         }

         if (entity instanceof LivingEntity) {
            LivingEntity livingentity1 = (LivingEntity)entity;
            if (entity1 instanceof LivingEntity) {
               EnchantmentHelper.doPostHurtEffects(livingentity1, entity1);
               EnchantmentHelper.doPostDamageEffects((LivingEntity)entity1, livingentity1);
            }

            this.doPostHurtEffects(livingentity1);
         }
      }

      this.setDeltaMovement(this.getDeltaMovement().multiply(-0.01D, -0.1D, -0.01D));

      // TODO
//      this.playSound(soundevent, f1, 1.0F);
   }

   protected boolean tryPickup(Player player) {
      return super.tryPickup(player) || this.isNoPhysics() && this.ownedBy(player) && player.getInventory().add(this.getPickupItem());
   }

   protected SoundEvent getDefaultHitGroundSoundEvent() {
      // TODO
      return SoundEvents.TRIDENT_HIT_GROUND;
   }

   public void playerTouch(Player player) {
      if (this.ownedBy(player) || this.getOwner() == null) {
         super.playerTouch(player);
      }

   }

   public void readAdditionalSaveData(CompoundTag tag) {
      super.readAdditionalSaveData(tag);
      if (tag.contains("Javelin", 10)) {
         this.javelinItem = ItemStack.of(tag.getCompound("Javelin"));
      }

      this.dealtDamage = tag.getBoolean("DealtDamage");
   }

   public void addAdditionalSaveData(CompoundTag tag) {
      super.addAdditionalSaveData(tag);
      tag.put("Javelin", this.javelinItem.save(new CompoundTag()));
      tag.putBoolean("DealtDamage", this.dealtDamage);
   }

   public void tickDespawn() {
      if (this.pickup != Pickup.ALLOWED) {
         super.tickDespawn();
      }
   }

   protected float getWaterInertia() {
      return 0.99F;
   }

   public boolean shouldRender(double p_37588_, double p_37589_, double p_37590_) {
      return true;
   }
}