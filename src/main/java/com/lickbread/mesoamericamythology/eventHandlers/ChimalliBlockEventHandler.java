package com.lickbread.mesoamericamythology.eventHandlers;

import com.lickbread.mesoamericamythology.items.ChimalliShield;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.projectile.Arrow;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.phys.AABB;
import net.minecraft.world.phys.Vec3;
import net.minecraftforge.event.TickEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;

import java.util.List;

@Mod.EventBusSubscriber
public class ChimalliBlockEventHandler {

    @SubscribeEvent
    public static void onPlayerTickEvent(TickEvent.PlayerTickEvent event) {
        if (!(event.player.isBlocking() && event.player.getItemInHand(InteractionHand.OFF_HAND).getItem() instanceof ChimalliShield && event.side.isServer())) {
            return;
        }

        AABB axisAlignedBB = new AABB(event.player.position().subtract(new Vec3(1.0D, 1.0D, 1.0D)),
                event.player.position().add(new Vec3(1.0D, 1.0D, 1.0D)));

        List<Arrow> entities = event.player.level.getEntitiesOfClass(Arrow.class, axisAlignedBB);

        for (Arrow entity : entities) {
            System.out.println("FUCK");
            entity.remove(Entity.RemovalReason.KILLED);
        }
    }

}
