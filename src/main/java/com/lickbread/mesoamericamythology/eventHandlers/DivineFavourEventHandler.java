package com.lickbread.mesoamericamythology.eventHandlers;

import com.lickbread.mesoamericamythology.MesoamericaMythologyMod;
import com.lickbread.mesoamericamythology.capabilities.DivineFavourCapabilityProvider;
import com.lickbread.mesoamericamythology.statueHead.relationship.DivineFavourCapability;
import com.lickbread.mesoamericamythology.statueHead.relationship.IDivineFavourCapability;
import net.minecraft.world.entity.player.Player;
import net.minecraft.server.level.ServerPlayer;
import net.minecraftforge.common.util.LazyOptional;
import net.minecraftforge.event.entity.player.PlayerEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;

@Mod.EventBusSubscriber
public class DivineFavourEventHandler {

    /**
     * Copy data from dead player to the new player
     */
    @SubscribeEvent
    public static void onPlayerClone(PlayerEvent.Clone event) {
        if (event.getOriginal().level.isClientSide()) {
            return;
        }

        Player player = event.getEntity();
        LazyOptional<IDivineFavourCapability> newDivineFavour = player.getCapability(DivineFavourCapabilityProvider.DIVINE_FAVOUR_CAPABILITY, null);
        LazyOptional<IDivineFavourCapability> oldDivineFavour = event.getOriginal().getCapability(DivineFavourCapabilityProvider.DIVINE_FAVOUR_CAPABILITY, null);

        if (!newDivineFavour.isPresent() || !oldDivineFavour.isPresent()) {
            MesoamericaMythologyMod.LOGGER.error("Divine favour not found on player clone event.");
            return;
        }

        newDivineFavour.orElseGet(DivineFavourCapability::new).copyFrom(oldDivineFavour.orElseGet(DivineFavourCapability::new));
    }

    @SubscribeEvent
    public static void onPlayerDeath(PlayerEvent.PlayerRespawnEvent event) {
        if (event.getEntity().level.isClientSide()) {
            return;
        }

        Player player = event.getEntity();
        player.getCapability(DivineFavourCapabilityProvider.DIVINE_FAVOUR_CAPABILITY, null).orElseGet(null).sync((ServerPlayer) player);
    }

    @SubscribeEvent
    public static void onPlayerChangedDimension(PlayerEvent.PlayerChangedDimensionEvent event) {
        if (event.getEntity().level.isClientSide()) {
            return;
        }

        Player player = event.getEntity();
        player.getCapability(DivineFavourCapabilityProvider.DIVINE_FAVOUR_CAPABILITY, null).orElseGet(null).sync((ServerPlayer) player);
    }

    @SubscribeEvent
    public static void onPlayerLogin(PlayerEvent.PlayerLoggedInEvent event) {
        if (event.getEntity().level.isClientSide()) {
            return;
        }

        Player player = event.getEntity();
        player.getCapability(DivineFavourCapabilityProvider.DIVINE_FAVOUR_CAPABILITY, null).orElseGet(null).sync((ServerPlayer) player);
    }
}
