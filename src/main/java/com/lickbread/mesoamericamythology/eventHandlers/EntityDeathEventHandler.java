package com.lickbread.mesoamericamythology.eventHandlers;

import com.lickbread.mesoamericamythology.MesoamericaMythologyMod;
import com.lickbread.mesoamericamythology.api.MesoamericaMythologyAPI;
import com.lickbread.mesoamericamythology.statueHead.event.PlayerDeathEvent;
import com.lickbread.mesoamericamythology.statueHead.event.PlayerKillEvent;
import com.lickbread.mesoamericamythology.blockEntities.AbstractBloodBasinBlockEntity;
import com.lickbread.mesoamericamythology.util.BlockPosUtil;
import com.lickbread.mesoamericamythology.util.MinMaxBounds;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.entity.player.Player;
import net.minecraft.core.BlockPos;
import net.minecraft.world.phys.Vec3;
import net.minecraft.core.Vec3i;
import net.minecraft.world.level.Level;
import net.minecraftforge.event.entity.living.LivingDeathEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;

import java.util.Comparator;
import java.util.List;
import java.util.Optional;

import static com.lickbread.mesoamericamythology.eventHandlers.QuestEventHandler.processEvent;

@Mod.EventBusSubscriber
public class EntityDeathEventHandler {

    public static final int MAX_DEATH_RANGE = 7;
    public static final Vec3i MAX_DEATH_RANGE_VEC = new Vec3i(MAX_DEATH_RANGE, MAX_DEATH_RANGE, MAX_DEATH_RANGE);

    @SubscribeEvent
    public static void entityDeath(LivingDeathEvent event) {
        LivingEntity entity = event.getEntity();
        Level worldIn = entity.level;

        if (worldIn.isClientSide()) {
            return;
        }

        Vec3 posVec = entity.position();
        BlockPos pos = entity.blockPosition();

        MinMaxBounds bounds = new MinMaxBounds(pos.subtract(MAX_DEATH_RANGE_VEC), pos.offset(MAX_DEATH_RANGE_VEC));

        List<AbstractBloodBasinBlockEntity> basins = BlockPosUtil.getAllBlockEntitiesWithinBounds(worldIn, bounds, AbstractBloodBasinBlockEntity.class);

        Optional<AbstractBloodBasinBlockEntity> nearestBasin = basins.stream()
                .min(Comparator.comparingDouble(o -> o.getBlockPos().distToCenterSqr(posVec.x, posVec.y, posVec.z)));

        // Transfer blood
        nearestBasin.ifPresent(basin -> {
                    float bloodAmount = MesoamericaMythologyAPI.getBloodForEntity(entity);
                    basin.addBlood(bloodAmount, posVec);
                    MesoamericaMythologyMod.LOGGER.debug("Added " + bloodAmount + " blood to pool at " + basin.getBlockPos().toString() + ". Total blood: " + basin.getTotalBloodAmount());
                });

        // Handle player source
        if (event.getSource().getDirectEntity() instanceof Player) {
            processEvent((Player) event.getSource().getDirectEntity(), new PlayerKillEvent((Player) event.getSource().getDirectEntity(), event.getEntity(), nearestBasin));
        }

        if (event.getEntity() instanceof Player) {
            processEvent((Player) event.getEntity(), new PlayerDeathEvent((Player) event.getEntity(), event.getSource()));
        }
    }
}
