package com.lickbread.mesoamericamythology.eventHandlers;

import com.lickbread.mesoamericamythology.items.EnrichedPlayerHeartItem;
import com.lickbread.mesoamericamythology.items.LivingHeartItem;
import com.lickbread.mesoamericamythology.items.PlayerHeartItem;
import com.lickbread.mesoamericamythology.registry.ModItems;
import net.minecraft.server.level.ServerPlayer;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.entity.EntityType;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.entity.item.ItemEntity;
import net.minecraft.world.entity.monster.Monster;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.Level;
import net.minecraft.world.phys.Vec3;
import net.minecraftforge.event.entity.living.LivingDropsEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;

import java.util.Random;

@Mod.EventBusSubscriber
public class LivingHeartEventHandler {

    private static final float MONSTER_HEART_DROP_CHANCE = 5.0F;

    @SubscribeEvent
    public static void entityDrops(LivingDropsEvent event) {
        LivingEntity entity = event.getEntity();
        Level worldIn = entity.level;

        if (!worldIn.isClientSide()) {
            if (!(event.getSource().getDirectEntity() instanceof ServerPlayer)) {
                return;
            }

            tryMonsterDrops(entity, event);
            tryPlayerDrops(entity, event);
        }
    }

    private static void tryPlayerDrops(LivingEntity entity, LivingDropsEvent event) {
        if (!(entity instanceof Player)) {
            return;
        }

        if (!event.getSource().type().msgId().equals("obsidian_knife")) {
            return;
        }

        if (event.getSource().getDirectEntity() != entity) {
            return;
        }

        ServerPlayer playerEntity = (ServerPlayer) event.getSource().getDirectEntity();

        ItemStack heartItemStack;
        if (playerEntity.experienceLevel > 50) {
            heartItemStack = EnrichedPlayerHeartItem.createFromEntityType(playerEntity, ModItems.ENRICHED_PLAYER_HEART.get(), playerEntity.experienceLevel);
        } else {
            heartItemStack = PlayerHeartItem.createFromEntityType(playerEntity, ModItems.PLAYER_HEART.get(), playerEntity.experienceLevel);
        }

        Vec3 position = event.getEntity().position();
        event.getDrops().add(new ItemEntity(event.getEntity().level, position.x(), position.y(), position.z(), heartItemStack));
    }

    private static void tryMonsterDrops(LivingEntity entity, LivingDropsEvent event) {
        if (!(entity instanceof Monster)) {
            return;
        }

        if (!(event.getSource().getDirectEntity() instanceof ServerPlayer)) {
            return;
        }

        ServerPlayer playerEntity = (ServerPlayer) event.getSource().getDirectEntity();

        if (playerEntity.getItemInHand(InteractionHand.MAIN_HAND).getItem() != ModItems.OBSIDIAN_KNIFE.get() && playerEntity.getItemInHand(InteractionHand.OFF_HAND).getItem() != ModItems.OBSIDIAN_KNIFE.get()) {
            return;
        }

        Random random = new Random();

        // TODO Add looting
        if (random.nextDouble() < MONSTER_HEART_DROP_CHANCE) {
            ItemStack heartItemStack = LivingHeartItem.createFromEntityType((EntityType<? extends LivingEntity>) event.getEntity().getType(), ModItems.MONSTER_HEART.get());
            Vec3 position = event.getEntity().position();
            event.getDrops().add(new ItemEntity(event.getEntity().level, position.x(), position.y(), position.z(), heartItemStack));
        }
    }
}
