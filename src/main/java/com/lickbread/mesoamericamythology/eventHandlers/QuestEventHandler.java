package com.lickbread.mesoamericamythology.eventHandlers;

import com.lickbread.mesoamericamythology.capabilities.DivineFavourCapabilityProvider;
import com.lickbread.mesoamericamythology.statueHead.event.IGodStatueHeadQuestEvent;
import com.lickbread.mesoamericamythology.statueHead.relationship.IDivineFavourCapability;
import net.minecraft.world.entity.player.Player;
import net.minecraft.server.level.ServerPlayer;

public class QuestEventHandler {

    public static void processEvent(Player playerEntity, IGodStatueHeadQuestEvent event) {
        IDivineFavourCapability divineFavourCapability = playerEntity.getCapability(DivineFavourCapabilityProvider.DIVINE_FAVOUR_CAPABILITY).orElse(null);
        divineFavourCapability.processEvent(event, (ServerPlayer) playerEntity);
    }
}
