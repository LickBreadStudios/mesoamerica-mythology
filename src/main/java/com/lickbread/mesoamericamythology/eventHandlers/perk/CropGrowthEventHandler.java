package com.lickbread.mesoamericamythology.eventHandlers.perk;

import com.lickbread.mesoamericamythology.capabilities.DivineFavourCapabilityProvider;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.phys.AABB;
import net.minecraft.core.BlockPos;
import net.minecraft.core.Vec3i;
import net.minecraft.world.level.LevelAccessor;
import net.minecraftforge.common.util.LazyOptional;
import net.minecraftforge.event.level.BlockEvent;
import net.minecraftforge.eventbus.api.Event;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;

import java.util.List;

@Mod.EventBusSubscriber
public class CropGrowthEventHandler {

    private static final int GROWTH_INCREASE_RANGE = 4;

    // Increases crop growth
    @SubscribeEvent
    public static void cropGrowPre(BlockEvent.CropGrowEvent.Pre event) {
        LevelAccessor level = event.getLevel();
        if (event.getLevel().isClientSide()) {
            return;
        }

        BlockPos min = event.getPos().subtract(new Vec3i(GROWTH_INCREASE_RANGE, GROWTH_INCREASE_RANGE, GROWTH_INCREASE_RANGE));
        BlockPos max = event.getPos().offset(new Vec3i(GROWTH_INCREASE_RANGE, GROWTH_INCREASE_RANGE, GROWTH_INCREASE_RANGE));

        List<Player> nearbyPlayers = level.getEntitiesOfClass(Player.class, new AABB(min, max));
        boolean shouldGrow = nearbyPlayers.stream()
                .map(playerEntity -> playerEntity.getCapability(DivineFavourCapabilityProvider.DIVINE_FAVOUR_CAPABILITY))
                .filter(LazyOptional::isPresent)
                .map(capability -> capability.orElseGet(null))
                .anyMatch(capability -> capability.hasPerk("gathering_crop_growth_increase"));

        if (shouldGrow) {
            event.setResult(Event.Result.ALLOW);
        }
    }
}
