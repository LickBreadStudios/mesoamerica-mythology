package com.lickbread.mesoamericamythology.init;

import com.lickbread.mesoamericamythology.blockEntities.*;
import net.minecraft.world.level.block.entity.BlockEntityType;
import net.minecraftforge.registries.ObjectHolder;

import static com.lickbread.mesoamericamythology.MesoamericaMythologyMod.MOD_ID;


public class ModBlockEntityTypeObjects {

    @ObjectHolder(registryName = "minecraft:block_entity_type", value = MOD_ID + ":stone_altar")
    public static final BlockEntityType<StoneAltarBlockEntity> STONE_ALTAR = null;

    @ObjectHolder(registryName = "minecraft:block_entity_type", value = MOD_ID + ":god_statue")
    public static final BlockEntityType<GodStatueHeadBlockEntity> GOD_STATUE = null;

    @ObjectHolder(registryName = "minecraft:block_entity_type", value = MOD_ID + ":totem")
    public static final BlockEntityType<TotemBlockEntity> TOTEM = null;

    @ObjectHolder(registryName = "minecraft:block_entity_type", value = MOD_ID + ":cuauhxicalli")
    public static final BlockEntityType<CuauhxicalliBlockEntity> CUAUHXICALLI = null;

    @ObjectHolder(registryName = "minecraft:block_entity_type", value = MOD_ID + ":chacmool")
    public static final BlockEntityType<ChacmoolBlockEntity> CHACMOOL = null;

    @ObjectHolder(registryName = "minecraft:block_entity_type", value = MOD_ID + ":fire_trap")
    public static final BlockEntityType<FireTrapBlockEntity> FIRE_TRAP = null;

    @ObjectHolder(registryName = "minecraft:block_entity_type", value = MOD_ID + ":spike_trap")
    public static final BlockEntityType<SpikeTrapBlockEntity> SPIKE_TRAP = null;

    @ObjectHolder(registryName = "minecraft:block_entity_type", value = MOD_ID + ":spear_trap")
    public static final BlockEntityType<SpearTrapBlockEntity> SPEAR_TRAP = null;

    @ObjectHolder(registryName = "minecraft:block_entity_type", value = MOD_ID + ":pedestal")
    public static final BlockEntityType<PedestalBlockEntity> PEDESTAL = null;

    @ObjectHolder(registryName = "minecraft:block_entity_type", value = MOD_ID + ":sacrifice_pedestal")
    public static final BlockEntityType<SacrificePedestalBlockEntity> SACRIFICE_PEDESTAL = null;

    @ObjectHolder(registryName = "minecraft:block_entity_type", value = MOD_ID + ":power_pedestal")
    public static final BlockEntityType<PowerPedestalBlockEntity> POWER_PEDESTAL = null;

    @ObjectHolder(registryName = "minecraft:block_entity_type", value = MOD_ID + ":infusion_pedestal")
    public static final BlockEntityType<InfusionPedestalBlockEntity> INFUSION_PEDESTAL = null;

    @ObjectHolder(registryName = "minecraft:block_entity_type", value = MOD_ID + ":blood_basin")
    public static final BlockEntityType<BloodBasinBlockEntity> BLOOD_BASIN = null;

    @ObjectHolder(registryName = "minecraft:block_entity_type", value = MOD_ID + ":xolotls_vessel")
    public static final BlockEntityType<XolotlsVesselBlockEntity> XOLOTLS_VESSEL = null;

    @ObjectHolder(registryName = "minecraft:block_entity_type", value = MOD_ID + ":offering_bowl")
    public static final BlockEntityType<OfferingBowlBlockEntity> OFFERING_BOWL = null;
}
