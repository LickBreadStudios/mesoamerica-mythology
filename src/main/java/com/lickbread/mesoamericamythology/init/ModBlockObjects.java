package com.lickbread.mesoamericamythology.init;

import net.minecraft.world.level.block.Block;
import net.minecraftforge.registries.ObjectHolder;

import static com.lickbread.mesoamericamythology.MesoamericaMythologyMod.MOD_ID;

public class ModBlockObjects {

    @ObjectHolder(registryName = "minecraft:block", value = MOD_ID + ":stone_altar")
    public static final Block STONE_ALTAR = null;

    @ObjectHolder(registryName = "minecraft:block", value = MOD_ID + ":cuauhxicalli")
    public static final Block CUAUHXICALLI = null;

    @ObjectHolder(registryName = "minecraft:block", value = MOD_ID + ":chacmool")
    public static final Block CHACMOOL = null;

    @ObjectHolder(registryName = "minecraft:block", value = MOD_ID + ":fire_trap")
    public static final Block FIRE_TRAP = null;

    @ObjectHolder(registryName = "minecraft:block", value = MOD_ID + ":spike_trap")
    public static final Block SPIKE_TRAP = null;

    @ObjectHolder(registryName = "minecraft:block", value = MOD_ID + ":spear_trap")
    public static final Block SPEAR_TRAP = null;

    @ObjectHolder(registryName = "minecraft:block", value = MOD_ID + ":inert_pedestal")
    public static final Block INERT_PEDESTAL = null;

    @ObjectHolder(registryName = "minecraft:block", value = MOD_ID + ":power_pedestal")
    public static final Block POWER_PEDESTAL = null;

    @ObjectHolder(registryName = "minecraft:block", value = MOD_ID + ":infusion_pedestal")
    public static final Block INFUSION_PEDESTAL = null;

    @ObjectHolder(registryName = "minecraft:block", value = MOD_ID + ":sacrifice_pedestal")
    public static final Block SACRIFICE_PEDESTAL = null;

    @ObjectHolder(registryName = "minecraft:block", value = MOD_ID + ":blood_basin")
    public static final Block BLOOD_BASIN = null;

    @ObjectHolder(registryName = "minecraft:block", value = MOD_ID + ":xolotls_vessel")
    public static final Block XOLOTLS_VESSEL = null;

    @ObjectHolder(registryName = "minecraft:block", value = MOD_ID + ":offering_bowl")
    public static final Block OFFERING_BOWL = null;
}
