package com.lickbread.mesoamericamythology.items;

import com.lickbread.mesoamericamythology.client.renderer.item.CustomBlockEntityWithoutLevelRenderer;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.BlockEntityWithoutLevelRenderer;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.InteractionResultHolder;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.ShieldItem;
import net.minecraft.world.level.Level;
import net.minecraftforge.client.extensions.common.IClientItemExtensions;

import java.util.function.Consumer;

public class ChimalliShield extends ShieldItem {

    public static final String CHIMALLI_SHIELD = "ChimalliShield";
    public static final String IS_BLOCKING_KEY = "IsBlocking";

    public ChimalliShield() {
        super(new Item.Properties().defaultDurability(596));
    }

    @Override
    public InteractionResultHolder<ItemStack> use(Level worldIn, Player playerIn, InteractionHand handIn) {
        ItemStack itemstack = playerIn.getItemInHand(handIn);
        CompoundTag nbt = itemstack.getOrCreateTagElement(CHIMALLI_SHIELD);
        nbt.putBoolean(IS_BLOCKING_KEY, true);
        return super.use(worldIn, playerIn, handIn);
    }

    @Override
    public void releaseUsing(ItemStack stack, Level worldIn, LivingEntity entityLiving, int timeLeft) {
        CompoundTag nbt = stack.getOrCreateTagElement(CHIMALLI_SHIELD);
        nbt.putBoolean(IS_BLOCKING_KEY, false);
        stack.setTag(nbt);
        super.releaseUsing(stack, worldIn, entityLiving, timeLeft);
    }

    @Override
    public ItemStack finishUsingItem(ItemStack stack, Level worldIn, LivingEntity entityLiving) {
        return super.finishUsingItem(stack, worldIn, entityLiving);
    }

    @Override
    public void initializeClient(Consumer<IClientItemExtensions> consumer) {
        consumer.accept(new IClientItemExtensions() {

            @Override
            public BlockEntityWithoutLevelRenderer getCustomRenderer() {
                return new CustomBlockEntityWithoutLevelRenderer(Minecraft.getInstance().getBlockEntityRenderDispatcher(), Minecraft.getInstance().getEntityModels());
            }
        });
    }
}
