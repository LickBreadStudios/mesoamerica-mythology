package com.lickbread.mesoamericamythology.items;

import net.minecraft.world.item.Rarity;

public class EnrichedPlayerHeartItem extends PlayerHeartItem {

    public EnrichedPlayerHeartItem() {
        super(new Properties().rarity(Rarity.UNCOMMON).stacksTo(1));
    }
}
