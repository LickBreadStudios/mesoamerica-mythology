package com.lickbread.mesoamericamythology.items;

import net.minecraft.world.item.Rarity;

import net.minecraft.world.item.Item.Properties;

public class HeartOfPowerItem extends LivingHeartItem {

    public HeartOfPowerItem() {
        super(new Properties().rarity(Rarity.EPIC));
    }
}
