package com.lickbread.mesoamericamythology.items;

import net.minecraft.world.item.Rarity;

import net.minecraft.world.item.Item.Properties;

public class HeartOfVoidItem extends LivingHeartItem {

    public HeartOfVoidItem() {
        super(new Properties().rarity(Rarity.EPIC));
    }
}
