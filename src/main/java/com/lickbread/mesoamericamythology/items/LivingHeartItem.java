package com.lickbread.mesoamericamythology.items;

import net.minecraft.client.resources.language.I18n;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.network.chat.Component;
import net.minecraft.world.entity.EntityType;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.TooltipFlag;
import net.minecraft.world.level.Level;
import net.minecraftforge.registries.ForgeRegistries;

import javax.annotation.Nullable;
import java.util.List;
import java.util.Optional;

public abstract class LivingHeartItem extends Item {

    private static final String LIVING_HEART_TOOLTIP = "item.mesoamericamythology.living_heart.tooltip";
    protected static final String ENTITY_TYPE = "EntityType";

    public LivingHeartItem(Properties properties) {
        super(properties.stacksTo(1));
    }

    public static <T extends LivingHeartItem> ItemStack createFromEntityType(EntityType<? extends LivingEntity> entityType, Item item) {
        ItemStack itemStack = new ItemStack(item);
        CompoundTag nbt = itemStack.getTag();
        if (nbt == null) {
            nbt = new CompoundTag();
        }
        nbt.putString(ENTITY_TYPE, ForgeRegistries.ENTITY_TYPES.getKey(entityType).toString());
        itemStack.setTag(nbt);
        return itemStack;
    }

    @Override
    public void appendHoverText(ItemStack stack, @Nullable Level worldIn, List<Component> tooltip, TooltipFlag flagIn) {
        super.appendHoverText(stack, worldIn, tooltip, flagIn);
        Optional<EntityType<? extends LivingEntity>> entityType = getEntityType(stack);
        entityType.ifPresent(type -> tooltip.add(Component.translatable(I18n.get(LIVING_HEART_TOOLTIP, I18n.get(type.getDescriptionId())))));
    }

    public static Optional<EntityType<? extends LivingEntity>> getEntityType(ItemStack itemStack) {
        if (!(itemStack.getItem() instanceof LivingHeartItem)) {
            return Optional.empty();
        }

        CompoundTag nbt = itemStack.getTag();
        if (nbt == null || !nbt.contains(ENTITY_TYPE)) {
            return Optional.empty();
        }

        String entityTypeString = nbt.getString(ENTITY_TYPE);
        Optional<EntityType<?>> entityType = EntityType.byString(entityTypeString);

        if (!entityType.isPresent()) {
            return Optional.empty();
        }

        return Optional.of((EntityType<? extends LivingEntity>) entityType.get());
    }
}
