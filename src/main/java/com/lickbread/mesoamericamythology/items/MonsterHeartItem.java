package com.lickbread.mesoamericamythology.items;

import net.minecraft.world.item.Item;
import net.minecraft.world.item.Rarity;

public class MonsterHeartItem extends LivingHeartItem {

    public MonsterHeartItem() {
        super(new Item.Properties().rarity(Rarity.UNCOMMON));
    }
}
