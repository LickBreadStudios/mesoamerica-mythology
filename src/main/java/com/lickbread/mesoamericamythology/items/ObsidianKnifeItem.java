package com.lickbread.mesoamericamythology.items;

import com.lickbread.mesoamericamythology.registry.ModDamageSources;
import com.lickbread.mesoamericamythology.registry.ModItems;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.InteractionResultHolder;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.Items;
import net.minecraft.world.item.Rarity;
import net.minecraft.world.level.Level;

public class ObsidianKnifeItem extends Item {

    public ObsidianKnifeItem() {
        super(new Item.Properties().stacksTo(1).rarity(Rarity.UNCOMMON));
    }

    // On right click
    @Override
    public InteractionResultHolder<ItemStack> use(Level level, Player player, InteractionHand hand) {
        if (!level.isClientSide() && hand == InteractionHand.MAIN_HAND) {
            ModDamageSources.attackPlayerWithObsidianKnife(level, player);
            if (player.getItemInHand(InteractionHand.OFF_HAND).getItem() == Items.BOWL) {
                ItemStack stack = player.getItemInHand(InteractionHand.OFF_HAND).copy();
                stack.setCount(stack.getCount() - 1);
                player.setItemInHand(InteractionHand.OFF_HAND, stack);
                player.addItem(new ItemStack(ModItems.BLOOD_BOWL.get(), 1));
            }
            return InteractionResultHolder.success(player.getItemInHand(InteractionHand.MAIN_HAND));
        }
        return InteractionResultHolder.pass(player.getItemInHand(hand));
    }
}
