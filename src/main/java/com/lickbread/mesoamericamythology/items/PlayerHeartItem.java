package com.lickbread.mesoamericamythology.items;

import net.minecraft.client.resources.language.I18n;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.network.chat.Component;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.Rarity;
import net.minecraft.world.item.TooltipFlag;
import net.minecraft.world.level.Level;
import net.minecraftforge.registries.ForgeRegistries;

import javax.annotation.Nullable;
import java.util.List;

public class PlayerHeartItem extends LivingHeartItem {

    private static final String PLAYER_HEART_NAME_TOOLTIP = "item.mesoamericamythology.player_heart.tooltip.name";
    private static final String PLAYER_HEART_EXPERIENCE_TOOLTIP = "item.mesoamericamythology.player_heart.tooltip.experience";
    private static final String PLAYER_NAME = "PlayerName";
    private static final String EXPERIENCE_LEVEL = "ExperienceLevel";

    public PlayerHeartItem(Properties properties) {
        super(properties);
    }

    public PlayerHeartItem() {
        super(new Properties().rarity(Rarity.UNCOMMON).stacksTo(1));
    }

    public static <T extends LivingHeartItem> ItemStack createFromEntityType(Player playerEntity, Item item, int expLevel) {
        ItemStack itemStack = new ItemStack(item);
        CompoundTag nbt = itemStack.getTag();
        if (nbt == null) {
            nbt = new CompoundTag();
        }
        nbt.putString(PLAYER_NAME, (playerEntity.getName()).getString());
        nbt.putInt(EXPERIENCE_LEVEL, expLevel);
        nbt.putString(ENTITY_TYPE, ForgeRegistries.ENTITY_TYPES.getKey(playerEntity.getType()).toString());
        itemStack.setTag(nbt);
        return itemStack;
    }

    @Override
    public void appendHoverText(ItemStack stack, @Nullable Level worldIn, List<Component> tooltip, TooltipFlag flagIn) {
        super.appendHoverText(stack, worldIn, tooltip, flagIn);

        CompoundTag nbt = stack.getTag();
        if (nbt == null || !nbt.contains(PLAYER_NAME) || !nbt.contains(EXPERIENCE_LEVEL)) {
            return;
        }

        String playerName = nbt.getString(PLAYER_NAME);
        int expLevel = nbt.getInt(EXPERIENCE_LEVEL);

        tooltip.add(Component.translatable(I18n.get(PLAYER_HEART_NAME_TOOLTIP, playerName)));
        tooltip.add(Component.translatable(I18n.get(PLAYER_HEART_EXPERIENCE_TOOLTIP, expLevel)));
    }

}
