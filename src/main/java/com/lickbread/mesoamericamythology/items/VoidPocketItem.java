package com.lickbread.mesoamericamythology.items;

import net.minecraft.client.resources.language.I18n;
import net.minecraft.core.BlockPos;
import net.minecraft.core.NonNullList;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.nbt.ListTag;
import net.minecraft.network.chat.Component;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.InteractionResult;
import net.minecraft.world.InteractionResultHolder;
import net.minecraft.world.MenuProvider;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.Entity.RemovalReason;
import net.minecraft.world.entity.EntityType;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.Rarity;
import net.minecraft.world.item.TooltipFlag;
import net.minecraft.world.item.context.UseOnContext;
import net.minecraft.world.level.ClipContext;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.phys.BlockHitResult;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.capabilities.CapabilityManager;
import net.minecraftforge.common.capabilities.CapabilityToken;
import net.minecraftforge.common.util.LazyOptional;
import net.minecraftforge.items.IItemHandler;

import javax.annotation.Nullable;
import java.util.List;
import java.util.Optional;
import java.util.stream.IntStream;

public class VoidPocketItem extends Item {

    static Capability<IItemHandler> ITEM_HANDLER_CAPABILITY = CapabilityManager.get(new CapabilityToken<>(){});

    private static final String VOID_POCKET_HELD_ENTITY_TOOLTIP_TEXT = "item.mesoamericamythology.void_pocket.tooltip.held_entity";
    private static final String VOID_POCKET_HELD_INVENTORY_TOOLTIP_TEXT = "item.mesoamericamythology.void_pocket.tooltip.held_inventory";
    private static final String VOID_POCKET_HELD_INVENTORY_SIZE_TOOLTIP_TEXT = "item.mesoamericamythology.void_pocket.tooltip.held_inventory_size";

    private static final String VOID_POCKET_KEY = "VoidPocket";
    private static final String HELD_ENTITY_KEY = "HeldEntity";
    private static final String HELD_ENTITY_NAME_KEY = "HeldEntityName";
    private static final String HELD_INVENTORY_KEY = "HeldInventory";
    private static final String HELD_INVENTORY_NAME_KEY = "HeldInventoryName";
    private static final String LAST_INTERACT_TIME_KEY = "LastInteractTime";

    private static final int MIN_INTERACT_TIME = 5;

    public VoidPocketItem() {
        super(new Properties().stacksTo(1).rarity(Rarity.RARE));
    }

    /**
     * Called to trigger the item's "innate" right click behavior. To handle when this item is used on a Block, see
     * {@link #use}.
     */
    @Override
    public InteractionResultHolder<ItemStack> use(Level worldIn, Player playerIn, InteractionHand handIn) {
        // Attempt to spawn mob
        if (worldIn.isClientSide() || handIn != InteractionHand.MAIN_HAND) {
            return InteractionResultHolder.pass(playerIn.getItemInHand(handIn));
        }

        ItemStack heldItem = playerIn.getItemInHand(handIn);

        CompoundTag nbt = playerIn.getItemInHand(handIn).getOrCreateTagElement(VOID_POCKET_KEY);

        if (nbt.contains(LAST_INTERACT_TIME_KEY) && worldIn.getGameTime() - nbt.getLong(LAST_INTERACT_TIME_KEY) <= MIN_INTERACT_TIME) {
            return InteractionResultHolder.pass(playerIn.getItemInHand(handIn));
        }

        if (!nbt.contains(HELD_ENTITY_KEY)) {
            return InteractionResultHolder.pass(playerIn.getItemInHand(handIn));
        }

        this.spawnEntity(nbt, worldIn, playerIn);

        return InteractionResultHolder.success(heldItem);
    }

    /**
     * Called when this item is used when targeting a Block
     */
    @Override
    public InteractionResult useOn(UseOnContext context) {
        if (context.getLevel().isClientSide() || context.getHand() != InteractionHand.MAIN_HAND) {
            return InteractionResult.PASS;
        }

        CompoundTag nbt = context.getItemInHand().getOrCreateTagElement(VOID_POCKET_KEY);

        if (nbt.contains(LAST_INTERACT_TIME_KEY) && context.getLevel().getGameTime() - nbt.getLong(LAST_INTERACT_TIME_KEY) <= MIN_INTERACT_TIME) {
            return InteractionResult.PASS;
        }

        if (nbt.contains(HELD_ENTITY_KEY)) {
            this.spawnEntity(nbt, context.getLevel(), context.getPlayer());
            context.getItemInHand().addTagElement(VOID_POCKET_KEY, nbt);
            return InteractionResult.CONSUME;
        } else if (nbt.contains(HELD_INVENTORY_KEY) && this.canTransferInventory(context.getLevel(), context.getClickedPos())) {
            this.transferInventory(nbt, context.getLevel(), context.getClickedPos());
            nbt.remove(LAST_INTERACT_TIME_KEY);
            nbt.putLong(LAST_INTERACT_TIME_KEY, context.getLevel().getGameTime());
            return InteractionResult.CONSUME;
        }

        if (this.canTransferInventory(context.getLevel(), context.getClickedPos())) {
            this.obtainInventory(nbt, context.getLevel(), context.getClickedPos());
            nbt.remove(LAST_INTERACT_TIME_KEY);
            nbt.putLong(LAST_INTERACT_TIME_KEY, context.getLevel().getGameTime());
            return InteractionResult.CONSUME;
        }

        return InteractionResult.PASS;
    }

    private void transferInventory(CompoundTag nbt, Level level, BlockPos pos) {
        BlockEntity blockEntity = level.getBlockEntity(pos);
        LazyOptional<IItemHandler> itemHandlerLazyOptional = blockEntity.getCapability(ITEM_HANDLER_CAPABILITY);
        if (!itemHandlerLazyOptional.isPresent()) {
            return;
        }

        IItemHandler iItemHandler = itemHandlerLazyOptional.orElse(null);
        int slots = iItemHandler.getSlots();

        ListTag heldItems = (ListTag) nbt.get(HELD_INVENTORY_KEY);
        NonNullList<ItemStack> contents = NonNullList.withSize(heldItems.size(), ItemStack.EMPTY);
        for (int i = 0; i < heldItems.size(); i++) {
            CompoundTag tag = heldItems.getCompound(i);
            contents.set(i, ItemStack.of(tag));
        }

        int actualSlots = Math.min(slots, contents.size());

        // Attempt to insert items
        for (int i = 0; i < actualSlots; i++) {
            ItemStack itemStack = contents.get(i);
            // Attempt to insert into correct slot
            itemStack = iItemHandler.insertItem(i, itemStack, false);
            for (int j = 0; j < actualSlots && !itemStack.isEmpty(); j++) {
                itemStack = iItemHandler.insertItem(j, itemStack, false);
            }
            contents.set(i, itemStack);
        }

        // Evaluate remaining items
        if (contents.stream().allMatch(ItemStack::isEmpty)) {
            nbt.remove(HELD_INVENTORY_KEY);
            nbt.remove(HELD_INVENTORY_NAME_KEY);
        } else {
            nbt.remove(HELD_INVENTORY_KEY);
            ListTag itemsList = new ListTag();
            for (int i = 0; i < contents.size(); i++) {
                itemsList.add(contents.get(i).serializeNBT());
            }
            nbt.put(HELD_INVENTORY_KEY, itemsList);
        }
    }

    private void obtainInventory(CompoundTag nbt, Level level, BlockPos pos) {
        BlockEntity blockEntity = level.getBlockEntity(pos);
        LazyOptional<IItemHandler> itemHandlerLazyOptional = blockEntity.getCapability(ITEM_HANDLER_CAPABILITY);
        if (!itemHandlerLazyOptional.isPresent()) {
            return;
        }

        IItemHandler iItemHandler = itemHandlerLazyOptional.orElse(null);
        int slots = iItemHandler.getSlots();
        NonNullList<ItemStack> contents = NonNullList.withSize(slots, ItemStack.EMPTY);
        IntStream.range(0, slots).forEach(i -> contents.set(i, iItemHandler.extractItem(i, 64, false)));

        nbt.remove(HELD_INVENTORY_KEY);
        nbt.remove(HELD_INVENTORY_NAME_KEY);

        if (contents.stream().allMatch(ItemStack::isEmpty)) {
            return;
        }

        ListTag itemsList = new ListTag();
        for (ItemStack content : contents) {
            itemsList.add(content.serializeNBT());
        }
        nbt.put(HELD_INVENTORY_KEY, itemsList);
        if (blockEntity instanceof MenuProvider) {
            nbt.putString(HELD_INVENTORY_NAME_KEY, (((MenuProvider) blockEntity).getDisplayName().getString()));
        } else {
            nbt.putString(HELD_INVENTORY_NAME_KEY, I18n.get(blockEntity.getBlockState().getBlock().getDescriptionId()));
        }
    }

    private boolean canTransferInventory(Level level, BlockPos pos) {
        BlockEntity blockEntity = level.getBlockEntity(pos);
        if (blockEntity == null) {
            return false;
        }

        return blockEntity.getCapability(ITEM_HANDLER_CAPABILITY).isPresent();
    }

    /**
     * Returns true if the item can be used on the given entity, e.g. shears on sheep.
     */
    @Override
    public InteractionResult interactLivingEntity(ItemStack stack, Player playerIn, LivingEntity target, InteractionHand hand) {
        if (target.level.isClientSide() || target instanceof Player || hand != InteractionHand.MAIN_HAND) {
            return InteractionResult.PASS;
        }

        CompoundTag nbt = stack.getOrCreateTagElement(VOID_POCKET_KEY);

        if (nbt.contains(HELD_ENTITY_KEY) || nbt.contains(HELD_INVENTORY_KEY)) {
            return InteractionResult.PASS;
        }

        if (nbt.contains(LAST_INTERACT_TIME_KEY) && target.level.getGameTime() - nbt.getLong(LAST_INTERACT_TIME_KEY) <= MIN_INTERACT_TIME) {
            return InteractionResult.PASS;
        }

        CompoundTag entityNbt = new CompoundTag();
        target.save(entityNbt);

        nbt.put(HELD_ENTITY_KEY, entityNbt);
        nbt.putString(HELD_ENTITY_NAME_KEY, target.getDisplayName().getString());
        nbt.remove(LAST_INTERACT_TIME_KEY);
        nbt.putLong(LAST_INTERACT_TIME_KEY, target.level.getGameTime());

        stack.addTagElement(VOID_POCKET_KEY, nbt);

        target.remove(RemovalReason.DISCARDED);

        return InteractionResult.SUCCESS;
    }

    private void spawnEntity(CompoundTag nbt, Level worldIn, Player playerIn) {
        CompoundTag compoundNBT = nbt.getCompound(HELD_ENTITY_KEY);

        Optional<Entity> optionalEntity = EntityType.create(compoundNBT, worldIn);
        optionalEntity.ifPresent(entity -> {
            BlockHitResult rayTraceResult = (BlockHitResult) getPlayerPOVHitResult(worldIn, playerIn, ClipContext.Fluid.NONE);
            entity.setPos(rayTraceResult.getBlockPos().getX(), rayTraceResult.getBlockPos().getY() + 1.0D, rayTraceResult.getBlockPos().getZ());
            worldIn.addFreshEntity(entity);
        });

        nbt.remove(HELD_ENTITY_KEY);
        nbt.remove(HELD_ENTITY_NAME_KEY);
        nbt.remove(LAST_INTERACT_TIME_KEY);
        nbt.putLong(LAST_INTERACT_TIME_KEY, worldIn.getGameTime());
    }

    @Override
    public void appendHoverText(ItemStack stack, @Nullable Level worldIn, List<Component> tooltip, TooltipFlag flagIn) {
        super.appendHoverText(stack, worldIn, tooltip, flagIn);

        Optional<String> entityName = getEntityName(stack);
        entityName.ifPresent(name -> tooltip.add(Component.translatable(I18n.get(VOID_POCKET_HELD_ENTITY_TOOLTIP_TEXT, name))));

        Optional<String> inventoryName = getInventoryName(stack);
        inventoryName.ifPresent(name -> tooltip.add(Component.translatable(I18n.get(VOID_POCKET_HELD_INVENTORY_TOOLTIP_TEXT, name))));

        Optional<Integer> inventorySize = getInventorySize(stack);
        inventorySize.ifPresent(size -> tooltip.add(Component.translatable(I18n.get(VOID_POCKET_HELD_INVENTORY_SIZE_TOOLTIP_TEXT, size))));
    }

    public static Optional<String> getEntityName(ItemStack itemStack) {
        if (!(itemStack.getItem() instanceof VoidPocketItem)) {
            return Optional.empty();
        }

        CompoundTag nbt = itemStack.getOrCreateTagElement(VOID_POCKET_KEY);
        if (!nbt.contains(HELD_ENTITY_NAME_KEY)) {
            return Optional.empty();
        }

        return Optional.of(nbt.getString(HELD_ENTITY_NAME_KEY));
    }

    public static Optional<String> getInventoryName(ItemStack itemStack) {
        if (!(itemStack.getItem() instanceof VoidPocketItem)) {
            return Optional.empty();
        }

        CompoundTag nbt = itemStack.getOrCreateTagElement(VOID_POCKET_KEY);
        if (!nbt.contains(HELD_INVENTORY_NAME_KEY)) {
            return Optional.empty();
        }

        return Optional.of(nbt.getString(HELD_INVENTORY_NAME_KEY));
    }

    public static Optional<Integer> getInventorySize(ItemStack itemStack) {
        if (!(itemStack.getItem() instanceof VoidPocketItem)) {
            return Optional.empty();
        }

        CompoundTag nbt = itemStack.getOrCreateTagElement(VOID_POCKET_KEY);
        if (!nbt.contains(HELD_INVENTORY_KEY)) {
            return Optional.empty();
        }

        ListTag heldItems = (ListTag) nbt.get(HELD_INVENTORY_KEY);
        NonNullList<ItemStack> contents = NonNullList.withSize(heldItems.size(), ItemStack.EMPTY);
        for (int i = 0; i < heldItems.size(); i++) {
            CompoundTag tag = heldItems.getCompound(i);
            contents.set(i, ItemStack.of(tag));
        }

        return Optional.of((int)contents.stream().filter(stack -> !stack.isEmpty()).count());
    }

    public static boolean hasEntity(ItemStack itemStack) {
        if (!(itemStack.getItem() instanceof VoidPocketItem)) {
            return false;
        }

        CompoundTag nbt = itemStack.getOrCreateTagElement(VOID_POCKET_KEY);
        return nbt.contains(HELD_ENTITY_NAME_KEY);
    }

    public static Optional<LivingEntity> getEntity(ItemStack itemStack, Level worldIn) {
        if (!(itemStack.getItem() instanceof VoidPocketItem)) {
            return Optional.empty();
        }

        CompoundTag nbt = itemStack.getOrCreateTagElement(VOID_POCKET_KEY);
        CompoundTag compoundNBT = nbt.getCompound(HELD_ENTITY_KEY);

        Optional<Entity> optionalEntity = EntityType.create(compoundNBT, worldIn);

        nbt.remove(HELD_ENTITY_KEY);
        nbt.remove(HELD_ENTITY_NAME_KEY);
        nbt.remove(LAST_INTERACT_TIME_KEY);
        nbt.putLong(LAST_INTERACT_TIME_KEY, worldIn.getGameTime());

        return Optional.ofNullable((LivingEntity) optionalEntity.orElse(null));
    }

    public static void setEntity(ItemStack itemStack, LivingEntity entity) {
        if (!(itemStack.getItem() instanceof VoidPocketItem)) {
            return;
        }

        CompoundTag nbt = itemStack.getOrCreateTagElement(VOID_POCKET_KEY);

        CompoundTag entityNbt = new CompoundTag();
        entity.save(entityNbt);

        nbt.put(HELD_ENTITY_KEY, entityNbt);
        nbt.putString(HELD_ENTITY_NAME_KEY, entity.getDisplayName().getString());
        nbt.remove(LAST_INTERACT_TIME_KEY);
        nbt.putLong(LAST_INTERACT_TIME_KEY, entity.level.getGameTime());

        itemStack.addTagElement(VOID_POCKET_KEY, nbt);
    }
}
