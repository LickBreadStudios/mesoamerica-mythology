package com.lickbread.mesoamericamythology.network;

import com.lickbread.mesoamericamythology.MesoamericaMythologyMod;
import com.lickbread.mesoamericamythology.statueHead.relationship.DivineFavourCapability;
import com.lickbread.mesoamericamythology.statueHead.relationship.IDivineFavourCapability;
import io.netty.buffer.ByteBuf;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;
import net.minecraft.client.multiplayer.ClientLevel;
import net.minecraft.world.entity.player.Player;

import java.nio.charset.StandardCharsets;
import java.util.UUID;

import static com.lickbread.mesoamericamythology.api.MesoamericaMythologyAPI.GSON;
import static com.lickbread.mesoamericamythology.capabilities.DivineFavourCapabilityProvider.DIVINE_FAVOUR_CAPABILITY;

/**
 * This should only be sent from the server to the client
 */
@AllArgsConstructor
@Getter
@ToString
public class DivineFavourCapabilityMessageToClient {
    private final DivineFavourCapability capability;
    private final UUID playerUUID;

    /**
     * This message is called from the Client thread
     */
    public static void processMessage(ClientLevel clientWorld, DivineFavourCapabilityMessageToClient message) {
        Player playerEntity = clientWorld.getPlayerByUUID(message.getPlayerUUID());
        IDivineFavourCapability capability = playerEntity.getCapability(DIVINE_FAVOUR_CAPABILITY).orElse(null);
        capability.copyFrom(message.getCapability());
        MesoamericaMythologyMod.LOGGER.debug("Updated Capability on client for player [{}]", message.playerUUID.toString());
    }

    /**
     * Called by the network code once it has received the message bytes over the network.
     * Used to read the ByteBuf contents into your member variables
     */
    public static DivineFavourCapabilityMessageToClient decode(ByteBuf buf) {
        return GSON.fromJson(buf.toString(StandardCharsets.UTF_8), DivineFavourCapabilityMessageToClient.class);
    }

    /**
     * Called by the network code.
     * Used to write the contents of your message member variables into the ByteBuf, ready for transmission over the network.
     */
    public void encode(ByteBuf buf) {
        String str = GSON.toJson(this);
        buf.writeBytes(str.getBytes(StandardCharsets.UTF_8));
    }
}