package com.lickbread.mesoamericamythology.network;

import com.lickbread.mesoamericamythology.MesoamericaMythologyMod;
import net.minecraft.client.multiplayer.ClientLevel;
import net.minecraft.world.level.Level;
import net.minecraftforge.common.util.LogicalSidedProvider;
import net.minecraftforge.fml.LogicalSide;
import net.minecraftforge.network.NetworkEvent;

import java.util.Optional;
import java.util.function.Supplier;

public class MessageHandlerOnClient {

    /**
     * Called when a message is received of the appropriate type.
     * CALLED BY THE NETWORK THREAD, NOT THE CLIENT THREAD
     */
    public static void onMessageReceived(final DivineFavourCapabilityMessageToClient message, Supplier<NetworkEvent.Context> ctxSupplier) {
        NetworkEvent.Context ctx = ctxSupplier.get();
        LogicalSide sideReceived = ctx.getDirection().getReceptionSide();
        ctx.setPacketHandled(true);

        if (sideReceived != LogicalSide.CLIENT) {
            MesoamericaMythologyMod.LOGGER.warn("DivineFavourCapabilityMessageToClient received on wrong side:" + ctx.getDirection().getReceptionSide());
            return;
        }

        if (message.getCapability() == null) {
            MesoamericaMythologyMod.LOGGER.warn("DivineFavourCapabilityMessageToClient was invalid: " + message.toString());
            return;
        }

        // We know for sure that this handler is only used on the client side, so it is ok to assume that the ctx handler is a client, and that Minecraft exists.
        // Packets received on the server side must be handled differently!
        Optional<Level> clientLevel = LogicalSidedProvider.CLIENTWORLD.get(sideReceived);
        if (!clientLevel.isPresent()) {
            MesoamericaMythologyMod.LOGGER.warn("DivineFavourCapabilityMessageToClient context could not provide a ClientWorld.");
            return;
        }

        // This code creates a new task which will be executed by the client during the next tick
        ctx.enqueueWork(() -> {
            DivineFavourCapabilityMessageToClient.processMessage((ClientLevel) clientLevel.get(), message);
        });
    }

}
