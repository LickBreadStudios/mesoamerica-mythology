package com.lickbread.mesoamericamythology.network;

import com.lickbread.mesoamericamythology.MesoamericaMythologyMod;
import net.minecraft.resources.ResourceLocation;
import net.minecraftforge.network.NetworkRegistry;
import net.minecraftforge.network.simple.SimpleChannel;

public class ModChannels {

    private static final String PROTOCOL_VERSION = "1";
    public static final SimpleChannel MAIN_PACKET_CHANNEL_INSTANCE = NetworkRegistry.newSimpleChannel(
            new ResourceLocation(MesoamericaMythologyMod.MOD_ID, "main_packet_channel"),
            () -> PROTOCOL_VERSION,
            PROTOCOL_VERSION::equals,
            PROTOCOL_VERSION::equals
    );

}
