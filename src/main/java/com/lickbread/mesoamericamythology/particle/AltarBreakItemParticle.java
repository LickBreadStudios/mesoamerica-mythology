package com.lickbread.mesoamericamythology.particle;

import net.minecraft.client.particle.BreakingItemParticle;
import net.minecraft.client.multiplayer.ClientLevel;
import net.minecraft.world.item.ItemStack;

public class AltarBreakItemParticle extends BreakingItemParticle {

    public static final int AGE = 10;

    public AltarBreakItemParticle(ClientLevel level, double xStart, double yStart, double zStart, double xEnd, double yEnd, double zEnd, ItemStack itemStack) {
        super(level, xStart, yStart, zStart, itemStack);
        this.hasPhysics = false;
        this.lifetime = AGE;
        this.gravity = 0.00F;
        this.scale(0.5F);
        this.xd = (xEnd - xStart) / AGE;
        this.yd = (yEnd - yStart) / AGE;
        this.zd = (zEnd - zStart) / AGE;
    }

    /**
     * Called once per tick to update the Particle position, calculate collisions, remove when max lifetime is reached, etc
     */
    @Override
    public void tick() {
        this.xo = this.x;
        this.yo = this.y;
        this.zo = this.z;

        move(xd, yd, zd);

        if (this.age++ >= this.lifetime) {
            this.remove();
        }
    }
}
