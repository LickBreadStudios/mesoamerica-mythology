package com.lickbread.mesoamericamythology.particle;

import com.lickbread.mesoamericamythology.registry.ModParticleTypes;
import com.lickbread.mesoamericamythology.util.SerializerUtil;
import com.mojang.brigadier.StringReader;
import com.mojang.brigadier.exceptions.CommandSyntaxException;
import net.minecraft.commands.arguments.item.ItemInput;
import net.minecraft.commands.arguments.item.ItemParser;
import net.minecraft.core.particles.ParticleOptions;
import net.minecraft.core.particles.ParticleType;
import net.minecraft.core.registries.BuiltInRegistries;
import net.minecraft.network.FriendlyByteBuf;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.phys.Vec3;
import net.minecraftforge.registries.ForgeRegistries;

import net.minecraft.core.particles.ParticleOptions.Deserializer;

public class AltarBreakItemParticleData implements ParticleOptions {
    @Override
    public ParticleType<AltarBreakItemParticleData> getType() {
        return ModParticleTypes.ALTAR_BREAK_ITEM_PARTICLE.get();
    }

    private final ParticleType<AltarBreakItemParticleData> particleType;
    private final Vec3 destination;
    private final ItemStack itemStack;

    public AltarBreakItemParticleData(ParticleType<AltarBreakItemParticleData> particleType, Vec3 destination, ItemStack itemStack) {
        this.particleType = particleType;
        this.destination = destination;
        this.itemStack = itemStack.copy();
    }

    public Vec3 getDestination() {
        return this.destination;
    }

    public ItemStack getItemStack() {
        return this.itemStack;
    }

    public void writeToNetwork(FriendlyByteBuf buffer) {
        buffer.writeItem(this.itemStack);
    }

    @Override
    public String writeToString() {
        return ForgeRegistries.PARTICLE_TYPES.getKey(this.getType()) + " " + (new ItemInput(this.itemStack.getItemHolder(), this.itemStack.getTag())).serialize();
    }

    public static final Deserializer<AltarBreakItemParticleData> DESERIALIZER = new Deserializer<AltarBreakItemParticleData>() {

        @Override
        public AltarBreakItemParticleData fromCommand(ParticleType<AltarBreakItemParticleData> particleType, StringReader stringReader) throws CommandSyntaxException {
            stringReader.expect(' ');
            ItemParser.ItemResult itemResult = ItemParser.parseForItem(BuiltInRegistries.ITEM.asLookup(), stringReader);
            ItemStack itemstack = (new ItemInput(itemResult.item(), itemResult.nbt())).createItemStack(1, false);
            stringReader.expect(' ');
            Vec3 destination = SerializerUtil.readVec3(stringReader);
            return new AltarBreakItemParticleData(particleType, destination, itemstack);
        }

        @Override
        public AltarBreakItemParticleData fromNetwork(ParticleType<AltarBreakItemParticleData> particleType, FriendlyByteBuf packetBuffer) {
            return new AltarBreakItemParticleData(particleType, new Vec3(packetBuffer.readDouble(), packetBuffer.readDouble(), packetBuffer.readDouble()), packetBuffer.readItem());
        }
    };
}
