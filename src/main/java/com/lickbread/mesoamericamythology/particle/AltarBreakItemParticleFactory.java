package com.lickbread.mesoamericamythology.particle;

import net.minecraft.client.particle.ParticleProvider;
import net.minecraft.client.particle.Particle;
import net.minecraft.client.multiplayer.ClientLevel;

import javax.annotation.Nullable;

public class AltarBreakItemParticleFactory implements ParticleProvider<AltarBreakItemParticleData> {

    @Nullable
    @Override
    public Particle createParticle(AltarBreakItemParticleData altarBreakItemParticleData, ClientLevel level, double x, double y, double z, double xVel, double yVel, double zVel) {
        return new AltarBreakItemParticle(level, x, y, z, altarBreakItemParticleData.getDestination().x, altarBreakItemParticleData.getDestination().y, altarBreakItemParticleData.getDestination().z, altarBreakItemParticleData.getItemStack());
    }
}
