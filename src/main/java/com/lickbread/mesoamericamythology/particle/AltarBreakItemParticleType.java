package com.lickbread.mesoamericamythology.particle;

import com.mojang.serialization.Codec;
import net.minecraft.core.particles.ParticleType;

public class AltarBreakItemParticleType extends ParticleType<AltarBreakItemParticleData> {

    public AltarBreakItemParticleType() {
        super(false, AltarBreakItemParticleData.DESERIALIZER);
    }

    @Override
    public Codec<AltarBreakItemParticleData> codec() {
        return null;
    }
}
