package com.lickbread.mesoamericamythology.particle;

import com.lickbread.mesoamericamythology.util.MathUtil;
import net.minecraft.client.Minecraft;
import net.minecraft.client.particle.ParticleRenderType;
import net.minecraft.client.particle.TextureSheetParticle;
import net.minecraft.client.multiplayer.ClientLevel;
import net.minecraft.util.profiling.ProfilerFiller;
import net.minecraft.world.phys.Vec3;

public abstract class BezierCurveMoveParticle extends TextureSheetParticle {

    private final Vec3 start0;
    private final Vec3 start1;
    private final Vec3 end0;
    private final Vec3 end1;

    public BezierCurveMoveParticle(ClientLevel level, Vec3 start0, Vec3 start1, Vec3 end0, Vec3 end1, int age) {
        super(level, start0.x, start0.y, start0.z);
        this.lifetime = age;
        this.gravity = 0.0F;
        this.start0 = start0;
        this.start1 = start1;
        this.end0 = end0;
        this.end1 = end1;
        this.xd = 0f;
        this.yd = 0f;
        this.zd = 0f;
    }

    /**
     * Called once per tick to update the Particle position, calculate collisions, remove when max lifetime is reached, etc
     */
    @Override
    public void tick() {
        ProfilerFiller profiler = Minecraft.getInstance().getProfiler();
        profiler.push("BezierCurveMoveParticle");
        xo = x;
        yo = y;
        zo = z;

        Vec3 newPos = MathUtil.cubeBezier3(start0, start1, end0, end1, this.age / (double) this.lifetime);
        this.move(newPos.x - xo, newPos.y - yo, newPos.z - zo);

        if (this.age++ >= this.lifetime) {
            this.remove();
        }
        profiler.pop();
    }

    @Override
    public void move(double x, double y, double z) {
        this.setBoundingBox(this.getBoundingBox().move(x, y, z));
        this.setLocationFromBoundingbox();
    }

    @Override
    public ParticleRenderType getRenderType() {
        return ParticleRenderType.PARTICLE_SHEET_TRANSLUCENT;
    }
}
