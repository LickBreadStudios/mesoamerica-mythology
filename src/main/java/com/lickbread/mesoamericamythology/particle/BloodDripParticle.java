package com.lickbread.mesoamericamythology.particle;

import com.lickbread.mesoamericamythology.util.MathUtil;
import net.minecraft.client.particle.ParticleRenderType;
import net.minecraft.client.particle.TextureSheetParticle;
import net.minecraft.client.multiplayer.ClientLevel;

import java.util.Random;

public class BloodDripParticle extends TextureSheetParticle {

    private static final double GRAVITY_ACCELERATION_PER_TICK = -0.01D;

    private final double gravityMultiplier;

    public BloodDripParticle(ClientLevel level, double x, double y, double z, double gravityMultiplier) {
        super(level, x, y, z);
        this.hasPhysics = true;
        this.lifetime = 30 + MathUtil.getInRandom(new Random(), 2);
        this.scale(0.10F);
        this.gravity = 0.02F * (float) gravityMultiplier + MathUtil.getInRandom(new Random(), 0.005F);
        this.gravityMultiplier = gravityMultiplier;
    }

    /**
     * Called once per tick to update the Particle position, calculate collisions, remove when max lifetime is reached, etc
     */
    @Override
    public void tick() {
        xo = x;
        yo = y;
        zo = z;

        yd += this.onGround ? 0D : GRAVITY_ACCELERATION_PER_TICK * this.gravityMultiplier;
        this.move(xd  * 0.001D, yd, zd * 0.001D);  // simple linear motion.  You can change speed by changing xd, yd

        if (this.age++ >= this.lifetime) {
            this.remove();
        }
    }

    @Override
    public ParticleRenderType getRenderType() {
        return ParticleRenderType.PARTICLE_SHEET_TRANSLUCENT;
    }
}
