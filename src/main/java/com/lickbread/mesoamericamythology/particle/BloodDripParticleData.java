package com.lickbread.mesoamericamythology.particle;

import com.lickbread.mesoamericamythology.registry.ModParticleTypes;
import com.mojang.brigadier.StringReader;
import com.mojang.brigadier.exceptions.CommandSyntaxException;
import lombok.AllArgsConstructor;
import lombok.Getter;
import net.minecraft.core.particles.ParticleOptions;
import net.minecraft.core.particles.ParticleType;
import net.minecraft.network.FriendlyByteBuf;
import net.minecraftforge.registries.ForgeRegistries;

import java.util.Locale;

@Getter
@AllArgsConstructor
public class BloodDripParticleData implements ParticleOptions {

    private final double gravityMultiplier;

    @Override
    public ParticleType<BloodDripParticleData> getType() {
        return ModParticleTypes.BLOOD_DRIP_PARTICLE.get();
    }

    @Override
    public void writeToNetwork(FriendlyByteBuf packetBuffer) {
        packetBuffer.writeDouble(this.gravityMultiplier);
    }

    @Override
    public String writeToString() {
        return String.format(Locale.ROOT, "%s %.2f", ForgeRegistries.PARTICLE_TYPES.getKey(this.getType()), this.gravityMultiplier);
    }

    public static final Deserializer<BloodDripParticleData> DESERIALIZER = new Deserializer<BloodDripParticleData>() {
        @Override
        public BloodDripParticleData fromCommand(ParticleType<BloodDripParticleData> particleType, StringReader stringReader) throws CommandSyntaxException {
            stringReader.expect(' ');
            double gravityMultiplier = stringReader.readDouble();
            return new BloodDripParticleData(gravityMultiplier);
        }

        @Override
        public BloodDripParticleData fromNetwork(ParticleType<BloodDripParticleData> particleType, FriendlyByteBuf packetBuffer) {
            return new BloodDripParticleData(packetBuffer.readDouble());
        }
    };
}
