package com.lickbread.mesoamericamythology.particle;

import lombok.AllArgsConstructor;
import net.minecraft.client.particle.SpriteSet;
import net.minecraft.client.particle.ParticleProvider;
import net.minecraft.client.particle.Particle;
import net.minecraft.client.multiplayer.ClientLevel;

import javax.annotation.Nullable;

@AllArgsConstructor
public class BloodDripParticleFactory implements ParticleProvider<BloodDripParticleData> {

    private final SpriteSet sprites;

    private BloodDripParticleFactory() {
        throw new UnsupportedOperationException("You did wrong boi.");
    }

    @Nullable
    @Override
    public Particle createParticle(BloodDripParticleData bloodDripParticleData, ClientLevel level, double x, double y, double z, double xVel, double yVel, double zVel) {
        BloodDripParticle particle = new BloodDripParticle(level, x, y, z, bloodDripParticleData.getGravityMultiplier());
        particle.pickSprite(this.sprites);
        return particle;
    }
}
