package com.lickbread.mesoamericamythology.particle;

import com.mojang.serialization.Codec;
import net.minecraft.core.particles.ParticleType;

public class BloodDripParticleType extends ParticleType<BloodDripParticleData> {

    public BloodDripParticleType() {
        super(false, BloodDripParticleData.DESERIALIZER);
    }

    @Override
    public Codec<BloodDripParticleData> codec() {
        return null;
    }
}
