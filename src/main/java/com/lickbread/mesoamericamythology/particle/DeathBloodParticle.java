package com.lickbread.mesoamericamythology.particle;

import net.minecraft.client.multiplayer.ClientLevel;
import net.minecraft.world.phys.Vec3;

import java.util.Random;

public class DeathBloodParticle extends BezierCurveMoveParticle {

    private static final int MAX_AGE = 30;

    public DeathBloodParticle(ClientLevel level, Vec3 start0, Vec3 start1, Vec3 end0, Vec3 end1) {
        super(level, start0, start1, end0, end1, MAX_AGE - new Random().nextInt(MAX_AGE / 3));
        this.scale(0.50F);
    }
}
