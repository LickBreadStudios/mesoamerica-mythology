package com.lickbread.mesoamericamythology.particle;

import lombok.AllArgsConstructor;
import net.minecraft.client.particle.SpriteSet;
import net.minecraft.client.particle.ParticleProvider;
import net.minecraft.client.particle.Particle;
import net.minecraft.client.multiplayer.ClientLevel;

import javax.annotation.Nullable;

@AllArgsConstructor
public class DeathBloodParticleFactory implements ParticleProvider<DeathBloodParticleData> {

    private final SpriteSet sprites;

    private DeathBloodParticleFactory() {
        throw new UnsupportedOperationException("You did wrong boi.");
    }

    @Nullable
    @Override
    public Particle createParticle(DeathBloodParticleData deathBloodParticleData, ClientLevel level, double x, double y, double z, double xVel, double yVel, double zVel) {
        DeathBloodParticle particle = new DeathBloodParticle(level, deathBloodParticleData.getStart0(), deathBloodParticleData.getStart1(), deathBloodParticleData.getEnd0(), deathBloodParticleData.getEnd1());
        particle.pickSprite(this.sprites);
        return particle;
    }
}
