package com.lickbread.mesoamericamythology.particle;

import com.mojang.serialization.Codec;
import net.minecraft.core.particles.ParticleType;

public class DeathBloodParticleType extends ParticleType<DeathBloodParticleData> {

    public DeathBloodParticleType() {
        super(false, DeathBloodParticleData.DESERIALIZER);
    }

    @Override
    public Codec<DeathBloodParticleData> codec() {
        return null;
    }
}
