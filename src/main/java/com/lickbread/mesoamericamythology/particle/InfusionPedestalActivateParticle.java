package com.lickbread.mesoamericamythology.particle;

import net.minecraft.client.multiplayer.ClientLevel;
import net.minecraft.world.phys.Vec3;

public class InfusionPedestalActivateParticle extends BezierCurveMoveParticle {

    public InfusionPedestalActivateParticle(ClientLevel level, Vec3 start0, Vec3 start1, Vec3 end0, Vec3 end1, int lifetime) {
        super(level, start0, start1, end0, end1, lifetime);
        this.scale(0.10F);
    }
}
