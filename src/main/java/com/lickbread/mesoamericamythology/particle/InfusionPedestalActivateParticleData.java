package com.lickbread.mesoamericamythology.particle;

import com.lickbread.mesoamericamythology.registry.ModParticleTypes;
import com.lickbread.mesoamericamythology.util.SerializerUtil;
import com.mojang.brigadier.StringReader;
import com.mojang.brigadier.exceptions.CommandSyntaxException;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import net.minecraft.core.particles.ParticleOptions;
import net.minecraft.core.particles.ParticleType;
import net.minecraft.network.FriendlyByteBuf;
import net.minecraft.world.phys.Vec3;
import net.minecraftforge.registries.ForgeRegistries;

import net.minecraft.core.particles.ParticleOptions.Deserializer;

@Getter
@Setter
@AllArgsConstructor
public class InfusionPedestalActivateParticleData implements ParticleOptions {
    @Override
    public ParticleType<InfusionPedestalActivateParticleData> getType() {
        return ModParticleTypes.INFUSION_PEDESTAL_ACTIVATE_PARTICLE.get();
    }

    private final Vec3 start0;
    private final Vec3 start1;
    private final Vec3 end0;
    private final Vec3 end1;
    private int lifetime;

    @Override
    public void writeToNetwork(FriendlyByteBuf packetBuffer) {
        packetBuffer.writeDouble(this.start0.x);
        packetBuffer.writeDouble(this.start0.y);
        packetBuffer.writeDouble(this.start0.z);
        packetBuffer.writeDouble(this.start1.x);
        packetBuffer.writeDouble(this.start1.y);
        packetBuffer.writeDouble(this.start1.z);
        packetBuffer.writeDouble(this.end0.x);
        packetBuffer.writeDouble(this.end0.y);
        packetBuffer.writeDouble(this.end0.z);
        packetBuffer.writeDouble(this.end1.x);
        packetBuffer.writeDouble(this.end1.y);
        packetBuffer.writeDouble(this.end1.z);
        packetBuffer.writeInt(this.lifetime);
    }

    @Override
    public String writeToString() {
        return ForgeRegistries.PARTICLE_TYPES.getKey(this.getType()) + " "
                + SerializerUtil.writeVec3(this.start0) + " "
                + SerializerUtil.writeVec3(this.start1) + " "
                + SerializerUtil.writeVec3(this.end0) + " "
                + SerializerUtil.writeVec3(this.end1) + " "
                + this.lifetime;
    }

    public static final Deserializer<InfusionPedestalActivateParticleData> DESERIALIZER = new Deserializer<InfusionPedestalActivateParticleData>() {
        @Override
        public InfusionPedestalActivateParticleData fromCommand(ParticleType<InfusionPedestalActivateParticleData> particleType, StringReader stringReader) throws CommandSyntaxException {
            stringReader.expect(' ');
            Vec3 start0 = SerializerUtil.readVec3(stringReader);
            stringReader.expect(' ');
            Vec3 start1 = SerializerUtil.readVec3(stringReader);
            stringReader.expect(' ');
            Vec3 end0 = SerializerUtil.readVec3(stringReader);
            stringReader.expect(' ');
            Vec3 end1 = SerializerUtil.readVec3(stringReader);
            stringReader.expect(' ');
            int lifetime = stringReader.readInt();
            return new InfusionPedestalActivateParticleData(start0, start1, end0, end1, lifetime);
        }

        @Override
        public InfusionPedestalActivateParticleData fromNetwork(ParticleType<InfusionPedestalActivateParticleData> particleType, FriendlyByteBuf packetBuffer) {
            return new InfusionPedestalActivateParticleData(SerializerUtil.readVec3(packetBuffer), SerializerUtil.readVec3(packetBuffer), SerializerUtil.readVec3(packetBuffer), SerializerUtil.readVec3(packetBuffer), packetBuffer.readInt());
        }
    };
}
