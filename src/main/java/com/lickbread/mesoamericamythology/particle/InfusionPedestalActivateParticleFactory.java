package com.lickbread.mesoamericamythology.particle;

import lombok.AllArgsConstructor;
import net.minecraft.client.particle.SpriteSet;
import net.minecraft.client.particle.ParticleProvider;
import net.minecraft.client.particle.Particle;
import net.minecraft.client.multiplayer.ClientLevel;

import javax.annotation.Nullable;

@AllArgsConstructor
public class InfusionPedestalActivateParticleFactory implements ParticleProvider<InfusionPedestalActivateParticleData> {

    private final SpriteSet sprites;

    private InfusionPedestalActivateParticleFactory() {
        throw new UnsupportedOperationException("You did wrong boi.");
    }

    @Nullable
    @Override
    public Particle createParticle(InfusionPedestalActivateParticleData data, ClientLevel level, double x, double y, double z, double xVel, double yVel, double zVel) {
        InfusionPedestalActivateParticle particle = new InfusionPedestalActivateParticle(level, data.getStart0(), data.getStart1(), data.getEnd0(), data.getEnd1(), data.getLifetime());
        particle.pickSprite(this.sprites);
        return particle;
    }
}
