package com.lickbread.mesoamericamythology.particle;

import com.mojang.serialization.Codec;
import net.minecraft.core.particles.ParticleType;

public class InfusionPedestalActivateParticleType extends ParticleType<InfusionPedestalActivateParticleData> {

    public InfusionPedestalActivateParticleType() {
        super(false, InfusionPedestalActivateParticleData.DESERIALIZER);
    }

    @Override
    public Codec<InfusionPedestalActivateParticleData> codec() {
        return null;
    }
}
