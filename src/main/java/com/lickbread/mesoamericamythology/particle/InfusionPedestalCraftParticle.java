package com.lickbread.mesoamericamythology.particle;

import com.lickbread.mesoamericamythology.util.MathUtil;
import net.minecraft.client.particle.BreakingItemParticle;
import net.minecraft.client.multiplayer.ClientLevel;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.phys.Vec3;

public class InfusionPedestalCraftParticle extends BreakingItemParticle {

    public static final int AGE = 30;
    private final Vec3 start0;
    private final Vec3 start1;
    private final Vec3 end0;
    private final Vec3 end1;

    public InfusionPedestalCraftParticle(ClientLevel level, Vec3 start0, Vec3 start1, Vec3 end0, Vec3 end1, ItemStack itemStack) {
        super(level, start0.x, start0.y, start0.z, itemStack);
        this.hasPhysics = false;
        this.lifetime = AGE;
        this.gravity = 0.0F;
        this.start0 = start0;
        this.start1 = start1;
        this.end0 = end0;
        this.end1 = end1;
        this.scale(0.5F);
    }

    /**
     * Called once per tick to update the Particle position, calculate collisions, remove when max lifetime is reached, etc
     */
    @Override
    public void tick() {
        xo = x;
        yo = y;
        zo = z;

        Vec3 newPos = MathUtil.cubeBezier3(start0, start1, end0, end1, this.age / (double) this.lifetime);
        this.move(newPos.x - xo, newPos.y - yo, newPos.z - zo);

        if (this.age++ >= this.lifetime) {
            this.remove();
        }
    }
}