package com.lickbread.mesoamericamythology.particle;

import com.lickbread.mesoamericamythology.registry.ModParticleTypes;
import com.lickbread.mesoamericamythology.util.SerializerUtil;
import com.mojang.brigadier.StringReader;
import com.mojang.brigadier.exceptions.CommandSyntaxException;
import lombok.Getter;
import net.minecraft.commands.arguments.item.ItemInput;
import net.minecraft.commands.arguments.item.ItemParser;
import net.minecraft.commands.arguments.item.ItemParser.ItemResult;
import net.minecraft.core.particles.ParticleOptions;
import net.minecraft.core.particles.ParticleType;
import net.minecraft.core.registries.BuiltInRegistries;
import net.minecraft.network.FriendlyByteBuf;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.phys.Vec3;
import net.minecraftforge.registries.ForgeRegistries;

@Getter
public class InfusionPedestalCraftParticleData implements ParticleOptions {

    @Override
    public ParticleType<InfusionPedestalCraftParticleData> getType() {
        return ModParticleTypes.INFUSION_PEDESTAL_CRAFT_PARTICLE.get();
    }

    private final Vec3 start0;
    private final Vec3 start1;
    private final Vec3 end0;
    private final Vec3 end1;

    private final ParticleType<InfusionPedestalCraftParticleData> particleType;
    private final ItemStack itemStack;

    public InfusionPedestalCraftParticleData(ParticleType<InfusionPedestalCraftParticleData> particleType, ItemStack itemStack, Vec3 start0, Vec3 start1, Vec3 end0, Vec3 end1) {
        this.particleType = particleType;
        this.itemStack = itemStack.copy();
        this.start0 = start0;
        this.start1 = start1;
        this.end0 = end0;
        this.end1 = end1;
    }

    @Override
    public void writeToNetwork(FriendlyByteBuf packetBuffer) {
        packetBuffer.writeItem(this.itemStack);
        packetBuffer.writeDouble(this.start0.x);
        packetBuffer.writeDouble(this.start0.y);
        packetBuffer.writeDouble(this.start0.z);
        packetBuffer.writeDouble(this.start1.x);
        packetBuffer.writeDouble(this.start1.y);
        packetBuffer.writeDouble(this.start1.z);
        packetBuffer.writeDouble(this.end0.x);
        packetBuffer.writeDouble(this.end0.y);
        packetBuffer.writeDouble(this.end0.z);
        packetBuffer.writeDouble(this.end1.x);
        packetBuffer.writeDouble(this.end1.y);
        packetBuffer.writeDouble(this.end1.z);
    }

    @Override
    public String writeToString() {
        return ForgeRegistries.PARTICLE_TYPES.getKey(this.getType()) + " "
                + (new ItemInput(this.itemStack.getItemHolder(), this.itemStack.getTag())).serialize() + " "
                + SerializerUtil.writeVec3(this.start0) + " "
                + SerializerUtil.writeVec3(this.start1) + " "
                + SerializerUtil.writeVec3(this.end0) + " "
                + SerializerUtil.writeVec3(this.end1);
    }

    public static final Deserializer<InfusionPedestalCraftParticleData> DESERIALIZER = new Deserializer<InfusionPedestalCraftParticleData>() {

        @Override
        public InfusionPedestalCraftParticleData fromCommand(ParticleType<InfusionPedestalCraftParticleData> particleType, StringReader stringReader) throws CommandSyntaxException {
            stringReader.expect(' ');
            ItemResult itemResult = ItemParser.parseForItem(BuiltInRegistries.ITEM.asLookup(), stringReader);
            ItemStack itemstack = (new ItemInput(itemResult.item(), itemResult.nbt())).createItemStack(1, false);
            stringReader.expect(' ');
            Vec3 start0 = SerializerUtil.readVec3(stringReader);
            stringReader.expect(' ');
            Vec3 start1 = SerializerUtil.readVec3(stringReader);
            stringReader.expect(' ');
            Vec3 end0 = SerializerUtil.readVec3(stringReader);
            stringReader.expect(' ');
            Vec3 end1 = SerializerUtil.readVec3(stringReader);
            return new InfusionPedestalCraftParticleData(particleType, itemstack, start0, start1, end0, end1);
        }

        @Override
        public InfusionPedestalCraftParticleData fromNetwork(ParticleType<InfusionPedestalCraftParticleData> particleType, FriendlyByteBuf packetBuffer) {
            return new InfusionPedestalCraftParticleData(particleType, packetBuffer.readItem(),
                    SerializerUtil.readVec3(packetBuffer), SerializerUtil.readVec3(packetBuffer), SerializerUtil.readVec3(packetBuffer), SerializerUtil.readVec3(packetBuffer));
        }
    };
}
