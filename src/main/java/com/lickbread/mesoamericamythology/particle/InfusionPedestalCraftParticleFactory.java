package com.lickbread.mesoamericamythology.particle;

import net.minecraft.client.particle.ParticleProvider;
import net.minecraft.client.particle.Particle;
import net.minecraft.client.multiplayer.ClientLevel;

import javax.annotation.Nullable;

public class InfusionPedestalCraftParticleFactory implements ParticleProvider<InfusionPedestalCraftParticleData> {

    @Nullable
    @Override
    public Particle createParticle(InfusionPedestalCraftParticleData particleData, ClientLevel level, double x, double y, double z, double xVel, double yVel, double zVel) {
        return new InfusionPedestalCraftParticle(level, particleData.getStart0(), particleData.getStart1(), particleData.getEnd0(), particleData.getEnd1(), particleData.getItemStack());
    }
}
