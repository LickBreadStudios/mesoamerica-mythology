package com.lickbread.mesoamericamythology.particle;

import com.mojang.serialization.Codec;
import net.minecraft.core.particles.ParticleType;

public class InfusionPedestalCraftParticleType extends ParticleType<InfusionPedestalCraftParticleData> {

    public InfusionPedestalCraftParticleType() {
        super(false, InfusionPedestalCraftParticleData.DESERIALIZER);
    }

    @Override
    public Codec<InfusionPedestalCraftParticleData> codec() {
        return null;
    }
}
