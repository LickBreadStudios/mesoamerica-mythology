package com.lickbread.mesoamericamythology.particle;

import lombok.AllArgsConstructor;
import net.minecraft.client.particle.SpriteSet;
import net.minecraft.client.particle.ParticleProvider;
import net.minecraft.client.particle.Particle;
import net.minecraft.client.multiplayer.ClientLevel;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;

@OnlyIn(Dist.CLIENT)
@AllArgsConstructor
public class InfusionPedestalRunesParticleFactory implements ParticleProvider<InfusionPedestalRunesParticleData> {

    private final SpriteSet sprites;

    public Particle createParticle(InfusionPedestalRunesParticleData dataIn, ClientLevel worldIn, double x, double y, double z, double xSpeed, double ySpeed, double zSpeed) {
        InfusionPedestalRunesParticle particle = new InfusionPedestalRunesParticle(worldIn, x, y, z, xSpeed, ySpeed, zSpeed);
        particle.pickSprite(this.sprites);
        particle.setColor(dataIn.getRed(), dataIn.getGreen(), dataIn.getBlue());
        return particle;
    }
}
