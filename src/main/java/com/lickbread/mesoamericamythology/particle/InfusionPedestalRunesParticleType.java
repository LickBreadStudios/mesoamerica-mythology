package com.lickbread.mesoamericamythology.particle;

import com.mojang.serialization.Codec;
import net.minecraft.core.particles.ParticleType;

public class InfusionPedestalRunesParticleType extends ParticleType<InfusionPedestalRunesParticleData> {

    public InfusionPedestalRunesParticleType() {
        super(false, InfusionPedestalRunesParticleData.DESERIALIZER);
    }

    @Override
    public Codec<InfusionPedestalRunesParticleData> codec() {
        return null;
    }
}
