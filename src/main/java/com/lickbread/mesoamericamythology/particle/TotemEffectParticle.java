package com.lickbread.mesoamericamythology.particle;

import net.minecraft.client.particle.ParticleRenderType;
import net.minecraft.client.particle.TextureSheetParticle;
import net.minecraft.client.multiplayer.ClientLevel;

public class TotemEffectParticle extends TextureSheetParticle {

    public TotemEffectParticle(ClientLevel level, double x, double y, double z, float r, float b, float g) {
        super(level, x, y, z);
        this.hasPhysics = true;
        this.lifetime = 30;
        this.scale(0.35F);
        this.gravity = 0.00F;
        this.setColor(r, b, g);
        this.yd = 0.2D;
    }

    /**
     * Called once per tick to update the Particle position, calculate collisions, remove when max lifetime is reached, etc
     */
    @Override
    public void tick() {
        xo = x;
        yo = y;
        zo = z;

        this.move(xd  * 0.001D, yd, zd * 0.001D);  // simple linear motion.  You can change speed by changing xd, yd
        alpha = (this.lifetime - this.age) / (float) this.lifetime;

        if (this.age++ >= this.lifetime) {
            this.remove();
        }
    }

    @Override
    public ParticleRenderType getRenderType() {
        return ParticleRenderType.PARTICLE_SHEET_TRANSLUCENT;
    }
}
