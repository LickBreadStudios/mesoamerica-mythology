package com.lickbread.mesoamericamythology.particle;

import com.lickbread.mesoamericamythology.registry.ModParticleTypes;
import com.mojang.brigadier.StringReader;
import com.mojang.brigadier.exceptions.CommandSyntaxException;
import lombok.AllArgsConstructor;
import lombok.Getter;
import net.minecraft.core.particles.ParticleOptions;
import net.minecraft.core.particles.ParticleType;
import net.minecraft.network.FriendlyByteBuf;
import net.minecraftforge.registries.ForgeRegistries;

import java.util.Locale;

@AllArgsConstructor
@Getter
public class TotemEffectParticleData implements ParticleOptions {

    private final float red;
    private final float green;
    private final float blue;

    @Override
    public ParticleType<TotemEffectParticleData> getType() {
        return ModParticleTypes.TOTEM_EFFECT_PARTICLE.get();
    }

    @Override
    public void writeToNetwork(FriendlyByteBuf packetBuffer) {
        packetBuffer.writeFloat(this.red);
        packetBuffer.writeFloat(this.green);
        packetBuffer.writeFloat(this.blue);
    }

    @Override
    public String writeToString() {
        return String.format(Locale.ROOT, "%s %.2f %.2f %.2f", ForgeRegistries.PARTICLE_TYPES.getKey(this.getType()), this.red, this.green, this.blue);
    }

    public static final Deserializer<TotemEffectParticleData> DESERIALIZER = new Deserializer<TotemEffectParticleData>() {
        @Override
        public TotemEffectParticleData fromCommand(ParticleType<TotemEffectParticleData> particleType, StringReader stringReader) throws CommandSyntaxException {
            stringReader.expect(' ');
            float f = (float)stringReader.readDouble();
            stringReader.expect(' ');
            float f1 = (float)stringReader.readDouble();
            stringReader.expect(' ');
            float f2 = (float)stringReader.readDouble();
            return new TotemEffectParticleData(f, f1, f2);
        }

        @Override
        public TotemEffectParticleData fromNetwork(ParticleType<TotemEffectParticleData> particleType, FriendlyByteBuf packetBuffer) {
            return new TotemEffectParticleData(packetBuffer.readFloat(), packetBuffer.readFloat(), packetBuffer.readFloat());
        }
    };
}
