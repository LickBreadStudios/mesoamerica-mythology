package com.lickbread.mesoamericamythology.particle;

import lombok.AllArgsConstructor;
import net.minecraft.client.particle.SpriteSet;
import net.minecraft.client.particle.ParticleProvider;
import net.minecraft.client.particle.Particle;
import net.minecraft.client.multiplayer.ClientLevel;

import javax.annotation.Nullable;

@AllArgsConstructor
public class TotemEffectParticleFactory implements ParticleProvider<TotemEffectParticleData> {

    private final SpriteSet sprites;

    private TotemEffectParticleFactory() {
        throw new UnsupportedOperationException("You did wrong boi.");
    }

    @Nullable
    @Override
    public Particle createParticle(TotemEffectParticleData totemEffectParticleData, ClientLevel level, double x, double y, double z, double xVel, double yVel, double zVel) {
        TotemEffectParticle particle = new TotemEffectParticle(level, x, y, z, totemEffectParticleData.getRed(), totemEffectParticleData.getGreen(), totemEffectParticleData.getBlue());
        particle.pickSprite(this.sprites);
        return particle;
    }
}
