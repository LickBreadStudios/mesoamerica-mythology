package com.lickbread.mesoamericamythology.particle;

import com.mojang.serialization.Codec;
import net.minecraft.core.particles.ParticleType;

public class TotemEffectParticleType extends ParticleType<TotemEffectParticleData> {

    public TotemEffectParticleType() {
        super(false, TotemEffectParticleData.DESERIALIZER);
    }

    @Override
    public Codec<TotemEffectParticleData> codec() {
        return null;
    }
}
