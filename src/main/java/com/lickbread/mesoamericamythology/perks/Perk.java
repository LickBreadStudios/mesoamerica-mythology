package com.lickbread.mesoamericamythology.perks;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@ToString
public class Perk {

    private static final String TITLE_TEXT_REPLACEMENT = "mesoamericamythology.perk.%s.title";
    private static final String DESCRIPTION_TEXT_REPLACEMENT = "mesoamericamythology.perk.%s.description";

    private String id;
    private PerkDisplayInfo displayInfo;
    private PerkTier tier;
    private PerkSection section;

    public String getTitleStringId() {
        return String.format(TITLE_TEXT_REPLACEMENT, this.id);
    }

    public String getDescriptionStringId() {
        return String.format(DESCRIPTION_TEXT_REPLACEMENT, this.id);
    }
}
