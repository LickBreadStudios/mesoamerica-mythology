package com.lickbread.mesoamericamythology.perks;

import com.lickbread.mesoamericamythology.client.screen.FrameType;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;
import net.minecraft.world.item.Item;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@ToString
public class PerkDisplayInfo {
    private Item icon;
    private FrameType type;
}
