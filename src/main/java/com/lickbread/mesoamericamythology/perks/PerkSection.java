package com.lickbread.mesoamericamythology.perks;

import com.google.gson.annotations.SerializedName;
import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum PerkSection {
    @SerializedName("adventure")
    ADVENTURE(0),
    @SerializedName("combat")
    COMBAT(60),
    @SerializedName("crafting")
    CRAFTING(120),
    @SerializedName("gathering")
    GATHERING(180),
    @SerializedName("hunting")
    HUNTING(240),
    @SerializedName("magic")
    MAGIC(300);

    private int theta;
}
