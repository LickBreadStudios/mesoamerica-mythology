package com.lickbread.mesoamericamythology.perks;

import com.google.gson.annotations.SerializedName;
import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum PerkTier {
    @SerializedName("0")
    TIER_0(0, 0),
    @SerializedName("1")
    TIER_1(50, 0),
    @SerializedName("2")
    TIER_2(100, 0),
    @SerializedName("3")
    TIER_3(150, 0),
    @SerializedName("4")
    TIER_4(200, 0);

    // Distance from center in Perk Tree
    private int r;

    // Distance from section edge in Perk tree
    private int theta;
}
