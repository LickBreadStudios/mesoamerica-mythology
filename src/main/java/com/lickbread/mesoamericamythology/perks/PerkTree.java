package com.lickbread.mesoamericamythology.perks;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@AllArgsConstructor
@NoArgsConstructor
@ToString
public class PerkTree {

    private static final String ROOT_PERK_ID = "root";

    @Getter
    private Map<String, Perk> perks = new HashMap<>();

    public void addPerk(Perk perk) {
        this.perks.put(perk.getId(), perk);
    }

    public Perk getPerk(String id) {
        return perks.get(id);
    }

    public Perk getRootPerk() {
        return perks.get(ROOT_PERK_ID);
    }

    public List<String> getPerksInTierAndSection(PerkTier perkTier, PerkSection perkSection) {
        return perks.values().stream()
                .filter(perk -> perk.getSection() == perkSection && perk.getTier() == perkTier)
                .map(Perk::getId)
                .collect(Collectors.toList());
    }
}
