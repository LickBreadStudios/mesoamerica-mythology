package com.lickbread.mesoamericamythology.registry;

import com.lickbread.mesoamericamythology.criterionTrigger.AnyAltarRecipeCompletedTrigger;
import com.lickbread.mesoamericamythology.criterionTrigger.AnyPedestalRecipeCompletedTrigger;
import net.minecraft.advancements.CriteriaTriggers;

public class ModAdvancementTriggers {

    public static AnyAltarRecipeCompletedTrigger ANY_ALTAR_RECIPE_COMPLETED_TRIGGER = null;
    public static AnyPedestalRecipeCompletedTrigger ANY_PEDESTAL_RECIPE_COMPLETED_TRIGGER = null;

    public static void registerAdvancementTriggers() {
        ANY_ALTAR_RECIPE_COMPLETED_TRIGGER = CriteriaTriggers.register(new AnyAltarRecipeCompletedTrigger());
        ANY_PEDESTAL_RECIPE_COMPLETED_TRIGGER = CriteriaTriggers.register(new AnyPedestalRecipeCompletedTrigger());
    }
}
