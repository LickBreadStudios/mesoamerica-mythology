package com.lickbread.mesoamericamythology.registry;

import com.lickbread.mesoamericamythology.MesoamericaMythologyMod;
import com.lickbread.mesoamericamythology.blockEntities.*;
import com.lickbread.mesoamericamythology.init.ModBlockObjects;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.entity.BlockEntityType;
import net.minecraftforge.registries.RegistryObject;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.ForgeRegistries;

public class ModBlockEntities {

    public static final DeferredRegister<BlockEntityType<?>> TILE_ENTITIES = DeferredRegister.create(ForgeRegistries.BLOCK_ENTITY_TYPES, MesoamericaMythologyMod.MOD_ID);

    public static final RegistryObject<BlockEntityType<?>> STONE_ALTAR = TILE_ENTITIES.register("stone_altar", () -> BlockEntityType.Builder.of(StoneAltarBlockEntity::new, ModBlockObjects.STONE_ALTAR).build(null));
    public static final RegistryObject<BlockEntityType<?>> CUAUHXICALLI = TILE_ENTITIES.register("cuauhxicalli", () -> BlockEntityType.Builder.of(CuauhxicalliBlockEntity::new, ModBlockObjects.CUAUHXICALLI).build(null));
    public static final RegistryObject<BlockEntityType<?>> CHACMOOL = TILE_ENTITIES.register("chacmool", () -> BlockEntityType.Builder.of(ChacmoolBlockEntity::new, ModBlockObjects.CHACMOOL).build(null));
    public static final RegistryObject<BlockEntityType<?>> GOD_STATUE = TILE_ENTITIES.register("god_statue", () -> BlockEntityType.Builder.of(GodStatueHeadBlockEntity::new).build(null));
    public static final RegistryObject<BlockEntityType<?>> FIRE_TRAP = TILE_ENTITIES.register("fire_trap", () -> BlockEntityType.Builder.of(FireTrapBlockEntity::new, ModBlockObjects.FIRE_TRAP).build(null));
    public static final RegistryObject<BlockEntityType<?>> SPIKE_TRAP = TILE_ENTITIES.register("spike_trap", () -> BlockEntityType.Builder.of(SpikeTrapBlockEntity::new, ModBlockObjects.SPIKE_TRAP).build(null));
    public static final RegistryObject<BlockEntityType<?>> SPEAR_TRAP = TILE_ENTITIES.register("spear_trap", () -> BlockEntityType.Builder.of(SpearTrapBlockEntity::new, ModBlockObjects.SPEAR_TRAP).build(null));
    public static final RegistryObject<BlockEntityType<?>> PEDESTAL = TILE_ENTITIES.register("pedestal", () -> BlockEntityType.Builder.of(PedestalBlockEntity::new, ModBlockObjects.INERT_PEDESTAL).build(null));
    public static final RegistryObject<BlockEntityType<?>> SACRIFICE_PEDESTAL = TILE_ENTITIES.register("sacrifice_pedestal", () -> BlockEntityType.Builder.of(SacrificePedestalBlockEntity::new, ModBlockObjects.SACRIFICE_PEDESTAL).build(null));
    public static final RegistryObject<BlockEntityType<?>> POWER_PEDESTAL = TILE_ENTITIES.register("power_pedestal", () -> BlockEntityType.Builder.of(PowerPedestalBlockEntity::new, ModBlockObjects.POWER_PEDESTAL).build(null));
    public static final RegistryObject<BlockEntityType<?>> INFUSION_PEDESTAL = TILE_ENTITIES.register("infusion_pedestal", () -> BlockEntityType.Builder.of(InfusionPedestalBlockEntity::new, ModBlockObjects.INFUSION_PEDESTAL).build(null));
    public static final RegistryObject<BlockEntityType<?>> BLOOD_BASIN = TILE_ENTITIES.register("blood_basin", () -> BlockEntityType.Builder.of(BloodBasinBlockEntity::new, ModBlockObjects.BLOOD_BASIN).build(null));
    public static final RegistryObject<BlockEntityType<?>> XOLOTLS_VESSEL = TILE_ENTITIES.register("xolotls_vessel", () -> BlockEntityType.Builder.of(XolotlsVesselBlockEntity::new, ModBlockObjects.XOLOTLS_VESSEL).build(null));
    public static final RegistryObject<BlockEntityType<?>> OFFERING_BOWL = TILE_ENTITIES.register("offering_bowl", () -> BlockEntityType.Builder.of(OfferingBowlBlockEntity::new, ModBlockObjects.OFFERING_BOWL).build(null));

    public static final RegistryObject<BlockEntityType<?>> TOTEM = TILE_ENTITIES.register("totem", () -> {
        Block[] validBlocks = new Block[ModBlocks.TOTEMS.size()];
        validBlocks = ModBlocks.TOTEMS.values().stream().map(RegistryObject::get).toList().toArray(validBlocks);
        return BlockEntityType.Builder.of(TotemBlockEntity::new, validBlocks).build(null);
    });

}
