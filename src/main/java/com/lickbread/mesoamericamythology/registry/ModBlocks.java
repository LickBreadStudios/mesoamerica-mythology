package com.lickbread.mesoamericamythology.registry;

import com.lickbread.mesoamericamythology.MesoamericaMythologyMod;
import com.lickbread.mesoamericamythology.blocks.*;
import com.lickbread.mesoamericamythology.blocks.altar.*;
import com.lickbread.mesoamericamythology.blocks.traps.*;
import com.lickbread.mesoamericamythology.registry.event.ModCreativeModeTabs;
import com.lickbread.mesoamericamythology.statueHead.God;
import net.minecraft.world.item.BlockItem;
import net.minecraft.world.item.Item;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.material.Material;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.ForgeRegistries;
import net.minecraftforge.registries.RegistryObject;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Supplier;

public class ModBlocks {

    public static final DeferredRegister<Block> BLOCKS = DeferredRegister.create(ForgeRegistries.BLOCKS, MesoamericaMythologyMod.MOD_ID);
    public static final DeferredRegister<Item> ITEM_BLOCKS = DeferredRegister.create(ForgeRegistries.ITEMS, MesoamericaMythologyMod.MOD_ID);

    public static final Map<TotemType, RegistryObject<Block>> TOTEMS = new HashMap<>();

    public static final RegistryObject<Block> BLOOD_SPHERE = register("blood_sphere", () -> new Block(Block.Properties.of(Material.WOOL).strength(100.0F, 100.0F)), false);
    public static final RegistryObject<Block> PEDESTAL_RUNES =register("pedestal_runes", PedestalRunesBlock::new, false);
    public static final RegistryObject<Block> CARVED_STONE = register("carved_stone", CarvedStoneBlock::new);
    public static final RegistryObject<Block> STONE_ALTAR = register("stone_altar", StoneAltarBlock::new);
    public static final RegistryObject<Block> CUAUHXICALLI = register("cuauhxicalli", CuauhxicalliBlock::new);
    public static final RegistryObject<Block> CHACMOOL = register("chacmool", ChacmoolBlock::new);
    public static final RegistryObject<Block> JADE_BLOCK = register("jade_block", () -> new Block(Block.Properties.of(Material.STONE).strength(5.0F, 50.0F)));
    public static final RegistryObject<Block> JADE_ORE = register("jade_ore", () -> new Block(Block.Properties.of(Material.STONE).strength(5.0F, 50.0F)));
    public static final RegistryObject<Block> FIRE_TRAP = register("fire_trap", FireTrapBlock::new);
    public static final RegistryObject<Block> SPIKE_TRAP = register("spike_trap", SpikeTrapBlock::new);
    public static final RegistryObject<Block> SPEAR_TRAP = register("spear_trap", SpearTrapBlock::new);
    public static final RegistryObject<Block> SPEAR = register("spear", SpearBlock::new, false);
    public static final RegistryObject<Block> SPIKES = register("spikes", SpikesBlock::new, false);
    public static final RegistryObject<Block> FALLING_TRAP = register("falling_trap", FallingTrapBlock::new);
    public static final RegistryObject<Block> INERT_PEDESTAL = register("inert_pedestal", InertPedestalBlock::new);
    public static final RegistryObject<Block> POWER_PEDESTAL = register("power_pedestal", PowerPedestalBlock::new);
    public static final RegistryObject<Block> INFUSION_PEDESTAL = register("infusion_pedestal", InfusionPedestalBlock::new);
    public static final RegistryObject<Block> DAMAGED_INFUSION_PEDESTAL = register("damaged_infusion_pedestal", DamagedInfusionPedestalBlock::new);
    public static final RegistryObject<Block> SACRIFICE_PEDESTAL = register("sacrifice_pedestal", SacrificePedestalBlock::new);
    public static final RegistryObject<Block> BLOOD_BASIN = register("blood_basin", BloodBasinBlock::new);
    public static final RegistryObject<Block> XOLOTLS_VESSEL = register("xolotls_vessel", XolotlsVesselBlock::new);
    public static final RegistryObject<Block> OFFERING_BOWL = register("offering_bowl", OfferingBowlBlock::new);

    public static final RegistryObject<Block> GOD_STATUE_HEAD_HUITZILOPOCHTLI = register("god_statue_head_" + God.HUITZILOPOCHTLI, () -> new GodStatueHeadBlock(God.HUITZILOPOCHTLI));

    static {
        registerTotems();
    }

    private static void registerTotems() {
        for (TotemType totemType : TotemType.values()) {
            RegistryObject<Block> totem = register("totem_" + totemType.getSerializedName(), () -> new TotemBlock(totemType));
            TOTEMS.put(totemType, totem);
        }
    }


    private static RegistryObject<Block> register(String name, Supplier<Block> block) {
        return register(name, block, true);
    }

    private static RegistryObject<Block> register(String name, Supplier<Block> block, boolean addToCreativeTab) {
        RegistryObject<Block> registeredBlock = BLOCKS.register(name, block);
        RegistryObject<Item> registeredItem = ITEM_BLOCKS.register(name, () -> new BlockItem(registeredBlock.get(), new Item.Properties()));
        if (addToCreativeTab) {
            ModCreativeModeTabs.CREATIVE_MODE_TAB_ITEMS.add(registeredItem);
        }
        return registeredBlock;
    }
}
