package com.lickbread.mesoamericamythology.registry;

import com.lickbread.mesoamericamythology.MesoamericaMythologyMod;
import net.minecraft.core.registries.Registries;
import net.minecraft.resources.ResourceKey;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.damagesource.DamageSource;
import net.minecraft.world.damagesource.DamageType;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.level.Level;

import javax.annotation.Nullable;

public class ModDamageSources {

    public static void attackPlayerWithObsidianKnife(Level level, Player playerEntity) {
        playerEntity.hurt(source(level, OBSIDIAN_KNIFE, playerEntity), 2.0F);
    }

    public static final ResourceKey<DamageType> FIRE_TRAP = register("fire_trap");
    public static final ResourceKey<DamageType> SPIKE_TRAP = register("spike_trap");
    public static final ResourceKey<DamageType> SPEAR_TRAP = register("spear_trap");
    public static final ResourceKey<DamageType> OBSIDIAN_KNIFE = register("obsidian_knife");
    public static final ResourceKey<DamageType> THROWN_JAVELIN = register("thrown_javelin");

    private static ResourceKey<DamageType> register(String name) {
        return ResourceKey.create(Registries.DAMAGE_TYPE, new ResourceLocation(MesoamericaMythologyMod.MOD_ID, name));
    }

    public static DamageSource source(Level level, ResourceKey<DamageType> key) {
        return new DamageSource(level.registryAccess().registryOrThrow(Registries.DAMAGE_TYPE).getHolderOrThrow(key));
    }

    public static DamageSource source(Level level, ResourceKey<DamageType> key, @Nullable Entity entity) {
        return new DamageSource(level.registryAccess().registryOrThrow(Registries.DAMAGE_TYPE).getHolderOrThrow(key), entity);
    }

    public static DamageSource source(Level level, ResourceKey<DamageType> key, @Nullable Entity causingEntity, @Nullable Entity directEntity) {
        return new DamageSource(level.registryAccess().registryOrThrow(Registries.DAMAGE_TYPE).getHolderOrThrow(key), causingEntity, directEntity);
    }
}