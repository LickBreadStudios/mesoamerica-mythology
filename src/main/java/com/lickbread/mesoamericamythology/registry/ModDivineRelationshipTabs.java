package com.lickbread.mesoamericamythology.registry;

import com.lickbread.mesoamericamythology.api.MesoamericaMythologyAPI;
import com.lickbread.mesoamericamythology.client.screen.GodInfoTabGui;
import com.lickbread.mesoamericamythology.client.screen.PerksTreeTabGui;
import com.lickbread.mesoamericamythology.client.screen.RecipeTabGui;
import com.lickbread.mesoamericamythology.statueHead.God;

import java.util.Arrays;

public class ModDivineRelationshipTabs {

    public static void registerTabs() {
        MesoamericaMythologyAPI.addDivineRelationshipsTabGui(new PerksTreeTabGui());
        MesoamericaMythologyAPI.addDivineRelationshipsTabGui(new RecipeTabGui());
        Arrays.asList(God.values()).forEach(god -> MesoamericaMythologyAPI.addDivineRelationshipsTabGui(new GodInfoTabGui(god)));
    }
}
