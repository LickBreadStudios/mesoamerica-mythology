package com.lickbread.mesoamericamythology.registry;

import com.lickbread.mesoamericamythology.MesoamericaMythologyMod;
import com.lickbread.mesoamericamythology.entity.SpanishConquistadorEntity;
import com.lickbread.mesoamericamythology.entity.SpanishConquistadorGeneralEntity;
import com.lickbread.mesoamericamythology.entity.ThrownJavelin;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.entity.EntityType;
import net.minecraft.world.entity.MobCategory;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.ForgeRegistries;
import net.minecraftforge.registries.RegistryObject;

import static com.lickbread.mesoamericamythology.entity.SpanishConquistadorEntity.SPANISH_CONQUISTADOR_NAME;
import static com.lickbread.mesoamericamythology.entity.SpanishConquistadorGeneralEntity.SPANISH_CONQUISTADOR_GENERAL_NAME;
import static com.lickbread.mesoamericamythology.entity.ThrownJavelin.THROWN_JAVELIN_NAME;

public class ModEntityTypes {

    public static final DeferredRegister<EntityType<?>> ENTITY_TYPES = DeferredRegister.create(ForgeRegistries.ENTITY_TYPES, MesoamericaMythologyMod.MOD_ID);

    public static final RegistryObject<EntityType<SpanishConquistadorEntity>> SPANISH_CONQUISTADOR = ENTITY_TYPES.register(SPANISH_CONQUISTADOR_NAME, () ->
            EntityType.Builder.of(SpanishConquistadorEntity::new, MobCategory.MONSTER)
                    .sized(EntityType.ZOMBIE.getWidth(), EntityType.ZOMBIE.getHeight())
                    .build(new ResourceLocation(MesoamericaMythologyMod.MOD_ID, SPANISH_CONQUISTADOR_NAME).toString())
    );

    public static final RegistryObject<EntityType<SpanishConquistadorGeneralEntity>> SPANISH_CONQUISTADOR_GENERAL = ENTITY_TYPES.register(SPANISH_CONQUISTADOR_GENERAL_NAME, () ->
            EntityType.Builder.of(SpanishConquistadorGeneralEntity::new, MobCategory.MONSTER)
                    .sized(EntityType.ZOMBIE.getWidth(), EntityType.ZOMBIE.getHeight())
                    .build(new ResourceLocation(MesoamericaMythologyMod.MOD_ID, SPANISH_CONQUISTADOR_GENERAL_NAME).toString())
    );

    public static final RegistryObject<EntityType<ThrownJavelin>> THROWN_JAVELIN = ENTITY_TYPES.register(THROWN_JAVELIN_NAME, () ->
            EntityType.Builder.<ThrownJavelin>of(ThrownJavelin::new, MobCategory.MISC)
                    .sized(0.5F, 0.5F)
                    .clientTrackingRange(4)
                    .updateInterval(20)
                    .build(new ResourceLocation(MesoamericaMythologyMod.MOD_ID, THROWN_JAVELIN_NAME).toString())
    );

}
