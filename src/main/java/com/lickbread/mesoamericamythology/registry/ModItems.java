package com.lickbread.mesoamericamythology.registry;

import com.lickbread.mesoamericamythology.MesoamericaMythologyMod;
import com.lickbread.mesoamericamythology.items.*;
import com.lickbread.mesoamericamythology.registry.event.ModCreativeModeTabs;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.SwordItem;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.ForgeRegistries;
import net.minecraftforge.registries.RegistryObject;

import java.util.function.Supplier;

public class ModItems {

    private static final Supplier<Item> BASIC_ITEM = () -> new Item(new Item.Properties());

    public static final DeferredRegister<Item> ITEMS = DeferredRegister.create(ForgeRegistries.ITEMS, MesoamericaMythologyMod.MOD_ID);

    public static final RegistryObject<Item> OBSIDIAN_KNIFE = register("obsidian_knife", ObsidianKnifeItem::new);
    public static final RegistryObject<Item> OBSIDIAN_SHARD = register("obsidian_shard", BASIC_ITEM);
    public static final RegistryObject<Item> BLOOD_BOWL = register("blood_bowl", BASIC_ITEM);
    public static final RegistryObject<Item> MONSTER_HEART = register("monster_heart", MonsterHeartItem::new);
    public static final RegistryObject<Item> PLAYER_HEART = register("player_heart", PlayerHeartItem::new);
    public static final RegistryObject<Item> ENRICHED_PLAYER_HEART = register("enriched_player_heart", EnrichedPlayerHeartItem::new);
    public static final RegistryObject<Item> HEART_OF_POWER = register("heart_of_power", HeartOfPowerItem::new);
    public static final RegistryObject<Item> HEART_OF_VOID = register("heart_of_void", HeartOfVoidItem::new);
    public static final RegistryObject<Item> MAQUAHUITL = register("maquahuitl", () -> new SwordItem(ModItemTier.OBSIDIAN, 4, -2.4F, new Item.Properties()));
    public static final RegistryObject<Item> CHIMALLI = register("chimalli", ChimalliShield::new);
    public static final RegistryObject<Item> FEATHER_SHIELD = register("feather_shield", FeatherShield::new);
    public static final RegistryObject<Item> JADE = register("jade", BASIC_ITEM);
    public static final RegistryObject<Item> JADE_FEATHER = register("jade_feather", BASIC_ITEM);
    public static final RegistryObject<Item> VOID_POCKET = register("void_pocket", VoidPocketItem::new);
    public static final RegistryObject<Item> JAVELIN = register("javelin", JavelinItem::new);

    private static RegistryObject<Item> register(String name, Supplier<Item> item) {
        return register(name, item, true);
    }

    private static RegistryObject<Item> register(String name, Supplier<Item> item, boolean addToCreativeTab) {
        RegistryObject<Item> registeredItem = ITEMS.register(name, item);
        if (addToCreativeTab) {
            ModCreativeModeTabs.CREATIVE_MODE_TAB_ITEMS.add(registeredItem);
        }
        return registeredItem;
    }
}
