package com.lickbread.mesoamericamythology.registry;

import com.lickbread.mesoamericamythology.MesoamericaMythologyMod;
import net.minecraft.resources.ResourceLocation;

public class ModLootTables {

    public static final ResourceLocation CHESTS_SCOUT_CHEST = new ResourceLocation(MesoamericaMythologyMod.MOD_ID, "chests/spanish_settlement/scout_chest");
    public static final ResourceLocation CHESTS_ARMOURY_CHEST = new ResourceLocation(MesoamericaMythologyMod.MOD_ID, "chests/spanish_settlement/armoury_chest");
    public static final ResourceLocation CHESTS_GENERAL_CHEST = new ResourceLocation(MesoamericaMythologyMod.MOD_ID, "chests/spanish_settlement/general_chest");
    public static final ResourceLocation CHESTS_SECRET_CHEST = new ResourceLocation(MesoamericaMythologyMod.MOD_ID, "chests/spanish_settlement/secret_chest");
}
