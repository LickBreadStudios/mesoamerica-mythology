package com.lickbread.mesoamericamythology.registry;

import net.minecraft.client.model.geom.ModelLayerLocation;
import net.minecraft.resources.ResourceLocation;

public class ModModelLayerLocations {

    public static ModelLayerLocation CHIMALLI_LAYER = new ModelLayerLocation(new ResourceLocation("mesoamericanmythology:chimalli"), "chimalli");
    public static ModelLayerLocation FEATHER_SHIELD_LAYER = new ModelLayerLocation(new ResourceLocation("mesoamericanmythology:feather_shield"), "feather_shield");
    public static ModelLayerLocation THROWN_JAVELIN_LAYER = new ModelLayerLocation(new ResourceLocation("mesoamericanmythology:thrown_javelin"), "thrown_javelin");
}
