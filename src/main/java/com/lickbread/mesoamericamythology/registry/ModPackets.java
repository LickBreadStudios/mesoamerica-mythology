package com.lickbread.mesoamericamythology.registry;

import com.lickbread.mesoamericamythology.network.DivineFavourCapabilityMessageToClient;
import com.lickbread.mesoamericamythology.network.MessageHandlerOnClient;

import java.util.Optional;

import static com.lickbread.mesoamericamythology.network.ModChannels.MAIN_PACKET_CHANNEL_INSTANCE;
import static net.minecraftforge.network.NetworkDirection.PLAY_TO_CLIENT;

public class ModPackets {

    public static void registerPackets() {
        int index = 0;
        MAIN_PACKET_CHANNEL_INSTANCE.registerMessage(index++, DivineFavourCapabilityMessageToClient.class,
                DivineFavourCapabilityMessageToClient::encode, DivineFavourCapabilityMessageToClient::decode,
                MessageHandlerOnClient::onMessageReceived,
                Optional.of(PLAY_TO_CLIENT));
    }
}
