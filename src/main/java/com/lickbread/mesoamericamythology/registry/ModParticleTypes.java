package com.lickbread.mesoamericamythology.registry;

import com.lickbread.mesoamericamythology.MesoamericaMythologyMod;
import com.lickbread.mesoamericamythology.particle.*;
import net.minecraft.core.particles.ParticleType;
import net.minecraftforge.registries.RegistryObject;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.ForgeRegistries;

public class ModParticleTypes {

    public static final DeferredRegister<ParticleType<?>> PARTICLES = DeferredRegister.create(ForgeRegistries.PARTICLE_TYPES, MesoamericaMythologyMod.MOD_ID);

    public static RegistryObject<BloodDripParticleType> BLOOD_DRIP_PARTICLE = PARTICLES.register("blood_drip_particle", BloodDripParticleType::new);
    public static RegistryObject<TotemEffectParticleType> TOTEM_EFFECT_PARTICLE = PARTICLES.register("totem_effect_particle", TotemEffectParticleType::new);
    public static RegistryObject<DeathBloodParticleType> DEATH_BLOOD_PARTICLE = PARTICLES.register("death_blood_particle", DeathBloodParticleType::new);
    public static RegistryObject<AltarBreakItemParticleType> ALTAR_BREAK_ITEM_PARTICLE = PARTICLES.register("altar_break_particle", AltarBreakItemParticleType::new);
    public static RegistryObject<InfusionPedestalActivateParticleType> INFUSION_PEDESTAL_ACTIVATE_PARTICLE = PARTICLES.register("infusion_pedestal_activate_particle", InfusionPedestalActivateParticleType::new);
    public static RegistryObject<InfusionPedestalCraftParticleType> INFUSION_PEDESTAL_CRAFT_PARTICLE = PARTICLES.register("infusion_pedestal_craft_particle", InfusionPedestalCraftParticleType::new);
    public static RegistryObject<InfusionPedestalRunesParticleType> INFUSION_PEDESTAL_RUNES_PARTICLE = PARTICLES.register("infusion_pedestal_runes_particle", InfusionPedestalRunesParticleType::new);

}
