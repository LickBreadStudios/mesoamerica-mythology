package com.lickbread.mesoamericamythology.registry;

import com.google.common.collect.ImmutableList;
import com.lickbread.mesoamericamythology.api.MesoamericaMythologyAPI;
import com.lickbread.mesoamericamythology.api.recipes.altar.PotionAltarRecipeMatcher;
import com.lickbread.mesoamericamythology.api.recipes.altar.StackAltarRecipeMatcher;
import com.lickbread.mesoamericamythology.api.recipes.outputs.ItemStackRecipeOutput;
import com.lickbread.mesoamericamythology.api.recipes.outputs.PerkRecipeOutput;
import com.lickbread.mesoamericamythology.api.recipes.pedestal.CuauhxicalliHeartPedestalRecipeMatcher;
import com.lickbread.mesoamericamythology.api.recipes.pedestal.SacrificialPedestalRecipeMatcher;
import com.lickbread.mesoamericamythology.api.recipes.pedestal.StackInfusionPedestalRecipeMatcher;
import com.lickbread.mesoamericamythology.api.recipes.pedestal.StackPowerPedestalRecipeMatcher;
import com.lickbread.mesoamericamythology.items.MonsterHeartItem;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.entity.EntityType;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.Items;
import net.minecraft.world.item.alchemy.Potions;

public class ModRecipes {

    public static void registerRecipes() {
        // Jade Feather
        MesoamericaMythologyAPI.addStoneAltarRecipe(ImmutableList.of(
                new StackAltarRecipeMatcher(new ItemStack(Items.FEATHER ,4)),
                new StackAltarRecipeMatcher(new ItemStack(ModItems.JADE.get(), 2)),
                new PotionAltarRecipeMatcher(Potions.LEAPING, 1)),
                ItemStackRecipeOutput.of(ModItems.JADE_FEATHER.get(), 4), 4000);
        // Chimalli
        MesoamericaMythologyAPI.addStoneAltarRecipe(ImmutableList.of(
                new StackAltarRecipeMatcher(new ItemStack(ModItems.JADE_FEATHER.get() ,7)),
                new StackAltarRecipeMatcher(new ItemStack(ModItems.FEATHER_SHIELD.get(), 1))),
                ItemStackRecipeOutput.of(ModItems.CHIMALLI.get(), 1), 5000);
        // Pedestal of Power
        MesoamericaMythologyAPI.addStoneAltarRecipe(ImmutableList.of(
                new StackAltarRecipeMatcher(new ItemStack(ModBlocks.INERT_PEDESTAL.get() ,1)),
                new StackAltarRecipeMatcher(new ItemStack(ModItems.HEART_OF_POWER.get(), 1)),
                new PotionAltarRecipeMatcher(Potions.STRONG_STRENGTH, 2),
                new StackAltarRecipeMatcher(new ItemStack(Items.DIAMOND_SWORD, 2)),
                new StackAltarRecipeMatcher(new ItemStack(ModBlocks.JADE_BLOCK.get(), 2))),
                ItemStackRecipeOutput.of(ModBlocks.POWER_PEDESTAL.get()), 10000);
        // Pedestal of Infusion
        MesoamericaMythologyAPI.addStoneAltarRecipe(ImmutableList.of(
                new StackAltarRecipeMatcher(new ItemStack(ModBlocks.DAMAGED_INFUSION_PEDESTAL.get() ,1)),
                new StackAltarRecipeMatcher(new ItemStack(Blocks.OBSIDIAN, 2)),
                new StackAltarRecipeMatcher(new ItemStack(ModItems.ENRICHED_PLAYER_HEART.get(), 1))),
                ItemStackRecipeOutput.of(ModBlocks.INFUSION_PEDESTAL.get()), 10000);
        // Pedestal of Sacrifice
        MesoamericaMythologyAPI.addInfusionPedestalRecipe(ImmutableList.of(
                new CuauhxicalliHeartPedestalRecipeMatcher<>((MonsterHeartItem) ModItems.MONSTER_HEART.get(), EntityType.ZOMBIE, 1),
                new CuauhxicalliHeartPedestalRecipeMatcher<>((MonsterHeartItem) ModItems.MONSTER_HEART.get(), EntityType.SKELETON, 1),
                new CuauhxicalliHeartPedestalRecipeMatcher<>((MonsterHeartItem) ModItems.MONSTER_HEART.get(), EntityType.CREEPER, 1),
                new StackInfusionPedestalRecipeMatcher(new ItemStack(ModBlocks.INERT_PEDESTAL.get(), 1))),
                ItemStackRecipeOutput.of(ModBlocks.SACRIFICE_PEDESTAL.get()), 10000);

        // Tests
        MesoamericaMythologyAPI.addStoneAltarRecipe(ImmutableList.of(new StackAltarRecipeMatcher(new ItemStack(Items.BOOK))), ItemStackRecipeOutput.of(Blocks.BOOKSHELF), 800);
        MesoamericaMythologyAPI.addStoneAltarRecipe(ImmutableList.of(new StackAltarRecipeMatcher(new ItemStack(Blocks.BOOKSHELF))), ItemStackRecipeOutput.of(Items.BOOK), 800);
        MesoamericaMythologyAPI.addInfusionPedestalRecipe(ImmutableList.of(new StackInfusionPedestalRecipeMatcher(new ItemStack(Blocks.BOOKSHELF))), ItemStackRecipeOutput.of(Items.BOOK), 800);
        MesoamericaMythologyAPI.addInfusionPedestalRecipe(ImmutableList.of(new StackInfusionPedestalRecipeMatcher(new ItemStack(Blocks.DIAMOND_BLOCK)), new StackPowerPedestalRecipeMatcher(new ItemStack(Items.STICK, 3))), ItemStackRecipeOutput.of(Items.BOOK), 800);
        MesoamericaMythologyAPI.addInfusionPedestalRecipe(ImmutableList.of(new StackInfusionPedestalRecipeMatcher(new ItemStack(Blocks.DIAMOND_BLOCK)), new SacrificialPedestalRecipeMatcher(EntityType.PIG, 1)), ItemStackRecipeOutput.of(Items.EMERALD), 800);
        MesoamericaMythologyAPI.addStoneAltarRecipe(ImmutableList.of(new StackAltarRecipeMatcher(new ItemStack(Items.FEATHER))), PerkRecipeOutput.of("perk_0"), 500);
    }
}
