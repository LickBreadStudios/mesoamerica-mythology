package com.lickbread.mesoamericamythology.registry.data;

import com.lickbread.mesoamericamythology.MesoamericaMythologyMod;
import com.lickbread.mesoamericamythology.api.MesoamericaMythologyAPI;
import com.lickbread.mesoamericamythology.statueHead.God;
import com.lickbread.mesoamericamythology.statueHead.GodStatueHead;
import net.minecraft.client.Minecraft;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.server.packs.resources.Resource;
import org.apache.commons.io.IOUtils;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Optional;

import static com.lickbread.mesoamericamythology.api.MesoamericaMythologyAPI.GSON;

public class GodStatueHeadParser {

    public static void registerGodStatueHeads() {
        for (God god : God.values()) {
            try {
                Optional<Resource> resource = Minecraft.getInstance().getResourceManager().getResource(new ResourceLocation(MesoamericaMythologyMod.MOD_ID, "gods/" + god.toString() + ".json"));
                String fileString = IOUtils.toString(resource.get().open(), StandardCharsets.UTF_8);
                GodStatueHead godStatueHead = GSON.fromJson(fileString, GodStatueHead.class);
                godStatueHead.setGod(god);
                MesoamericaMythologyAPI.addGodStatueHead(god, godStatueHead);
                MesoamericaMythologyMod.LOGGER.info("Found and successfully parsed god data for " + god.toString());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

}
