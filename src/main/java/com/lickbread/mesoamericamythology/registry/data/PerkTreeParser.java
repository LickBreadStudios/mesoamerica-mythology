package com.lickbread.mesoamericamythology.registry.data;

import com.google.gson.reflect.TypeToken;
import com.lickbread.mesoamericamythology.MesoamericaMythologyMod;
import com.lickbread.mesoamericamythology.api.MesoamericaMythologyAPI;
import com.lickbread.mesoamericamythology.perks.Perk;
import net.minecraft.client.Minecraft;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.server.packs.resources.Resource;
import org.apache.commons.io.IOUtils;

import java.io.IOException;
import java.lang.reflect.Type;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static com.lickbread.mesoamericamythology.api.MesoamericaMythologyAPI.GSON;

public class PerkTreeParser {

    public static void registerPerkTree() {
        try {
            Optional<Resource> resource = Minecraft.getInstance().getResourceManager().getResource(new ResourceLocation(MesoamericaMythologyMod.MOD_ID, "perks/tree.json"));
            String fileString = IOUtils.toString(resource.get().open(), StandardCharsets.UTF_8);
            Type listType = new TypeToken<ArrayList<Perk>>(){}.getType();
            List<Perk> perks = GSON.fromJson(fileString, listType);
            perks.forEach(MesoamericaMythologyAPI::addPerk);
            MesoamericaMythologyMod.LOGGER.info("Found and successfully parsed perk tree data:\n" + MesoamericaMythologyAPI.getPerkTree().toString());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
