package com.lickbread.mesoamericamythology.registry.event;

import com.lickbread.mesoamericamythology.MesoamericaMythologyMod;
import com.lickbread.mesoamericamythology.capabilities.DivineFavourCapabilityProvider;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.player.Player;
import net.minecraftforge.event.AttachCapabilitiesEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;

import static com.lickbread.mesoamericamythology.capabilities.DivineFavourCapabilityProvider.DIVINE_FAVOUR_CAPABILITY;
import static com.lickbread.mesoamericamythology.capabilities.DivineFavourCapabilityProvider.DIVINE_FAVOUR_CAPABILITY_KEY;

@Mod.EventBusSubscriber
public class ModAttachCapabilities {

    @SubscribeEvent
    public static void onAttachCapabilitiesEvent(AttachCapabilitiesEvent<Entity> event) {
        if (!(event.getObject() instanceof Player)) {
            return;
        }

        if (!event.getObject().getCapability(DIVINE_FAVOUR_CAPABILITY).isPresent()) {
            MesoamericaMythologyMod.LOGGER.debug("Attached Capability");
            event.addCapability(DIVINE_FAVOUR_CAPABILITY_KEY, new DivineFavourCapabilityProvider());
        }
    }
}
