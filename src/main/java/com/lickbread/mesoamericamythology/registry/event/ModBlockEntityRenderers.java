package com.lickbread.mesoamericamythology.registry.event;

import com.lickbread.mesoamericamythology.MesoamericaMythologyMod;
import com.lickbread.mesoamericamythology.client.renderer.blockEntity.*;
import com.lickbread.mesoamericamythology.init.ModBlockEntityTypeObjects;
import net.minecraftforge.client.event.EntityRenderersEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;

/**
 * For some reason this has to be registered in the Mod class
 */
@Mod.EventBusSubscriber(modid = MesoamericaMythologyMod.MOD_ID)
public class ModBlockEntityRenderers {

    @SubscribeEvent
    public static void listen(EntityRenderersEvent.RegisterRenderers event) {
        event.registerBlockEntityRenderer(ModBlockEntityTypeObjects.STONE_ALTAR, StoneAltarRenderer::new);
        event.registerBlockEntityRenderer(ModBlockEntityTypeObjects.GOD_STATUE, GodStatueHeadRenderer::new);
        event.registerBlockEntityRenderer(ModBlockEntityTypeObjects.TOTEM, TotemRenderer::new);
        event.registerBlockEntityRenderer(ModBlockEntityTypeObjects.SPIKE_TRAP, SpikeTrapRenderer::new);
        event.registerBlockEntityRenderer(ModBlockEntityTypeObjects.SPEAR_TRAP, SpearTrapRenderer::new);
        event.registerBlockEntityRenderer(ModBlockEntityTypeObjects.PEDESTAL, PedestalRenderer::new);
        event.registerBlockEntityRenderer(ModBlockEntityTypeObjects.SACRIFICE_PEDESTAL, SacrificePedestalRenderer::new);
        event.registerBlockEntityRenderer(ModBlockEntityTypeObjects.POWER_PEDESTAL, PowerPedestalRenderer::new);
        event.registerBlockEntityRenderer(ModBlockEntityTypeObjects.INFUSION_PEDESTAL, InfusionPedestalRenderer::new);
        event.registerBlockEntityRenderer(ModBlockEntityTypeObjects.BLOOD_BASIN, BloodBasinRenderer::new);
        event.registerBlockEntityRenderer(ModBlockEntityTypeObjects.OFFERING_BOWL, OfferingBowlRenderer::new);
    }
}
