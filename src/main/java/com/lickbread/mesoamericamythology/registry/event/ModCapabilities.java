package com.lickbread.mesoamericamythology.registry.event;

import com.lickbread.mesoamericamythology.MesoamericaMythologyMod;
import com.lickbread.mesoamericamythology.statueHead.relationship.DivineFavourCapability;
import net.minecraftforge.common.capabilities.RegisterCapabilitiesEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;

@Mod.EventBusSubscriber(modid = MesoamericaMythologyMod.MOD_ID)
public class ModCapabilities {

    @SubscribeEvent
    public static void listen(RegisterCapabilitiesEvent event) {
        event.register(DivineFavourCapability.class);
    }
}
