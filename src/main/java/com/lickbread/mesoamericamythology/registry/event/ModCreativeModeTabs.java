package com.lickbread.mesoamericamythology.registry.event;

import com.lickbread.mesoamericamythology.MesoamericaMythologyMod;
import com.lickbread.mesoamericamythology.registry.ModItems;
import net.minecraft.network.chat.Component;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.item.CreativeModeTab;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraftforge.event.CreativeModeTabEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.registries.RegistryObject;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Mod.EventBusSubscriber(modid = MesoamericaMythologyMod.MOD_ID)
public class ModCreativeModeTabs {

    public static List<RegistryObject<Item>> CREATIVE_MODE_TAB_ITEMS = new ArrayList<>();

    public static ResourceLocation CREATIVE_TAB_LOCATION = new ResourceLocation(MesoamericaMythologyMod.MOD_ID, MesoamericaMythologyMod.MOD_ID);
    public static CreativeModeTab CREATIVE_TAB = null;

    @SubscribeEvent
    public static void listen(CreativeModeTabEvent.Register event) {
        CREATIVE_TAB = event.registerCreativeModeTab(CREATIVE_TAB_LOCATION, builder -> {
            builder.title(Component.translatable("itemGroup.mesoamericamythology"))
                    .icon(() -> new ItemStack(ModItems.OBSIDIAN_KNIFE.get()))
                    .displayItems((params, output) -> {
                        output.acceptAll(CREATIVE_MODE_TAB_ITEMS.stream().map((registryObject -> new ItemStack(registryObject.get()))).collect(Collectors.toList()));
                    });
        });
    }
}
