package com.lickbread.mesoamericamythology.registry.event;

import com.lickbread.mesoamericamythology.MesoamericaMythologyMod;
import com.lickbread.mesoamericamythology.entity.SpanishConquistadorEntity;
import com.lickbread.mesoamericamythology.entity.SpanishConquistadorGeneralEntity;
import com.lickbread.mesoamericamythology.registry.ModEntityTypes;
import net.minecraftforge.event.entity.EntityAttributeCreationEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;

@Mod.EventBusSubscriber(modid = MesoamericaMythologyMod.MOD_ID)
public class ModEntityAttributes {

    @SubscribeEvent
    public static void listen(EntityAttributeCreationEvent event) {
        event.put(ModEntityTypes.SPANISH_CONQUISTADOR.get(), SpanishConquistadorEntity.setCustomAttributes().build());
        event.put(ModEntityTypes.SPANISH_CONQUISTADOR_GENERAL.get(), SpanishConquistadorGeneralEntity.setCustomAttributes().build());
    }
}
