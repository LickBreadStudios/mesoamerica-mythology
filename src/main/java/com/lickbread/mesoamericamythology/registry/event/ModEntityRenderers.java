package com.lickbread.mesoamericamythology.registry.event;

import com.lickbread.mesoamericamythology.MesoamericaMythologyMod;
import com.lickbread.mesoamericamythology.client.renderer.item.ThrownJavelinRenderer;
import com.lickbread.mesoamericamythology.registry.ModEntityTypes;
import net.minecraftforge.client.event.EntityRenderersEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;

@Mod.EventBusSubscriber(modid = MesoamericaMythologyMod.MOD_ID)
public class ModEntityRenderers {

    @SubscribeEvent
    public static void listen(EntityRenderersEvent.RegisterRenderers event) {
//        event.registerEntityRenderer(ModEntityTypes.SPANISH_CONQUISTADOR.get(), SpanishConquistadorRenderer::new);
//        event.registerEntityRenderer(ModEntityTypes.SPANISH_CONQUISTADOR_GENERAL.get(), SpanishConquistadorGeneralRenderer::new);
        event.registerEntityRenderer(ModEntityTypes.THROWN_JAVELIN.get(), ThrownJavelinRenderer::new);
    }
}
