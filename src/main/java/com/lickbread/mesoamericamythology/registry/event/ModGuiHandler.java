package com.lickbread.mesoamericamythology.registry.event;

import com.lickbread.mesoamericamythology.client.overlay.BloodLevelGui;
import net.minecraftforge.client.event.RenderGuiOverlayEvent;
import net.minecraftforge.client.gui.overlay.VanillaGuiOverlay;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;

@Mod.EventBusSubscriber
public class ModGuiHandler {

    @SubscribeEvent
    public static void renderGameOverlay(RenderGuiOverlayEvent.Post event) {
        if (event.getOverlay() == VanillaGuiOverlay.HOTBAR.type()) {
            new BloodLevelGui().render();
        }
    }
}
