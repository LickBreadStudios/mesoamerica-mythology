package com.lickbread.mesoamericamythology.registry.event;

import com.lickbread.mesoamericamythology.MesoamericaMythologyMod;
import net.minecraft.client.KeyMapping;
import net.minecraftforge.client.event.RegisterKeyMappingsEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;

@Mod.EventBusSubscriber(modid = MesoamericaMythologyMod.MOD_ID)
public class ModKeyBindings {

    private static String KEY_CATEGORY = "key.categories.mesoamericamythology";

    public static KeyMapping OPEN_DIVINE_FAVOUR_RELATIONSHIP =new KeyMapping("key.mesoamericamythology.open_divine_favour_screen", 75 /*K*/, KEY_CATEGORY);

    @SubscribeEvent
    public static void listen(RegisterKeyMappingsEvent event) {
        event.register(OPEN_DIVINE_FAVOUR_RELATIONSHIP);
    }
}
