package com.lickbread.mesoamericamythology.registry.event;

import com.lickbread.mesoamericamythology.MesoamericaMythologyMod;
import com.lickbread.mesoamericamythology.client.renderer.model.ChimalliModel;
import com.lickbread.mesoamericamythology.client.renderer.model.FeatherShieldModel;
import com.lickbread.mesoamericamythology.client.renderer.model.JavelinModel;
import net.minecraftforge.client.event.EntityRenderersEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;

import static com.lickbread.mesoamericamythology.registry.ModModelLayerLocations.*;

@Mod.EventBusSubscriber(modid = MesoamericaMythologyMod.MOD_ID)
public class ModModelLayers {

    @SubscribeEvent
    public static void listen(EntityRenderersEvent.RegisterLayerDefinitions event) {
        event.registerLayerDefinition(CHIMALLI_LAYER, ChimalliModel::createLayerDefinition);
        event.registerLayerDefinition(FEATHER_SHIELD_LAYER, FeatherShieldModel::createLayerDefinition);
        event.registerLayerDefinition(THROWN_JAVELIN_LAYER, JavelinModel::createLayerDefinition);
    }
}

