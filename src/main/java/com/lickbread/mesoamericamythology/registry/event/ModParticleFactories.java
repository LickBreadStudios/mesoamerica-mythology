package com.lickbread.mesoamericamythology.registry.event;

import com.lickbread.mesoamericamythology.MesoamericaMythologyMod;
import com.lickbread.mesoamericamythology.particle.*;
import com.lickbread.mesoamericamythology.registry.ModParticleTypes;
import net.minecraft.client.Minecraft;
import net.minecraftforge.client.event.RegisterParticleProvidersEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;

/**
 * For some reason this has to be registered in the Mod class
 */
@Mod.EventBusSubscriber(modid = MesoamericaMythologyMod.MOD_ID)
public class ModParticleFactories {

    @SubscribeEvent
    public static void listen(RegisterParticleProvidersEvent event) {
        // Don't change to lambda reference as it causes infinite loop
        Minecraft.getInstance().particleEngine.register(ModParticleTypes.BLOOD_DRIP_PARTICLE.get(), sprite -> new BloodDripParticleFactory(sprite));
        Minecraft.getInstance().particleEngine.register(ModParticleTypes.TOTEM_EFFECT_PARTICLE.get(), sprite -> new TotemEffectParticleFactory(sprite));
        Minecraft.getInstance().particleEngine.register(ModParticleTypes.ALTAR_BREAK_ITEM_PARTICLE.get(), new AltarBreakItemParticleFactory());
        Minecraft.getInstance().particleEngine.register(ModParticleTypes.DEATH_BLOOD_PARTICLE.get(), sprite -> new DeathBloodParticleFactory(sprite));
        Minecraft.getInstance().particleEngine.register(ModParticleTypes.INFUSION_PEDESTAL_ACTIVATE_PARTICLE.get(), sprite -> new InfusionPedestalActivateParticleFactory(sprite));
        Minecraft.getInstance().particleEngine.register(ModParticleTypes.INFUSION_PEDESTAL_CRAFT_PARTICLE.get(), new InfusionPedestalCraftParticleFactory());
        Minecraft.getInstance().particleEngine.register(ModParticleTypes.INFUSION_PEDESTAL_RUNES_PARTICLE.get(), sprite -> new InfusionPedestalRunesParticleFactory(sprite));
    }
}
