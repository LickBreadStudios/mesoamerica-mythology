package com.lickbread.mesoamericamythology.statueHead;

import com.google.gson.annotations.SerializedName;
import lombok.AllArgsConstructor;

@AllArgsConstructor
public enum God {
    @SerializedName("huitzilopochtli")
    HUITZILOPOCHTLI("huitzilopochtli"),
    @SerializedName("xolotl")
    XOLOTL("xolotl");

    private final String name;

    @Override
    public String toString() {
        return this.name;
    }

    public static God getEnum(String string) {
        for (God god : God.values()) {
            if (god.toString().equals(string)) {
                return god;
            }
        }
        throw new IllegalArgumentException("Enum value does not exist: " + string);
    }
}
