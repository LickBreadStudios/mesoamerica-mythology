package com.lickbread.mesoamericamythology.statueHead;

import com.lickbread.mesoamericamythology.statueHead.relationship.GodStatueHeadQuest;
import com.lickbread.mesoamericamythology.statueHead.quest.GodStatueHeadQuestBuilder;
import com.lickbread.mesoamericamythology.statueHead.relationship.DivineFavourRelationship;
import com.lickbread.mesoamericamythology.util.WeightedList;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
public class GodStatueHead {

    @Getter
    private Interactions interactions;
    @Getter
    @Setter
    private God god;
    private WeightedList<GodStatueHeadQuestBuilder> questBuilders;

    public GodStatueHeadQuest getNewQuest(DivineFavourRelationship relationship) {
        GodStatueHeadQuestBuilder newQuest = questBuilders.next();
        while (!newQuest.canObtainQuest(relationship.getDivineFavour())) {
            newQuest = questBuilders.next();
        }
        return newQuest.buildQuest(god);
    }
}
