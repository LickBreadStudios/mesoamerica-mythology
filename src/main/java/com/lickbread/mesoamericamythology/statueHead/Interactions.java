package com.lickbread.mesoamericamythology.statueHead;

import com.lickbread.mesoamericamythology.util.RandomList;
import com.lickbread.mesoamericamythology.util.SpeechFragment;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
public class Interactions {
    private RandomList<List<SpeechFragment>> firstMetSpeeches;
    private RandomList<List<SpeechFragment>> wakeUpSpeeches;
    private RandomList<List<SpeechFragment>> noQuestAvailableSpeeches;

    public List<SpeechFragment> getFirstMetSpeech() {
        return this.firstMetSpeeches.next();
    }

    public List<SpeechFragment> getWakeUpSpeech() {
        return this.wakeUpSpeeches.next();
    }

    public List<SpeechFragment> getNoQuestAvailableSpeech() {
        return this.noQuestAvailableSpeeches.next();
    }
}
