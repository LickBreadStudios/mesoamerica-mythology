package com.lickbread.mesoamericamythology.statueHead.event;

import lombok.AllArgsConstructor;
import lombok.Getter;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.damagesource.DamageSource;

@Getter
@AllArgsConstructor
public class PlayerDeathEvent implements IGodStatueHeadQuestEvent {
    private final Player playerEntity;
    private final DamageSource damageSource;
}
