package com.lickbread.mesoamericamythology.statueHead.event;

import com.lickbread.mesoamericamythology.blockEntities.AbstractBloodBasinBlockEntity;
import lombok.AllArgsConstructor;
import lombok.Getter;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.entity.player.Player;

import java.util.Optional;

/**
 * Event fired when a player kills a living entity
 */
@Getter
@AllArgsConstructor
public class PlayerKillEvent implements IGodStatueHeadQuestEvent {
    private final Player playerEntity;
    private final LivingEntity livingEntity;
    private final Optional<AbstractBloodBasinBlockEntity> bloodBasinBlockEntity;
}
