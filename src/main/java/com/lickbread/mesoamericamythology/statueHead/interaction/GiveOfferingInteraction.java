package com.lickbread.mesoamericamythology.statueHead.interaction;

import com.lickbread.mesoamericamythology.blocks.state.GodStatueHeadState;
import com.lickbread.mesoamericamythology.statueHead.God;
import com.lickbread.mesoamericamythology.blockEntities.GodStatueHeadBlockEntity;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.ItemStack;

public class GiveOfferingInteraction extends GodStatueHeadInteraction {

    private static final int PLEASED_TIME_MILLIS = 1000;

    private final ItemStack itemStack;

    public GiveOfferingInteraction(GodStatueHeadBlockEntity godStatueHeadBlockEntity, God god, Player playerEntity, ItemStack itemStack) {
        super(godStatueHeadBlockEntity, god, playerEntity);
        this.itemStack = itemStack;
    }

    @Override
    protected GodStatueHeadInteractionType getType() {
        return GodStatueHeadInteractionType.GIVE_OFFERING;
    }

    @Override
    protected void performInteraction() {
        this.setGodStatueHeadState(GodStatueHeadState.PLEASED);
        try {
            Thread.sleep(PLEASED_TIME_MILLIS);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
