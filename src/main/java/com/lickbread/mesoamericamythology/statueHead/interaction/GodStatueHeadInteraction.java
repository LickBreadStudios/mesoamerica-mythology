package com.lickbread.mesoamericamythology.statueHead.interaction;

import com.lickbread.mesoamericamythology.blockEntities.GodStatueHeadBlockEntity;
import com.lickbread.mesoamericamythology.blocks.GodStatueHeadBlock;
import com.lickbread.mesoamericamythology.blocks.state.GodStatueHeadState;
import com.lickbread.mesoamericamythology.capabilities.DivineFavourCapabilityProvider;
import com.lickbread.mesoamericamythology.statueHead.God;
import com.lickbread.mesoamericamythology.statueHead.relationship.DivineFavourRelationship;
import com.lickbread.mesoamericamythology.statueHead.relationship.IDivineFavourCapability;
import com.lickbread.mesoamericamythology.util.SpeechFragment;
import net.minecraft.network.chat.Component;
import net.minecraft.world.entity.player.Player;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public abstract class GodStatueHeadInteraction {

    private static final int TALK_STATE_CHANGE_MILLIS = 100;

    protected GodStatueHeadBlockEntity godStatueHeadBlockEntity;
    protected God god;
    protected Player playerEntity;

    protected GodStatueHeadInteraction(GodStatueHeadBlockEntity godStatueHeadBlockEntity, God god, Player playerEntity) {
        this.godStatueHeadBlockEntity = godStatueHeadBlockEntity;
        this.god = god;
        this.playerEntity = playerEntity;
    }

    public final void interact() {
        final GodStatueHeadInteraction godStatueHeadInteraction = this;
        new Thread(() -> {
            godStatueHeadInteraction.performInteraction();
            godStatueHeadInteraction.completeInteraction();
        }).start();
    }

    private void completeInteraction() {
        this.godStatueHeadBlockEntity.finishInteraction();
    }

    protected IDivineFavourCapability getCapability() {
        return playerEntity.getCapability(DivineFavourCapabilityProvider.DIVINE_FAVOUR_CAPABILITY).orElse(null);
    }

    protected DivineFavourRelationship getRelationship() {
        return this.getRelationship(this.god);
    }

    protected DivineFavourRelationship getRelationship(God god) {
        return this.getCapability().getOrCreateRelationship(god);
    }

    protected void talk(List<SpeechFragment> speechFragments) {
        for (SpeechFragment speechFragment : speechFragments) {
            this.playerEntity.sendSystemMessage(Component.translatable(speechFragment.getTranslatedText()));
            try {
                int currentMillis = 0;
                int maxMillis = speechFragment.getDurationInMillis();
                while (currentMillis < maxMillis) {
                    this.setGodStatueHeadState(this.getTalkState());
                    int durationToSleep = Math.max(maxMillis - currentMillis, TALK_STATE_CHANGE_MILLIS);
                    currentMillis += durationToSleep;
                    Thread.sleep(durationToSleep);
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    protected final void setGodStatueHeadState(GodStatueHeadState state) {
        this.godStatueHeadBlockEntity.setGodStatueHeadState(state);
    }

    private GodStatueHeadState getTalkState() {
        List<GodStatueHeadState> godStatueHeadStates = new ArrayList<>();
        godStatueHeadStates.add(GodStatueHeadState.TALKING_0);
        godStatueHeadStates.add(GodStatueHeadState.TALKING_1);
        godStatueHeadStates.add(GodStatueHeadState.TALKING_2);
        godStatueHeadStates.remove(this.godStatueHeadBlockEntity.getBlockState().getValue(GodStatueHeadBlock.HEAD_STATE));
        return godStatueHeadStates.get(new Random().nextInt(godStatueHeadStates.size()));
    }

    protected abstract GodStatueHeadInteractionType getType();
    protected abstract void performInteraction();

}
