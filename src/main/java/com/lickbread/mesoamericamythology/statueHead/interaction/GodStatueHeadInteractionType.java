package com.lickbread.mesoamericamythology.statueHead.interaction;

public enum GodStatueHeadInteractionType {
    TALK,
    GIVE_OFFERING
}
