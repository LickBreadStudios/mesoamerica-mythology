package com.lickbread.mesoamericamythology.statueHead.interaction;

import com.lickbread.mesoamericamythology.api.MesoamericaMythologyAPI;
import com.lickbread.mesoamericamythology.blockEntities.GodStatueHeadBlockEntity;
import com.lickbread.mesoamericamythology.statueHead.God;
import com.lickbread.mesoamericamythology.statueHead.relationship.DivineFavourRelationship;
import com.lickbread.mesoamericamythology.statueHead.relationship.GodStatueHeadQuest;
import net.minecraft.server.level.ServerPlayer;
import net.minecraft.world.entity.player.Player;

public class TalkInteraction extends GodStatueHeadInteraction {

    private final boolean isNewStatueHead;

    public TalkInteraction(GodStatueHeadBlockEntity godStatueHeadBlockEntity, God god, Player playerEntity, boolean isNewStatueHead) {
        super(godStatueHeadBlockEntity, god, playerEntity);
        this.isNewStatueHead = isNewStatueHead;
    }

    @Override
    protected GodStatueHeadInteractionType getType() {
        return GodStatueHeadInteractionType.TALK;
    }

    @Override
    protected void performInteraction() {
        DivineFavourRelationship relationship = this.getRelationship();

        if (this.isNewStatueHead) {
            this.performWakeupGreeting();
        }

        if (!relationship.hasMet()) {
            this.performFirstMeetGreeting();
            return;
        }

        if (!relationship.getCurrentQuest().isPresent()) {
            this.performGetQuest();
            return;
        }

        if (!relationship.getCurrentQuest().get().isComplete()) {
            this.performUpdateCurrentQuest();
            return;
        }

        this.performCompleteQuest();
    }

    private void performCompleteQuest() {
        this.talk(this.getRelationship().getCurrentQuest().get().getCompleteQuestSpeech());
        this.getCapability().completeQuest(this.god, this.godStatueHeadBlockEntity, (ServerPlayer) this.playerEntity);
    }

    private void performUpdateCurrentQuest() {
        this.talk(this.getRelationship().getCurrentQuest().get().getCheckCurrentQuestProgressSpeech());
    }

    private void performGetQuest() {
        GodStatueHeadQuest newQuest = MesoamericaMythologyAPI.getGodStatueHead(this.god).getNewQuest(this.getRelationship());
        this.talk(newQuest.getStartQuestSpeech());
        this.getCapability().getOrCreateRelationship(this.god).setCurrentQuest(newQuest);
    }

    private void performWakeupGreeting() {
        this.talk(MesoamericaMythologyAPI.getGodStatueHead(this.god).getInteractions().getWakeUpSpeech());

    }

    private void performFirstMeetGreeting() {
        this.talk(MesoamericaMythologyAPI.getGodStatueHead(this.god).getInteractions().getFirstMetSpeech());
        this.getCapability().meet(this.god, (ServerPlayer) this.playerEntity);
    }
}
