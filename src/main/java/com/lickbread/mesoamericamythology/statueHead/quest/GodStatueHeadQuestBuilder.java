package com.lickbread.mesoamericamythology.statueHead.quest;

import com.lickbread.mesoamericamythology.statueHead.God;
import com.lickbread.mesoamericamythology.statueHead.quest.requirement.QuestRequirement;
import com.lickbread.mesoamericamythology.statueHead.quest.reward.QuestReward;
import com.lickbread.mesoamericamythology.statueHead.relationship.GodStatueHeadQuest;
import com.lickbread.mesoamericamythology.util.RandomList;
import com.lickbread.mesoamericamythology.util.SpeechFragment;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.stream.Collectors;

@AllArgsConstructor
@NoArgsConstructor
public class GodStatueHeadQuestBuilder {

    private int minDivineFavour;
    private int maxDivineFavour;

    private List<QuestRequirement> requirements;
    private List<QuestReward> rewards;
    private RandomList<List<SpeechFragment>> startQuestSpeeches;
    private RandomList<List<SpeechFragment>> checkCurrentQuestProgressSpeeches;
    private RandomList<List<SpeechFragment>> completeQuestSpeeches;

    public boolean canObtainQuest(int divineFavour) {
        return this.minDivineFavour <= divineFavour && divineFavour <= this.maxDivineFavour;
    }

    public GodStatueHeadQuest buildQuest(God god) {
        List<SpeechFragment> startQuestSpeech = this.startQuestSpeeches.next();
        List<SpeechFragment> checkCurrentQuestProgressSpeech = this.checkCurrentQuestProgressSpeeches.next();
        List<SpeechFragment> completeQuestSpeech = this.completeQuestSpeeches.next();

        List<QuestRequirement> requirements = this.requirements.stream().map(QuestRequirement::clone).collect(Collectors.toList());
        List<QuestReward> rewards = this.rewards.stream().map(QuestReward::clone).collect(Collectors.toList());

        return GodStatueHeadQuest.builder()
                .startQuestSpeech(startQuestSpeech)
                .checkCurrentQuestProgressSpeech(checkCurrentQuestProgressSpeech)
                .completeQuestSpeech(completeQuestSpeech)
                .requirements(requirements)
                .rewards(rewards)
                .god(god)
                .build();
    }
}
