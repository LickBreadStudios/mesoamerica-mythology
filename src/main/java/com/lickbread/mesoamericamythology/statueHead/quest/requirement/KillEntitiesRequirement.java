package com.lickbread.mesoamericamythology.statueHead.quest.requirement;

import com.lickbread.mesoamericamythology.statueHead.God;
import com.lickbread.mesoamericamythology.statueHead.event.IGodStatueHeadQuestEvent;
import com.lickbread.mesoamericamythology.statueHead.event.PlayerKillEvent;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.ToString;
import net.minecraft.network.chat.Component;
import net.minecraft.resources.ResourceLocation;
import net.minecraftforge.registries.ForgeRegistries;

@NoArgsConstructor
@AllArgsConstructor
@ToString
public class KillEntitiesRequirement extends QuestRequirement {

    private String entityType;
    private int requiredAmount;
    private int currentAmount = 0;

    @Override
    public boolean isComplete() {
        return this.currentAmount >= this.requiredAmount;
    }

    @Override
    public void processEvent(IGodStatueHeadQuestEvent event, God god) {
        if (!(event instanceof PlayerKillEvent)) {
            return;
        }

        PlayerKillEvent playerKillEvent = (PlayerKillEvent) event;
        if (this.entityType.equals(ForgeRegistries.ENTITY_TYPES.getKey(playerKillEvent.getLivingEntity().getType()).toString())) {
            this.currentAmount++;
        }
    }

    @Override
    public Component displayText() {
        return Component.translatable("quests.mesoamericamythology.requirement.kill_entities.display_text", ForgeRegistries.ENTITY_TYPES.getValue(new ResourceLocation(entityType)).toString(), currentAmount, requiredAmount);
    }

    @Override
    public QuestRequirement clone() {
        return new KillEntitiesRequirement(this.entityType, this.requiredAmount, this.currentAmount);
    }
}
