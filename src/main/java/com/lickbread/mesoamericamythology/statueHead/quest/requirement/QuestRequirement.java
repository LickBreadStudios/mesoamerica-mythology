package com.lickbread.mesoamericamythology.statueHead.quest.requirement;

import com.lickbread.mesoamericamythology.statueHead.God;
import com.lickbread.mesoamericamythology.statueHead.event.IGodStatueHeadQuestEvent;
import net.minecraft.network.chat.Component;

public abstract class QuestRequirement {

    public abstract boolean isComplete();

    public abstract void processEvent(IGodStatueHeadQuestEvent event, God god);

    public abstract Component displayText();

    @Override
    public abstract QuestRequirement clone();
}
