package com.lickbread.mesoamericamythology.statueHead.quest.requirement;

import com.lickbread.mesoamericamythology.blockEntities.AbstractBloodBasinBlockEntity;
import com.lickbread.mesoamericamythology.blockEntities.GodStatueHeadBlockEntity;
import com.lickbread.mesoamericamythology.statueHead.God;
import com.lickbread.mesoamericamythology.statueHead.event.IGodStatueHeadQuestEvent;
import com.lickbread.mesoamericamythology.statueHead.event.PlayerKillEvent;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.ToString;
import net.minecraft.network.chat.Component;
import net.minecraft.resources.ResourceLocation;
import net.minecraftforge.registries.ForgeRegistries;

@NoArgsConstructor
@AllArgsConstructor
@ToString
public class SacrificeEntitiesRequirement extends QuestRequirement {

    private String entityType;
    private int requiredAmount;
    private int currentAmount = 0;

    @Override
    public boolean isComplete() {
        return this.currentAmount >= this.requiredAmount;
    }

    @Override
    public void processEvent(IGodStatueHeadQuestEvent event, God god) {
        if (!(event instanceof PlayerKillEvent)) {
            return;
        }

        PlayerKillEvent playerKillEvent = (PlayerKillEvent) event;

        if (!this.entityType.equals(ForgeRegistries.ENTITY_TYPES.getKey(playerKillEvent.getLivingEntity().getType()).toString())) {
            return;
        }

        if (!playerKillEvent.getBloodBasinBlockEntity().isPresent()) {
            return;
        }

        AbstractBloodBasinBlockEntity bloodBasinBlockEntity = playerKillEvent.getBloodBasinBlockEntity().get();
        if (!bloodBasinBlockEntity.getAttachedGodStatueHead().isPresent()) {
            return;
        }

        GodStatueHeadBlockEntity godStatueHeadBlockEntity = (GodStatueHeadBlockEntity) bloodBasinBlockEntity.getLevel().getBlockEntity(bloodBasinBlockEntity.getAttachedGodStatueHead().get());
        if (godStatueHeadBlockEntity.getGod() == god) {
            this.currentAmount++;
        }
    }

    @Override
    public Component displayText() {
        return Component.translatable("quests.mesoamericamythology.requirement.sacrifice_entities.display_text", ForgeRegistries.ENTITY_TYPES.getValue(new ResourceLocation(entityType)).toString(), currentAmount, requiredAmount);
    }

    @Override
    public QuestRequirement clone() {
        return new SacrificeEntitiesRequirement(this.entityType, this.requiredAmount, this.currentAmount);
    }
}
