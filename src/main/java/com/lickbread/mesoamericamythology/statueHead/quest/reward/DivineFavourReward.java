package com.lickbread.mesoamericamythology.statueHead.quest.reward;

import com.lickbread.mesoamericamythology.blockEntities.GodStatueHeadBlockEntity;
import com.lickbread.mesoamericamythology.capabilities.DivineFavourCapabilityProvider;
import com.lickbread.mesoamericamythology.statueHead.relationship.IDivineFavourCapability;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.ToString;
import net.minecraft.client.resources.language.I18n;
import net.minecraft.network.chat.Component;
import net.minecraft.server.level.ServerPlayer;
import net.minecraft.world.entity.player.Player;

@AllArgsConstructor
@NoArgsConstructor
@ToString
public class DivineFavourReward extends QuestReward {

    private int amount;

    @Override
    public void getReward(GodStatueHeadBlockEntity blockEntity, Player playerEntity) {
        System.out.println("OBTAINING REWARD. Is Remote:" + blockEntity.getLevel().isClientSide());

        IDivineFavourCapability divineFavourCapability = playerEntity.getCapability(DivineFavourCapabilityProvider.DIVINE_FAVOUR_CAPABILITY).orElse(null);
        divineFavourCapability.addDivineFavour(blockEntity.getGod(), this.amount, (ServerPlayer) playerEntity);
            playerEntity.sendSystemMessage(Component.translatable(I18n.get("mesoamericamythology.talk.divineFavourIncrease",
                    blockEntity.getGod().toString(), this.amount, divineFavourCapability.getOrCreateRelationship(blockEntity.getGod()).getDivineFavour())));
    }

    @Override
    public QuestReward clone() {
        return new DivineFavourReward(this.amount);
    }
}
