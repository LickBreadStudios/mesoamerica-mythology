package com.lickbread.mesoamericamythology.statueHead.quest.reward;

import com.lickbread.mesoamericamythology.blocks.GodStatueHeadBlock;
import com.lickbread.mesoamericamythology.blockEntities.GodStatueHeadBlockEntity;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.ToString;
import net.minecraft.world.entity.ExperienceOrb;
import net.minecraft.world.entity.player.Player;
import net.minecraft.core.BlockPos;
import net.minecraft.core.Vec3i;
import net.minecraft.world.level.Level;

@AllArgsConstructor
@NoArgsConstructor
@ToString
public class ExperienceReward extends QuestReward {

    private static final double V_MOD = 0.5D;

    private int amount;

    @Override
    public void getReward(GodStatueHeadBlockEntity blockEntity, Player playerEntity) {
        Level level = blockEntity.getLevel();
        BlockPos pos = blockEntity.getBlockPos();
        Vec3i direction = level.getBlockState(pos).getValue(GodStatueHeadBlock.FACING).getNormal();
        ExperienceOrb entity = new ExperienceOrb(level,
                pos.getX() + direction.getX(),
                pos.getY() + direction.getY(),
                pos.getZ() + direction.getZ(),
                amount);
        entity.setDeltaMovement(direction.getX() * V_MOD, direction.getY() * V_MOD, direction.getZ() * V_MOD);
        level.addFreshEntity(entity);
    }

    @Override
    public QuestReward clone() {
        return new ExperienceReward(this.amount);
    }
}
