package com.lickbread.mesoamericamythology.statueHead.quest.reward;

import com.lickbread.mesoamericamythology.blocks.GodStatueHeadBlock;
import com.lickbread.mesoamericamythology.blocks.state.GodStatueHeadState;
import com.lickbread.mesoamericamythology.blockEntities.GodStatueHeadBlockEntity;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.ToString;
import net.minecraft.world.entity.item.ItemEntity;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.ItemStack;
import net.minecraft.core.BlockPos;
import net.minecraft.core.Vec3i;
import net.minecraft.world.level.Level;

@NoArgsConstructor
@AllArgsConstructor
@ToString
public class ItemReward extends QuestReward {

    private static final int SPITTING_TIME_MILLIS = 500;

    private static final double V_MOD = 0.5D;
    private static final int DELAY = 10;

    private ItemStack reward;

    @Override
    public void getReward(GodStatueHeadBlockEntity blockEntity, Player playerEntity) {
        Level level = blockEntity.getLevel();
        BlockPos pos = blockEntity.getBlockPos();
        Vec3i direction = blockEntity.getBlockState().getValue(GodStatueHeadBlock.FACING).getNormal();
        ItemEntity entity = new ItemEntity(level,
                pos.getX() + direction.getX(),
                pos.getY() + direction.getY(),
                pos.getZ() + direction.getZ(),
                reward);
        entity.setDeltaMovement(direction.getX() * V_MOD, direction.getY() * V_MOD, direction.getZ() * V_MOD);
        entity.setPickUpDelay(DELAY);
        level.addFreshEntity(entity);
        blockEntity.setGodStatueHeadState(GodStatueHeadState.SPITTING);
        try {
            Thread.sleep(SPITTING_TIME_MILLIS);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Override
    public QuestReward clone() {
        return new ItemReward(this.reward);
    }
}
