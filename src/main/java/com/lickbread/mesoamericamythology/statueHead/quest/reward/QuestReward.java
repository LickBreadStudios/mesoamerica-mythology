package com.lickbread.mesoamericamythology.statueHead.quest.reward;

import com.lickbread.mesoamericamythology.blockEntities.GodStatueHeadBlockEntity;
import net.minecraft.world.entity.player.Player;

public abstract class QuestReward {

    public abstract void getReward(GodStatueHeadBlockEntity blockEntity, Player playerEntity);

    @Override
    public abstract QuestReward clone();
}
