package com.lickbread.mesoamericamythology.statueHead.relationship;

import com.lickbread.mesoamericamythology.blockEntities.GodStatueHeadBlockEntity;
import com.lickbread.mesoamericamythology.network.DivineFavourCapabilityMessageToClient;
import com.lickbread.mesoamericamythology.perks.Perk;
import com.lickbread.mesoamericamythology.statueHead.God;
import com.lickbread.mesoamericamythology.statueHead.event.IGodStatueHeadQuestEvent;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.ToString;
import net.minecraft.server.level.ServerPlayer;
import net.minecraftforge.network.PacketDistributor;

import java.util.*;

import static com.lickbread.mesoamericamythology.network.ModChannels.MAIN_PACKET_CHANNEL_INSTANCE;

@AllArgsConstructor
@NoArgsConstructor
@ToString
public class DivineFavourCapability implements IDivineFavourCapability {

    private Map<String, Perk> perks = new HashMap<>();
    private Map<God, DivineFavourRelationship> relationships = new HashMap<>();

    @Override
    public Map<God, DivineFavourRelationship> getRelationshipMap() {
        return this.relationships;
    }

    @Override
    public List<DivineFavourRelationship> getRelationships() {
        return new ArrayList<>(this.relationships.values());
    }

    @Override
    public Optional<DivineFavourRelationship> getRelationship(God god) {
        return Optional.ofNullable(relationships.get(god));
    }

    @Override
    public DivineFavourRelationship getOrCreateRelationship(God god) {
        if (!this.getRelationship(god).isPresent()) {
            this.relationships.put(god, new DivineFavourRelationship());
        }
        return this.getRelationship(god).orElseThrow(() -> new IllegalStateException("The relationship should exist at this point"));
    }

    @Override
    public Optional<Perk> getPerk(String id) {
        return Optional.ofNullable(this.perks.get(id));
    }

    @Override
    public boolean hasPerk(String id) {
        return this.getPerk(id).isPresent();
    }

    @Override
    public Map<String, Perk> getPerks() {
        return this.perks;
    }

    @Override
    public void addPerk(Perk perk, ServerPlayer playerEntity) {
        this.perks.put(perk.getId(), perk);
        this.sync(playerEntity);
    }

    @Override
    public void removePerk(Perk perk, ServerPlayer playerEntity) {
        this.perks.remove(perk.getId());
        this.sync(playerEntity);
    }

    @Override
    public void clearPerks(ServerPlayer playerEntity) {
        this.perks = new HashMap<>();
        this.sync(playerEntity);
    }

    @Override
    public void clearCurrentQuest(God god, ServerPlayer playerEntity) {
        this.getOrCreateRelationship(god).clearCurrentQuest();
        this.sync(playerEntity);
    }

    @Override
    public void processEvent(IGodStatueHeadQuestEvent event, ServerPlayer serverPlayerEntity) {
        List<DivineFavourRelationship> relationships = this.getRelationships();
        for (DivineFavourRelationship relationship : relationships) {
            relationship.processEvent(event);
        }
        this.sync(serverPlayerEntity);
    }

    @Override
    public void completeQuest(God god, GodStatueHeadBlockEntity godStatueHeadBlockEntity, ServerPlayer serverPlayerEntity) {
        this.getOrCreateRelationship(god).getCurrentQuest().ifPresent(quest -> quest.completeQuest(godStatueHeadBlockEntity, serverPlayerEntity));
        this.getOrCreateRelationship(god).clearCurrentQuest();
        this.sync(serverPlayerEntity);
    }

    @Override
    public void meet(God god, ServerPlayer serverPlayerEntity) {
        this.getOrCreateRelationship(god).meet();
        this.sync(serverPlayerEntity);
    }

    @Override
    public void addDivineFavour(God god, int amount, ServerPlayer serverPlayerEntity) {
        this.getOrCreateRelationship(god).addDivineFavour(amount);
        this.sync(serverPlayerEntity);
    }

    @Override
    public void copyFrom(IDivineFavourCapability divineFavourCapability) {
        if (divineFavourCapability == null || divineFavourCapability.getRelationshipMap() == null || divineFavourCapability.getPerks() == null) {
            this.relationships = new HashMap<>();
            this.perks = new HashMap<>();
            return;
        }

        this.relationships = divineFavourCapability.getRelationshipMap();
        this.perks = divineFavourCapability.getPerks();
    }

    @Override
    public void sync(ServerPlayer playerEntity) {
        MAIN_PACKET_CHANNEL_INSTANCE.send(PacketDistributor.PLAYER.with(() -> playerEntity), new DivineFavourCapabilityMessageToClient(this, playerEntity.getUUID()));
    }
}
