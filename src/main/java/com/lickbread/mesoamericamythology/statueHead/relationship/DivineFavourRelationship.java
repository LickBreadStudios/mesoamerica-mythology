package com.lickbread.mesoamericamythology.statueHead.relationship;

import com.lickbread.mesoamericamythology.MesoamericaMythologyMod;
import com.lickbread.mesoamericamythology.statueHead.event.IGodStatueHeadQuestEvent;
import lombok.*;

import java.util.Optional;

@NoArgsConstructor
@AllArgsConstructor
@ToString
public class DivineFavourRelationship {

    @Getter
    private int divineFavour = 0;
    @Setter
    private GodStatueHeadQuest currentQuest = null;
    private boolean hasMet = false;

    void clearCurrentQuest() {
        this.currentQuest = null;
    }

    public Optional<GodStatueHeadQuest> getCurrentQuest() {
        return Optional.ofNullable(this.currentQuest);
    }

    void addDivineFavour(int divineFavour) {
        this.divineFavour += divineFavour;
    }

    void processEvent(IGodStatueHeadQuestEvent event) {
        MesoamericaMythologyMod.LOGGER.debug("Processing event: " + event.getClass().toString());
        this.getCurrentQuest().ifPresent(quest -> quest.processEvent(event));
    }

    void meet() {
        this.hasMet = true;
    }

    public boolean hasMet() {
        return this.hasMet;
    }
}
