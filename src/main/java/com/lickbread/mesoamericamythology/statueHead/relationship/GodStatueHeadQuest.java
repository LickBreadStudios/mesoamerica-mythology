package com.lickbread.mesoamericamythology.statueHead.relationship;

import com.lickbread.mesoamericamythology.statueHead.God;
import com.lickbread.mesoamericamythology.statueHead.event.IGodStatueHeadQuestEvent;
import com.lickbread.mesoamericamythology.statueHead.quest.requirement.QuestRequirement;
import com.lickbread.mesoamericamythology.statueHead.quest.reward.QuestReward;
import com.lickbread.mesoamericamythology.blockEntities.GodStatueHeadBlockEntity;
import com.lickbread.mesoamericamythology.util.SpeechFragment;
import lombok.*;
import net.minecraft.world.entity.player.Player;

import java.util.List;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@ToString
public class GodStatueHeadQuest {

    private List<QuestRequirement> requirements;
    private List<QuestReward> rewards;
    private List<SpeechFragment> startQuestSpeech;
    private List<SpeechFragment> checkCurrentQuestProgressSpeech;
    private List<SpeechFragment> completeQuestSpeech;
    private God god;

    public boolean isComplete() {
        return this.requirements.stream().allMatch(QuestRequirement::isComplete);
    }

    void processEvent(IGodStatueHeadQuestEvent event) {
        this.requirements.forEach(requirement -> requirement.processEvent(event, god));
    }

    void completeQuest(GodStatueHeadBlockEntity godStatueHeadBlockEntity, Player playerEntity) {
        this.rewards.forEach(reward -> reward.getReward(godStatueHeadBlockEntity, playerEntity));
    }
}
