package com.lickbread.mesoamericamythology.statueHead.relationship;

import com.lickbread.mesoamericamythology.perks.Perk;
import com.lickbread.mesoamericamythology.statueHead.God;
import com.lickbread.mesoamericamythology.statueHead.event.IGodStatueHeadQuestEvent;
import com.lickbread.mesoamericamythology.blockEntities.GodStatueHeadBlockEntity;
import net.minecraft.server.level.ServerPlayer;

import java.util.List;
import java.util.Map;
import java.util.Optional;

public interface IDivineFavourCapability {

    Map<God, DivineFavourRelationship> getRelationshipMap();

    List<DivineFavourRelationship> getRelationships();

    Optional<DivineFavourRelationship> getRelationship(God god);

    DivineFavourRelationship getOrCreateRelationship(God god);

    void copyFrom(IDivineFavourCapability iDivineFavourCapability);

    Optional<Perk> getPerk(String id);

    boolean hasPerk(String id);

    Map<String, Perk> getPerks();

    void addPerk(Perk perk, ServerPlayer playerEntity);

    void removePerk(Perk perk, ServerPlayer playerEntity);

    void clearPerks(ServerPlayer playerEntity);

    void clearCurrentQuest(God god, ServerPlayer playerEntity);

    void processEvent(IGodStatueHeadQuestEvent event, ServerPlayer serverPlayerEntity);

    void completeQuest(God god, GodStatueHeadBlockEntity godStatueHeadBlockEntity, ServerPlayer serverPlayerEntity);

    void meet(God god, ServerPlayer serverPlayerEntity);

    void addDivineFavour(God god, int amount, ServerPlayer serverPlayerEntity);

    void sync(ServerPlayer playerEntity);
}
