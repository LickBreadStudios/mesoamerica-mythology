//package com.lickbread.mesoamericamythology.structure;
//
//import com.google.common.collect.ImmutableMap;
//import com.lickbread.mesoamericamythology.MesoamericaMythologyMod;
//import com.lickbread.mesoamericamythology.entity.SpanishConquistadorEntity;
//import com.lickbread.mesoamericamythology.entity.SpanishConquistadorGeneralEntity;
//import com.lickbread.mesoamericamythology.registry.ModEntityTypes;
//import com.lickbread.mesoamericamythology.registry.ModLootTables;
//import com.lickbread.mesoamericamythology.registry.ModStructurePieceTypes;
//import net.minecraft.world.level.block.Blocks;
//import net.minecraft.world.level.block.SlabBlock;
//import net.minecraft.world.entity.MobSpawnType;
//import net.minecraft.world.item.ItemStack;
//import net.minecraft.world.item.Items;
//import net.minecraft.nbt.CompoundTag;
//import net.minecraft.world.level.block.state.properties.SlabType;
//import net.minecraft.world.level.block.entity.ChestBlockEntity;
//import net.minecraft.world.level.block.entity.BlockEntity;
//import net.minecraft.world.InteractionHand;
//import net.minecraft.world.level.block.Mirror;
//import net.minecraft.resources.ResourceLocation;
//import net.minecraft.world.level.block.Rotation;
//import net.minecraft.core.BlockPos;
//import net.minecraft.world.level.levelgen.structure.BoundingBox;
//import net.minecraft.world.level.ServerLevelAccessor;
//import net.minecraft.server.level.WorldGenRegion;
//import net.minecraft.world.level.levelgen.structure.StructurePiece;
//import net.minecraft.world.level.levelgen.structure.TemplateStructurePiece;
//import net.minecraft.world.level.levelgen.structure.templatesystem.BlockIgnoreProcessor;
//import net.minecraft.world.level.levelgen.structure.templatesystem.StructurePlaceSettings;
//import net.minecraft.world.level.levelgen.structure.templatesystem.StructureTemplate;
//import net.minecraft.world.level.levelgen.structure.templatesystem.StructureManager;
//
//import java.util.List;
//import java.util.Map;
//import java.util.Random;
//
//public class SpanishSettlementPieces {
//    private static final ResourceLocation SETTLEMENT_LEFT = new ResourceLocation(MesoamericaMythologyMod.MOD_ID, "spanish_settlement_1");
//    private static final ResourceLocation SETTLEMENT_RIGHT = new ResourceLocation(MesoamericaMythologyMod.MOD_ID, "spanish_settlement_2");
//    private static final Map<ResourceLocation, BlockPos> CENTRE_OFFSETS = ImmutableMap.of(SETTLEMENT_LEFT, new BlockPos(10, 8, 16), SETTLEMENT_RIGHT, new BlockPos(7, 8, 16));
//    private static final Map<ResourceLocation, BlockPos> ZERO_RELATIVE_OFFSETS = ImmutableMap.of(SETTLEMENT_LEFT, BlockPos.ZERO, SETTLEMENT_RIGHT, new BlockPos(20, 0, 0));
//
//    public static void generate(StructureManager templateManager, BlockPos blockPos, Rotation ro, List<StructurePiece> components) {
//        components.add(new SpanishSettlementPieces.Piece(templateManager, SETTLEMENT_LEFT, blockPos, ro));
//        components.add(new SpanishSettlementPieces.Piece(templateManager, SETTLEMENT_RIGHT, blockPos, ro));
//    }
//
//    public static class Piece extends TemplateStructurePiece {
//        private final ResourceLocation pieceResourceLocation;
//        private final Rotation rotation;
//
//        public Piece(StructureManager templateMgr, CompoundTag nbt) {
//            super(ModStructurePieceTypes.SPANISH_SETTLEMENT_PIECE, nbt);
//            this.pieceResourceLocation = new ResourceLocation(nbt.getString("Template"));
//            this.rotation = Rotation.valueOf(nbt.getString("Rot"));
//            this.setupTemplate(templateMgr);
//        }
//
//        public Piece(StructureManager templateManager, ResourceLocation pieceResourceLocation, BlockPos blockPos, Rotation rotation) {
//            super(ModStructurePieceTypes.SPANISH_SETTLEMENT_PIECE, 0);
//            this.pieceResourceLocation = pieceResourceLocation;
//            this.templatePosition = blockPos.offset(SpanishSettlementPieces.ZERO_RELATIVE_OFFSETS.get(pieceResourceLocation));
//            this.rotation = rotation;
//            this.setupTemplate(templateManager);
//        }
//
//        private void setupTemplate(StructureManager templateManager) {
//            StructureTemplate template = templateManager.get(this.pieceResourceLocation);
//            StructurePlaceSettings placementsettings = (new StructurePlaceSettings())
//                    .setRotation(this.rotation)
//                    .setMirror(Mirror.NONE)
//                    .setRotationPivot(SpanishSettlementPieces.CENTRE_OFFSETS.get(this.pieceResourceLocation))
//                    .addProcessor(BlockIgnoreProcessor.STRUCTURE_BLOCK);
//            this.setup(template, this.templatePosition, placementsettings);
//        }
//
//        protected void addAdditionalSaveData(CompoundTag tagCompound) {
//            super.addAdditionalSaveData(tagCompound);
//            tagCompound.putString("Template", this.pieceResourceLocation.toString());
//            tagCompound.putString("Rot", this.rotation.name());
//        }
//
//        @Override
//        protected void handleDataMarker(String function, BlockPos pos, ServerLevelAccessor worldIn, Random rand, BoundingBox sbb) {
//            //spanish_conquistador
//            //spanish_conquistador_general
//            //general_chest
//            //armoury_chest
//            //secret_chest
//            //scout_chest
//
//            System.out.println("HandleDataMarker: " + function);
//
//            switch (function) {
//                case "scout_chest":
//                    this.setupScoutChest(pos, worldIn, rand);
//                    break;
//                case "armoury_chest":
//                    this.setupArmouryChest(pos, worldIn, rand);
//                    break;
//                case "general_chest":
//                    this.setupGeneralChest(pos, worldIn, rand);
//                    break;
//                case "secret_chest":
//                    this.setupSecretChest(pos, worldIn, rand);
//                    break;
//                case "spanish_conquistador":
//                    this.setupSpanishConquistador(pos, worldIn, rand);
//                    break;
//                case "spanish_conquistador_general":
//                    this.setupSpanishConquistadorGeneral(pos, worldIn, rand);
//                    break;
//            }
//        }
//
//        private void setupScoutChest(BlockPos pos, ServerLevelAccessor worldIn, Random rand) {
//            worldIn.setBlock(pos, Blocks.AIR.defaultBlockState(), 3);
//            BlockEntity blockEntity = worldIn.getBlockEntity(pos.below());
//            if (blockEntity instanceof ChestBlockEntity) {
//                ((ChestBlockEntity)blockEntity).setLootTable(ModLootTables.CHESTS_SCOUT_CHEST, rand.nextLong());
//            }
//        }
//
//        private void setupArmouryChest(BlockPos pos, ServerLevelAccessor worldIn, Random rand) {
//            worldIn.setBlock(pos, Blocks.AIR.defaultBlockState(), 3);
//            BlockEntity blockEntity = worldIn.getBlockEntity(pos.below());
//            if (blockEntity instanceof ChestBlockEntity) {
//                ((ChestBlockEntity)blockEntity).setLootTable(ModLootTables.CHESTS_ARMOURY_CHEST, rand.nextLong());
//            }
//            BlockEntity blockEntity2 = worldIn.getBlockEntity(pos.below().below());
//            if (blockEntity2 instanceof ChestBlockEntity) {
//                ((ChestBlockEntity)blockEntity2).setLootTable(ModLootTables.CHESTS_ARMOURY_CHEST, rand.nextLong());
//            }
//        }
//
//        private void setupGeneralChest(BlockPos pos, ServerLevelAccessor worldIn, Random rand) {
//            worldIn.setBlock(pos, Blocks.AIR.defaultBlockState(), 3);
//            BlockEntity blockEntity = worldIn.getBlockEntity(pos.below());
//            if (blockEntity instanceof ChestBlockEntity) {
//                ((ChestBlockEntity)blockEntity).setLootTable(ModLootTables.CHESTS_GENERAL_CHEST, rand.nextLong());
//            }
//        }
//
//        private void setupSecretChest(BlockPos pos, ServerLevelAccessor worldIn, Random rand) {
//            worldIn.setBlock(pos, Blocks.OAK_SLAB.defaultBlockState().setValue(SlabBlock.TYPE, SlabType.TOP), 3);
//            BlockEntity blockEntity = worldIn.getBlockEntity(pos.below());
//            if (blockEntity instanceof ChestBlockEntity) {
//                ((ChestBlockEntity)blockEntity).setLootTable(ModLootTables.CHESTS_SECRET_CHEST, rand.nextLong());
//            }
//        }
//
//        private void setupSpanishConquistador(BlockPos pos, ServerLevelAccessor worldIn, Random rand) {
//            worldIn.setBlock(pos, Blocks.AIR.defaultBlockState(), 3);
//            SpanishConquistadorEntity entity = new SpanishConquistadorEntity(ModEntityTypes.SPANISH_CONQUISTADOR.get(), ((WorldGenRegion) worldIn).getLevel());
////            entity.setPatrolTarget(pos);
//            entity.setPos(pos.getX(), pos.getY(), pos.getZ());
//            entity.finalizeSpawn(worldIn, worldIn.getCurrentDifficultyAt(pos), MobSpawnType.STRUCTURE, null, null);
//            entity.setItemInHand(InteractionHand.MAIN_HAND, new ItemStack(Items.IRON_SWORD));
//            worldIn.addFreshEntity(entity);
//        }
//
//        private void setupSpanishConquistadorGeneral(BlockPos pos, ServerLevelAccessor worldIn, Random rand) {
//            worldIn.setBlock(pos, Blocks.AIR.defaultBlockState(), 3);
//            SpanishConquistadorGeneralEntity entity = new SpanishConquistadorGeneralEntity(ModEntityTypes.SPANISH_CONQUISTADOR_GENERAL.get(), ((WorldGenRegion) worldIn).getLevel());
////            entity.setPatrolTarget(pos);
//            entity.setPos(pos.getX(), pos.getY(), pos.getZ());
//            entity.finalizeSpawn(worldIn, worldIn.getCurrentDifficultyAt(pos), MobSpawnType.STRUCTURE, null, null);
//            entity.setItemInHand(InteractionHand.MAIN_HAND, new ItemStack(Items.IRON_SWORD));
//            worldIn.addFreshEntity(entity);
//        }
//    }
//}