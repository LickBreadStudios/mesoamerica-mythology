//package com.lickbread.mesoamericamythology.structure;
//
//import com.lickbread.mesoamericamythology.MesoamericaMythologyMod;
//import com.mojang.serialization.Codec;
//import net.minecraft.resources.ResourceLocation;
//import net.minecraft.world.level.block.Rotation;
//import net.minecraft.core.BlockPos;
//import net.minecraft.world.level.levelgen.structure.BoundingBox;
//import net.minecraft.core.RegistryAccess;
//import net.minecraft.world.level.biome.Biome;
//import net.minecraft.world.level.chunk.ChunkGenerator;
//import net.minecraft.world.level.levelgen.feature.configurations.NoneFeatureConfiguration;
//import net.minecraft.world.level.levelgen.feature.StructureFeature;
//import net.minecraft.world.level.levelgen.structure.StructureStart;
//import net.minecraft.world.level.levelgen.structure.templatesystem.StructureManager;
//
//import java.util.ArrayList;
//import java.util.List;
//
//public class SpanishSettlementStructure extends StructureFeature<NoneFeatureConfiguration> {
//
//    public static final List<Biome.BiomeCategory> SUPPORTED_BIOMES = new ArrayList<>();
//    static {
//        SUPPORTED_BIOMES.add(Biome.BiomeCategory.JUNGLE);
//        SUPPORTED_BIOMES.add(Biome.BiomeCategory.PLAINS);
//        SUPPORTED_BIOMES.add(Biome.BiomeCategory.SAVANNA);
//        SUPPORTED_BIOMES.add(Biome.BiomeCategory.FOREST);
//    }
//
//    public static final ResourceLocation STRUCTURE_RESOURCE_LOC = new ResourceLocation(MesoamericaMythologyMod.MOD_ID, "spanish_settlement");
//
//    public SpanishSettlementStructure(Codec<NoneFeatureConfiguration> codec) {
//        super(codec);
//    }
//
//    @Override
//    public String getFeatureName() {
//        return "Spanish_Settlement";
//    }
//
////    public int getSize() {
////        return 2;
////    }
//
//    @Override
//    public StructureFeature.StructureStartFactory getStartFactory() {
//        return Start::new;
//    }
//
////    protected int getSeedModifier() {
////        return 12357618;
////    }
//
//    public static class Start extends StructureStart<NoneFeatureConfiguration> {
//        public Start(StructureFeature<NoneFeatureConfiguration> structure, int chunkPosX, int chunkPosZ, BoundingBox bounds, int references, long largeFeatureSeed) {
//            super(structure, chunkPosX, chunkPosZ, bounds, references, largeFeatureSeed);
//        }
//
//        // init
//        @Override
//        public void generatePieces(RegistryAccess dynamicRegistries, ChunkGenerator chunkGenerator, StructureManager templateManager, int chunkX, int chunkZ, Biome p_230364_6_, NoneFeatureConfiguration featureConfig) {
//            int worldX = chunkX * 16;
//            int worldZ = chunkZ * 16;
//            BlockPos blockpos = new BlockPos(worldX, 90, worldZ);
//            SpanishSettlementPieces.generate(templateManager, blockpos, Rotation.NONE, this.pieces);
//            this.calculateBoundingBox();
//        }
//    }
//}