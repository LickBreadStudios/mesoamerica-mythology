package com.lickbread.mesoamericamythology.util;

import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.core.BlockPos;
import net.minecraft.world.phys.Vec3;
import net.minecraft.world.level.Level;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class BlockPosUtil {

    public static Vec3 toVec3(BlockPos pos) {
        return new Vec3(pos.getX(), pos.getY(), pos.getZ());
    }

    public static int getNearestAirYWithRange(Level level, BlockPos pos, int range, boolean down) {
        int y1 = 0;
        int i1 = 0;
        boolean found1 = false;
        int y2 = 0;
        int i2 = 0;
        boolean found2 = false;

        for (int i = pos.getY(); i >= pos.getY() - range; i--) {
            if (!level.isEmptyBlock(new BlockPos(pos.getX(), pos.getY() - y1, pos.getZ())) || i == 0) {
                if (y1 == 0) {
                    break;
                }
                i1 = i + 1;
                found1 = true;
                break;
            }
            y1++;
        }

        for (int j = pos.getY(); j <= pos.getY() + range; j++) {
            if (level.isEmptyBlock(new BlockPos(pos.getX(), pos.getY() + y2, pos.getZ())) && y2 != 0 || j == 255) {
                i2 = j;
                found2 = true;
                break;
            }
            y2++;
        }

        if (down) {
            if (found1) {
                return i1;
            } else if (found2) {
                return i2;
            }
        } else {
            if (found2) {
                return i2;
            } else if (found1) {
                return i1;
            }
        }

        return pos.getY();
    }

    public static Vec3 getPosTopCentre(BlockPos pos) {
        return new Vec3(pos.getX() + 0.5D, pos.getY() + 1.0D, pos.getZ() + 0.5D);
    }

    public static Vec3 getPosBottomCentre(BlockPos pos) {
        return new Vec3(pos.getX() + 0.5D, pos.getY(), pos.getZ() + 0.5D);
    }

    public static Vec3 getPosCentre(BlockPos pos) {
        return new Vec3(pos.getX() + 0.5D, pos.getY() + 0.5D, pos.getZ() + 0.5D);
    }

    public static <T extends BlockEntity> List<T> getAllBlockEntitiesWithinBounds(Level level, MinMaxBounds minMaxBounds, Class<T> clazz) {
        return getAllBlockEntities(level, BlockPos.betweenClosedStream(minMaxBounds.min, minMaxBounds.max)
                .map(BlockPos::new) // Copy as otherwise we iterate over the same value since it is mutable by default
                .collect(Collectors.toList()), clazz);
    }

    public static <T extends BlockEntity> List<T> getAllBlockEntities(Level level, List<BlockPos> positions, Class<T> clazz) {
        return positions.stream()
                .map(level::getBlockEntity)
                .filter(Objects::nonNull)
                .filter(blockEntity -> clazz.isAssignableFrom(blockEntity.getClass()))
                .map(blockEntity -> (T) blockEntity)
                .collect(Collectors.toList());
    }
}
