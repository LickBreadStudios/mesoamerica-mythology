package com.lickbread.mesoamericamythology.util;

import net.minecraft.core.Direction;
import net.minecraft.world.phys.AABB;
import net.minecraft.core.BlockPos;
import net.minecraft.util.Mth;
import net.minecraft.world.phys.Vec3;
import net.minecraft.core.Vec3i;

import java.util.Random;

public class MathUtil {

    public static Vec3 convertVector3i(Vec3i v3i) {
        return new Vec3(v3i.getX(), v3i.getY(), v3i.getZ());
    }

    public static Vec3 converBlockpos(Vec3i v3i) {
        return new Vec3(v3i.getX(), v3i.getY(), v3i.getZ());
    }

    public static Vec3 getRandomPointInSphere() {
        Random rand = new Random();
        double u = rand.nextDouble();
        double x = rand.nextDouble();
        double y = rand.nextDouble();
        double z = rand.nextDouble();
        double mag = Math.sqrt(x*x + y*y + z*z);
        x /= mag;
        y /= mag;
        z /= mag;
        double c = Math.cbrt(u);
        x *= c;
        y *= c;
        z *= c;

        return new Vec3(x, y, z);
    }

    public static Vec3 lerp(Vec3 p0, Vec3 p1, double t) {
        return new Vec3(
                p1.x + p0.x * (t - p1.x),
                p1.y + p0.y * (t - p1.y),
                p1.z + p0.z * (t - p1.z)
        );
    }

    public static float lerp(float f0, float f1, float t) {
       return f1 + f0 * (t - f1);
    }

    public static double lerp(double d0, double d1, double t) {
       return d1 + d0 * (t - d1);
    }

    public static Vec3 cubeBezier3(Vec3 p0, Vec3 p1, Vec3 p2, Vec3 p3, double t) {
        double r = 1f - t;
        double f0 = r * r * r;
        double f1 = r * r * t * 3;
        double f2 = r * t * t * 3;
        double f3 = t * t * t;
        return new Vec3(
                f0*p0.x + f1*p1.x + f2*p2.x + f3*p3.x,
                f0*p0.y + f1*p1.y + f2*p2.y + f3*p3.y,
                f0*p0.z + f1*p1.z + f2*p2.z + f3*p3.z
        );
    }

    public static AABB getDamageBox(BlockPos pos, Direction direction, double width, double length) {
        double x = pos.getX() + 0.5D;
        double y = pos.getY() + 0.5D;
        double z = pos.getZ() + 0.5D;

        switch (direction) {
            case EAST:
                return new AABB(x, y, z, x + length + width, y + width, z + width);
            case WEST:
                return new AABB(x + width, y, z, x - length, y + width, z + width);
            case NORTH:
                return new AABB(x, y, z + width, x + width, y + width, z - length);
            case SOUTH:
                return new AABB(x, y, z, x + width, y + width, z + length + width);
            case DOWN:
                return new AABB(x, y + width, z, x + width, y - length, z + width);
            case UP:
                return new AABB(x, y, z, x + width, y + length + width, z + width);
        }
        
        return new AABB(x - width, y - width, z - width, x + width, y + width, z + width);
    }

    public static Vec3 randomOffset(double x, double y, double z) {
        Random random = new Random();
        return new Vec3(-x + random.nextDouble() * 2 * x, -y + random.nextDouble() * 2 * y, -z + random.nextDouble() * 2 * z);
    }

    public static double getInRandom(Random random, double dist) {
        return -dist + random.nextDouble() * 2.0D * dist;
    }

    public static float getInRandom(Random random, float dist) {
        return -dist + random.nextFloat() * 2.0F * dist;
    }

    public static int getInRandom(Random random, int dist) {
        return -dist + Mth.floor(random.nextDouble() * 2 * dist);
    }

    public static int getInRandom(Random random, int min, int max) {
        return min + Mth.floor(random.nextDouble() * (max - min));
    }

    public static double getInRandom(Random random, double min, double max) {
        return min + Mth.floor(random.nextDouble() * (max - min));
    }
}
