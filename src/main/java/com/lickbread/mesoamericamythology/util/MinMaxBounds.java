package com.lickbread.mesoamericamythology.util;

import net.minecraft.world.phys.AABB;
import net.minecraft.core.BlockPos;

public  class MinMaxBounds {
    public static final MinMaxBounds ZERO = new MinMaxBounds(BlockPos.ZERO, BlockPos.ZERO);

    public final BlockPos min;
    public final BlockPos max;

    public MinMaxBounds(BlockPos min, BlockPos max) {
        this.min = min;
        this.max = max; //.offset(new Vector3i(1, 1, 1)); // Not sure why this is here
    }

    public AABB convertToAxisAlignedBB() {
        return new AABB(min, max);
    }
}