package com.lickbread.mesoamericamythology.util;

import java.util.*;

public class RandomList<E> {

    private final List<E> list = new ArrayList<>();
    private transient final Random random;

    public RandomList() {
        this(new Random());
    }

    public RandomList(Random random) {
        this.random = random;
    }

    public RandomList<E> add(E result) {
        list.add(result);
        return this;
    }

    public E next() {
        return this.list.get(random.nextInt(this.list.size()));
    }
}
