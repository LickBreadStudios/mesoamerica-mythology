package com.lickbread.mesoamericamythology.util;

import com.mojang.blaze3d.platform.Lighting;
import com.mojang.blaze3d.vertex.PoseStack;
import net.minecraft.client.Minecraft;
import net.minecraft.client.StringSplitter;
import net.minecraft.client.gui.Font;
import net.minecraft.client.gui.GuiComponent;
import net.minecraft.client.renderer.MultiBufferSource;
import net.minecraft.client.renderer.RenderType;
import net.minecraft.client.renderer.Sheets;
import net.minecraft.client.renderer.block.BlockRenderDispatcher;
import net.minecraft.core.BlockPos;
import net.minecraft.network.chat.Component;
import net.minecraft.network.chat.FormattedText;
import net.minecraft.network.chat.Style;
import net.minecraft.util.RandomSource;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.level.block.state.BlockState;

import java.util.List;

public class RendererUtil {

    private static final int[] TEST_SPLIT_OFFSETS = new int[]{0, 10, -10, 25, -25};

    public static void renderSolidModel(BlockEntity blockEntity, float partialTicks, PoseStack poseStack, MultiBufferSource buffer, int combinedLightIn, int combinedOverlayIn) {
        Level level = blockEntity.getLevel();
        BlockPos pos = blockEntity.getBlockPos();
        BlockState state = level.getBlockState(pos);
        Lighting.setupFor3DItems();
        poseStack.pushPose();
        renderModel(level, pos, state, Sheets.cutoutBlockSheet(), partialTicks, poseStack, buffer, combinedLightIn, combinedOverlayIn);
        poseStack.popPose();
    }

    public static void renderModel(Level level, BlockPos pos, BlockState state, RenderType renderType, float partialTicks, PoseStack poseStack, MultiBufferSource buffer, int combinedLightIn, int combinedOverlayIn) {
        BlockRenderDispatcher blockRenderer = Minecraft.getInstance().getBlockRenderer();
        blockRenderer.getModelRenderer().tesselateBlock(level, blockRenderer.getBlockModel(state), state, pos, poseStack, buffer.getBuffer(renderType), false, RandomSource.create(), combinedLightIn, combinedOverlayIn);
    }

    public static List<FormattedText> findOptimalLines(Font font, Component text, int maxWidth) {
        StringSplitter charactermanager = font.getSplitter();
        List<FormattedText> list = null;
        float f = Float.MAX_VALUE;

        for(int i : TEST_SPLIT_OFFSETS) {
            List<FormattedText> list1 = charactermanager.splitLines(text, maxWidth - i, Style.EMPTY);
            float f1 = Math.abs(getMaxWidth(charactermanager, list1) - (float)maxWidth);
            if (f1 <= 10.0F) {
                return list1;
            }

            if (f1 < f) {
                f = f1;
                list = list1;
            }
        }

        return list;
    }

    public static void render9Sprite(GuiComponent gui, PoseStack poseStack, int i1, int i2, int i3, int i4, int i5, int i6, int i7, int i8, int i9) {
        gui.blit(poseStack, i1, i2, i8, i9, i5, i5);
        renderRepeating(gui, poseStack, i1 + i5, i2, i3 - i5 - i5, i5, i8 + i5, i9, i6 - i5 - i5, i7);
        gui.blit(poseStack, i1 + i3 - i5, i2, i8 + i6 - i5, i9, i5, i5);
        gui.blit(poseStack, i1, i2 + i4 - i5, i8, i9 + i7 - i5, i5, i5);
        renderRepeating(gui, poseStack, i1 + i5, i2 + i4 - i5, i3 - i5 - i5, i5, i8 + i5, i9 + i7 - i5, i6 - i5 - i5, i7);
        gui.blit(poseStack, i1 + i3 - i5, i2 + i4 - i5, i8 + i6 - i5, i9 + i7 - i5, i5, i5);
        renderRepeating(gui, poseStack, i1, i2 + i5, i5, i4 - i5 - i5, i8, i9 + i5, i6, i7 - i5 - i5);
        renderRepeating(gui, poseStack, i1 + i5, i2 + i5, i3 - i5 - i5, i4 - i5 - i5, i8 + i5, i9 + i5, i6 - i5 - i5, i7 - i5 - i5);
        renderRepeating(gui, poseStack, i1 + i3 - i5, i2 + i5, i5, i4 - i5 - i5, i8 + i6 - i5, i9 + i5, i6, i7 - i5 - i5);
    }

    public static float getMaxWidth(StringSplitter characterManager, List<FormattedText> lines) {
        return (float)lines.stream().mapToDouble(characterManager::stringWidth).max().orElse(0.0D);
    }

    public static void renderRepeating(GuiComponent gui, PoseStack poseStack, int i1, int i2, int i3, int i4, int i5, int i6, int i7, int i8) {
        for(int i = 0; i < i3; i += i7) {
            int j = i1 + i;
            int k = Math.min(i7, i3 - i);

            for(int l = 0; l < i4; l += i8) {
                int ii1 = i2 + l;
                int j1 = Math.min(i8, i4 - l);
                gui.blit(poseStack, j, ii1, i5, i6, k, j1);
            }
        }
    }
}
