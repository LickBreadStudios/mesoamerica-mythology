package com.lickbread.mesoamericamythology.util;

import com.mojang.brigadier.StringReader;
import com.mojang.brigadier.exceptions.CommandSyntaxException;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.network.FriendlyByteBuf;
import net.minecraft.core.BlockPos;
import net.minecraft.world.phys.Vec3;

public class SerializerUtil {

    private static final String POS_X = "PosX";
    private static final String POS_Y = "PosY";
    private static final String POS_Z = "PosZ";

    public static CompoundTag serialize(BlockPos blockPos) {
        CompoundTag posNbt = new CompoundTag();
        posNbt.putInt(POS_X, blockPos.getX());
        posNbt.putInt(POS_Y, blockPos.getY());
        posNbt.putInt(POS_Z, blockPos.getZ());
        return posNbt;
    }

    public static BlockPos deserializeBlockPos(CompoundTag nbt) {
        int x = nbt.getInt(POS_X);
        int y = nbt.getInt(POS_Y);
        int z = nbt.getInt(POS_Z);
        return new BlockPos(x ,y ,z);
    }

    public static CompoundTag serialize(Vec3 Vec3) {
        CompoundTag posNbt = new CompoundTag();
        posNbt.putDouble(POS_X, Vec3.x());
        posNbt.putDouble(POS_Y, Vec3.y());
        posNbt.putDouble(POS_Z, Vec3.z());
        return posNbt;
    }

    public static Vec3 deserializeVec3(CompoundTag nbt) {
        double x = nbt.getDouble(POS_X);
        double y = nbt.getDouble(POS_Y);
        double z = nbt.getDouble(POS_Z);
        return new Vec3(x ,y ,z);
    }

    public static String writeVec3(Vec3 Vec3) {
        return Vec3.x + " " + Vec3.y + " " + Vec3.z;
    }

    public static Vec3 readVec3(StringReader stringReader) throws CommandSyntaxException {
        double x = stringReader.readDouble();
        stringReader.expect(' ');
        double y = stringReader.readDouble();
        stringReader.expect(' ');
        double z = stringReader.readDouble();
        return new Vec3(x ,y , z);
    }

    public static Vec3 readVec3(FriendlyByteBuf packetBuffer) {
        return new Vec3(packetBuffer.readDouble(), packetBuffer.readDouble(), packetBuffer.readDouble());
    }
}
