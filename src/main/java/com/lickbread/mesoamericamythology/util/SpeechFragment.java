package com.lickbread.mesoamericamythology.util;

import lombok.AllArgsConstructor;
import lombok.Getter;
import net.minecraft.client.resources.language.I18n;
import net.minecraft.nbt.CompoundTag;
import net.minecraftforge.common.util.INBTSerializable;

@AllArgsConstructor
@Getter
public class SpeechFragment implements INBTSerializable<CompoundTag> {

    private final int durationInMillis;
    private final String textId;

    public String getTranslatedText() {
        return I18n.get(this.textId);
    }

    @Override
    public CompoundTag serializeNBT() {
        throw new UnsupportedOperationException();
    }

    @Override
    public void deserializeNBT(CompoundTag nbt) {
        throw new UnsupportedOperationException();
    }
}
