package com.lickbread.mesoamericamythology.util.gson;

import com.google.gson.*;
import net.minecraft.world.entity.EntityType;
import net.minecraft.resources.ResourceLocation;
import net.minecraftforge.registries.ForgeRegistries;

import java.lang.reflect.Type;

public class EntityTypeSerializer implements JsonSerializer<EntityType<?>>, JsonDeserializer<EntityType<?>> {

    private static final String TYPE_KEY = "type";

    @Override
    public EntityType<?> deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        String id = ((JsonObject) json).get(TYPE_KEY).getAsString();
        return ForgeRegistries.ENTITY_TYPES.getValue(new ResourceLocation(id));
    }

    @Override
    public JsonElement serialize(EntityType<?> src, Type typeOfSrc, JsonSerializationContext context) {
        JsonObject serialized = new JsonObject();
        serialized.add(TYPE_KEY, new JsonPrimitive(ForgeRegistries.ENTITY_TYPES.getKey(src).toString()));
        return serialized;
    }
}
