package com.lickbread.mesoamericamythology.util.gson;

import com.google.gson.*;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.resources.ResourceLocation;
import net.minecraftforge.registries.ForgeRegistries;

import java.lang.reflect.Type;

public class ItemStackSerializer implements JsonSerializer<ItemStack>, JsonDeserializer<ItemStack> {

    private static final String ID_KEY = "id";
    private static final String COUNT_KEY = "count";

    @Override
    public ItemStack deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        String id = ((JsonObject) json).get(ID_KEY).getAsString();
        int count = ((JsonObject) json).get(COUNT_KEY).getAsInt();
        Item item = ForgeRegistries.ITEMS.getValue(new ResourceLocation(id));
        return new ItemStack(item, count);
    }

    @Override
    public JsonElement serialize(ItemStack src, Type typeOfSrc, JsonSerializationContext context) {
        JsonObject serialized = new JsonObject();
        serialized.add(ID_KEY, new JsonPrimitive(ForgeRegistries.ITEMS.getKey(src.getItem()).toString()));
        serialized.add(COUNT_KEY, new JsonPrimitive(src.getCount()));
        return serialized;
    }
}
